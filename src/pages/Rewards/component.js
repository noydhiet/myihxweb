import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';

class Rewards extends Component {
    render() {
        const { browseRewards, vouchersPage } = this.props;
        const config = {
            carouselTab: {
                className: "tab-list",
                dots: false,
                infinite: false,
                arrows: false,
                speed: 500,
                slidesToShow: 2,
                slidesToScroll: 2,
                responsive: [
                    {
                      breakpoint: 991,
                      settings: {
                        dots: false,
                        slidesToShow: 2,
                        slidesToScroll: 2
                      }
                    },
                    {
                      breakpoint: 575,
                      settings: {
                        dots: false,
                        slidesToShow: 2,
                        slidesToScroll: 2
                      }
                    },
                    {
                      breakpoint: 324,
                      settings: {
                        dots: false,
                        slidesToShow: 2,
                        slidesToScroll: 2
                      }
                    }
                ]
            }
        }
        return (
        <div>
            <div className="cover cover-sm cover-bg-white-sm  bg-red-gradient">
                <div className="header-actions d-block d-md-none">
                    <Link
                    to="/shop/"
                    className="btn btn-link"
                    >
                    <i className="fa fa-arrow-left"></i>
                    </Link>
                </div>
                <div className="container">
                    <nav aria-label="breadcrumb" className="d-none d-md-block">
                    <ol className="breadcrumb animated fadeInUp">
                        <li className="breadcrumb-item"><Link to="/home"><i className="fa fa-arrow-left mr-1"></i>Home</Link></li>
                    </ol>
                    </nav>
                    <div className="cover-content-w-links">
                      <div className="cover-content">
                        <p className="caption">Indihome Points</p>
                        <h1 className="cover-title animated fadeInUp delayp1">1.234</h1>
                        <p className="cover-text">20 pts will expire on 2 July 2020</p>
                      </div>
                      <ul className="rewards-nav ml-auto">
                        <li className="nav-item">
                          <Link to="/inbox/all" className="light nav-link-rewards">
                            {" "}
                            <i className="fas fa-search "></i>{" "}
                          </Link>
                        </li>
                        <li className="nav-item">
                          <Link to="/inbox/all" className="light nav-link-rewards">
                            {" "}
                            <i className="fas fa-history"></i>{" "}
                          </Link>
                        </li>
                      </ul>
                    </div>
                </div>
            </div>
            <section className="shop-tab-list animated fadeInUp delayp3">
                <div className="container">
                <Slider {...config.carouselTab}>
                    <div className={"tab-item " + browseRewards}> 
                        <Link to="/rewards/browse">Browse Rewards</Link> 
                    </div>
                    <div className={"tab-item " + vouchersPage}> 
                        <Link to="/rewards/vouchers">My Vouchers</Link>
                    </div>
                </Slider>
                </div>
            </section>
        </div>
    )
  }
}

export default Rewards;
