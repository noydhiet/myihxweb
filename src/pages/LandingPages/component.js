import React, { Component, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import { CardContentStacked } from './../../include/CardPackage';
import Slider from "react-slick";
import TrackVisibility from "react-on-screen";

class LandingPages extends Component {
    static contextType = PageContext;
    componentDidMount() {
        this.context.trigger(2);
       // this.context.navCase("w-md-up-block");
        this.context.pageCase("l-bg");
    }
    render() {
    const { common, brand, sample } = this.context;
    const images = { common, brand, sample };
        return ( 
            <TextContext.Consumer>{(context)=>{
                const { landingPagesText, generalText } = context;
                return (
                    <>
                    <div className="cover cover-responsive bg-wave red">
                    <div className="cover-responsive-img landing-pages-cover">
                    {/* <div className="cover-responsive-overlay"></div> */}
                    </div>
                    <div className="container">
                        <div className="cover-content">
                            <h3 className="cover-title animated fadeInUp delayp1">
                            {landingPagesText.banner.bannerText}
                            </h3>
                            <p className="cover-text animated fadeInUp delayp2">
                            {landingPagesText.banner.bannerTextDesc}
                            </p>
                            <div className="btn-placeholder animated fadeInUp delayp3">
                            <Link to="#" className="btn btn-light">
                            {landingPagesText.banner.bannerTextBtn}
                            </Link>
                            </div>
                        </div>
                    </div>
                </div>
                <AvailabilitySection images={images} generalText={generalText}/>
                <TrackVisibility once offset={400}>
                    <Benefits images={images} landingPagesText={landingPagesText}/>
                </TrackVisibility>
                <TrackVisibility once offset={400}>
                    <IndiPromo images={images} generalText={generalText}/>
                </TrackVisibility>
                <TrackVisibility once offset={400}>
                    <IndiApps images={images} landingPagesText={landingPagesText}/>
                </TrackVisibility>
                <TrackVisibility once offset={400}>
                    <IndiTv images={images} generalText={generalText}/>
                </TrackVisibility>
                <TrackVisibility once offset={400}>
                    <IndiFaQ images={images} landingPagesText={landingPagesText}/>
                </TrackVisibility>
                </>
                )
            }}  
            </TextContext.Consumer>
         );
    }
}
 
export default LandingPages;

const AvailabilitySection = ({ images, generalText }) => {
    return (
        <section className="py-main">
            <div className="container">
                <div className="row">
                    <div className="col-md-6 order-md-last mb-3">
                        <img src={images.common["img_indonesia-map.png"]} className="img-fluid" alt="maps-img" />
                    </div>
                    <div className="col-md-6 order-md-first">
                        <div className="subheading animated animated fadeInUp delayp4"><p>{generalText.coverage.coverageCaption}</p></div>
                          {/* <h3 className="help-block text-uppercase animated fadeInUp delayp7">Cek KetersAediaan indihome di dareah anda</h3> */}
                          <div className="heading">
                            <h3 className="animated fadeInUp delayp5">{generalText.coverage.coverageText}</h3>
                          </div>
                          <div className="btn-placeholder animated fadeInUp delayp6">
                            <Link to="/check-coverage" className="btn btn-primary">{generalText.coverage.coverageBtn}</Link>
                            {/* <small className="help-block mt-2"> Misalnya: Nama kompleks, apartemen, atau gedung</small> */}
                          </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

const Benefits = ({images, isVisible, landingPagesText}) => {
    const effect = isVisible ? "fadeInUp" : "";
    return (
        <div className="cover cover-responsive bg-wave red" style={{  
            backgroundImage: `url(${images.common["bg-banner-benefit.png"]})`,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat'
          }}>
            <div className="cover-responsive-img banner-landing-pages-benefit">
            {/* <div className="cover-responsive-overlay"></div> */}
            </div>
            <div className="container">
                <div className="cover-content sm-left">
                    <div className="subheading">
                        <p className={"text-white-50 animated " + effect + " delayp1"}>{landingPagesText.benefit.benefitCaption}</p>
                    </div>
                    <div className="heading animated fadeInUp delayp8">
                        <h3 className={"text-white mb-3 animated " + effect + " delayp2"}>
                        {landingPagesText.benefit.benefitText}
                        </h3>
                    </div>
                    <ul className={"list-w-circle animated " + effect + " delayp3"}>
                        {
                            landingPagesText.benefit.benefitTextList.map((value, index)=>{
                                return (
                                    <li key={index}>{value}</li>
                                )
                            })
                        }
                    </ul>
                </div>
            </div>
        </div>
    )
}

const IndiPromo = ({images, isVisible, generalText}) => {
    const carousel = {
        className: "primary-carousel view-carousel",
        dots: false,
        infinite: false,
        arrows: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1.5,
              slidesToScroll: 1.5
            }
          }
        ]
    }
    const promo = [
        {
            link: "/shop/internet/package/details",
            img: "offer1.png",
            mainText: "Hemat 30% dengan berlangganan 2 paket MiniPack selama 1 tahun"
        },
        {
            link: "/shop/internet/package/details",
            img: "offer2.png",
            mainText: "Fan of Game of Thrones? Watch Season 8 in HBO & HBO Go now!"
        },
        {
            link: "/shop/internet/package/details",
            img: "offer3.png",
            mainText: "Special Sale: Get extra speed only Rp10.000 this month!"
        },
        {
            link: "/shop/internet/package/details",
            img: "offer1.png",
            mainText: "Hemat 30% dengan berlangganan 2 paket MiniPack selama 1 tahun"
        },
        {
            link: "/shop/internet/package/details",
            img: "offer2.png",
            mainText: "Fan of Game of Thrones? Watch Season 8 in HBO & HBO Go now!"
        },
        {
            link: "/shop/internet/package/details",
            img: "offer3.png",
            mainText: "Special Sale: Get extra speed only Rp10.000 this month!"
        }
    ]
    const effect = isVisible ? "fadeInUp" : "";
    return (
        <section className="py-main bg-white">
            <div className="container">
                <div className={"heading animated " + effect + " delayp1"}>
                    <h1>{generalText.newOffer}</h1>
                </div>
                <Slider {...carousel}>
                    {promo.map((value, index)=>{
                        let delaySum = index+1;
                        return (
                            <div className="carousel-item" key={index}>
                                <CardContentStacked
                                link={value.link}
                                className={"animated " + effect + " delayp" + delaySum}
                                img={value.img}
                                mainText={value.mainText}
                                images={images}
                                />
                            </div>
                        )
                    })}
                </Slider>
            </div>
        </section>
    )
}

const IndiApps = ({images, isVisible, landingPagesText}) => {
    const effect = {
        fadeInUp: isVisible ? "fadeInUp" : "",
        fadeInRight: isVisible ? "fadeInRight" : ""
    }
    return (
        <section className="py-main landing-pages-indiapps">
            <img src={images.common["phone-blob.png"]} className={"bg-red-waves-desktop d-none d-md-block animated " + effect.fadeInUp + " delayp1"} alt="bg-red" />
            <img src={images.common["Phone_front.png"]} className={"phone-left-desktop d-none d-md-block animated " + effect.fadeInRight + " delayp3"} alt="bg-red" />
            <img src={images.common["Phone_back.png"]} className={"phone-right-desktop d-none d-md-block animated " + effect.fadeInRight + " delayp5"} alt="bg-red" />
            <div className="container">
                <div className="row">
                    <div className="col-md-5 order-md-last position-relative">
                    <img src={images.common["phone-blob.png"]} className="bg-red-waves d-md-none" alt="bg-red" />
                    <img src={images.common["Phone_front.png"]} className="phone-left d-md-none" alt="bg-red" />
                    <img src={images.common["Phone_back.png"]} className="phone-right d-md-none" alt="bg-red" />
                    </div>
                    <div className="col-md-7 order-md-first section-text">
                        <div className={"subheading animated " + effect.fadeInUp + " delayp1"}>
                            <p>{landingPagesText.app.appCaption}</p>
                        </div>
                        <div className={"heading animated " + effect.fadeInUp + " delayp2"}>
                            <h1>{landingPagesText.app.appTitle}</h1>
                            <p>{landingPagesText.app.appDesc}</p>
                        </div>
                        <div className={"row animated " + effect.fadeInUp + " delayp3"}>
                            {
                                landingPagesText.app.appTextList.map((value, index)=>{
                                    return (
                                        <div className="col-6" key={index}>
                                            <p className="text-w-circle">{value}</p>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        </section>

    )
}

const IndiTv = ({images, isVisible, generalText}) => {
    const settings = {
        dots: false,
        fade: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        className:"primary-carousel overflow-inherit"
    };
    const effect = isVisible ? "fadeInUp" : "";
    return (
        <section className="py-main bg-white landing-pages-indi-tv">
            <div className="container">
                <div className="row">
                    <div className="col-md-6 col-12 position-relative">
                        <div className="tv-placeholder">
                            <Slider {...settings} style={{position: "relative", top: ".5rem"}}>
                                <div className="carousel-item">
                                    <img src={images.common["tv-screen-2.png"]} className="img-fluid tv-load-1" alt="tv-load" />
                                    <div className="tv-prop">
                                        <img src={images.common["tv-prop.png"]} alt="tv-banner" className="img-fluid icon-prop"/>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <img src={images.common["tv-screen-2.png"]} className="img-fluid tv-load-1" alt="tv-load" />
                                    <div className="tv-prop">
                                        <img src={images.common["tv_prop2.png"]} alt="tv-banner" className="img-fluid icon-prop"/>
                                    </div>
                                </div>
                            </Slider>
                        </div>
                    </div>
                    <div className="col-md-6 col-12 content-center-left">
                        <ul className={"icon-list mb-3 animated " + effect + " delayp1"}>
                            <li className="icon-list-item">
                            <img
                                alt=""
                                src={images.common["logo_iflix.jpg"]}
                                className="img-fluid w-75px"
                            />
                            </li>
                            <li className="icon-list-item">
                            <img
                                alt=""
                                src={images.common["logo_hooq.jpg"]}
                                className="img-fluid w-100px"
                            />
                            </li>
                            <li className="icon-list-item">
                            <img
                                alt=""
                                src={images.common["logo_catchplay.jpg"]}
                                className="img-fluid w-125px"
                            />
                            </li>
                        </ul>
                        <div className={"heading animated " + effect + " delayp2"}>
                            <h1>{generalText.movieSubcribe.movieSubcribeTitle}</h1>
                            <p>{generalText.movieSubcribe.movieSubcribeDesc}</p>
                        </div>
                        <div className={"btn-placeholder animated " + effect + " delayp3"}>
                            <button className="btn btn-primary">Subscribe</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

const IndiFaQ = ({images, isVisible, landingPagesText}) => {
    const effect = isVisible ? "fadeInUp" : "";
    const carousel = {
        className: "primary-carousel view-carousel animated " + effect + " delayp1" ,
        dots: false,
        infinite: false,
        arrows: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 5,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1.5,
              slidesToScroll: 1.5
            }
          }
        ]
      }
    return (
        <section className="py-main">
            <div className="container">
                <div className="row">
                    <div className="col-md-10 mx-auto">
                        <div className={"subheading animated " + effect + " delayp1"}>
                            <p>{landingPagesText.faq.faqCaption}</p>
                        </div>
                        <div className={"heading animated " + effect + " delayp2"}>
                            <h1>{landingPagesText.faq.faqTitle}</h1>
                            <p>{landingPagesText.faq.faqDesc}</p>
                        </div>
                        <Slider {...carousel}>
                            <div className="carousel-item">
                                <div className="card card-feature w-img red">
                                    <img src={images.common["ic_selection_wifi_light.png"]} className="w-50px"  alt="icon" />
                                    <h5>Internet</h5>
                                    <p>Bantuan dan pertanyaan seputar paket Internet Anda</p>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="card card-feature w-img blue animated fadeInUp delayp5">
                                    <img src={images.common["ic_selection_tv_light.png"]} className="w-50px"  alt="icon" />
                                    <h5>TV</h5>
                                    <p>Bantuan seputar TV interaktif dan minipack</p>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="card card-feature w-img green animated fadeInUp delayp5">
                                    <img src={images.common["ic_shop_call_white.png"]} className="w-50px"  alt="icon" />
                                    <h5>Telephone</h5>
                                    <p>Bantuan seputar Telephone</p>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="card card-feature w-img blue-light animated fadeInUp delayp5">
                                    <img src={images.common["ic-selection-addon.png"]} className="w-50px"  alt="icon" />
                                    <h5>Add-On</h5>
                                    <p>Bantuan seputar Add-On</p>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="card card-feature w-img gray animated fadeInUp delayp5">
                                    <img src={images.common["ic-selection-admin.png"]} className="w-50px"  alt="icon" />
                                    <h5>Admin</h5>
                                    <p>Bantuan seputar Admin</p>
                                </div>
                            </div>
                        </Slider>
                    </div>
                </div>
            </div>
        </section>
    )
}