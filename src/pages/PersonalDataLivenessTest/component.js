import React, { useContext, useEffect } from 'react';
import { PageContext } from './../../context/PageContext';
import { Link } from 'react-router-dom';

const PersonalDataLivenessTest = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = {brand, common, sample};
    useEffect(() => {
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
      }, [trigger, navCase, pageCase]);
    return (
    <section>
        <div className="container container-xs">
          <div className="box box-main animated fadeInUp">
            <header className="header-light white icon-center animated fadeInUp delayp1">
              <div className="header-actions">
                <Link to="/register/otp" className="btn btn-link"><i className="fal fa-times" /></Link>
              </div>
              <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
            </header>
            <section className="animated fadeInUp delayp2">
              <h1>Liveness Test</h1>
              <p>Perform a quick test to confirm your identity. This test will require you fron camera.</p>
              <div className="list-group list-group-dark">
                <div className="list-group-item">
                    <img src={images.common["icon_ic_selection_time.png"]} alt="timer-icon" className="img-fluid w-50px" />
                    <div className="list-group-item-content">
                    <p className="text-uppercase help-block mb-0">Complete By</p>
                    <h5 className="text-primary mb-0">23:59:59</h5>
                    <p className="desc">to avoid order cancellation</p>
                    </div>
                </div>
                </div>
            </section>
            <div className="btn-placeholder bottom animated fadeInUp delayp3">
              <Link to="/login:" className="btn btn-primary btn-block">Next</Link>
            </div>
          </div>
        </div>
      </section>
    )
}

export default PersonalDataLivenessTest;