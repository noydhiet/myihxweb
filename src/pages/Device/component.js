import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';

class Device extends Component {
    render() {
        const { indihomeSmartPage, wifiExtenderPage, hybridBoxPage } = this.props;
        const config = {
            carouselTab: {
                className: "tab-list",
                dots: false,
                infinite: false,
                arrows: false,
                speed: 500,
                slidesToShow: 3,
                slidesToScroll: 3,
                responsive: [
                    {
                      breakpoint: 991,
                      settings: {
                        dots: false,
                        slidesToShow: 3,
                        slidesToScroll: 3
                      }
                    },
                    {
                      breakpoint: 575,
                      settings: {
                        dots: false,
                        slidesToShow: 3,
                        slidesToScroll: 3
                      }
                    },
                    {
                      breakpoint: 324,
                      settings: {
                        dots: false,
                        slidesToShow: 3,
                        slidesToScroll: 3
                      }
                    }
                ]
            }
          }
        return (
        <div>
            <div className="cover cover-sm cover-bg-white-sm  bg-red-gradient">
                <div className="header-actions d-block d-md-none">
                    <Link
                    to="/shop/"
                    className="btn btn-link"
                    >
                    <i className="fa fa-arrow-left"></i>
                    </Link>
                </div>
                <div className="container">
                    <nav aria-label="breadcrumb" className="d-none d-md-block">
                    <ol className="breadcrumb animated fadeInUp">
                        <li className="breadcrumb-item"><Link to="/shop"><i className="fa fa-arrow-left mr-1"></i>Home</Link></li>
                    </ol>
                    </nav>
                    <div className="cover-content">
                    <h1 className="cover-title animated fadeInUp delayp1">Device</h1>
                    </div>
                </div>
            </div>
            <section className="shop-tab-list animated fadeInUp delayp3">
                <div className="container">
                <Slider {...config.carouselTab}>
                    <div className={"tab-item " + indihomeSmartPage}> 
                        <Link to="/shop/tv/minipack">IndiHome Smart</Link> 
                    </div>
                    <div className={"tab-item " + wifiExtenderPage}> 
                        <Link to="/shop/tv/apps">Wifi Extender</Link>
                    </div>
                    <div className={"tab-item " + hybridBoxPage}> 
                        <Link to="/shop/tv/edukids">Hybrid Box</Link> 
                    </div>
                </Slider>
                </div>
            </section>
        </div>
    )
  }
}

export default Device;