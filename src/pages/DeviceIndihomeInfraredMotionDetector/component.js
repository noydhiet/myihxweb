import React, { Component } from "react";
import { Link } from "react-router-dom";
import { PageContext } from '../../context/PageContext';

class DeviceIndihomeInfraredMotionDetector extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    const { common, brand, sample } = this.context;
    const images = { common, brand, sample };
    return (
      <div>
        <div className="cover cover-details bg-red-gradient">
          <div className="header-actions d-block d-md-none">
            <Link to="/shop/indihome-smart" className="btn btn-link">
              <i className="fa fa-arrow-left"></i>
            </Link>
          </div>
          <div className="container">
            <nav aria-label="breadcrumb" className="d-none d-md-block">
              <ol className="breadcrumb animated fadeInUp">
                <li className="breadcrumb-item">
                  <Link to="/shop/">
                    <i className="fa fa-arrow-left mr-1"></i> Shop
                        </Link>
                </li>
                <li className="breadcrumb-item">
                  <Link to="/shop/indihome-smart">Indihome Smart</Link>
                </li>
              </ol>
            </nav>
            <div className="cover-content">
              <h1 className="cover-title animated fadeInUp delayp1">
                Infrared Motion Detector
                    </h1>
              <p className="cover-text animated fadeInUp delayp2 mb-3">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500
                    </p>
              <Link
                to="/"
                className="btn btn-link p-0 text-white animated fadeInUp delayp3"
              >
                How It Works{" "}
                <i className="fal fa-angle-right"></i>
              </Link>
            </div>
            <div className="shop-price animated fadeInUp delayp4 d-none d-md-block">
              <div className="container">
                <div className="shop-price-info">
                  <h3>
                    Rp2.190.000 <small>/month</small>
                  </h3>
                  <Link to="#" className="">
                    View Pricing Detail <i className="fal fa-angle-right"></i>
                  </Link>
                </div>
                <Link to="/shop/indihome-smart/confirm" className="btn btn-primary">
                  Subscribe
                        </Link>
              </div>
            </div>
          </div>
        </div>
        <div
          className="shop-price fixed-bottom animated fadeInUp delayp8"
          id="shop-price"
        >
          <div className="container">
            <div className="shop-price-info">
              <h3>
                Rp2.190.000 <small>/month</small>
              </h3>
              <Link to="#" className="">
                View Pricing Detail <i className="fal fa-angle-right"></i>
              </Link>
            </div>
            <Link to="/shop/indihome-smart/confirm" className="btn btn-primary">
              Subscribe
                        </Link>
          </div>
        </div>
        <PackageListSection images={images} />
        <SectionContent1 images={images} />
        {/* <SectionContent1 images={images} />
                <SectionContent2 images={images} /> */}
      </div>
    )
  }
}

export default DeviceIndihomeInfraredMotionDetector;

const PackageListSection = ({ images }) => {
  return (
    <section className="bg-gray-50 shop-detail-feature">
      <div className="container">
        <div className="list-group mb-0 pb-3">
          <div className="row">
            <div className="col-lg-4">
              <div className="list-group-item w-badge unclickable animated fadeInUp delayp4">
                <div className="badges badge-red">
                  <h3>100</h3>
                  <span>Mbps</span>
                </div>
                <div className="list-group-item-content">
                  <h5 className="title">Unlimited Internet</h5>
                  <span className="desc">
                    Perfect fot everyday internet users with light daily use
                    </span>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="list-group-item w-badge unclickable animated fadeInUp delayp5">
                <div className="badges badge-blue">
                  <h3>202</h3>
                  <span>Channel</span>
                </div>
                <div className="list-group-item-content">
                  <h5 className="title">USeeTV Channels</h5>
                  <span className="desc">
                    Enjoy TV on demand from domestic and international TV channels.
                    </span>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="list-group-item w-badge unclickable animated fadeInUp delayp6">
                <div className="badges badge-green">
                  <h3>100</h3>
                  <span>mins</span>
                </div>
                <div className="list-group-item-content">
                  <h5 className="title">Call Quota</h5>
                  <span className="desc">
                    Free call to local numbers up to 100 minutes
                    </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

const SectionContent1 = () => {
  return (
    <div className="py-main bg-gray-50 bg-md-up-white shop-internet-features">
      <div className="container">
        <div className="heading d-none d-md-block">
          <h3 className="animated fadeInUp delayp4">Lorem Ipsum Dolor Sit Ahmet</h3>
          <p className="animated fadeInUp delayp5 w-md-up-75">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500.</p>
        </div>
        <div className="subheading d-block d-md-none">
          <p className="animated fadeInUp delayp4">Our internet packages features</p>
        </div>
        <div className="row row-1 row-md-up-2">
          <div className="col-4">
            <div className="card card-feature red animated fadeInUp delayp5">
              <h5>Unlimited Internet Quota <i className="fal fa-angle-right"></i> </h5>
              <p>Enjoy unlimited internet for all your internet needs!</p>
            </div>
          </div>
          <div className="col-4">
            <div className="card card-feature blue animated fadeInUp delayp6">
              <h5>TV On Demand <i className="fal fa-angle-right"></i> </h5>
              <p>Watch TV shows on demand. Missed a show? No Problem!</p>
            </div>
          </div>
          <div className="col-4">
            <div className="card card-feature green animated fadeInUp delayp7">
              <h5>Free Quota & Bonuses <i className="fal fa-angle-right"></i></h5>
              <p>Get free quota for call & lots of extra bonuses</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

// const SectionContent2 = ({ images }) => {
//     return (
//         <div className="py-main bg-white animated fadeInUp delayp8">
//           <div className="container">
//             <div className="row row-4">
//             <div className="col-md-6 col-12 mb-3">
//                 <div className="subheading">
//                 <p>Package</p>
//                 </div>
//                 <div className="list-group mb-0">
//                     <Link to="/shop/internet/package-finder/budget/1" className="list-group-item w-arrow">
//                         <img src={images.common["ic_selection_budget.png"]} className="img-fluid w-50px" alt="Budget Icon" />
//                         <div className="list-group-item-content">
//                             <h5 className="title">3 Cameras for up to 3 rooms</h5>
//                         </div>
//                     </Link>
//                     <Link to="/shop/internet/package-finder/needs/1" className="list-group-item w-arrow">
//                         <img src={images.common["ic_selection_needs.png"]} className="img-fluid w-50px" alt="Need Icon" />
//                         <div className="list-group-item-content">
//                             <h5 className="title">Free 3 months maintenance</h5>
//                         </div>
//                     </Link>
//                     <Link to="/shop/internet/package-finder/budget/1" className="list-group-item w-arrow">
//                         <img src={images.common["ic_selection_budget.png"]} className="img-fluid w-50px" alt="Budget Icon" />
//                         <div className="list-group-item-content">
//                             <h5 className="title">3 Cameras for up to 3 rooms</h5>
//                         </div>
//                     </Link>
//                     <Link to="/shop/internet/package-finder/needs/1" className="list-group-item w-arrow">
//                         <img src={images.common["ic_selection_needs.png"]} className="img-fluid w-50px" alt="Need Icon" />
//                         <div className="list-group-item-content">
//                             <h5 className="title">Free 3 months maintenance</h5>
//                         </div>
//                     </Link>
//                 </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     );
// };