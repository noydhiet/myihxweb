import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import { login } from '../../services';
import { setToken, setUserData, setExpireTime } from '../../utils/storage';

class SuccessRegister extends Component {
  static contextType = PageContext;
  state = { isBtnClicked: false, registrationData: JSON.parse(localStorage.getItem("registrationData")) }
  componentDidMount() {
    this.context.trigger(1);
    this.context.pageCase("w-bg");
  }
  getStarted = () => {
    const { history } = this.props;
    const { registrationData, isBtnClicked } = this.state;
    const { email, password } = registrationData;
    if(!isBtnClicked) {
      this.setState({isBtnClicked: true})
      login({ email, password})
      .then(({data}) => {
        if (data.status === 200 || data.status === 201) {
          const userData = JSON.parse(atob(data.data.token.split('.')[1]));
          userData.userName = data.data.userName;

          setToken(data.data.token);
          setUserData(userData);
          setExpireTime(userData.exp);
          window.location.href = '/home';
          localStorage.removeItem("registrationData");
        } else {history.push("/login")}
        this.setState({isBtnClicked: false})
      })
      .catch(err => {this.setState({isBtnClicked: false}); console.log(err)})
    }
  }

  render() {
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    return (
    <TextContext.Consumer>{(context)=>{
      const { registerText: { success }} = context;
      const { title, subtitle, btnStart } = success;
      return (
        <section>
          <div className="container container-xs">
            <div className="box box-main animated fadeInUp">
              <header className="header-light white icon-center animated fadeInUp delayp1">
                <div className="header-actions">
                  <Link to="/register/otp" className="btn btn-link"><i className="fal fa-times" /></Link>
                </div>
                <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
              </header>
              <section className="animated fadeInUp delayp2">
                <h1>{title}</h1>
                <p>{subtitle}</p>
              </section>
              <div className="btn-placeholder bottom animated fadeInUp delayp3">
                <Link to="#" onClick={e => {e.preventDefault(); this.getStarted()}} className="btn btn-primary btn-block"> {btnStart} </Link>
              </div>
            </div>
          </div>
        </section>
      )
    }}</TextContext.Consumer>
    )
  }
}


export default SuccessRegister


