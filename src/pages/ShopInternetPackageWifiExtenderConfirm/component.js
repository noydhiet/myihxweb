import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Form } from 'react-bootstrap';
import { PageContext } from './../../context/PageContext';

const ShopInternetPackageWifiExtenderConfirm = () => {
    const { trigger, navCase, pageCase } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
        <div className="container container-sm">
            <div className="box animated fadeInUp">
                <header>
                <div className="header-actions">
                    <Link to="/purchase/payment-method/payment-with-card" className="btn btn-link">
                    <i className="fa fa-arrow-left"></i>
                    </Link>
                </div>
                </header>
                <section className="section-header-text">
                    <div className="heading">
                        <h1>Confirm order</h1>
                        <p>Confirm your package selection and order</p>
                    </div>
                </section>
                <section className="py-main-sm pb-0">
                    <div className="subheading">
                        <p>Package</p>
                    </div>
                    <Link
                        to="#"
                        className="card card-packages w-arrow animated fadeInUp delayp2"
                        >
                        <div className="card-header">
                        <div className="badges-list">
                            <div className="badges badge-red left">
                            <h3>100</h3>
                            <span>Mbps</span>
                            </div>
                        </div>
                        <ul className="list-unstyled">
                            <li>Internet unlimited</li>
                            <li>203 USee TV channels</li>
                            <li>1000 minutes call</li>
                        </ul>
                        </div>
                        <div className="card-body w-btn">
                        <h3>
                            Rp1.890.000<small> / bln</small>
                        </h3>
                        <small className="help-block">Billing starts the month after installation</small>
                        <span className="btn btn-primary">Details</span>
                        </div>
                    </Link>
                    <div className="subheading">
                        <p>Your Information</p>
                    </div>
                    <div className="form-group">
                        <label>Address</label>
                        <p>Jl. Jurang Manggu Barat no.8, Pondok Aren, Tanggerang Selatan, Indonesia, 15123</p>
                    </div>
                    <div className="subheading mt-3">
                        <p>Fee Detail</p>
                    </div>
                    <div className="form-group">
                        <label>Amount</label>
                        <h4>Rp35.000</h4>
                    </div>
                    <div className="form-group">
                        <label>Installation Fee</label>
                        <h4>Rp15.000</h4>
                    </div>
                    <div className="form-group">
                        <label>Total</label>
                        <h4>Rp55.000</h4>
                        <small className="help-block">Include 10% tax. This amount will be billed every month after installation</small>
                    </div>
                    <Form>
                        <div className="btn-placeholder">
                            <div className="row">
                                <div className="col-6">
                                    <Link to="/signature" className="btn btn-transparent btn-block">Cancel</Link>
                                </div>
                                <div className="col-6">
                                    <Link to="/shop/internet/wifi-extender/success" className="btn btn-primary btn-block"> <span>Next</span> </Link>
                                </div>
                            </div>
                        </div>
                    </Form>
                </section>
            </div>
        </div>
    </section>
    )
}

export default ShopInternetPackageWifiExtenderConfirm;