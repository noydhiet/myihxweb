import React, { useState, useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { Modal, Form } from 'react-bootstrap'
import { PageContext } from './../../context/PageContext';

const FisikTicketStatusClosed = () => {
  const { trigger, navCase, pageCase, brand, common, sample, modal } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  const [cond, setCond] = useState({
    modalSetStar: false,
    notifReport: false,
    star1: null,
    star2: null,
    star3: null,
    star4: null,
    star5: null,
  });
  const triggerPage = (mode) => {
    switch (mode) {
      case "modalSetStar":
        setCond({
          modalSetStar: !cond.modalSetStar
        });
        break;

      case "close":
        setCond({
            modalSetStar: false
        })
        break;

      case "star1":
        setCond({
                star1: true,
                modalSetStar: true
        });
        break;

        case "star2":
            setCond({
                star1: true,
                star2: true,
                modalSetStar: true
            });
            break;

        case "star3":
            setCond({
                star1: true,
                star2: true,
                star3: true,
                modalSetStar: true
            });
            break;

        case "star4":
            setCond({
                star1: true,
                star2: true,
                star3: true,
                star4: true,
                modalSetStar: true
            });
            break;


        case "star5":
            setCond({
                star1: true,
                star2: true,
                star3: true,
                star4: true,
                star5: true,
                modalSetStar: true
            });
            break;

      default:
        return null
    }
  }

  const closeNotif = () => {
    setTimeout(() => {
      setCond({
        notifReport: false
      })
    }, 2000);
  }
  const notifView = cond.notifReport ? "active" : ""
  const images = { brand, common, sample };
  const active = {
      star1: cond.star1 ? "active" : "",
      star2: cond.star2 ? "active" : "",
      star3: cond.star3 ? "active" : "",
      star4: cond.star4 ? "active" : "",
      star5: cond.star5 ? "active" : "",
  }
  const disabled = cond.star1 || cond.star2 || cond.star3 || cond.star4 || cond.star5 ? "" : "disabled";
  if (cond.notifReport === true) {
    closeNotif();
  }
  return (
    <section>
      <div className="container container-sm">
        <div className="box position-relative animated fadeInUp">
          <header className="">
            <div className="header-actions">
              <Link to="/installation/fisik-ticket/status-assigned" className="btn btn-link"><i className="fa fa-arrow-left" /></Link>
            </div>
            <div className="heading animated fadeInUp delayp1">
              <h1>Issue Report</h1>
            </div>
          </header>
          <section className="section-header-card py-main-sm animated fadeInUp delayp2">
            <div className="subheading">
              <p>Your Appointment</p>
            </div>
            <div className="list-group mb-0">
              <div className="list-group-item unclickable">
                <div className="badges badge-date dark">
                  <small>Thu</small>
                  <h3>27</h3>
                  <span>July</span>
                </div>
                <div className="list-group-item-content">
                  <h5 className="title">New installation</h5>
                  <span className="desc mb-2">
                    Estimated arrival time 8:00 - 12:00
                                  </span>
                  <span className="font-weight-bold text-primary">
                    Reschedule
                                  </span>
                </div>
              </div>
            </div>
            <div className="subheading">
              <p>Your Ticket</p>
            </div>
            <div className="list-group">
              <Link
                to="/shop/internet/package-finder"
                className="list-group-item transparent w-shadow"
              >
                <div className="list-group-item-content">
                  <h5 className="title w-md-up-75 mb-0">#1234567</h5>
                  <p className='desc'>Internet has been slow for more than 7 days. Technician will visit to analyze the problem.</p>
                </div>
              </Link>
            </div>
            <div className="py-main-sm pb-0 pt-0">
              <div className="form-group">
                <label>Address</label>
                <p>Jl. Jurang Mangu Barat no. 8, Tangerang Selatan, Banten, 15223</p>
            </div>
            <div className="subheading">
                <p>Your Technician</p>
            </div>
            <div className="list-group">
              <Link
                to="/shop/internet/package-finder"
                className="list-group-item transparent w-shadow"
              >
                <img alt="" src={images.common["sampleImg.jpg"]} className="w-50px rounded-circle" />
                <div className="list-group-item-content">
                  <h5 className="title w-md-up-75 mb-0">Fajar Nugraha</h5>
                  <p className='desc'>On the way</p>
                </div>
                <div>
                  <i className="fas fa-comment-alt-dots text-primary pr-3"></i>
                  <i className="fas fa-phone text-primary"></i>
                </div>
              </Link>
            </div>
            <div className="milestone">
              <div className="milestone-item success">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Ticket Submitted!</h5>
                  <span className="desc">Ticket has been submitted. Our team is currently, processing your request</span>
                </div>
              </div>

              <div className="milestone-item success">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">In Progress</h5>
                  <span className="desc">Technician is on the way to you</span>
                </div>
              </div>
                <div className="milestone-item success">
                    <div className="milestone-item-img">
                    <div className="dot"></div>
                    </div>
                    <div className="milestone-item-content">
                    <h5 className="title">Repair Complete</h5>
                    <span className="desc">Our technician has finished repairing the issue </span>
                    </div>
                </div>
                <div className="milestone-item last-child">
                    <div className="milestone-item-img">
                    <div className="dot"></div>
                    </div>
                    <div className="milestone-item-content">
                    <h5 className="title">Issue Solved</h5>
                    <span className="desc">Issue has been resolved!</span>
                    </div>
                </div>
              </div>
            </div>
            <div className="text-center">
              <p className="help-block">Is your problem solved?</p>
              <div className="row">
                  <div className="col-6">
                    <Link to="/installation/fisik-ticket/re-open" className="btn btn-transparent btn-block">No</Link>
                  </div>
                  <div className="col-6">
                    <Link to="#" className="btn btn-primary btn-block" onClick={() => triggerPage("modalSetStar")}>Yes</Link>
                  </div>
              </div>
            </div>
          </section>
        </div>
      </div>
      {/* Modal */}
      <Modal show={cond.modalSetStar} onHide={() => this.triggerPage("close")}>
            <div className="modal-body p-box-popup">
              <div className="box box-main mb-0">
                <header className="header-light white w-l-icon icon-center">
                  <img className="header-img" src={images.common['grfx_grfx_rate.png']} alt="Icon Success" />
                </header>
                <section className="">
                  <h1>Issue Solved!</h1>
                  <p>We're glad your issue is solved! Please take a few seconds to rate your technician</p>
                  <div className="list-group list-group-2-stack">
                      <div className="list-group-item">
                          <div className="list-group-item-header">
                              <img src={images.common["sampleImg.jpg"]} className="w-50px rounded-circle" alt="img" />
                              <div className="list-group-item-content">
                                  <h4>Fajar Nugraha</h4>
                              </div>
                          </div>
                          <div className="star-rating">
                              <ul>
                                <li><i class={"fas fa-star " + active.star1} onClick={()=>triggerPage("star1")}></i></li>
                                <li><i class={"fas fa-star " + active.star2} onClick={()=>triggerPage("star2")}></i></li>
                                <li><i class={"fas fa-star " + active.star3} onClick={()=>triggerPage("star3")}></i></li>
                                <li><i class={"fas fa-star " + active.star4} onClick={()=>triggerPage("star4")}></i></li>
                                <li><i class={"fas fa-star " + active.star5} onClick={()=>triggerPage("star5")}></i></li>
                              </ul>
                          </div>
                      </div>
                  </div>
                </section>
                <div className="btn-placeholder inline bottom">
                  <Link to="/installation/fisik-ticket/done" className={"btn btn-primary btn-block " + disabled}>Done</Link>
                </div>
              </div>
            </div>
          </Modal>
      <div className={"card card-notif " + notifView}>
        <p className="notif-text">We're sorry you're having issues. We have received your report and will follow up within 1x24 hours</p>
      </div>
    </section>
  )
}

export default FisikTicketStatusClosed