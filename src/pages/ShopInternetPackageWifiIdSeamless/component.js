import React, { Component } from "react";
import { Link } from "react-router-dom";
import ShopInternet from "./../ShopInternet";
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import TrackVisibility from "react-on-screen";

class ShopInternetPackageWifiIdSeamless extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    return (
      <TextContext.Consumer>{(context)=>{
        const { shopText } = context;
        return (
          <section>
            <ShopInternet wifiIdActive="active" />
            <SectionWithBanner images={images} shopText={shopText}/>
            <TrackVisibility once offset={100}> 
              <SectionText images={images} shopText={shopText}/>
            </TrackVisibility>
            <TrackVisibility once offset={400}> 
              <SectionFeatures images={images} />
            </TrackVisibility>
          </section>
        )
      }}

      </TextContext.Consumer>
    );
  }
}

export default ShopInternetPackageWifiIdSeamless;

const SectionWithBanner = ({ images, shopText }) => {
  return (
    <div className="py-main bg-cover-section wifi-id content-center">
      <div className="container">
        <div className="row row-4">
          <div className="col-md-6 col-8 content-center-left">
            <div className="heading mb-2">
              <h3 className="animated fadeInUp delayp5">{shopText.shopInternetWifiIdBanner.title}</h3>
              <p className="mb-0 animated fadeInUp delayp6">
              {shopText.shopInternetWifiIdBanner.desc}
              </p>
            </div>
          </div>
          <div className="col-6 d-none d-md-block">
            <img
              src={images.common["banner_bnr_internet_wifiid.png"]}
              className="img-fluid animated fadeInUp delayp7 rounded"
              alt="wifi.id seamless"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

const SectionText = ({isVisible, shopText}) => {
  const effect = isVisible ? "fadeInUp" : "";
  return (
    <div className={"py-main bg-white animated " + effect + " delayp1"}>
      <div className="container">
        <p className="mb-0 w-md-up-50">
        {shopText.shopInternetWifiIdBanner.about}
        </p>
      </div>
    </div>
  );
};

const SectionFeatures = ({ images, isVisible }) => {
  const effect = isVisible ? "fadeInUp" : "";
  return (
    <div className="py-main-sm bg-white">
      <div className="container">
        <div className="row row-4">
          <div className={"col-md-6 col-12 mb-3 animated " + effect + " delayp1"}>
            <div className="subheading">
              <p>Features</p>
            </div>
            <div className="list-group mb-0">
              <Link
                to="/shop/internet/package-finder/budget/1"
                className="list-group-item transparent"
              >
                <img
                  src={images.common["grfx_grfx_line_benefit_1.png"]}
                  className="img-fluid w-75px"
                  alt="Budget Icon"
                />
                <div className="list-group-item-content">
                  <h5 className="title">Unlimited high speed internet</h5>
                  <span className="desc">
                    Get up to 100Mbps high speed internet
                  </span>
                </div>
              </Link>
              <Link
                to="/shop/internet/package-finder/budget/1"
                className="list-group-item transparent"
              >
                <img
                  src={images.common["grfx_grfx_line_benefit_1.png"]}
                  className="img-fluid w-75px"
                  alt="Budget Icon"
                />
                <div className="list-group-item-content">
                  <h5 className="title">Automatic seamless connection</h5>
                  <span className="desc">
                    Connect autimatically when your device is in range of a
                    hotspot
                  </span>
                </div>
              </Link>
              <Link
                to="/shop/internet/package-finder/budget/1"
                className="list-group-item transparent"
              >
                <img
                  src={images.common["grfx_grfx_line_benefit_1.png"]}
                  className="img-fluid w-75px"
                  alt="Budget Icon"
                />
                <div className="list-group-item-content">
                  <h5 className="title">Connect up to 5 devices</h5>
                  <span className="desc">
                    Use the same username for up to 5 devices
                  </span>
                </div>
              </Link>
              <Link
                to="/shop/internet/package-finder/budget/1"
                className="list-group-item transparent"
              >
                <img
                  src={images.common["grfx_grfx_line_benefit_1.png"]}
                  className="img-fluid w-75px"
                  alt="Budget Icon"
                />
                <div className="list-group-item-content">
                  <h5 className="title">Over 100.000 hotspots</h5>
                  <span className="desc">
                    Connect to over 100.000 hotspots all over indonesia
                  </span>
                  <Link to="/" className="btn btn-link p-0">
                    Find Hotspot
                  </Link>
                </div>
              </Link>
            </div>
          </div>
          <div className={"col-md-6 col-12 mb-3 animated " + effect + " delayp2"}>
            <div className="subheading">
              <p>How To Activate</p>
            </div>
            <div className="milestone">
              <div className="milestone-item active">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Activate from myIndiHome app</h5>
                  <span className="desc">
                    click the button below to activate wifi.id. You will get a
                    username and password
                  </span>
                </div>
              </div>
              <div className="milestone-item active">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">
                    Add a device by adding ther MAC Address on myIndiHome
                  </h5>
                  <Link to="" className="">
                    How to find my MAC Address
                  </Link>
                </div>
              </div>
              <div className="milestone-item active last-child">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">
                    Login to wifi.id seamless wifi hotspot with your username
                    and password
                  </h5>
                  <span className="desc">
                    Connect to a wifi.id hotspot and login to start using
                    wifi.id!
                  </span>
                </div>
              </div>
            </div>
            <div className="btn-placeholder">
              <Link
                to="/shop/internet/wifi-id-seamless/confirm"
                className="btn btn-primary block-sm"
              >
                Activate
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
