import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const ProfileCompletionTask = () => {
  const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  const images = { brand, common, sample };
  return (
    <section>
      <div className="container container-sm">
        <div className="box animated fadeInUp">
          <header>
            <div className="header-actions">
              <Link to="/profile" className="btn btn-link">
                <i className="fa fa-arrow-left"></i>
              </Link>
            </div>
          </header>
          <section className="section-header-text">
            <div className="heading">
              <h1>Complete Profile</h1>
            </div>
          </section>
          <section className="animated fadeInUp delayp2">
            <div className="box box-sm mb-3 box-progress">
              <h4>Your profile is 100% complete!</h4>
              <p>Complete your profile secure your account and get 5% off your next bill!</p>
              <ol className="wizard-list">
                <li className="wizard-list-item active">
                  <p className="dot"></p>
                </li>
                <li className="wizard-list-item active">
                  <p className="dot"></p>
                </li>
                <li className="wizard-list-item active">
                  <p className="dot"></p>
                </li>
                <li className="wizard-list-item active">
                  <p className="dot"></p>
                </li>
                <li className="wizard-list-item active">
                  <p className="dot"></p>
                </li>
              </ol>
              <div className="btn-placeholder mt-0">
                <button className="btn btn-primary btn-block">Claim Reward</button>
              </div>
            </div>
            <div className="subheading">
              <p>Task</p>
            </div>
            <div className="list-group mb-0 hidden">
              <Link to="/profile/edit" className="list-group-item pd-sm w-arrow">
                <img src={images.common["icon_ic_profile.png"]} className="img-fluid w-25px" alt="Budget Icon" />
                <div className="list-group-item-content">
                  <p className="mb-0">Complete profile Information</p>
                </div>
              </Link>
              <Link to="#" className="list-group-item pd-sm w-arrow">
                <img src={images.common["icon_ic_email.png"]} className="img-fluid w-25px" alt="Need Icon" />
                <div className="list-group-item-content">
                  <p className="mb-0">Verify e-mail address</p>
                </div>
              </Link>
              <Link to="/shop/internet/package" className="list-group-item pd-sm w-arrow">
                <img src={images.common["icon_ic_shop.png"]} className="img-fluid w-25px" alt="Budget Icon" />
                <div className="list-group-item-content">
                  <p className="mb-0">Subscribe to Indihome package</p>
                </div>
              </Link>
              <Link to="/personal-data" className="list-group-item pd-sm w-arrow">
                <img src={images.common["icon_ic_ktp.png"]} className="img-fluid w-25px" alt="Need Icon" />
                <div className="list-group-item-content">
                  <p className="mb-0">Upload KTP</p>
                </div>
              </Link>
              <Link to="#" className="list-group-item pd-sm w-arrow">
                <img src={images.common["icon_ic_wifi.png"]} className="img-fluid w-25px" alt="Need Icon" />
                <div className="list-group-item-content">
                  <p className="mb-0">Connect to IndiHome WiFi</p>
                </div>
              </Link>
            </div>

            <div className="list-group mb-0">
              <Link to="/profile/edit" className="list-group-item pd-sm w-arrow">
                <img src={images.common["icon_ic_done.png"]} className="img-fluid w-25px" alt="Budget Icon" />
                <div className="list-group-item-content">
                  <p className="mb-0">Complete profile Information</p>
                </div>
              </Link>
              <Link to="#" className="list-group-item pd-sm w-arrow">
                <img src={images.common["icon_ic_done.png"]} className="img-fluid w-25px" alt="Need Icon" />
                <div className="list-group-item-content">
                  <p className="mb-0">Verify e-mail address</p>
                </div>
              </Link>
              <Link to="/shop/internet/package" className="list-group-item pd-sm w-arrow">
                <img src={images.common["icon_ic_done.png"]} className="img-fluid w-25px" alt="Budget Icon" />
                <div className="list-group-item-content">
                  <p className="mb-0">Subscribe to Indihome package</p>
                </div>
              </Link>
              <Link to="/personal-data" className="list-group-item pd-sm w-arrow">
                <img src={images.common["icon_ic_done.png"]} className="img-fluid w-25px" alt="Need Icon" />
                <div className="list-group-item-content">
                  <p className="mb-0">Upload KTP</p>
                </div>
              </Link>
              <Link to="#" className="list-group-item pd-sm w-arrow">
                <img src={images.common["icon_ic_done.png"]} className="img-fluid w-25px" alt="Need Icon" />
                <div className="list-group-item-content">
                  <p className="mb-0">Connect to IndiHome WiFi</p>
                </div>
              </Link>
            </div>
          </section>
        </div>
      </div>
    </section>
  )
}

export default ProfileCompletionTask;