import React, { useState, useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { PageContext } from './../../context/PageContext';

const FisikTicketHistory = () => {
    const { trigger, navCase, pageCase, brand, common, sample, modal } = useContext(PageContext);
    useEffect(() => {
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
<section>
      <div className="container container-sm">
        <div className="box position-relative animated fadeInUp">
          <header className="">
            <div className="header-actions">
              <Link to="/installation/fisik-ticket" className="btn btn-link"><i className="fa fa-arrow-left" /></Link>
            </div>
            <div className="heading animated fadeInUp delayp1">
              <h1>Active Issues</h1>
            </div>
          </header>
          <section className="section-header-card py-main-sm animated fadeInUp delayp2">
            <div className="list-group list-group-activity mb-0">
            <Link
                to="/history/activity/problem-report"
                className="list-group-item pr-0"
              >
                <div className="list-group-item-content mb-0">
                  <small className="caption">#123456</small>
                  <div className="list-group-item-content-w-status">
                    <p className="title">Billing Issue Report</p>
                    <p className="text-status on_progress">On Progress</p>
                  </div>
                </div>
              </Link>
              <Link
                to="/history/activity/problem-report"
                className="list-group-item pr-0"
              >
                <div className="list-group-item-content mb-0">
                  <small className="caption">#123456</small>
                  <div className="list-group-item-content-w-status">
                    <p className="title">Technical Issue Report</p>
                    <p className="text-status on_progress">On Progress</p>
                  </div>
                </div>
              </Link>
            </div>
          </section>
        </div>
      </div>
    </section>
    )
}

export default FisikTicketHistory;