import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { Form, Modal } from 'react-bootstrap';
import { PageContext } from './../../context/PageContext';
import { reportInstallationIssue, fetchIssuesWithoutCategoryId, userProfile } from '../../services'
import { getToken, setUserProfile, getUserProfile } from '../../utils/storage'

const InstallationDetailWaitingTicket = (props) => {
  const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
  const [ inputs, setInputs ] = useState({ issueIndex: '', indiHomeNumber: ''});
  const [ modelForbiddenRaiseTicket, setModelForbiddenRaiseTicket ] = useState(false);
  const [isButtonLoading, setButtonLoading] = useState(false);
  const [issueList, setIssueList] = useState(undefined);
  const [modalServiceUnavailable, setModalServiceUnavailable] = useState(false);
  const handleOnChange = (e) => (setInputs({ ...inputs, [e.target.id]: e.target.value }))
  const submitHelpTicket = (e) => {
    e.preventDefault();
    if(!isButtonLoading) {
      setButtonLoading(true);
      const { history } = props;
      const payload = {categoryId: issueList[inputs.issueIndex].categoryId, issueId: issueList[inputs.issueIndex].issueId, message: inputs.message};
      const queryParam = `selectedIndiHomeNum=${inputs.indiHomeNumber}`
      console.log('datadata', payload, queryParam)
      reportInstallationIssue(payload, queryParam)
      .then((response) => {
        console.log('response', response)
        if(response.data.status === 200 || response.data.status === 201) {
          history.replace({pathname: '/home'});
          pageCase("submitHelpTicket")
        } else if(response.data.status === 4005) {
          const { history } = props;
          history.push({pathname: '/installation/fisik-ticket', state: { data: getUserProfile(), payload: payload }})
        } else if(response.data.status === 4003 || response.data.status == 4001 || response.data.status === 4002) {
          setModelForbiddenRaiseTicket(true);
        } else {
          setModalServiceUnavailable(true)
        }
        setButtonLoading(false);
      }).catch((error) => {
        console.error(error.response)
        if(error.response && error.response.status === 403) {
          setModelForbiddenRaiseTicket(true);
        }
        setButtonLoading(false);
      });
    }
  } 
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
    if(!getToken()) {
      const {history} = props;
      history.push('/home');
    }
    fetchIssueListAndProfile();
  }, [trigger, navCase, pageCase]);
  const images = { brand, common, sample } ;
  const disabled = !(inputs.issueIndex) || isButtonLoading ? 'disabled' : '';
  const modal = action => {
    switch(action) {
      case 'close':
          setModelForbiddenRaiseTicket(false)
        break;
    }
  }
  const fetchIssueListAndProfile = () => {
    if(getToken()) {
      fetchIssuesWithoutCategoryId().then( response => {
        console.log('fetchIssuesWithoutCategoryId', response);
        if(response.status === 200 && response.data.data.length > 0) {
          setIssueList(response.data.data)
        } else {
          setIssueList(null)
        }
      }).catch( error => { setIssueList(null); console.error(error); });

      userProfile().then(({data}) => {
        console.log('responseresponse', data)
        if((data.status === 200  || data.status === 201) && data.data.accounts.length > 0) {
          setInputs({ ...inputs, ['indiHomeNumber']: data.data.accounts[0].indiHomeNum })
          setUserProfile(data.data);
        }
      }).catch(e => console.error(e))
    }
  }
  const Loading = ({ images }) => {
    return (
      <span><i className="fa fa-circle-notch fa-spin"></i></span>
    )
  }
  return (
    <section>
      <div className="container container-sm">
        <div className="box position-relative animated fadeInUp">
          <header className="">
            <div className="header-actions">
              <Link to="#" onClick={e => props.history.goBack()} className="btn btn-link">
                <i className="fa fa-arrow-left" />
              </Link>
            </div>
          </header>
          <section className="section-header-text">
            <div className="heading">
              <h1>Report an Issue</h1>
              <p>We're sorry to hear that you're having difficulties. Report your issue here and our team will get in contact with you to fix it.</p>
            </div>
          </section>
          <Form>
            <section className="animated fadeInUp delayp2">
              <Form.Group>
                <Form.Label>Issue</Form.Label>
                <Form.Control as="select"
                  className="custom-select"
                  id="issueIndex"
                  onChange={handleOnChange}
                >
                  <option value="">{issueList === undefined ? 'Loading...' : (issueList === null ? 'No data found' : 'Choose One') }</option>
                  {
                    issueList === undefined
                    ? 
                    (null)
                    :
                    (issueList === null ? (null) : issueList.map( (item, index) => <option key={index} value={index}>{item.issue}</option>) )
                  }
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>Comments (Optional)</Form.Label>
                <Form.Control
                  as="textarea"
                  placeholder="Enter message"
                  id="message"
                  value={inputs.message}
                  onChange={handleOnChange}
                  rows="2"
                />
              </Form.Group>
            </section>
            <div className="btn-placeholder bottom">
            <Link to="#" className={`btn btn-primary btn-block ` + disabled} onClick={(e) => submitHelpTicket(e)} >{isButtonLoading ? <Loading images={images}/> : 'Submit Ticket'}</Link>
            </div>
          </Form>
        </div>
      </div>
       {/* Model for ticket raise not allowed */}
       <Modal show={modelForbiddenRaiseTicket} onHide={() => modal("close")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                <img className="header-img" src={images.common['grfx_warning.png']} alt="Icon Failed" />
              </header>
              <section>
                <h1>Sorry you have a pending report</h1>
                <p>At the moment you can only have one issue report at time</p>
              </section>
              <Link to="#" className="btn btn-primary btn-block mb-3" onClick={(e) => {e.preventDefault(); modal("close");} }>Close</Link>
                <Link to={{pathname: '/help', state: {redirect: 'fisik_detail'}}} className="btn btn-transparent btn-block">See my pending report</Link>
            </div>
          </div>
        </Modal>
        {/* Modal Session Time Out */}
        <Modal show={modalServiceUnavailable} onHide={() => setModalServiceUnavailable(false)} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <section>
                <h1>Service Error</h1>
                <p>A service error has occured while processing your request</p>
              </section>
              <div className="btn-placeholder inline bottom">
                <Link to="#" className="btn btn-primary btn-block" onClick={e => {e.preventDefault(); setModalServiceUnavailable(false)}}>Ok</Link>
              </div>
            </div>
          </div>
        </Modal>
    </section>
  )
}

export default InstallationDetailWaitingTicket;
