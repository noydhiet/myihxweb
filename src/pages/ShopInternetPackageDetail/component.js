import React, { Component, useState } from "react";
import { Link } from "react-router-dom";
import { Modal, Accordion } from 'react-bootstrap';
import Slider from "react-slick";
import TrackVisibility from "react-on-screen";
import { PageContext } from './../../context/PageContext';
import { getToken } from '../../utils/storage';

class PackageInternetDetail extends Component {
  static contextType = PageContext;
  state = {
    modalPricing: false,
  }

  triggerPage = (mode) => {
    switch (mode) {
      case "openDetailPrice":
        this.setState({
          modalPricing: !this.state.modalPricing
        });
        break;

      case "close":
        this.setState({
          modalPricing: false,
        });
        break;

      default:
        return null
    }
  }

  componentDidMount() {
    this.context.trigger(2);
    this.context.pageCase("l-bg");
    this.context.navCase("w-md-up-block");

    // const modalCentered = document.getElementsByClassName("modal-centered")[0].getElementsByClassName("modal-dialog")[0];
    // modalCentered.classList.add("modal-dialog-centered");

    let navbar = document.querySelector(".navbar-primary");
    let shopPrice = document.querySelector("#shop-price");
    window.onscroll = function () {
      if (window.pageYOffset > 5) {
        navbar.classList.add("navbar-scroll");
      } else {
        navbar.classList.remove("navbar-scroll");
      }

      if (window.pageYOffset > 190) {
        shopPrice.classList.add("is-scroll");
      } else {
        shopPrice.classList.remove("is-scroll");
      }
    };
  }

  componentWillUnmount() {
    window.onscroll = null;
  }

  render() {
    const { modalPricing } = this.state;
    const { common, brand, sample, modal } = this.context;
    const images = { common, brand, sample };
    const carousel = {
      iflixHooqSection: {
        className: "primary-carousel view-carousel",
        dots: false,
        infinite: false,
        arrows: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 5.5,
              slidesToScroll: 5.5
            }
          },
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 3.5,
              slidesToScroll: 3.5
            }
          },
          {
            breakpoint: 576,
            settings: {
              slidesToShow: 2.5,
              slidesToScroll: 2.5
            }
          }
        ]
      },
      channelSection: {
        className: "primary-carousel view-carousel",
        dots: false,
        infinite: false,
        arrows: true,
        speed: 500,
        slidesToShow: 7,
        slidesToScroll: 7,
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 5.5,
              slidesToScroll: 5.5
            }
          },
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 4.5,
              slidesToScroll: 4.5
            }
          },
          {
            breakpoint: 576,
            settings: {
              slidesToShow: 3.5,
              slidesToScroll: 3.5
            }
          }
        ]
      }
    }
    const loginViewDetail = () => {
      this.triggerPage('close');
      modal('modalSubscribe');
    }
    
    return (
      <div>
        <div className="cover cover-details bg-red-gradient">
          <div className="header-actions d-block d-md-none">
            <Link to="/shop/internet/package" className="btn btn-link">
              <i className="fa fa-arrow-left"></i>
            </Link>
          </div>
          <div className="container">
            <nav aria-label="breadcrumb" className="d-none d-md-block">
              <ol className="breadcrumb animated fadeInUp">
                <li className="breadcrumb-item">
                  <Link to="/shop/">
                    <i className="fa fa-arrow-left mr-1"></i> Shop
                  </Link>
                </li>
                <li className="breadcrumb-item">
                  <Link to="/shop/internet/package">Internet Package</Link>
                </li>
              </ol>
            </nav>
            <div className="cover-content">
              <h1 className="cover-title animated fadeInUp delayp1">
                Paket BUMN 100Mbps
              </h1>
              <p className="cover-text animated fadeInUp delayp2 mb-3">
                This package is perfect for you heavy internet users who wants
                rich channel options!
              </p>
              <Link
                to="/"
                className="btn btn-link p-0 text-white animated fadeInUp delayp3"
              >
                Learn More about Internet Speed{" "}
                <i className="fal fa-angle-right"></i>
              </Link>
            </div>
            <div className="shop-price animated fadeInUp delayp4 d-none d-md-block">
              <div className="container">
                <div className="shop-price-info">
                  <h3>
                    Rp380.000 <small>/month</small>
                  </h3>
                  <Link to="#" className="" onClick={() => this.triggerPage("openDetailPrice")}>
                    View Pricing Detail <i className="fal fa-angle-right"></i>
                  </Link>
                </div>
                {getToken() ? 
                (<Link to="/check-coverage?packageId=1" className="btn btn-primary">Subscribe</Link>) :
                (<button className="btn btn-primary" onClick={() => modal("modalSubcribe")}>Subscribe</button>)}
              </div>
            </div>
          </div>
        </div>
        <div
          className="shop-price fixed-bottom animated fadeInUp delayp8"
          id="shop-price"
        >
          <div className="container">
            <div className="shop-price-info">
              <h3>
                Rp380.000 <small>/month</small>
              </h3>
              <Link to="#" className="" onClick={() => this.triggerPage("openDetailPrice")}>
                View Pricing Detail <i className="fal fa-angle-right"></i>
              </Link>
            </div>
            {getToken() ? 
            (<Link to="/check-coverage?packageId=1" className="btn btn-primary">Subscribe</Link>) :
            (<button className="btn btn-primary" onClick={() => modal("modalSubscribe")}>Subscribe</button>)}
          </div>
        </div>
        <PackageListSection images={images} />
        <ChannelSection images={images}  carousel={carousel}/>
        <TrackVisibility once offset={90}>
          <IflixSection images={images} carousel={carousel}/>
        </TrackVisibility>
        <TrackVisibility once offset={230}>
          <HooqSection images={images} carousel={carousel}/>
        </TrackVisibility>

        {/* Modal - Detail Princing */}
        <Modal show={modalPricing} onHide={() => this.triggerPage('close')}>
          <div className="modal-body p-box">
            {/* <Link to="#" className="close animated fadeInUp" onClick={()=>this.triggerPage('close')}>
              <span><i className="far fa-times text-primary"></i></span>
            </Link> */}
            <section>
              <div className="list-group list-group-transparent">
                <div className="list-group-item pl-0 pr-0 pt-0">
                  <div className="list-group-item-content">
                    <h3>
                      Rp380.000 <small>/month</small>
                    </h3>
                    <p className="text-primary mb-0">View pricing details <i className="fal fa-angle-down"></i></p>
                  </div>
                  {getToken() ?
                  (<Link to="/check-coverage?packageId=1" className="btn btn-primary">Subscribe</Link>) :
                  (<button className="btn btn-primary" onClick={() => loginViewDetail()}>Subscribe</button>)}
                </div>
              </div>
              <div className="list-group list-group-transparent mb-0">
                <div className="list-group-item pd-sm p-0">
                  <div className="list-group-item-content">
                    <p className="help-block mb-0">Biaya IndiHome</p>
                  </div>
                  <p className="mb-0">Rp380.000</p>
                </div>
                <div className="list-group-item pd-sm p-0">
                  <div className="list-group-item-content">
                    <p className="help-block mb-0">Sewa STB</p>
                  </div>
                  <p className="mb-0">Rp80.000</p>
                </div>
                <div className="list-group-item pd-sm p-0">
                  <div className="list-group-item-content">
                    <p className="help-block mb-0">Limited time app discount</p>
                  </div>
                  <p className="mb-0 text-primary">Rp-80.000</p>
                </div>
              </div>
            </section>
          </div>
        </Modal>
      </div>
    );
  }
}

export default PackageInternetDetail;

const PackageListSection = () => {
  const packageList = [
    {
      badges: {
        color: "badge-red",
        title: "100",
        desc: "Mbps"
      },
      title: "Unlimited Internet",
      desc: "Perfect fot everyday internet users with light daily use"
    },
    {
      badges: {
        color: "badge-blue",
        title: "202",
        desc: "Channel"
      },
      title: "USeeTV Channels",
      desc: "Enjoy TV on demand from domestic and international TV channels."
    },
    {
      badges: {
        color: "badge-green",
        title: "100",
        desc: "mins"
      },
      title: "Call Quota",
      desc: "Free call to local numbers up to 100 minutes"
    },
  ]
  return (
    <section className="bg-gray-50 shop-detail-feature">
      <div className="container">
        <div className="list-group mb-0 pb-3">
          <div className="row">
            {packageList.map((value, index)=>{
              let sumDelay = index+1;
              return (
                <div key={index} className="col-lg-4">
                  <div className={"list-group-item w-badge unclickable animated fadeInUp delayp" + sumDelay}>
                    <div className={"badges " + value.badges.color}>
                      <h3>{value.badges.title}</h3>
                      <span>{value.badges.desc}</span>
                    </div>
                    <div className="list-group-item-content">
                      <h5 className="title">{value.title}</h5>
                      <span className="desc">
                        {value.desc}
                      </span>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    </section>
  );
};

const ChannelSection = ({ images }) => {
  const [ cond, condPage ] = useState({
    modalChannel: false
  })
  const triggerPage = () => {
    condPage({
      modalChannel: !cond.modalChannel
    });
  }
  const config = {
    carousel: {
      className: "primary-carousel view-carousel",
      dots: false,
      infinite: false,
      arrows: true,
      speed: 500,
      slidesToShow: 7,
      slidesToScroll: 7,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 5.5,
            slidesToScroll: 5.5
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 4.5,
            slidesToScroll: 4.5
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 3.5,
            slidesToScroll: 3.5
          }
        }
      ]
    }
  };
  const channel = ["channel1.png", "channel2.png", "channel3.png", "channel4.png", "channel5.png", "channel1.png", "channel2.png", "channel3.png"];
  return (
    <div className="py-main-sm pb-lg-sm bg-gray-50">
      <div className="container">
        <div className="subheading w-links animated fadeInUp delayp6">
          <p>Your Channel</p>
          <Link to="#" className="link font-weight-bold" onClick={triggerPage}>
            View All
          </Link>
        </div>
        <Slider {...config.carousel}>
          {channel.map((value, index)=>{
            let sumDelay = index+1;
            return (
              <div className={"carousel-item animated fadeInUp delayp" + sumDelay} key={index}>
                <div className="card card-content shadow-card-sm animated fadeInUp delayp7">
                  <img alt="icon" src={images.common[`${value}`]} className="img-fluid" />
                </div>
              </div>
            )
          })}
        </Slider>

        <Modal show={cond.modalChannel} onHide={triggerPage}>
          <div className="modal-body p-box pb-0">
            <Link to="#" className="close animated fadeInUp" onClick={triggerPage}>
              <span><i className="far fa-times text-primary"></i></span>
            </Link>
            <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
              <h3>Channel List</h3>
            </div>
            <section className="animated fadeInUp delayp2 mb-4">
              <Accordion defaultActiveKey="0">
                <Accordion.Toggle eventKey="0" style={{ width: "100%", background: "transparent", border: "none", paddingLeft: "0", paddingRight: "0", height: "40px" }}>
                  <div className="heading heading-w-link mb-0">
                    <p className="text-body font-size-lg mb-0"><strong>Local Channels</strong></p>
                    <p className="text-primary mb-0">
                      <small className="text-gray-500">32 Channels</small>{" "}
                      <i className={"fas fa-chevron-down arrow " + config.accordionActive}></i>
                    </p>
                  </div>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                  <div className="row">
                    {channel.map((value, index)=>{
                      return (
                        <div className="col-3 col-md-4" key={index}>
                          <div className="card card-packages">
                              <img alt="icon" src={images.common[`${value}`]} className="img-fluid" />
                          </div>
                        </div>
                      )
                    })}
                  </div>
                </Accordion.Collapse>

                <Accordion.Toggle eventKey="1" style={{ width: "100%", background: "transparent", border: "none", paddingLeft: "0", paddingRight: "0", height: "40px" }}>
                  <div className="heading heading-w-link mb-0">
                    <p className="text-body font-size-lg mb-0"><strong>Entertainment</strong></p>
                    <p className="text-primary mb-0">
                      <small className="text-gray-500">12 Channels</small>{" "}
                      <i className={"fas fa-chevron-down arrow " + config.accordionActive}></i>
                    </p>
                  </div>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="1">
                  <div className="row">
                    {channel.map((value, index)=>{
                      return (
                        <div className="col-3 col-md-4" key={index}>
                          <div className="card card-packages">
                              <img alt="icon" src={images.common[`${value}`]} className="img-fluid" />
                          </div>
                        </div>
                      )
                    })}
                  </div>
                </Accordion.Collapse>

                <Accordion.Toggle eventKey="2" style={{ width: "100%", background: "transparent", border: "none", paddingLeft: "0", paddingRight: "0", height: "40px" }}>
                  <div className="heading heading-w-link mb-0">
                    <p className="text-body font-size-lg mb-0"><strong>News</strong></p>
                    <p className="text-primary mb-0">
                      <small className="text-gray-500">3 Channels</small>{" "}
                      <i className={"fas fa-chevron-down arrow " + config.accordionActive}></i>
                    </p>
                  </div>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="2">
                  <div className="row">
                    {channel.map((value, index)=>{
                      return (
                        <div className="col-3 col-md-4" key={index}>
                          <div className="card card-packages">
                              <img alt="icon" src={images.common[`${value}`]} className="img-fluid" />
                          </div>
                        </div>
                      )
                    })}
                  </div>
                </Accordion.Collapse>
              </Accordion>
            </section>
          </div>
        </Modal>
      </div>
    </div>
  );
};

const IflixSection = ({ images, isVisible, carousel }) => {
  const poster = ["poster3.png", "poster4.png", "poster3.png", "poster2.png", "poster1.png", "poster5.png", "poster4.png", "poster3.png"];
  const effect = isVisible ? "fadeInUp" : "";
  return (
    <div className="py-main-sm pb-lg-sm bg-gray-50">
      <div className="container">
        <div className={"subheading animated " + effect + " delayp1"}>
          <p>Watch free on iflix</p>
        </div>
        <Slider {...carousel.iflixHooqSection}>
          {poster.map((value, index)=>{
            let sumDelay = index+1;
            return (
              <div className={"carousel-item animated " + effect + " delayp" + sumDelay} key={index}>
                <div className="card card-content">
                  <img alt="icon" src={images.common[`${value}`]} className="img-fluid" />
                </div>
              </div>
            )
          })}
        </Slider>
      </div>
    </div>
  );
};

const HooqSection = ({ images, isVisible, carousel }) => {
  const movieBanner = ["poster1.png", "poster2.png", "poster3.png", "poster4.png", "poster5.png", "poster1.png", "poster2.png", "poster3.png"];
  const effect = isVisible ? "fadeInUp visible" : "";
  return (
    <div className="py-main bg-gray-50">
      <div className="container">
        <div className="subheading">
          <p className={"animated " + effect + " delayp1"}>Watch free on Hooq</p>
        </div>
        <Slider {...carousel.iflixHooqSection}>
          {movieBanner.map((value, index)=>{
            let sumDelay = index+1;
            return (
              <div className={"carousel-item animated " + effect + " delayp" + sumDelay} key={index}>
                <div className="card card-content">
                  <img alt="icon" src={images.common[`${value}`]} className="img-fluid" />
                </div>
              </div>
            )
          })}
        </Slider>
      </div>
    </div>
  );
};
