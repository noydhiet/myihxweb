import React, { useEffect, useContext, useState } from 'react'
import { Link } from 'react-router-dom'
import { PageContext } from './../../context/PageContext';
import { actionSupportTicket, scheduleService, actionSupportTicketFeasibilty } from '../../services';
import moment from 'moment';
import { Modal } from 'react-bootstrap'
import Slider from "react-slick";
import { getUserProfile } from '../../utils/storage';

const HistoryTicket1 = (props) => {
  const { trigger, navCase, pageCase, modal, brand, common, sample } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
    getTicketData();
    
  }, [trigger, navCase, pageCase]);
  const [ status, setStatus ] = useState({
    ticket: {},
    steps: [],
  });
  const [modalServiceUnavailable, setModalServiceUnavailable] = useState(false);
  const [attemptExhausted, setAttemptExhausted] = useState(false);
  const [feasibleReschedule, setFeasibleReschedule] = useState(false);
  const [feasibilityLoading, setFeasibilityLoading] = useState(false);
  const [dateSlots, setDateSlots] = useState(undefined);
  const [modalReschedule, setModalReschedule] = useState(false);
  const [isBtnSubmitting, setIsBtnSubmitting] = useState(false);
  const [selectedDateIndex, setSelectedDateIndex] = useState(undefined);
  const [selectedDate, setSelectedDate] = useState(undefined);
  const [cardMorningAfternoon, setCardMorningAfternoon] = useState([{ show: false, slotId: '' }, { show: false, slotId: '' }, { show: false, slotId: '' }, { show: false, slotId: '' }]);
  const [morning_1, setMorning_1] = useState(false);
  const [morning_2, setMorning_2] = useState(false);
  const [evening_1, setEvening_1] = useState(false);
  const [evening_2, setEvening_2] = useState(false);
  const [selectedCard, setSelectedCard] = useState(undefined);
  const [isFetchingSlots, setIsFetchingSlots] = useState(false);
  const [showConfirmationReschedule, setShowConfirmationReschedule] = useState(false);
  const [isButtonLoading, setButtonLoading] = useState(false);
  const getTicketData = () => {
    const { location: { state }} = props;
    console.log('statestate', state)
    if(state) {
      setStatus({...status, ticket: state.ticket, steps: state.ticket.ticketStatus.steps})
    }
    // supportTicketStatus(ticketId)
    //   .then(({ data }) => {
    //     console.log('datadataas', data)
    //     data.ok && setStatus({
    //       ...status,
    //       ticket: data.data.data,
    //       steps: data.data.data.ticketStatus.steps
    //     })
    //   })
    //   .catch(err => console.log(err))
  }

  const closeTicket = e => {
    e.preventDefault();
    const { ticket: { ticketId}} = status;
    const { history } = props;
    setIsBtnSubmitting(true);
    actionSupportTicket(ticketId, 'CLOSED', null).then(response => {
      modal('rating')
      history.goBack();
      console.log('responsexz', response)
      setIsBtnSubmitting(false);
    }).catch(error => {
      console.error('errorzx', error.response)
      setIsBtnSubmitting(false);
    })
  }
  const { history, location: { state } } = props;
  if(!state) {
    history.push('/help')
    return(null)
  }
  const Loading = () => {
    return (
      <span><i className="fa fa-circle-notch fa-spin"></i></span>
    )
  }
  const carousel = {
    className: "primary-carousel overflow-inherit mb-4",
    dots: false,
    infinite: false,
    arrows: false,
    speed: 500,
    slidesToShow: 5.3,
    slidesToScroll: 5.3,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 7,
          slidesToScroll: 7
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 5.5,
          slidesToScroll: 5.5
        }
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 4.5,
          slidesToScroll: 4.5
        }
      },
      {
        breakpoint: 320,
        settings: {
          slidesToShow: 3.5,
          slidesToScroll: 3.5
        }
      }
    ]
  };
  const  carouselDate = {
    className: "primary-carousel overflow-inherit mb-4",
    dots: false,
    infinite: false,
    arrows: false,
    speed: 500,
    slidesToShow: 5.3,
    slidesToScroll: 5.3,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 5.5,
          slidesToScroll: 5.5
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3.5,
          slidesToScroll: 3.5
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 4.5,
          slidesToScroll: 4.5
        }
      },
      {
        breakpoint: 320,
        settings: {
          slidesToShow: 3.5,
          slidesToScroll: 3.5
        }
      }
    ]
  };
  const setSchedule = (mode, localDateSlots, formattedDate) => {
    const cardIndexSelect = [ false, false, false, false ];
    setCardMorningAfternoon(oldArray => ({...oldArray, [0]: {'show': false}}))
    setCardMorningAfternoon(oldArray => ({...oldArray, [1]: {'show': false}}))
    setCardMorningAfternoon(oldArray => ({...oldArray, [2]: {'show': false}}))
    setCardMorningAfternoon(oldArray => ({...oldArray, [3]: {'show': false}}))
    for(const [index, item] of localDateSlots[formattedDate].entries()) {
      if(item.timeSlot.slotId === 'morning-1' && item.availability === 1) {
        cardIndexSelect[0] = true;
        setCardMorningAfternoon(oldArray => ({...oldArray, [0]: {'show': true, 'slotId': item.bookingId}}))
      }
      if(item.timeSlot.slotId === 'morning-2' && item.availability === 1) {
        cardIndexSelect[1] = true;
        setCardMorningAfternoon(oldArray => ({...oldArray, [1]: {'show': true, 'slotId': item.bookingId}}))
      }
      if(item.timeSlot.slotId === 'evening-1' && item.availability === 1) {
        cardIndexSelect[2] = true;
        setCardMorningAfternoon(oldArray => ({...oldArray, [2]: {'show': true, 'slotId': item.bookingId}}))
      }
      if(item.timeSlot.slotId === 'evening-2' && item.availability === 1) {
        cardIndexSelect[3] = true;
        setCardMorningAfternoon(oldArray => ({...oldArray, [3]: {'show': true, 'slotId': item.bookingId}}))
      }
    }
    setSelectedDate(formattedDate);
    setSelectedDateIndex(mode)
    if(cardIndexSelect[0]) {
      estimatedArrival("0")
    } else if(cardIndexSelect[1]) {
      estimatedArrival("1")
    } else if(cardIndexSelect[2]) {
      estimatedArrival("2")
    } else if(cardIndexSelect[3]) {
      estimatedArrival("3")
    } else {
      estimatedArrival(undefined)
    }
  }

  const reScheduleTechnicianClick = e => {
    e.preventDefault();
    setShowConfirmationReschedule(false)
    setModalReschedule(true)
    if(!isButtonLoading) {
      setButtonLoading(true)
      let bookingId = undefined;
      let time = undefined;
      for(const [index, item] of dateSlots[selectedDate].entries()) {
        if(item.availability === 1) {
          console.log('selectedCard=>>', selectedCard, item)
          if((selectedCard === "0" && item.timeSlot.slotId === "morning-1") || (selectedCard === "1" && item.timeSlot.slotId === "morning-2")
          || (selectedCard === "2" && item.timeSlot.slotId === "evening-1") || (selectedCard === "3" && item.timeSlot.slotId === "evening-2")) {
            bookingId = item.bookingId;
            time = item.timeSlot.slot;
          }
        }
      }
      const { ticket: { ticketId}} = status;
      const body = {
        "bookingId": bookingId
      }
      console.log('body', body, ticketId)
      actionSupportTicket(ticketId, 'RESCHEDULE', body).then(response => {
        console.log('responsexz', response)
        if(response.data.status === 4006) {
          setAttemptExhausted(true)
          setModalReschedule(false)
        } else if(response.data.status === 200 || response.data.status === 201) {
          const { history } = props;
          history.push('/installation/fisik-ticket/re-open/success/' + selectedDate + '--' + time)
        } else {
          setModalReschedule(false)
          setModalServiceUnavailable(true)
        }
        setButtonLoading(false);
      }).catch(error => {
        console.error('errorzx', error.response)
        setButtonLoading(false);
      })
    }
  }

  const estimatedArrival = (mode) => {
    switch (mode) {
      case "0":
        setSelectedCard("0");
        setMorning_1(true);
        setMorning_2(false);
        setEvening_1(false);
        setEvening_2(false);
        break;
      case "1":
        setSelectedCard("1");
        setMorning_1(false);
        setMorning_2(true);
        setEvening_1(false);
        setEvening_2(false);
        break;
      case "2":
        setSelectedCard("2");
        setMorning_1(false);
        setMorning_2(false);
        setEvening_1(true);
        setEvening_2(false);
      break;
      case "3":
        setSelectedCard("3");
        setMorning_1(false);
        setMorning_2(false);
        setEvening_1(false);
        setEvening_2(true);
      break;
      default:
        setSelectedCard(undefined);
        setMorning_1(false);
        setMorning_2(false);
        setEvening_1(false);
        setEvening_2(false);
      break;
    }
  };
  const fetchRescheduleSlots = () => {
    const { ticket: { ticketId }} = status;
    setFeasibilityLoading(true)
    actionSupportTicketFeasibilty(ticketId).then( ({data}) => {
      console.log('actionSupportTicketFeasibilty', data);
      if(data.status === 200 || data.status === 201) {
        if(data.data.rescheduleFeasibility) {
          if(data.data.rescheduleAttempts === 0) {
            setAttemptExhausted(true)
          } else {
            setModalReschedule(true)
            if(!isFetchingSlots && !dateSlots) {
              setIsFetchingSlots(true);
              scheduleService().then(response => {
                console.log('scheduleService', response);
                if(response.statusText === 'OK' && response.data.data.availableTimes) {
                    var tempDate = undefined;
                    const groupedDateSlots = response.data.data.availableTimes.length === 0 ? null : response.data.data.availableTimes.reduce((groupedData, { availability, date, bookingId, crewId, dateTime, information, timeSlot }) => {
                      const formattedDate = moment(dateTime, 'DD-MM-YYYY HH:mm').format('DD-MM-YYYY')
                      if(!tempDate) tempDate = formattedDate
                      if (!groupedData[formattedDate]) groupedData[formattedDate] = [];
                      groupedData[formattedDate].push({bookingId, crewId, information, timeSlot, availability, date});
                      return groupedData;
                    }, {});
                    console.log('groupedDateSlots', groupedDateSlots);
                    if(groupedDateSlots) {
                      setDateSlots(groupedDateSlots)
                      setSchedule(0, groupedDateSlots, tempDate);
                    } else {
                      setDateSlots(null)
                    }
                    setIsFetchingSlots(false);
                } else {
                  setDateSlots(null)
                  setIsFetchingSlots(false);
                }
              }).catch(error => {
                setIsFetchingSlots(false);
                setDateSlots(null)
                console.error(error);
              })
            }
          }
        } else {
          setFeasibleReschedule(true)
        }
      } else {
        setModalServiceUnavailable(true)
      }
      setFeasibilityLoading(false)
    }).catch(e => {
      console.error(e)
      setFeasibilityLoading(false)
      setModalServiceUnavailable(true);
    });
  }
  const userProfile = getUserProfile();
  const accounts = userProfile && userProfile.accounts.length > 0 ? userProfile.accounts[0] : null;
  const location = accounts && Object.keys(accounts.Location).length > 0 ? accounts.Location : null;
  const ticketHasData = Object.keys(status.ticket).length > 0 ? true : false
  const isFISIK = ticketHasData && status.ticket.categoryType === 'FISIK' ? true : false;
  const recommend = ["bnr_recommend01.png", "bnr_recommend02.png", "bnr_recommend03.png"];
  const images = { brand, common, sample };
  const date1 =  ticketHasData && isFISIK && status.ticket.bookingSlot ? moment(status.ticket.bookingSlot.timeSlot.date, 'DD-MM-YYYY') : undefined;
  const time1 =  ticketHasData && isFISIK && status.ticket.bookingSlot ? status.ticket.bookingSlot.timeSlot.slot : undefined;
  const isRechedulable = ticketHasData ? status.ticket.ticketStatus.status : undefined;
  
  const title_status = ticketHasData ? status.ticket.ticketStatus.title : undefined;
  const ticketId = ticketHasData ? status.ticket.ticketId : undefined;
  console.log('moment', dateSlots)
  const disabled = isBtnSubmitting ? 'disabled' : '';
  const carousalCards = !dateSlots
      ?
      <div key={0}
          className="carousel-item-sm">
          <div className={`badges badge-date active selector`}>
            <small>No slots available</small>
            <h3></h3>
            <span></span>
          </div>
      </div>
      :
      Object.keys(dateSlots).map((formattedDate, index) => {
        const date = moment(formattedDate, 'DD-MM-YYYY');
        return(
         <div key={index}
              className="carousel-item-sm"
              onClick={() => setSchedule(index, dateSlots, formattedDate)} >
              <div className={`badges badge-date ${index === selectedDateIndex ? 'active' : ''} selector`}>
                <small>{date.format('ddd')}</small>
                <h3>{date.format('DD')}</h3>
                <span>{date.format('MMM')}</span>
              </div>
          </div>
        );
      })
  return (
    <section>
      <div className="container container-sm">
        <div className="box position-relative animated fadeInUp">
          <header className="">
            <div className="header-actions">
              <Link to="#" onClick={e => {e.preventDefault(); history.goBack(); }} className="btn btn-link"><i className="fa fa-arrow-left" /></Link>
            </div>
            <div className="heading animated fadeInUp delayp1">
              <h1>Issue Report</h1>
            </div>
          </header>
          <section className="section-header-card py-main-sm animated fadeInUp delayp2">
          {isFISIK
            ?
          (<div className="subheading">
          <p>Your Appointment</p>
        </div>)
        :
        (null)}
            {
              isFISIK
              ?
              (<div className="list-group mb-0">
              <div className="list-group-item unclickable">
                <div className="badges badge-date dark">
                  <small>{ date1 ? date1.format('ddd') : '' }</small>
                  <h3>{date1 ? date1.format('DD') : ''}</h3>
                  <span>{date1 ? date1.format('MMM') : ''}</span>
                </div>
                <div className="list-group-item-content">
                  <h5 className="title">Repair Request</h5>
                  <span className="desc mb-2">
                    Estimated arrival time {time1}
                  </span>
                  {
                  isRechedulable === 'SUBMITTED' ?
                   (<Link to="#" onClick={e => {e.preventDefault(); fetchRescheduleSlots()}} className="font-weight-bold" >{feasibilityLoading ? <Loading /> : 'Reschedule'}</Link>) 
                   :
                   (<span className="font-weight-bold text-muted">
                    Reschedule
                  </span>)}
                  
                  
                </div>
              </div>
            </div>)
              :
              (null)
            }
           
          
            
            <div className="subheading">
                  <p>Your {isFISIK ? 'Ticket' : 'Report'}</p>
            </div>
            <div className="list-group">
              <Link
                to="/shop/internet/package-finder"
                className="list-group-item transparent w-shadow"
              >
                <div className="list-group-item-content">
                  <h5 className="title w-md-up-75 mb-0">{`${status.ticket.EXTTransactionID}`}</h5>
                  <p className='desc'>{(status.ticket.incidentList && status.ticket.incidentList.incident.length > 0) ? status.ticket.incidentList.incident[0].description : 'Internet has been slow for more than 7 days. Technician will visit to analyze the problem.'}</p>
                </div>
              </Link>
            </div>
            <div className="py-main-sm pt-0 pb-0">
              {
                isFISIK
                ?
                (<div className="form-group">
                <label>Address</label>
                <p>
                  {location ? `${location.street}, ${location.district}, ${location.city}, ${location.province}, ${location.postalCode}` : ''}
                </p>
              </div>)
                :
                (null)
              }
             {
               isRechedulable !== 'SUBMITTED' && isFISIK
               ?
               (<div className="subheading">
               <p>Your Technician</p>
             </div>)
             :
             (null)
             }
             {
               isRechedulable !== 'SUBMITTED' && isFISIK
               ?
               (<div className="list-group">
               <div
                 to="/shop/internet/package-finder"
                 className="list-group-item transparent"
               >
                 <img
                   alt=""
                   src={images.common["sampleImg.jpg"]}
                   className="w-50px rounded-circle"
                 />
                 <div className="list-group-item-content">
                   <h5 className="title w-md-up-75 mb-0">{ticketHasData && Object.keys(status.ticket.technician).length > 0 ? status.ticket.technician.displayname : 'Technician is yet to be assigned'}</h5>
                   <p className="desc">{title_status}</p>
                 </div>
                 <div>
                   {
                     ticketHasData && Object.keys(status.ticket.technician).length > 0 && status.ticket.technician.email === ""
                     ?
                     <i className="fas fa-comment-alt-dots text-primary pr-3"></i>
                     :
                     <a href={`mailto:${ticketHasData && Object.keys(status.ticket.technician).length > 0 && status.ticket.technician.email}`}>
                       <i className="fas fa-comment-alt-dots text-primary pr-3"></i>
                     </a>
                   } 
                   {
                     ticketHasData && Object.keys(status.ticket.technician).length > 0 && status.ticket.technician.phone === ""
                     ?
                     <i className="fas fa-phone text-primary"></i> 
                     :
                     <a href={`tel:${ticketHasData && Object.keys(status.ticket.technician).length > 0 && status.ticket.technician.phone}`}>
                       <i className="fas fa-phone text-primary"></i>
                     </a>
                   }
                 
                 </div>
               </div>
             </div>)
               :
               (null)
             }
             
            
            <div className="py-main-sm pt-0 pb-0">
                <div className="subheading">
                <p>Report Process</p>
                </div>
                <div className="milestone mb-3">
                  { status.steps.map( (step, index) => (
                    <div key={index} className={`milestone-item${step.active ? ' success' : ''}${status.steps.length-1 === index ? ' last-child' : ''}`}>
                      <div className="milestone-item-img">
                        <div className="dot"></div>
                      </div>
                      <div className="milestone-item-content">
                        <h5 className="title">{step.title}</h5>
                        <span className="desc">{step.text}</span>
                      </div>
                    </div> ))
                  }
                </div>
            </div>
            </div>
            {
              isFISIK
              ?
              (
                isRechedulable === 'COMPLETED' || isRechedulable === 'RESOLVED'
                ?
                (
                <div className="text-center">
                  <p className="help-block">Problem has already solved?</p>
                  <div className="btn-placeholder inline bottom mt-0">
                    <Link to={`/installation/fisik-ticket/re-open/${ticketId}`} className="btn btn-transparent btn-block" >No</Link>
                    <Link to="#" onClick={closeTicket} className={`btn btn-primary btn-block ${disabled}`} >{isBtnSubmitting ? <Loading /> : 'Yes'}</Link>
                  </div>
                </div>
                )
                :
                (null)
              )
              :
              (
              <div className="text-center">
                  <p className="help-block">Problem has already solved?</p>
                <div className="btn-placeholder">
                  <Link to="#" onClick={closeTicket} className={`btn btn-primary btn-block ${disabled}`}>{isBtnSubmitting ? <Loading /> : 'Problem Solved'}</Link>
                </div>
              </div>
              )
            }
            
          </section>
        </div>
      </div>
      {/* Modal Set Reschedule */}
      <Modal show={modalReschedule} onHide={() => setModalReschedule(false)} centered={true}>
          <div className="modal-body p-box pb-0">
            <div className="heading pt-sm-down-4">
              <h3>Reschedule Service</h3>
              <p>Select a new time and date. You only have 2 chance to reschedule your service</p>
            </div>
            <section className="mb-4">
              <div className="subheading mb-3">
                <p>Available Dates</p>
              </div>
             {
            dateSlots === undefined
              ? 
              <>
                <Slider {...carousel}>
                  {recommend.map((value, index)=>{
                    let sumDelay = index+1;
                    return (
                      <div className={"carousel-item animated fadeInUp delayp" + sumDelay} key={index}>
                        <section className="card-skeleton">
                          <figure className="card-image loading"></figure>
                        </section>
                      </div>
                    )
                  })}
                </Slider>
                </>
              :
              <Slider {...carouselDate}>
                {carousalCards}
              </Slider>
              }
              <div className="subheading mb-3">
                <p>Estimated Arrival</p>
              </div>
              <div className="list-group">
                <div className="row row-1 mb-0">
                  <div className="col-6">
                    <button
                      className={`btn btn-block list-group-item bg-green-color list-check ${morning_1 ? 'active' : ''} inherit-height${cardMorningAfternoon[0].show === false ? ' disabled': ''}`}
                      disabled={cardMorningAfternoon[0].show === false ? true : false}
                      onClick={() => estimatedArrival("0")}
                    >
                      <div className="list-group-item-content">
                        <h5 className="title text-center">08:00 - 10:00</h5>
                        <div className="custom-control custom-radio d-none">
                          <input
                            type="radio"
                            className="custom-control-input"
                            id="morning"
                            name="radio-stacked"
                            required
                          />
                        </div>
                        {/* <label className="input-check-card">
                                        <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                    </label> */}
                      </div>
                    </button>
                  </div>
                  <div className="col-6">
                    <button
                      className={`btn btn-block list-group-item bg-green-color list-check ${morning_2 ? 'active' : ''} inherit-height${cardMorningAfternoon[1].show === false ? ' disabled': ''}`}
                      disabled={cardMorningAfternoon[1].show === false ? true : false}
                      onClick={() => estimatedArrival("1")}
                    >
                      <div className="list-group-item-content">
                        <h5 className="title text-center">10:00 - 12:00</h5>
                        <div className="custom-control custom-radio d-none">
                          <input
                            type="radio"
                            className="custom-control-input"
                            id="afternoon"
                            name="radio-stacked"
                            required
                          />
                        </div>
                        {/* <label className="input-check-card">
                                        <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                    </label> */}
                      </div>
                    </button>
                  </div>
                  <div className="col-6">
                    <button
                      className={`btn btn-block list-group-item bg-green-color list-check ${evening_1 ? 'active' : ''} inherit-height${cardMorningAfternoon[2].show === false ? ' disabled': ''}`}
                      disabled={cardMorningAfternoon[2].show === false ? true : false}
                      onClick={() => estimatedArrival("2")}
                    >
                      <div className="list-group-item-content">
                        <h5 className="title text-center">13:00 - 15:00</h5>
                        <div className="custom-control custom-radio d-none">
                          <input
                            type="radio"
                            className="custom-control-input"
                            id="afternoon"
                            name="radio-stacked"
                            required
                          />
                        </div>
                        {/* <label className="input-check-card">
                                        <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                    </label> */}
                      </div>
                    </button>
                  </div>
                  <div className="col-6">
                    <button
                      className={`btn btn-block list-group-item bg-green-color list-check ${evening_2 ? 'active' : ''} inherit-height${cardMorningAfternoon[3].show === false ? ' disabled': ''}`}
                      disabled={cardMorningAfternoon[3].show === false ? true : false}
                      onClick={() => estimatedArrival("3")}
                    >
                      <div className="list-group-item-content">
                        <h5 className="title text-center">15:00 - 17:00</h5>
                        <div className="custom-control custom-radio d-none">
                          <input
                            type="radio"
                            className="custom-control-input"
                            id="afternoon"
                            name="radio-stacked"
                            required
                          />
                        </div>
                        {/* <label className="input-check-card">
                                        <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                    </label> */}
                      </div>
                    </button>
                  </div>
                </div>
              </div>
              <p className="help-block">Time range is the Estimated is the estimated arrival time. You will receive a notification when the technician is heading to your location</p>
              <div className="btn-placeholder inline">
                <Link to="#" className="btn btn-transparent btn-block" onClick={() => setModalReschedule(false)}>Cancel</Link>
                <Link to="#" className={`btn btn-primary btn-block ${!selectedCard || isButtonLoading ? 'disabled': ''}`} onClick={(e) => { e.preventDefault(); if(!isButtonLoading) {setModalReschedule(false); setShowConfirmationReschedule(true);} } } >{isButtonLoading ? <Loading images={images}/> : "Reschedule"}</Link>
              </div>
            </section>
            </div>
        </Modal>
         {/* Reschedule confirmation dialog */}
         <Modal show={showConfirmationReschedule} centered={true} onHide={() => {setModalReschedule(true); setShowConfirmationReschedule(false);}} className="modal-centered">
        <div className="box box-main mb-0">
          <section>
            <h1>Confirm Reschedule</h1>
            <p>Are you sure to reschedule the service?</p>
          </section>
          <section>
              <div className="row">
                <div className="col-6">
                    <button className="btn btn-primary btn-block" onClick={ e => reScheduleTechnicianClick(e)}>Yes</button>
                  </div>
                <div className="col-6">
                  <button className="btn btn-transparent btn-block" onClick={e => { e.preventDefault(); setModalReschedule(true); setShowConfirmationReschedule(false);}}>No</button>
                </div>
              </div>
            </section>
        </div>
      </Modal>
      {/* Model for booking exhausted */}
      <Modal show={attemptExhausted} onHide={() => setAttemptExhausted(false)} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                <img className="header-img" src={images.common['grfx_grfx_status_failed.png']} alt="Icon Failed" />
              </header>
              <section>
                <h1>Sorry you can't reschedule anymore</h1>
                <p>You have exceeded reschedule installation limit. Please contact our customer service for further information</p>
              </section>
              <Link to="#" className="btn btn-primary btn-block mb-3" onClick={e => { e.preventDefault(); setAttemptExhausted(false)}}>Close</Link>
                <a href="tel:121" className="btn btn-transparent btn-block" onClick={e => { setAttemptExhausted(false)}}>Contact Us</a>
            </div>
          </div>
        </Modal>
        {/* Model for booking exhausted */}
        <Modal show={feasibleReschedule} onHide={() => setFeasibleReschedule(false)} centered={true}>
        <div className="modal-body p-box-popup">
          <div className="box box-main mb-0">
            <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
              <img className="header-img" src={images.common['grfx_grfx_status_failed.png']} alt="Icon Failed" />
            </header>
            <section>
              <h1>Sorry you can't reschedule</h1>
              <p>Your Ticket ID is not updated yet, please try after sometime</p>
            </section>
            <Link to="#" className="btn btn-primary btn-block mb-3" onClick={e => {e.preventDefault();setFeasibleReschedule(false)}}>Close</Link>
              <a href="tel:121" className="btn btn-transparent btn-block">Contact Us</a>
          </div>
        </div>
      </Modal>
         {/* Modal Session Time Out */}
      <Modal show={modalServiceUnavailable} onHide={() => setModalServiceUnavailable(false)} centered={true}>
        <div className="modal-body p-box-popup">
          <div className="box box-main mb-0">
            <section>
              <h1>Service Error</h1>
              <p>A service error has occured while processing your request</p>
            </section>
            <div className="btn-placeholder inline bottom">
              <Link to="#" className="btn btn-primary btn-block" onClick={e => {e.preventDefault(); setModalServiceUnavailable(false);}}>Ok</Link>
            </div>
          </div>
        </div>
      </Modal>
    </section>
  )
}

export default HistoryTicket1