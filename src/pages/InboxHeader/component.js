import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Slider from 'react-slick'

class InboxHeader extends Component {
  render() {
    const { allPage, personalPage, promoPage } = this.props;
    const config = {
      carouselTab: {
        className: "tab-list",
        dots: false,
        infinite: false,
        arrows: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
          {
            breakpoint: 991,
            settings: {
              dots: false,
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 575,
            settings: {
              dots: false,
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 324,
            settings: {
              dots: false,
              slidesToShow: 3,
              slidesToScroll: 3
            }
          }
        ]
      }
    }
    return (
      <div>
        <div className="cover cover-sm cover-bg-white-sm bg-red-gradient">
          <div className="header-actions d-block d-md-none">
            <Link
              to="/shop/"
              className="btn btn-link"
            >
              <i className="fa fa-arrow-left"></i>
            </Link>
          </div>
          <div className="container">
            <nav aria-label="breadcrumb" className="d-none d-md-block">
              <ol className="breadcrumb animated fadeInUp">
                <li className="breadcrumb-item"><Link to="/"><i className="fa fa-arrow-left mr-1"></i>Home</Link></li>
              </ol>
            </nav>
            <div className="cover-content">
              <h1 className="cover-title animated fadeInUp delayp1">Inbox</h1>
            </div>
          </div>
        </div>
        <section className="shop-tab-list d-md-none animated fadeInUp delayp3">
          <div className="container">
            <Slider {...config.carouselTab}>
              <div className={"tab-item " + (allPage || '')}>
                <Link to="/inbox/all">All</Link>
              </div>
              <div className={"tab-item " + (personalPage || '')}>
                <Link to="/inbox/personal">Personal</Link>
              </div>
              <div className={"tab-item " + (promoPage || '')}>
                <Link to="/inbox/promo">Promo</Link>
              </div>
            </Slider>
          </div>
        </section>
      </div>
    )
  }
}

export default InboxHeader
