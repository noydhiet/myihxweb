import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import numeral from 'numeral';
import { PageContext } from './../../context/PageContext';

const ByBudget1 = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = { brand, common, sample } ;
    const [inputs, setInputs] = useState({});
    const handleInputChange = (e) => {
        e.persist();
        setInputs(inputs => ({...inputs, [e.target.name]:e.target.value}));
    }
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
            <div className="container container-sm">
                <div className="box box-main animated fadeInUp">
                    <header className="header-light white w-icon main-icon-md animated fadeInUp delayp1">
                        <div className="header-actions">
                            <Link to="/shop/internet/package-finder" className="btn btn-link"><i className="fa fa-arrow-left"></i></Link>
                        </div>
                        <img className="header-img" src={images.common["graphic_find.png"]} alt="Icon Success" />
                    </header>
                    <section className="animated fadeInUp delayp2">
                        <div className="heading">
                            <h1>My maximum budget is</h1>
                        </div>
                        <form>
                            <div className="slider-budget">
                                <div className="desc-slider" id="dont">
                                    <span>Rp</span>
                                    <h1 id="budget">{numeral(inputs.price).format('0,0')}</h1>
                                    <span>/month</span>
                                </div>
                                <div className="slider-label" style={{display: "flex", justifyContent: "space-between"}}>
                                    <label htmlFor="min" id="minBudget">0</label>
                                    <label htmlFor="min" id="maxBudget">2.000.000</label>
                                </div>
                                    <input id="typeinp" type="range" min="" max="2000000" step="50000" defaultValue="0" name="price" onChange={handleInputChange} value={inputs.price}/>
                                <div className="mt-3">
                                    <span className="help-block text-center">Slide to select budget.</span>
                                </div>
                            </div>
                        </form>
                    </section>
                    <div className="btn-placeholder animated fadeInUp delayp3">
                        <Link to="/shop/internet/package-finder/budget/finish" className="btn btn-primary btn-block">Next</Link>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default ByBudget1
