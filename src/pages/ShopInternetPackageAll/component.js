import React, { Component } from "react";
import { Link } from "react-router-dom";
import { PageContext } from './../../context/PageContext';
class AllProductsSection extends Component {
  static contextType = PageContext;
  state = {
    modal: false,
    open: false,
    innerHeight: ""
  }
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("w-bg");
  }
  triggerPages = (mode) => {
    switch (mode) {
      case "modal":
        this.setState({
          modal: !this.state.modal
        });
        break;

      case "open":
        this.setState({
          open: !this.state.open,
          innerHeight: this.refs.inner.clientHeight
        });
        break;

      default:
        return null
    }
  }
  render() {
    const { common, brand, sample } = this.context;
    const images = { common, brand, sample };
    const allPackages = ["banner_all_packages_1.png", "banner_all_packages_2.png", "banner_all_packages_3.png", "banner_all_packages_1.png", "banner_all_packages_2.png", "banner_all_packages_3.png"];
    return (
      <section>
        <div className="container">
          <div className="box box-invisible animated fadeInUp">
            <header>
              <div className="header-actions d-block d-md-none">
                <Link to="/shop/internet/package" className="btn btn-link">
                  <i className="fa fa-arrow-left"></i>
                </Link>
              </div>
              <nav aria-label="breadcrumb" className="d-none d-md-block">
                <ol className="breadcrumb animated fadeInUp delayp1">
                  <li className="breadcrumb-item">
                    <Link to="/shop/internet/package">
                      <i className="fa fa-arrow-left mr-1"></i> Paket Internet
                  </Link>
                  </li>
                </ol>
              </nav>
              <div className="heading">
                <h1 className="mb-0 animated fadeInUp delayp2">
                  <span>All Packages</span>
                </h1>
                <p className=" animated fadeInUp delayp3">Browse all packages to find the perfect fit for you</p>
              </div>
            </header>
            <section className="section-header-card py-main">
              <div className="row">
                {allPackages.map((value,index)=>{
                  let sumDelay = index+1;
                  return (
                    <div className={"col-md-4 col-12 mb-3 animated fadeInUp delayp" + sumDelay} key={index}>
                      <Link to="/shop/internet/package/category">
                        <img src={images.common[`${value}`]} className="img-fluid" alt="banner"/>
                      </Link>
                    </div>
                  )
                })}
              </div>
            </section>
          </div>
        </div>
        {/* Modal - Unlimited Internet Quota */}
        {/* <Modal show={modal} onHide={() => this.triggerPages("modal")}>
          <div className="modal-body p-box">
            <Link to="#" className="close animated fadeInUp" onClick={() => this.triggerPages("modal")}>
              <span><i className="far fa-times text-primary"></i></span>
            </Link>
            <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
              <h3>Filter</h3>
            </div>

            <div className="list-group">
              <div className="list-group-item w-checkbox list-check disabled animated fadeInUp delayp2 pr-3">
                <img src={images.common["ic_selection_wifi.png"]} className="img-fluid w-50px" alt="Phone Icon" />
                <div className="list-group-item-content">
                  <h5 className="title">Internet</h5>
                  <span className="desc">You cannot deselect this item. Internet is a basic item in all packages.</span>
                </div>
                <div className="form-group checkbox">
                  <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" checked required />
                    <label className="custom-control-label"></label>
                  </div>
                </div>
              </div>

              <div className="list-group-item animated fadeInUp delayp2 pr-2">
                <div className="list-group-header w-100">
                  <div>
                    <div className="d-flex align-items-center">
                      <img src={images.common["ic_selection_tv.png"]} className="img-fluid w-50px" alt="Phone Icon" />
                      <div className="list-group-item-content">
                        <h5 className="title">TV/USee TV</h5>
                        <span className="desc" onClick={() => this.select(0)}>Want to filter preferred channel categories?</span>
                        <Link to="#" onClick={() => this.triggerPages("open")}>Show Channel Categories <i className="fas fa-angle-down"></i></Link>
                      </div>
                      <div className="form-group checkbox mb-0">
                        <div className="custom-control custom-checkbox">
                          <input type="checkbox" className="custom-control-input check" checked required />
                          <label className="custom-control-label"></label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="list-group-box" style={config.styleBox}>
                    <div ref="inner">
                      <div className="d-flex align-items-center" style={{ justifyContent: "space-between" }} onClick={() => this.selectInBox(0)}>
                        <div className="d-flex align-items-center">
                          <img src={images.common["ic_selection_tv.png"]} className="img-fluid w-50px" alt="Phone Icon" />
                          <div style={{ marginRight: "1rem" }}>
                            <h5 className="title mb-0">Movies</h5>
                          </div>
                        </div>
                        <div className="form-group checkbox d-flex align-items-center mb-0">
                          <div className="custom-control- custom-checkbox">
                            <input type="checkbox" className="custom-control-input check-in-box" checked required />
                            <label className="custom-control-label"></label>
                          </div>
                        </div>
                      </div>
                      <div className="d-flex align-items-center" style={{ justifyContent: "space-between" }} onClick={() => this.selectInBox(1)}>
                        <div className="d-flex align-items-center">
                          <img src={images.common["ic_selection_tv.png"]} className="img-fluid w-50px" alt="Phone Icon" />
                          <div style={{ marginRight: "1rem" }}>
                            <h5 className="title mb-0">TV Series</h5>
                          </div>
                        </div>
                        <div className="form-group checkbox d-flex align-items-center mb-0">
                          <div className="custom-control- custom-checkbox">
                            <input type="checkbox" className="custom-control-input check-in-box" checked required />
                            <label className="custom-control-label"></label>
                          </div>
                        </div>
                      </div>
                      <div className="d-flex align-items-center" style={{ justifyContent: "space-between" }} onClick={() => this.selectInBox(2)}>
                        <div className="d-flex align-items-center">
                          <img src={images.common["ic_selection_tv.png"]} className="img-fluid w-50px" alt="Phone Icon" />
                          <div style={{ marginRight: "1rem" }}>
                            <h5 className="title mb-0">Asian Programs</h5>
                          </div>
                        </div>
                        <div className="form-group checkbox d-flex align-items-center mb-0">
                          <div className="custom-control- custom-checkbox">
                            <input type="checkbox" className="custom-control-input check-in-box" checked required />
                            <label className="custom-control-label"></label>
                          </div>
                        </div>
                      </div>
                      <div className="d-flex align-items-center" style={{ justifyContent: "space-between" }} onClick={() => this.selectInBox(3)}>
                        <div className="d-flex align-items-center">
                          <img src={images.common["ic_selection_tv.png"]} className="img-fluid w-50px" alt="Phone Icon" />
                          <div style={{ marginRight: "1rem" }}>
                            <h5 className="title mb-0">Sport</h5>
                          </div>
                        </div>
                        <div className="form-group checkbox d-flex align-items-center mb-0">
                          <div className="custom-control- custom-checkbox">
                            <input type="checkbox" className="custom-control-input check-in-box" checked required />
                            <label className="custom-control-label"></label>
                          </div>
                        </div>
                      </div>
                      <div className="d-flex align-items-center" style={{ justifyContent: "space-between" }} onClick={() => this.selectInBox(4)}>
                        <div className="d-flex align-items-center">
                          <img src={images.common["ic_selection_tv.png"]} className="img-fluid w-50px" alt="Phone Icon" />
                          <div style={{ marginRight: "1rem" }}>
                            <h5 className="title mb-0">Entertaiment</h5>
                          </div>
                        </div>
                        <div className="form-group checkbox d-flex align-items-center mb-0">
                          <div className="custom-control- custom-checkbox">
                            <input type="checkbox" className="custom-control-input check-in-box" checked required />
                            <label className="custom-control-label"></label>
                          </div>
                        </div>
                      </div>
                      <div className="d-flex align-items-center" style={{ justifyContent: "space-between" }} onClick={() => this.selectInBox(5)}>
                        <div className="d-flex align-items-center">
                          <img src={images.common["ic_selection_tv.png"]} className="img-fluid w-50px" alt="Phone Icon" />
                          <div style={{ marginRight: "1rem" }}>
                            <h5 className="title mb-0">Lifestyle</h5>
                          </div>
                        </div>
                        <div className="form-group checkbox d-flex align-items-center mb-0">
                          <div className="custom-control- custom-checkbox">
                            <input type="checkbox" className="custom-control-input check-in-box" checked required />
                            <label className="custom-control-label"></label>
                          </div>
                        </div>
                      </div>
                      <div className="d-flex align-items-center" style={{ justifyContent: "space-between" }} onClick={() => this.selectInBox(6)}>
                        <div className="d-flex align-items-center">
                          <img src={images.common["ic_selection_tv.png"]} className="img-fluid w-50px" alt="Phone Icon" />
                          <div style={{ marginRight: "1rem" }}>
                            <h5 className="title mb-0">Kids</h5>
                          </div>
                        </div>
                        <div className="form-group checkbox d-flex align-items-center mb-0">
                          <div className="custom-control- custom-checkbox">
                            <input type="checkbox" className="custom-control-input check-in-box" checked required />
                            <label className="custom-control-label"></label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="list-group-item w-checkbox list-check animated fadeInUp delayp2 pr-3" onClick={() => this.select(1)}>
                <img src={images.common["ic_selection_call.png"]} className="img-fluid w-50px" alt="Phone Icon" />
                <div className="list-group-item-content">
                  <h5 className="title">Internet</h5>
                </div>
                <div className="form-group checkbox">
                  <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input check" required />
                    <label className="custom-control-label"></label>
                  </div>
                </div>
              </div>

            </div>

            <div className="btn-placholder bottom animated fadeInUp delayp5">
              <button className="btn btn-primary btn-block">Apply</button>
            </div>
          </div>
        </Modal> */}
      </section>
    )
  }
}

export default AllProductsSection;
