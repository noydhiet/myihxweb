import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { Form } from 'react-bootstrap';
import { PageContext } from './../../context/PageContext';

const ShopInternetPackageWifiIdSeamlessConnect = () => {
  const { trigger, navCase, pageCase, closeNotif } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  const [cond, setCond] = useState({
    typePassword: true,
    typePasswordOnModal: true,
    password: "12345asdqwe",
    modalChangePassword: false,
    notif: false
  })
  const triggerPage = (mode) => {
    switch (mode) {
      case "valuePassword":
        setCond({
          typePassword: !cond.typePassword
        });
        break;
      case "changePassword":
        alert("test");
        setCond({
          modalChangePassword: true
        });
        break;
      case "changeValuePasswordOnModal":
        setCond({
          typePasswordOnModal: !cond.typePasswordOnModal
        });
        break;
      case "saveUpdate":
        setCond({
          modalChangePassword: false,
          notif: true
        });
        break;
      case "close":
        setCond({
          modalChangePassword: false
        });
        break;
      default:
        return null
    }
  }
    if(cond.notif) {
        closeNotif();
    }
    const viewPassword = cond.typePassword ? "password" : "text";
    // const typePasswordOnModal = cond.typePasswordOnModal ? "text" : "password";
    // const viewIconPassword = cond.viewPasswordUpdate ? "fas fa-eye" : "fas fa-eye-slash";
    return (
        <section>
            <div className="container container-sm">
                <div className="box animated fadeInUp">
                    <header>
                        <div className="header-actions">
                            <Link to="/shop/internet/package" className="btn btn-link">
                            <i className="fa fa-arrow-left"></i>
                            </Link>
                        </div>
                    </header>
                    <section className="section-header-text">
                        <div className="heading">
                            <h1>Connect to wifi.id</h1>
                            <p>Connect to wifi.id hotspot and login with your username and ID</p>
                        </div>
                    </section>
                    <section className="animated fadeInUp delayp2">
                        <div className="box box-sm box-progress">
                            <Form>
                                <div className="form-group">
                                    <label>Username</label>
                                    <p>johndoe1234567890</p>
                                </div>
                                <div className="form-group form-custom w-preppend mb-0">
                                    <label>Password</label>
                                    <Form.Control
                                    className="form-no-border pl-0"
                                    type={viewPassword}
                                    value={cond.password}
                                    disabled
                                    />
                                    <span className="preppend-icon"  onClick={()=>triggerPage("changePassword")}><i className="fas fa-pen text-primary"></i> </span>
                                </div>
                                <small className="text-primary font-weight-bold" onClick={()=>triggerPage("valuePassword")}>Show Password</small>
                            </Form>
                        </div>
                    </section>
                    <div className="btn-placeholder text-center bottom">
                        <p className="help-block mb-0">Having issues connecting?</p>
                        <Link to="/help" className="btn btn-link">Get Help</Link>
                    </div>
                </div>
              </div>
            </section>
    );
}

export default ShopInternetPackageWifiIdSeamlessConnect;
