import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';
import { getToken } from '../../utils/storage';

const PurchasePaymentWithLinkAjaPreview = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = { brand, common, sample } ;
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase])
    return (
        <section>
            <div className="container container-sm">
                <div className="box box-main animated fadeInUp">
                    <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                    <img className="header-img" src={images.common['grfx_grfx_linkaja.png']} alt="Icon Success" />
                    </header>
                    <section className="animated fadeInUp delayp2">
                        <h1>Sambungkan akun dengan LinkAja</h1>
                        <p>Sambungkan akun dengan LinkAja untuk melakukan pembayaran dengan cepat dan aman.</p>
                    </section>
                    <div className="btn-placeholder bottom animated fadeInUp delayp3">
                        <Link to="/purchase/connect-link-aja" className="btn btn-primary btn-block">Lanjut</Link>
                    </div>
                </div>
            </div>
       </section>
  )
}

export default PurchasePaymentWithLinkAjaPreview;