import React, { useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { PageContext } from './../../context/PageContext';
import { setInstallationFlag } from '../../utils/storage'
import moment from 'moment';

const InstallationScheduleSuccess = (props) => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    const images = { brand, common, sample } ;
    setInstallationFlag(false)
    const { match: { params } } = props;
    const elements = params.pathSuffix.split('--', 2);
    const selectedDate = moment(elements[0].replace(':',''), 'DD-MM-YYYY').format('dddd, DD MMMM YYYY');
    const time = elements[1];
    return (
      <section>
          <div className="container container-sm">
              <div className="box box-main animated fadeInUp">
                  <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                  <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
                  </header>
                  <section className="animated fadeInUp delayp2">
                      <h1>All set! Your Installation is scheduled</h1>
                      <p>Your IndiHome will be installed on</p>
                      <div className="text-box">
                            <h5 className="title">{selectedDate}</h5>
                            <p className="desc">Estimated arrival time {time}</p>
                      </div>
                      <p className="help-block">Adding appointment to calendar will not automatically update it if any changes appear</p>
                  </section>
                  <div className="btn-placeholder bottom animated fadeInUp delayp3">
                      <Link to="/home" className="btn btn-primary btn-block">Back To Home</Link>
                  </div>
              </div>
          </div>
      </section>
  )
}

export default InstallationScheduleSuccess