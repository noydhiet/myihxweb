import React from 'react';
import { Link } from 'react-router-dom';
import { Modal, Form, Card, Button } from 'react-bootstrap';
import ContainerBase from '../../include/ContainerBase';
import { checkUserExist } from '../../services';
import { NUMBER_BLOCKED, OTP_BLOCKED_TIME } from '../../utils/storage';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';

export default class RegisterAccountDetail extends React.Component {
  static contextType = PageContext;
  state = {
    isSubmitting: false,
    icon: false,
    fullName: '',
    mobileNumber: '',
    email: '',
    password: '',
    refCode: '',
    message: {
      fullName: '',
      mobileNumber: '',
      email: '',
      password: '',
      modal: '',
    },
  }

  componentDidMount() {
    this.context.trigger(1)
    this.context.pageCase("w-bg");
  }

  trigger = (mode) => {
    const { history } = this.props;
    const { email, fullName, mobileNumber, password } = this.state;
    const data = {
      email: email,
      name: fullName,
      password: password,
      mobile: mobileNumber.replace(/^\+*62/, '0'),
    };

    switch (mode) {
      case 'modal':
        this.setState({ message: { modal: '' } });
        break;

      case 'submit':
        this.setState({ isSubmitting: true });
        Promise.all([
          checkUserExist('email', data.email),
          checkUserExist('mobile', data.mobile),
        ])
          .then(([resEmail, resMobile]) => {
            if (resMobile.data.ok) this.setState({ message: { modal: 'Mobile Number already registered' } });
            else if (resEmail.data.ok) this.setState({ message: { modal: 'Email already registered' } });
            else {
              localStorage.removeItem(NUMBER_BLOCKED);
              localStorage.removeItem(OTP_BLOCKED_TIME);
              localStorage.setItem('registrationData', JSON.stringify(data));
              history.push('/register/activation');
            }
            this.setState({ isSubmitting: false });
          })
          .catch(({ message }) => { this.setState({ isSubmitting: false, message: { modal: message } }); })
        break;

      default:
        break;
    }
  }

  handleBlur = ({ target }) => {
    const { id, value } = target;
    const { message } = this.state;

    this.setState({ message: { ...message, [id]: this.setMessage(id, value) } })
  }

  handleChange = ({ target }) => {
    const { id, value } = target;
    const { message } = this.state;

    this.setState({
      [id]: value,
      message: { ...message, [id]: this.setMessage(id, value) },
    });
  }

  setMessage(id, value) {
    const reMail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    switch (id) {
      case 'fullName':
        if (!value.length) return 'errFullName';
        if (value.length > 30) return 'errMax30';
        if (/[^-'A-Za-z\s]/.test(value)) return 'errValidFullName';
        return '';
      case 'mobileNumber':
        const mobileCheck = v => {
          return /^((?:\+62|62)|0)[8]{1}[0-9]{6,12}$/.test(v)
          // let mobileNumber;
          // if(v.startsWith('08')) {
          //   mobileNumber = v.substring(1);
          // } else if(v.startsWith('628')) {
          //   mobileNumber = v.substring(2);
          // } else if(v.startsWith('+628')) {
          //   mobileNumber = v.substring(3);
          // } else {
          //   return false;
          // }
          // return !(/\s/g.test(mobileNumber) || !/^[0-9]+$/i.test(mobileNumber)) && ((mobileNumber.length < 14 && mobileNumber.length > 6 ))
        };
        return mobileCheck(value) ? '' : 'errValidPhone';
      case 'email':
        return !reMail.test(value) ? 'errValidEmail' : '';
      case 'password':
        const passwordCheck = v => !(/[A-Za-z]+/.test(v) && /[0-9]+/.test(v) && v.length >= 6);
        if (passwordCheck(value)) return 'errMixChar';
        return '';
      default:
        return '';
    }
  }

  _setModalMessage() {
    const { message } = this.state;

    if (message.modal === 'Mobile Number already registered') {
      return {
        title: 'Number is already registered',
        subtitle: 'Looks like this number is already registered! Do you want to login instead?',
      }
    }

    if (message.modal === 'Email already registered') {
      return {
        title: 'Email is already registered',
        subtitle: 'Looks like this email is already registered! Do you want to login instead?',
      }
    }

    return {
      title: 'Registration Error',
      subtitle: message.modal,
    }
  }

  _renderInput(label, id, placeholder, textButtom) {
    const { icon } = this.state;
    const value = this.state[id];
    const message = this.state.message[id];

    return (
      <TextContext.Consumer>{(context) => {
        const { registerText: { accDetails } } = context;
        const { alertFullName, alertMax30, alertValidFullName, alertValidEmail, alertValidPhone, alertMixChar } = accDetails;
        const alertMsg = (message => {
          switch (message) {
            case "errFullName":
              return alertFullName;
            case "errMax30":
              return alertMax30;
            case "errValidFullName":
              return alertValidFullName;
            case "errValidEmail":
              return alertValidEmail;
            case "errValidPhone":
              return alertValidPhone;
            case "errMixChar":
              return alertMixChar;
            default:
              break;
          }
        })(message);
        const type = (id => {
          if (id === 'email') return 'email';
          if (id === 'password' && !icon) return 'password';
          return 'text';
        })(id);
        return (
          <Form.Group className="form-custom w-preppend">
            <Form.Label>{label}</Form.Label>
            <Form.Control
              className={message ? 'is-invalid' : ''}
              id={id}
              onChange={this.handleChange}
              onBlur={this.handleBlur}
              placeholder={placeholder}
              type={type}
              value={value}
            />
            {id === 'password' && <span onClick={()=> this.setState({ icon: !icon })} className="preppend-icon" style={{ bottom: alertMsg ? '45%' : '33%' }}><i className={'fas fa-eye' + (icon ? '' : '-slash')}></i> </span>}
            <div className="invalid-feedback">{alertMsg}</div>
            <small className="help-block">{textButtom}</small>
          </Form.Group>
        )
      }}</TextContext.Consumer>
    );
  }

  _renderModal() {
    const { message } = this.state;
    const config = this._setModalMessage();

    return (
      <TextContext.Consumer>{(context) => {
        const { registerText: { accDetails } } = context;
        const { btnModalTryAgain, btnModalLogin, btnModalGetHelp } = accDetails;
        return (
          <Modal show={Boolean(message.modal)} centered={true} onHide={() => this.trigger('modal')} className="modal-centered">
            <div className="box box-main mb-0">
              <section>
                <h1>{config.title}</h1>
                {config.subtitle && (<p>{config.subtitle}</p>)}
              </section>
              <div className="btn-placeholder inline">
                <button className="btn btn-block w-border" onClick={() => this.trigger('modal')}> {btnModalTryAgain} </button>
                <Link to="/login:" className="btn btn-primary btn-block"> {btnModalLogin} </Link>
              </div>
              <div className="btn-placholder">
                <Link to="/help" className="btn btn-block"> {btnModalGetHelp} </Link>
              </div>
            </div>
          </Modal>
        )
      }}</TextContext.Consumer>
    );
  }

  render() {
    const { fullName, mobileNumber, email, password, refCode, message, isSubmitting } = this.state;
    const { common, brand, sample } = this.context;
    const images = { common, brand, sample };
    const config = {
      disabled: isSubmitting || !fullName.length ||
        !mobileNumber.length ||
        !email.length ||
        !password.length ||
        Object.values(message).filter(Boolean).length > 0
        ?
        'disabled' : ''
    };
    const Loading = () => {
      return (
        <span><i className="fa fa-circle-notch fa-spin"></i></span>
      )
    }
    return (
      <TextContext.Consumer>{(context) => {
        const { registerText: { accDetails } } = context;
        const { title, labelName, phName, labelMobile, phMobile, labelEmail, phEmail, labelPassword, phPassword, alertPassword, cardLabel, cardInputPh, btnRegister } = accDetails;
        const containerProps = {
          animateSection: true,
          image: { alt: 'Icon Success', src: images.common['ic_header_register.png'], },
          title,
        };

        return (
          <>
            <ContainerBase {...containerProps}>
              <Form className="needs-validation">
                {this._renderInput(labelName, 'fullName', phName)}
                {this._renderInput(labelMobile, 'mobileNumber', phMobile)}
                {this._renderInput(labelEmail, 'email', phEmail)}
                {this._renderInput(labelPassword, 'password', phPassword, alertPassword)}
                {/* <div className="form-group form-group-box mt-2">
                  <label className="mb-0" for="text" data-toggle="collapse" data-target="#collapseReferral" aria-expanded="false" aria-controls="collapseReferral">Referral Code</label>
                  <div className="collapse" id="collapseReferral">
                    <input type="text" className="form-control mt-2" id="password" placeholder="Enter referral code (optional)" />
                    <small className="help-block">Optionally enter referral code to reward your friend / agent</small>
                  </div>
                </div> */}
                <Form.Group>
                  <Card className="card-referral-code p-3">
                    <b className="mb-3">{cardLabel}</b>
                    <Form.Control
                      type="text"
                      placeholder={cardInputPh}
                      id="refCode"
                      value={refCode}
                      onChange={this.handleChange}
                    />
                    <div className="invalid-feedback">Sorry, referral code is not found</div>
                  </Card>
                </Form.Group>
                <div className="btn-placeholder">
              <Button className={"btn-primary btn-block " + config.disabled} disabled={config.disabled} onClick={e => {e.preventDefault(); this.trigger('submit');}}> {isSubmitting ? <Loading /> : <span> {btnRegister}</span>}  </Button>
                </div>
              </Form>
            </ContainerBase>
            {this._renderModal()}
          </>
        )
      }}</TextContext.Consumer>
    )
  }
}
