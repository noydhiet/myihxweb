import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';
import { CardContent } from './../../include/CardPackage'
import Slider from "react-slick";
import { getToken } from '../../utils/storage';

class ShopTvMinipackConfirm extends Component {
	static contextType = PageContext;
	constructor(props){
		super(props);
		if (!getToken()) {
			window.location.href='/login';
			return	
		}
	}
	componentDidMount(){
		this.context.trigger(2);
		this.context.navCase('w-md-up-block');
		this.context.pageCase('w-bg');
	}
	render(){
			const { brand, common, sample } = this.context;
			const images = { brand, common, sample } ;
			const config = {
					carousel4th: {
						className: "primary-carousel overflow-inherit",
						dots: false,
						infinite: false,
						arrows: false,
						speed: 500,
						slidesToShow: 6,
						slidesToScroll: 6,
						responsive: [
							{
								breakpoint: 1024,
								settings: {
									slidesToShow: 6,
									slidesToScroll: 6
								}
							},
							{
								breakpoint: 600,
								settings: {
									slidesToShow: 4,
									slidesToScroll: 4
								}
							},
							{
								breakpoint: 480,
								settings: {
									slidesToShow: 4,
									slidesToScroll: 4
								}
							}
						]
					}
				};
			const listChannel = [
					{
						link: "#",
						image: { alt: 'Channel Img', src: "channel1.png", },
					},
					{
						link: "#",
						image: { alt: 'Channel Img', src: "channel2.png", },
					},
					{
						link: "#",
						image: { alt: 'Channel Img', src: "channel3.png", },
					},
					{
						link: "#",
						image: { alt: 'Channel Img', src: "channel4.png", },
					},
					{
						link: "#",
						image: { alt: 'Channel Img', src: "channel5.png", },
					},
					{
						link: "#",
						image: { alt: 'Channel Img', src: "channel6.png", },
					},
					{
							link: "#",
							image: { alt: 'Channel Img', src: "channel1.png", },
					},
					{
							link: "#",
							image: { alt: 'Channel Img', src: "channel2.png", },
					},
					{
							link: "#",
							image: { alt: 'Channel Img', src: "channel3.png", },
					}
			]
			return (
					<section>
							<div className="container container-sm">
									<div className="box animated fadeInUp">
											<header>
											<div className="header-actions">
													<Link to="/shop/tv/minipack/details" className="btn btn-link">
													<i className="fa fa-arrow-left"></i>
													</Link>
											</div>
											</header>
											<section className="section-header-text">
													<div className="heading">
															<h1>Confirm order</h1>
															<p>Confirm your selection and order</p>
													</div>
											</section>
											<section className="py-main-sm pb-0">
													<div className="subheading">
															<p>Product</p>
													</div>
													<Link
															to="#"
															className="card card-packages w-arrow w-list-channel animated fadeInUp delayp2"
															>
															<div className="card-header">
																	<div className="badges-list">
																			<div className="badges badge-blue left">
																					<h3>8</h3>
																					<p>Channels</p>
																			</div>
																	</div>
																	<div className="header-text">
																			<h3>Dinasty 1</h3>
																			<p>Chinese channels including Kaku TV, Panda TV, ICN TV, Kungfu TV and more!</p>
																	</div>
															</div>
															<div className="card-body w-btn">
																	<div className="list-channel">
																			<Slider {...config.carousel4th}>
																			{listChannel.map(function(value, index){
																					return (
																					<div className="carousel-item" key={index}>
																							<CardContent
																							link={value.link}
																							img={value.image.src}
																							className="subheading"
																							images={images}
																							/>
																					</div>
																					)
																			})}
																			</Slider>
																	</div>
																	<h3>
																			Rp169.000<small>/month</small>
																	</h3>
															</div>
													</Link>
											</section>
											<section className="py-main-sm pb-0">
													<div className="form-group">
															<label>Total</label>
															<h4>Rp169.000</h4>
															<small className="help-block">Amount will be charged to your next bill. Includes 15% tax</small>
													</div>
													<div className="btn-placeholder bottom">
															<div className="row">
															<div className="col-6">
																	<Link to="/shop/tv/minipack" className="btn btn-transparent btn-block">Cancel</Link>
															</div>
																	<div className="col-6">
																			<Link to="/shop/tv/minipack/success" className="btn btn-primary btn-block"> <span>Confirm</span> </Link>
																	</div>
															</div>
													</div>
											</section>
									</div>
							</div>
					</section>
			)
	}
}

export default ShopTvMinipackConfirm;
