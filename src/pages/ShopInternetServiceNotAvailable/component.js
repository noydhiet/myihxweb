import React, { useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { PageContext } from '../../context/PageContext';

const ServiceNotAvailable = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = { brand, common, sample } ;
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
            <div className="container container-sm">
                <div className="box box-main animated fadeInUp">
                    <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                    <img className="header-img" src={images.common['grfx_grfx_sad.png']} alt="Icon Success" />
                    </header>
                    <section className="animated fadeInUp delayp2">
                        <h1>Sorry, this service is not yet available in your area</h1>
                        <p>Tap the button below to get notified when it's available in your area!</p>
                    </section>
                    <div className="btn-placeholder bottom animated fadeInUp delayp3">
                        <Link to="/home" className="btn btn-primary btn-block">Notify Me</Link>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default ServiceNotAvailable