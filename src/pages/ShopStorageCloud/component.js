import React, { Component } from "react";
import { Link } from 'react-router-dom';
import Slider from "react-slick";
import ShopStorage from './../ShopStorage';
import { PageContext } from './../../context/PageContext';

class ShopStorageCloud extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    return (
      <div>
        <ShopStorage cloudPage="active" shopPrice="Rp100.000" shopPriceFixed="Rp100.000" btnText="Subscribe" linkTo="/shop/storage/cloud/confirm" />
        <SectionWithBanner images={images} />
        <SectionFeature images={images} />
        <SectionPackages images={images} />
      </div>
    )
  }
}
export default ShopStorageCloud;

const SectionWithBanner = ({ images }) => {
  return (
    <div className="py-main bg-cover-section indihome-cloud content-center">
      <div className="container">
        <div className="row row-4">
          <div className="col-md-6 col-8 content-center-left">
            <div className="heading mb-2">
              <h3 className="animated fadeInUp delayp5">Upload,Access,Share!</h3>
              <p className="mb-0 animated fadeInUp delayp6">Keep your data online in IndiHome Cloud and access it from your devices</p>
            </div>
            <div className="btn-placeholder mt-2 animated fadeInUp delayp7">
              <div className="row d-none d-md-block">
                <div className="col-lg-5">
                  <Link
                    to="/shop/more/cloud-storage"
                    className="btn btn-primary"
                  >
                    More Details
                                </Link>
                </div>
              </div>
              <Link
                to="/shop/more/cloud-storage"
                className="btn btn-link light d-md-none"
              >
                More Details
                            </Link>
            </div>
          </div>
          <div className="col-6 d-none d-md-block">
            <div className="content-center" style={{ height: "100%" }}>
              <img src={images.common["banner_bnr_more_cloud.png"]} className="img-fluid rounded animated fadeInUp delayp7" alt="Indonesia map" />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const SectionFeature = ({ images }) => {
  return (
    <div className="py-main bg-gray-50 bg-md-up-white shop-internet-features">
      <div className="container">
        <div className="row row-1 row-md-up-2">
          <div className="col-4">
            <div className="card card-feature gray animated fadeInUp delayp5">
              <h5>Safe Digital Storage <i className="fal fa-angle-right"></i> </h5>
            </div>
          </div>
          <div className="col-4">
            <div className="card card-feature gray animated fadeInUp delayp6">
              <h5>Easy Device Access <i className="fal fa-angle-right"></i> </h5>
            </div>
          </div>
          <div className="col-4">
            <div className="card card-feature gray animated fadeInUp delayp7">
              <h5>Simple Sharing <i className="fal fa-angle-right"></i></h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

class SectionPackages extends Component {
  render() {
    const carousel = {
      className: "primary-carousel overflow-inherit mb-3",
      dots: false,
      infinite: false,
      arrows: false,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2.2,
            slidesToScroll: 2.2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1.5,
            slidesToScroll: 1.5
          }
        }
      ]
    };
    return (
      <div>
        <div className="py-main bg-gray-50">
          <div className="container">
            <Slider {...carousel}>
              <div className="carousel-item">
                <Link
                  to="/shop/more/cloud-storage/confirm"
                  className="card card-packages card-packages-sm animated fadeInUp delayp3"
                >
                  <div className="card-header">
                    <div className="badges-list">
                      <div className="badges badge-gray">
                        <h3>1</h3>
                        <span>GB</span>
                      </div>
                    </div>
                  </div>
                  <div className="card-body">
                    <h3>
                      Rp5.000<small> /month</small>
                    </h3>
                    <span className="btn btn-primary">Details</span>
                  </div>
                </Link>
              </div>
              <div className="carousel-item">
                <Link
                  to="/shop/more/cloud-storage/confirm"
                  className="card card-packages card-packages-sm animated fadeInUp delayp4"
                >
                  <div className="card-header">
                    <div className="badges-list">
                      <div className="badges badge-gray">
                        <h3>5</h3>
                        <span>GB</span>
                      </div>
                    </div>
                  </div>
                  <div className="card-body">
                    <h3>
                      Rp12.500<small> /month</small>
                    </h3>
                    <span className="btn btn-primary">Details</span>
                  </div>
                </Link>
              </div>
              <div className="carousel-item">
                <Link
                  to="/shop/more/cloud-storage/confirm"
                  className="card card-packages card-packages-sm animated fadeInUp delayp4"
                >
                  <div className="card-header">
                    <div className="badges-list">
                      <div className="badges badge-gray">
                        <h3>10</h3>
                        <span>GB</span>
                      </div>
                    </div>
                  </div>
                  <div className="card-body">
                    <h3>
                      Rp20.000<small> /month</small>
                    </h3>
                    <span className="btn btn-primary">Details</span>
                  </div>
                </Link>
              </div>
              <div className="carousel-item">
                <Link
                  to="/shop/more/cloud-storage/confirm"
                  className="card card-packages card-packages-sm animated fadeInUp delayp4"
                >
                  <div className="card-header">
                    <div className="badges-list">
                      <div className="badges badge-gray">
                        <h3>50</h3>
                        <span>GB</span>
                      </div>
                    </div>
                  </div>
                  <div className="card-body">
                    <h3>
                      Rp85.000<small> /month</small>
                    </h3>
                    <span className="btn btn-primary">Details</span>
                  </div>
                </Link>
              </div>
            </Slider>
          </div>
        </div>
      </div>
    );
  }
}