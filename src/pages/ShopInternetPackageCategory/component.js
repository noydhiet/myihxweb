import React, { Component, useState } from "react";
import { Link } from "react-router-dom";
import { Modal } from 'react-bootstrap';
import { PageContext } from './../../context/PageContext';
import TrackVisibility from 'react-on-screen';

class ShopInternetPackageCategory extends Component {
    static contextType = PageContext;
    componentDidMount() {
        this.context.trigger(2);
        this.context.navCase("w-md-up-block");
        this.context.pageCase("l-bg");
    }
    render() { 
        const { common, brand, sample } = this.context;
        const images = { common, brand, sample };
        return ( 
            <>
                <div className="cover cover-responsive cover-packages bg-wave red">
                    <div className="cover-responsive-img">
                    {/* <div className="cover-responsive-overlay"></div> */}
                    </div>
                    <div className="container">
                        <div className="cover-content">
                            <h3 className="cover-title animated fadeInUp delayp1">
                            Semarak Kebahagiaan
                            </h3>
                            <p className="cover-text animated fadeInUp delayp2">
                            Raih semaraknya. aktifkan paketnya! Raih Rp5 miliar untuk ratusan pemenang
                            </p>
                        </div>
                    </div>
                </div>
                <SectionBenefits images={images}/>
                <TrackVisibility once offset={450}>
                  <SectionPackages images={images}/>
                </TrackVisibility>
                <TrackVisibility once offset={450}>
                  <SectionTnc />
                </TrackVisibility>
                <TrackVisibility once offset={350}>
                  <AvailabilitySection images={images}/>
                </TrackVisibility>
            </>
         );
    }
}
 
export default ShopInternetPackageCategory;

const SectionBenefits = ({images}) => {
    const [cond, setCond] = useState({
        modalBenefits: false
    });
    const triggerPage = (mode) => {
        switch(mode) {
            case "openModalBenefits":
                setCond({
                    modalBenefits: !cond.modalBenefits
                });
                break;

            case "close":
                setCond({
                    modalBenefits: false
                });
                break;

            default:
                return null
        }
    }
    return (
        <section className="py-main-sm">
            <div className="container">
                <div className="subheading animated fadeInUp delayp1">
                    <p>Special Benefits</p>
                </div>
                <div className="row">
                    <div className="col-md-4 col-12">
                        <div className="list-group mb-0 animated fadeInUp delayp2" onClick={()=>triggerPage("openModalBenefits")}>
                            <Link to="#" className="list-group-item w-arrow">
                                <img src={images.common["graphic_grfx_package_benefit_copy.png"]} className="img-fluid w-100px" alt="Budget Icon" />
                                <div className="list-group-item-content">
                                    <h5 className="title">Get sweepstake coupons and win up to Rp 5 billion!</h5>
                                </div>
                            </Link>
                        </div>
                    </div>
                    <div className="col-md-4 col-12" onClick={()=>triggerPage("openModalBenefits")}>
                        <div className="list-group mb-0 animated fadeInUp delayp3">
                            <Link to="#" className="list-group-item w-arrow">
                                <img src={images.common["graphic_grfx_package_benefit_copy_3.png"]} className="img-fluid w-100px" alt="Budget Icon" />
                                <div className="list-group-item-content">
                                    <h5 className="title">Save up to 50% on extra channels & minipacks!</h5>
                                </div>
                            </Link>
                        </div>
                    </div>
                    <div className="col-md-4 col-12" onClick={()=>triggerPage("openModalBenefits")}>
                        <div className="list-group mb-0 animated fadeInUp delayp4">
                            <Link to="#" className="list-group-item w-arrow">
                                <img src={images.common["graphic_grfx_package_benefit_copy_5.png"]} className="img-fluid w-100px" alt="Budget Icon" />
                                <div className="list-group-item-content">
                                    <h5 className="title">Watch movies free on HOOQ, iFlix and Catchplay!</h5>
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <Modal show={cond.modalBenefits} onHide={()=>triggerPage('close')}>
                <div className="modal-body p-box">
                    <Link to="#" className="close animated fadeInUp" onClick={()=>triggerPage('close')}>
                    <span><i className="far fa-times text-primary"></i></span>
                    </Link>
                    <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
                    <h3>Get sweepstakes coupons and win up to Rp 5 billion!</h3>
                    <p>Lorem Ipsum dolor sit amet. consectur adipicsing elti. Lorem Ipsum dolor sit amet. consectur adipicsing elti.</p>
                    </div>
                </div>
            </Modal>
        </section>
    )
}

const SectionPackages = ({images, isVisible}) => {
    const effect = isVisible ? "fadeInUp" : "";
    return (
        <section className="py-main-sm animated fadeInUp delayp4">
            <div className="container">
                <div className={"subheading animated " + effect + " delayp1"}>
                    <p>Packages</p>
                </div>
                <div className="row">
                <div className="col-lg-4">
                  <Link
                    to="/shop/internet/package/category/details"
                    className={"card card-packages card-packages-lg animated " + effect + " delayp2"}
                  >
                    <div className="card-header">
                      <div className="badges-list">
                        <div className="badges badge-red left">
                          <h3>10</h3>
                          <span>Mbps</span>
                        </div>
                        <div className="badges badge-green middle">
                          <h3>92</h3>
                          <span>Channels</span>
                        </div>
                        <div className="badges badge-blue right">
                          <h3>100</h3>
                          <span>Minutes</span>
                        </div>
                      </div>
                      <div className="header-text">
                          <h3>Semarak Kebahagiaan</h3>
                          <p>Internet unlimited, 92 USee TV channels, 100 minutes voice call</p>
                      </div>
                    </div>
                    <div className="card-body w-btn">
                      <div className="card-body-top">
                        <div className="subheading">
                          <p>Semarak Kebahagiaan</p>
                        </div>
                        <h3>
                          Rp480.000<small> / bln</small>
                        </h3>
                        <div className="content">
                          <p>
                            Perfect for everyday internet users with light daily
                            use
                          </p>
                        </div>
                      </div>
                      <div className="card-body-bottom">
                        <div className="subheading">
                          <p>Bonus Subscriptions</p>
                        </div>
                        <div className="content">
                          <p>
                            Get bonus subscriptions from various apps and games!
                          </p>
                        </div>
                        <img
                          src={images.common["logo_add-ons.jpg"]}
                          className="img-fluid"
                          alt="Logo Add-Ons"
                        />
                      </div>
                      <span className="btn btn-primary">Details</span>
                    </div>
                  </Link>
                </div>
                <div className="col-lg-4">
                  <Link
                    to="/shop/internet/package/category/details"
                    className={"card card-packages card-packages-lg animated " + effect + " delayp3"}
                  >
                    <div className="card-header">
                      <div className="badges-list">
                        <div className="badges badge-red left">
                          <h3>20</h3>
                          <span>Mbps</span>
                        </div>
                        <div className="badges badge-green middle">
                          <h3>92</h3>
                          <span>Channels</span>
                        </div>
                        <div className="badges badge-blue right">
                          <h3>100</h3>
                          <span>Minutes</span>
                        </div>
                      </div>
                      <div className="header-text">
                          <h3>Semarak Kebahagiaan</h3>
                          <p>Internet unlimited, 92 USee TV channels, 100 minutes voice call</p>
                      </div>
                    </div>
                    <div className="card-body w-btn">
                      <div className="card-body-top">
                        <div className="subheading">
                          <p>Semarak Kebahagiaan</p>
                        </div>
                        <h3>
                          Rp535.000<small> / bln</small>
                        </h3>
                        <div className="content">
                          <p>
                            Paket dengan berbagai keuntungan di game favorit
                            Anda!
                          </p>
                        </div>
                      </div>
                      <div className="card-body-bottom">
                        <div className="subheading">
                          <p>Bonus Subscriptions</p>
                        </div>
                        <div className="content">
                          <p>
                            Get bonus subscriptions from various apps and games!
                          </p>
                        </div>
                        <img
                          src={images.common["logo_games.jpg"]}
                          className="img-fluid"
                          alt="Logo Games"
                        />
                      </div>
                      <span className="btn btn-primary">Details</span>
                    </div>
                  </Link>
                </div>
                <div className="col-lg-4">
                  <Link
                    to="/shop/internet/package/category/details"
                    className={"card card-packages card-packages-lg animated " + effect + " delayp4"}
                  >
                    <div className="card-header">
                      <div className="badges-list">
                        <div className="badges badge-red left">
                          <h3>50</h3>
                          <span>Mbps</span>
                        </div>
                        <div className="badges badge-blue right">
                          <h3>201</h3>
                          <span>Channels</span>
                        </div>
                        <div className="badges badge-green middle">
                          <h3>1000</h3>
                          <span>Minutes</span>
                        </div>
                      </div>
                      <div className="header-text">
                          <h3>Semarak Kebahagiaan</h3>
                          <p>Internet unlimited, 92 USee TV channels, 100 minutes voice call</p>
                      </div>
                    </div>
                    <div className="card-body w-btn">
                      <div className="card-body-top">
                        <div className="subheading">
                          <p>Semarak Kebahagiaan</p>
                        </div>
                        <h3>
                          Rp535.000<small> / bln</small>
                        </h3>
                        <div className="content">
                          <p>
                            Perfect for everyday internet users with light daily
                            use
                          </p>
                        </div>
                      </div>
                      <div className="card-body-bottom">
                        <div className="subheading">
                          <p>Bonus Subscriptions</p>
                        </div>
                        <div className="content">
                          <p>
                            Get bonus subscriptions from various apps and games!
                          </p>
                        </div>
                        <img
                          src={images.common["logo_add-ons.jpg"]}
                          className="img-fluid"
                          alt="Logo Add-Ons"
                        />
                      </div>
                      <span className="btn btn-primary">Details</span>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
        </section>
    )
}

const SectionTnc = ({isVisible}) => {
    const effect = isVisible ? "fadeInUp" : "";
    return (
        <section className="py-main-sm animated fadeInUp delayp5">
            <div className="container">
                <div className={"subheading animated " + effect + " delayp1"}>
                    <p>Terms & Conditions </p>
                </div>
                <h4 className={"animated " + effect + " delayp2"}>1. Lorem ipsum</h4>
                <p className={"animated " + effect + " delayp3"}>
                    he standard Lorem Ipsum passage, used since the 1500s "Lorem
                    ipsum dolor sit amet, consectetur adipiscing elit, sed do
                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                    enim ad minim veniam, quis nostrud exercitation ullamco laboris
                    nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                    in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                    nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                    sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <h4 className={"animated " + effect + " delayp4"}>2. Lorem ipsum</h4>
                <p className={"animated " + effect + " delayp5"}>
                    he standard Lorem Ipsum passage, used since the 1500s "Lorem
                    ipsum dolor sit amet, consectetur adipiscing elit, sed do
                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                    enim ad minim veniam, quis nostrud exercitation ullamco laboris
                    nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                    in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                    nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                    sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <h4 className={"animated " + effect + " delayp6"}>3. Lorem ipsum</h4>
                <p className={"animated " + effect + " delayp7"}>
                    he standard Lorem Ipsum passage, used since the 1500s "Lorem
                    ipsum dolor sit amet, consectetur adipiscing elit, sed do
                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                    enim ad minim veniam, quis nostrud exercitation ullamco laboris
                    nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                    in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                    nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                    sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
            </div>
        </section>
    )
}

const AvailabilitySection = ({ images, isVisible }) => {
  const effect = isVisible ? "fadeInUp" : "";
  return (
      <section className="py-main bg-white">
          <div className="container">
              <div className="row">
                  <div className="col-md-6 order-md-last mb-3">
                      <img src={images.common["img_indonesia-map.png"]} className="img-fluid" alt="maps-img" />
                  </div>
                  <div className="col-md-6 order-md-first">
                      <div className={"subheading animated " + effect + " delayp1"}><p>Cek ketersediaan IndiHome di dareah Anda</p></div>
                        {/* <h3 className="help-block text-uppercase animated fadeInUp delayp7">Cek KetersAediaan indihome di dareah anda</h3> */}
                        <div className={"heading animated " + effect + " delayp2"}>
                          <h3>Siap melayani Anda, dari Sabang<b className="d-none d-lg-block" /> sampai Merauke.</h3>
                        </div>
                        <div className={"btn-placeholder animated " + effect + " delayp3"}>
                          <Link to="/check-coverage" className="btn btn-primary">Cek Ketersediaan</Link>
                          {/* <small className="help-block mt-2"> Misalnya: Nama kompleks, apartemen, atau gedung</small> */}
                        </div>
                  </div>
              </div>
          </div>
      </section>
  )
}
