import React, { useContext, useEffect } from 'react';
import { PageContext } from './../../context/PageContext';
import { Link } from 'react-router-dom';

const PersonalDataVerificationProgress = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = {brand, common, sample};
    useEffect(() => {
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
      }, [trigger, navCase, pageCase]);
    return (
        <section>
        <div className="container container-xs">
          <div className="box box-main animated fadeInUp">
            <header className="header-light white icon-center animated fadeInUp delayp1">
              <div className="header-actions">
                <Link to="/register/otp" className="btn btn-link"><i className="fal fa-times" /></Link>
              </div>
              <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
            </header>
            <section className="animated fadeInUp delayp2">
              <h1>Verification is Progress</h1>
              <p>Please wait while we're verifying your ID. this process may take up to 15 minutes.</p>
              <p>We'll notify you once it's done! If you don't receive any notification, please check the app again in 15 minutes</p>
            </section>
            <div className="btn-placeholder bottom animated fadeInUp delayp3">
              <Link to="/login:" className="btn btn-primary btn-block">Back to Home</Link>
            </div>
          </div>
        </div>
      </section>
    )
}

export default PersonalDataVerificationProgress;