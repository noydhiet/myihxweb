import React, { useState, useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { Modal, Form } from 'react-bootstrap'
import { PageContext } from './../../context/PageContext';

const InstallationDetailActive = () => {
  const { trigger, navCase, pageCase, brand, common, sample, modal } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  const [inputs, setInputs] = useState({
    modalProcess: false,
    modalIssue: false,
    modalReportIssue: false,
    notifReport: false,
  });
  const triggerPage = (mode) => {
    switch (mode) {
      case "modalProcess":
        setInputs({
          modalProcess: !inputs.modalProcess
        });
        break;

      case "modalIssue":
        setInputs({
          modalIssue: !inputs.modalIssue
        });
        break;

      case "modalReportIssue":
        setInputs({
          modalIssue: !inputs.modalIssue,
          modalReportIssue: !inputs.modalReportIssue
        });
        break;

      case "reportIssue":
        setInputs({
          modalReportIssue: !inputs.modalReportIssue,
          notifReport: !inputs.notifReport
        });
        break;

      case "close":
        setInputs({
          modalProcess: false,
          modalIssue: false,
          modalReportIssue: false,
          notifReport: false,
        });
        break;

      default:
        return null
    }
  }

  const closeNotif = () => {
    setTimeout(() => {
      setInputs({
        notifReport: false
      })
    }, 2000);
  }
  const notifView = inputs.notifReport ? "active" : ""
  const images = { brand, common, sample };
  if (inputs.notifReport === true) {
    closeNotif();
  }
  return (
    <section>
      <div className="container container-sm">
        <div className="box position-relative animated fadeInUp">
          <header className="">
            <div className="header-actions">
              <Link to="/installation/detail-waiting" className="btn btn-link"><i className="fa fa-arrow-left" /></Link>
            </div>
            <div className="heading animated fadeInUp delayp1">
              <h1>Appointment Detail</h1>
            </div>
          </header>
          <section className="section-header-card py-main-sm animated fadeInUp delayp2">
            <div className="subheading">
              <p>Your Appointment</p>
            </div>
            <div className="list-group mb-0">
              <div className="list-group-item unclickable">
                <div className="badges badge-date dark">
                  <small>Thu</small>
                  <h3>27</h3>
                  <span>July</span>
                </div>
                <div className="list-group-item-content">
                  <h5 className="title">New installation</h5>
                  <span className="desc mb-2">
                    Estimated arrival time 8:00 - 12:00
                                  </span>
                  <span className="font-weight-bold text-muted">
                    Reschedule
                                  </span>
                </div>
              </div>
            </div>
            <div className="subheading hidden">
              <p>Package</p>
            </div>
            <Link
              to="/shop/internet/package/details"
              className="card card-packages w-arrow hidden animated fadeInUp delayp2"
            >
              <div className="card-header">
                <div className="badges-list">
                  <div className="badges badge-red left">
                    <h3>100</h3>
                    <span>Mbps</span>
                  </div>
                </div>
                {/* <ul className="list-unstyled">
                  <li>Internet unlimited</li>
                  <li>203 USee TV channels</li>
                  <li>1000 minutes call</li>
                </ul> */}
                <div className="card-header-content">
                  <h3>Paket BUMN</h3>
                  <p>Internet Unlimited, 203 USeeTV channels, 1000 minutes call</p>
                </div>
              </div>
              <div className="card-body w-btn">
                <h3>
                  Rp1.890.000<small> / month</small>
                </h3>
                <small className="help-block">Billing starts the month after intallation</small>
                <span className="btn btn-primary">Details</span>
              </div>
            </Link>
            <div className="form-group">
              <label>Address</label>
              <p>Jl. Jurang Mangu Barat no. 8, Tangerang Selatan, Banten, 15223</p>
            </div>
            <div className="text-center">
              <Link to="#" className="btn btn-link">View Installation Detail</Link>
            </div>
            <div className="subheading">
              <p>Your Technician</p>
            </div>
            <div className="list-group">
              <Link
                to="/shop/internet/package-finder"
                className="list-group-item transparent w-shadow"
              >
                <img alt="" src={images.common["sampleImg.jpg"]} className="w-50px rounded-circle" />
                <div className="list-group-item-content">
                  <h5 className="title w-md-up-75 mb-0">Fajar Nugraha</h5>
                  <p className='desc'>On the way</p>
                </div>
                <div>
                  <i className="fas fa-comment-alt-dots text-primary pr-3"></i>
                  <i className="fas fa-phone text-primary"></i>
                </div>
              </Link>
            </div>
            <div className="py-main-sm milestone">
              <div className="milestone-item active">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Technician is on its way</h5>
                  <span className="desc">Technician is on its way</span>
                </div>
              </div>
              <div className="milestone-item">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Installation is in progress</h5>
                  <span className="desc">
                    Technician is installing your package
                                              </span>
                  <Link to="#" className="" onClick={() => triggerPage("modalProcess")}>
                    Learn more about the installation process
                                              </Link>
                </div>
              </div>
              <div className="milestone-item last-child">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Installation completed</h5>
                  <span className="desc">
                    Installation completed! Enjoy your IndiHome package!
                                              </span>
                </div>
              </div>
              <button className="btn btn-primary" onClick={() => modal("installationStatus")}>Check Installation Status</button>
            </div>
            <div className="subheading">
              <p>Good To Know</p>
            </div>
            <div className="milestone mb-3">
              <div className="milestone-item pb-0 active">
                <div className="milestone-item-no-line">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <p className="text">Installation will take a least 3 hours. Installation may take longer if there are complications. Make sure that you or a contact person is available during that time.</p>
                </div>
              </div>
              <div className="milestone-item pb-0 active">
                <div className="milestone-item-no-line">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <p className="text">There may be additional charges once the installation begins. These charges will be confirmed to you beforehand</p>
                </div>
              </div>
              <div className="milestone-item pb-0 active">
                <div className="milestone-item-no-line">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <p className="text">If the installation is not successful, we will reschedule or issue a refund</p>
                </div>
              </div>
            </div>
            <div className="text-center">
              <p className="help-block mb-0">Are you having problems during the installation</p>
              <Link to="#" className="btn btn-link btn-lg" onClick={() => triggerPage("modalIssue")}>Report Issue</Link>
            </div>
          </section>
        </div>
      </div>
      {/* Modal */}
      {/* Modal Process */}
      <Modal show={inputs.modalProcess} onHide={() => triggerPage("modalProcess")} centered={true}>
        <div className="modal-body p-box pb-0">
          <Link to="#" className="close animated fadeInUp" onClick={() => triggerPage('close')}>
            <span><i className="far fa-times text-primary"></i></span>
          </Link>
          <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
            <h3>Installation Process</h3>
            <p>Your technician will do all the following process</p>
          </div>
          <section className="animated fadeInUp delayp2 mb-4">
            <div className="milestone mb-3">
              <div className="milestone-item">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Measure ODP distance to installation point</h5>
                </div>
              </div>
              <div className="milestone-item">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Installing cable &amp; internet</h5>
                </div>
              </div>
              <div className="milestone-item last-child">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Installing UseeTV hybrid box</h5>
                </div>
              </div>
            </div>
          </section>
        </div>
      </Modal>
      {/* Modal Report */}
      <Modal show={inputs.modalIssue} onHide={() => triggerPage("modalIssue")} centered={true}>
        <div className="modal-body p-box pb-0">
          <Link to="#" className="close" onClick={() => triggerPage('close')}>
            <span><i className="far fa-times text-primary"></i></span>
          </Link>
          <div className="headingpt-sm-down-4">
            <h3>Choose Issue</h3>
          </div>
          <section className="animated fadeInUp delayp2">
            <div className="list-group">
              <div className="list-group-item shadow-none w-bottom-separator" onClick={() => triggerPage("modalReportIssue")}>
                <div className="list-group-item-content mb-0">
                  <p className="text-muted mb-0">My technician is late</p>
                </div>
              </div>
              <div className="list-group-item shadow-none w-bottom-separator" onClick={() => triggerPage("modalReportIssue")}>
                <div className="list-group-item-content mb-0">
                  <p className="text-muted mb-0">My technician asked me for cash</p>
                </div>
              </div>
              <div className="list-group-item shadow-none w-bottom-separator" onClick={() => triggerPage("modalReportIssue")}>
                <div className="list-group-item-content mb-0">
                  <p className="text-muted mb-0">My internet connection is slow</p>
                </div>
              </div>
              <div className="list-group-item shadow-none w-bottom-separator" onClick={() => triggerPage("modalReportIssue")}>
                <div className="list-group-item-content mb-0">
                  <p className="text-muted mb-0">I can't communicate well with my technician</p>
                </div>
              </div>
              <div className="list-group-item shadow-none w-bottom-separator" onClick={() => triggerPage("modalReportIssue")}>
                <div className="list-group-item-content mb-0">
                  <p className="text-muted mb-0">My technician is rude or unprofessional</p>
                </div>
              </div>
              <div className="list-group-item shadow-none w-bottom-separator mb-0 last-child" onClick={() => triggerPage("modalReportIssue")}>
                <div className="list-group-item-content mb-0">
                  <p className="text-muted mb-0">Broken or lost item</p>
                </div>
              </div>
            </div>
          </section>
        </div>
      </Modal>
     
      {/* Modal Report Issue */}
      <Modal show={inputs.modalReportIssue} onHide={() => triggerPage("close")} centered={true}>
        <div className="modal-body p-box pb-0">
          <Link to="#" className="close animated fadeInUp" onClick={() => triggerPage('close')}>
            <span><i className="far fa-times text-primary"></i></span>
          </Link>
          <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
            <h3>Report Issue</h3>
          </div>
          <section className="mb-4 animated fadeInUp delayp2">
            <Form>
              <Form.Group>
                <Form.Label>Issue</Form.Label>
                <Form.Control as="select"
                  className="custom-select"
                  id="issue"
                >
                  <option>My technician is late</option>
                  <option>My technician asked me for cash</option>
                  <option>My internet connection is slow</option>
                  <option>I can't communicate well with my technician</option>
                  <option>Broken or lost item</option>
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>Comments (Optional)</Form.Label>
                <Form.Control
                  type="text"
                  id="comments"
                />
              </Form.Group>
              <div className="btn-placeholder">
                <Link to="#" className="btn btn-primary btn-block" onClick={() => triggerPage("reportIssue")}>Report</Link>
              </div>
            </Form>
          </section>
        </div>
      </Modal>
      <div className={"card card-notif " + notifView}>
        <p className="notif-text">We're sorry you're having issues. We have received your report and will follow up within 1x24 hours</p>
      </div>
    </section>
  )
}

export default InstallationDetailActive