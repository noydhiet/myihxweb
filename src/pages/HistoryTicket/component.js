import React, { Component } from "react";
import { Link } from "react-router-dom";
import History from "./../History";
import { NavMobile } from "./../../include/NavMobile";
import { PageContext } from './../../context/PageContext';

class HistoryTicket extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
    this.context.footerCase("w-nav-bottom");
  }
  componentWillMount() {
    this.context.footerCase("");
  }
  render() {
    const { common, brand, sample, condition } = this.context;
    const images = { common, brand, sample };
    return (
      <div>
        <NavMobile condition={condition} historyPage="active" />
        <History activityActive="active" />
        <SectionContent images={images} />
      </div>
    );
  }
}

export default HistoryTicket;

const SectionContent = ({ images }) => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-4 d-none d-md-block">
          <div className="box box-in-cover py-3">
            <div className="list-group list-group-activity mb-0">
              <Link to="/history/bill" className="list-group-item w-arrow">
                <div className="list-group-item-content">
                  <p className="title mb-0">Bill</p>
                </div>
              </Link>
              <Link to="/history/purchase" className="list-group-item w-arrow">
                <div className="list-group-item-content">
                  <p className="title mb-0">Purchase</p>
                </div>
              </Link>
              <Link to="/history/activity" className="list-group-item w-arrow">
                <div className="list-group-item-content">
                  <p className="title mb-0">Activity</p>
                </div>
              </Link>
            </div>
          </div>
        </div>
        <div className="col-md-8 col-12">
          <div className="box box-in-cover animated fadeInUp">
            <div className="subheading">
                <p>Active Issues</p>
            </div>
            <div className="list-group list-group-activity mb-0">
                <Link
                to="/history/activity/problem-report"
                className="list-group-item pr-0"
              >
                <div className="list-group-item-content mb-0">
                  <small className="caption">#123456</small>
                  <div className="list-group-item-content-w-status">
                    <p className="title">Billing Issue Report</p>
                    <p className="text-status on_progress">On Progress</p>
                  </div>
                </div>
              </Link>
              <Link
                to="/history/activity/problem-report"
                className="list-group-item pr-0"
              >
                <div className="list-group-item-content mb-0">
                  <small className="caption">#123456</small>
                  <div className="list-group-item-content-w-status">
                    <p className="title">Technical Issue Report</p>
                    <p className="text-status on_progress">On Progress</p>
                  </div>
                </div>
              </Link>
            </div>
            {/* Start - Blank State (Hidden) */}
            <div className="blank-state hidden">
              <img alt="" src={images.common["ic_header_failed.png"]} />
              <div className="heading">
                <p className="dark">
                  You don't have any active activity. <br /> Buy your first
                  package to start activity!
                </p>
              </div>
              <div className="btn-placeholder">
                <Link to="/shop/internet/package" className="btn btn-primary">
                  Browse Package
                </Link>
              </div>
            </div>
            {/* End - Blank State */}
          </div>
        </div>
      </div>
    </div>
  );
};
