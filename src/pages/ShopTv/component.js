import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';
import { getToken } from '../../utils/storage';

class ShopTv extends Component {
    render() {
        const { minipackPage, appsPage, edukidsPage, hybridPage, tvStoragePage, shopPrice, shopPriceFixed, btnText, linkTo, modal } = this.props;
        const config = {
            carouselTab: {
                className: "tab-list",
                dots: false,
                infinite: false,
                arrows: false,
                speed: 500,
                slidesToShow: 5,
                slidesToScroll: 5,
                responsive: [
                    {
                      breakpoint: 991,
                      settings: {
                        dots: false,
                        slidesToShow: 4.5,
                        slidesToScroll: 4.5
                      }
                    },
                    {
                      breakpoint: 575,
                      settings: {
                        dots: false,
                        slidesToShow: 3.5,
                        slidesToScroll: 3.5
                      }
                    },
                    {
                      breakpoint: 324,
                      settings: {
                        dots: false,
                        slidesToShow: 2.55,
                        slidesToScroll: 2.55
                      }
                    }
                ]
            }
          }
        const shopPriceView = shopPrice === undefined ? null : 
            <div className="shop-price animated fadeInUp delayp4 d-none d-md-block">
                <div className="container">
                    <div className="shop-price-info">
                    <h3>
                        {shopPrice} <small>/month</small>
                    </h3>
                    <Link to="#" className="">
                        View Pricing Detail <i className="fal fa-angle-right"></i>
                    </Link>
                    </div>
                    <Link to={linkTo} onClick={() => { if(!getToken()) { modal("modalSubscribe") } }} className="btn btn-primary">
                    {btnText}
                    </Link>
                </div>
            </div>;
        const shopPriceFixedView = shopPriceFixed === undefined ? null :
            <div
            className="shop-price fixed-bottom animated fadeInUp delayp8"
            id="shop-price"
            >
            <div className="container">
                <div className="shop-price-info">
                <h3>
                    {shopPriceFixed}<small>/month</small>
                </h3>
                <Link to="#" className="">
                    View Pricing Detail <i className="fal fa-angle-right"></i>
                </Link>
                </div>
                <Link to={linkTo} onClick={() => { if(!getToken()) { modal("modalSubscribe") } }} className="btn btn-primary">
                {btnText}
                </Link>
            </div>
            </div>;
  
        return (
        <div>
            <div className="cover cover-sm cover-bg-white-sm  bg-blue-gradient">
                <div className="header-actions d-block d-md-none">
                    <Link
                    to="/shop/"
                    className="btn btn-link"
                    >
                    <i className="fa fa-arrow-left"></i>
                    </Link>
                </div>
                <div className="container">
                    <nav aria-label="breadcrumb" className="d-none d-md-block">
                    <ol className="breadcrumb animated fadeInUp">
                        <li className="breadcrumb-item"><Link to="/shop"><i className="fa fa-arrow-left mr-1"></i> Shop</Link></li>
                    </ol>
                    </nav>
                    <div className="cover-content">
                    <h1 className="cover-title animated fadeInUp delayp1">TV</h1>
                    <p className="cover-text animated fadeInUp delayp2">Mulai berlangganan paket internet, TV dan telepon mulai dari <strong> Rp150.000 </strong> per bulan <sup>*</sup></p>
                    </div>
                    {shopPriceView}
                </div>
            </div>
            <section className="shop-tab-list animated fadeInUp delayp3">
                <div className="container">
                <Slider {...config.carouselTab}>
                    <div className={"tab-item " + minipackPage}> 
                        <Link to="/shop/tv/minipack">Minipack</Link> 
                    </div>
                    <div className={"tab-item " + appsPage}> 
                        <Link to="/shop/tv/apps">Apps</Link>
                    </div>
                    <div className={"tab-item " + edukidsPage}> 
                        <Link to="/shop/tv/edukids">Edukids</Link> 
                    </div>
                    <div className={"tab-item " + hybridPage}> 
                        <Link to="/shop/tv/hybrid-box">Hybrid Box</Link> 
                    </div>
                    <div className={"tab-item " + tvStoragePage}> 
                        <Link to="/shop/tv/tv-storage">TV Storage</Link> 
                    </div>
                    {/* <div className={"tab-item "}> 
                        <Link to="/shop/internet/cloud-storage">Lorem 2</Link> 
                    </div>
                    <div className={"tab-item "}> 
                        <Link to="/shop/internet/indihome-study">Lorem 3</Link> 
                    </div> */}
                </Slider>
                </div>
            </section>
            {shopPriceFixedView}
        </div>
    )
  }
}

export default ShopTv;