import React, { Component } from 'react';
import ContainerBase from '../../include/ContainerBase';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import { checkOtpIsBlocked, getBlockedNumber, OTP_CHANNEL } from '../../utils/storage';

export default class LoginMobileVerification extends Component {
  static contextType = PageContext;
  state = {
    alert: getBlockedNumber() === localStorage.getItem('mobile') && checkOtpIsBlocked() ? 'This number is temporarily blocked from requesting OTP. Please try again later.' : '',
    submitting: false,
    email: localStorage.getItem('email'),
    mobile: localStorage.getItem('mobile'),
  }

  componentDidMount() {
    this.context.trigger(1);
    this.context.pageCase("w-bg");
  }

  smsActivation = channel => () => {
    const { history } = this.props;
    const { email, mobile } = this.state;
    const { match: { params: { pathSuffix } } } = this.props;
    this.setState({ submitting: true });
    localStorage.setItem(OTP_CHANNEL, channel);
    history.push({pathname: `/login/mobile/otp${pathSuffix || ''}`, state: {channel, mobile, email} });
  }

  render() {
    const { alert, mobile, submitting } = this.state;
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    // const { match: { params: { pathSuffix } } } = this.props;
    // const cardTitle = (pathSuffix && pathSuffix.includes('recovery-code')) ? 'Recovery' : 'Verification';
    const disabled = Boolean(alert || submitting);

    return (
      <TextContext.Consumer>{(context)=>{
        const { loginText } = context;   
        const containerProps = {
          image: { alt: 'Icon Verification', src: images.common['ic_header_verification.png'], },
          title: loginText.mobileVer.title,
        };
        return (
          <ContainerBase {...containerProps}>
            <div className="list-group">
              <button disabled={disabled} onClick={this.smsActivation('whatsApp')} className="list-group-item w-arrow animated fadeInUp delayp2">
                <img src={images.common['ic_whatsapp.png']} className="img-fluid w-25px" alt="Icon WA" />
                <span>{loginText.mobileVer.labelWa()} {mobile.slice(-4).padStart(mobile.length, '*')}</span>
              </button>
              <button disabled={disabled} onClick={this.smsActivation('sms')} className="list-group-item w-arrow animated fadeInUp delayp2">
                <img src={images.common['ic_form_sms.png']} className="img-fluid w-25px" alt="Icon SMS" />
                <span>{loginText.mobileVer.labelSms()} {mobile.slice(-4).padStart(mobile.length, '*')}</span>
              </button>
              <div className="invalid-feedback" style={{ display: alert ? 'block' : 'none' }}>{alert}</div>
            </div>
          </ContainerBase>
        ) 
      }}
      </TextContext.Consumer>
      
    )
  }
}

LoginMobileVerification.defaultProps = {
  pageCase: () => { },
  trigger: () => { },
};
