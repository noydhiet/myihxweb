import React from 'react';
import ContainerBase from '../../include/ContainerBase';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import { checkOtpIsBlocked, getBlockedNumber, OTP_CHANNEL } from '../../utils/storage';

export default class ActivationRegister extends React.Component {
  static contextType = PageContext;
  state = {
    registrationData: JSON.parse(localStorage.getItem('registrationData')),
    alert: getBlockedNumber() === JSON.parse(localStorage.getItem('registrationData')).mobile && checkOtpIsBlocked() ? 'This number is temporarily blocked from requesting OTP. Please try again later.' : '',
    submitting: false
  }

  componentDidMount() {
    this.context.trigger(1);
    this.context.pageCase("w-bg");
  }

  sendOtp = channel => () => {
    const { history } = this.props;
    const { registrationData } = this.state;
    const { email, mobile } = registrationData;
    this.setState({ submitting: true });
    localStorage.setItem(OTP_CHANNEL, channel);
    history.push({ pathname: '/register/otp', state: { channel, email, mobile } });
  }

  render() {
    const { common, brand, sample } = this.context;
    const { alert, registrationData, submitting } = this.state;
    const { mobile } = registrationData;
    const images = { common, brand, sample };
    const disabled = Boolean(alert || submitting);

    return (
    <TextContext.Consumer>{(context)=>{
      const { registerText: { activation } } = context;
      const { title, subtitle, labelWa, labelSms } = activation;
      const containerProps = {
        image: { alt: 'Icon Verification', src: images.common['ic_header_verification.png'], },
        subtitle,
        title
      };
      return (
        <ContainerBase {...containerProps}>
          <div className="list-group">
            <button disabled={disabled} onClick={this.sendOtp('whatsApp')} className="list-group-item w-arrow animated fadeInUp delayp2">
              <img src={images.common['ic_whatsapp.png']} className="img-fluid w-25px" alt="Icon WA" />
              <span>{labelWa()} {mobile.slice(-4).padStart(mobile.length, '*')}</span>
            </button>
            <button disabled={disabled} onClick={this.sendOtp('sms')} className="list-group-item w-arrow animated fadeInUp delayp2">
              <img src={images.common['ic_form_sms.png']} className="img-fluid w-25px" alt="Icon SMS" />
              <span>{labelSms()} {mobile.slice(-4).padStart(mobile.length, '*')}</span>
            </button>
            <div className="invalid-feedback" style={{ display: alert ? 'block' : 'none' }}>{alert}</div>
          </div>
        </ContainerBase>
      )
    }}</TextContext.Consumer>
    )
  }
}
