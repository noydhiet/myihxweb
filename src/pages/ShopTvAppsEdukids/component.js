import React, { Component } from "react";
import ShopTv from './../ShopTv';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import { getToken } from '../../utils/storage';
class ShopTvAppsEdukids extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    const { brand, common, sample, modal } = this.context;
    const images = { brand, common, sample };
    const navigateTo = !getToken() ? '#' : '/shop/tv/edukids/confirm'
    return (
      <TextContext.Consumer>{(context)=>{
        const { shopText } = context;
        return (
          <div>
            <ShopTv edukidsPage="active" shopPrice="Rp100.000" shopPriceFixed="Rp100.000" btnText="Subscribe" linkTo={navigateTo} modal={modal} />
            <SectionWithBanner images={images} shopText={shopText}/>
            <SectionText shopText={shopText}/>
            <SectionFeatures images={images} />
          </div>
        )
      }}

      </TextContext.Consumer>
    )
  }
}
export default ShopTvAppsEdukids;

const SectionWithBanner = ({ images, shopText }) => {
  return (
    <div className="py-main bg-cover-section edukids content-center">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-8 content-center-left">
            <div className="heading mb-2">
              <h3 className="animated fadeInUp delayp5">{shopText.shopTvEdukidsBanner.title}</h3>
              <p className="animated fadeInUp delayp6">
              {shopText.shopTvEdukidsBanner.desc}
                </p>
            </div>
          </div>
          <div className="col-6 d-none d-md-block content-center">
            <img
              src={images.common["banner_bnr_tv_edukids.png"]}
              className="img-fluid animated fadeInUp delayp7 rounded"
              alt="Speed Boost"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

const SectionText = ({ shopText }) => {
  return (
    <div className="py-main-sm bg-white animated fadeInUp delayp7">
      <div className="container">
        <div className="subheading">
          <p>About This Product</p>
        </div>
        <p className="mb-0 w-md-up-50">
        {shopText.shopTvEdukidsBanner.about}
        </p>
      </div>
    </div>
  );
};

const SectionFeatures = ({ images }) => {
  const checkBox = (e) => {
    const check = document.getElementsByClassName("check");
    for (let i = 0; i < check.length; i++) {
      if (i === e) {
        if (check[i].checked) {
          check[i].checked = false
        } else {
          check[i].checked = true
        }
      }
    }
  }
  const listPackage = [
    {
      images: { alt: "img-list", src: "icon_ic_selection_edukids_1.png" },
      title: "Animation",
      desc: "Video Animasi yang edukatif dan menghibur"
    },
    {
      images: { alt: "img-list", src: "icon_ic_selection_edukids_2.png" },
      title: "Ebook",
      desc: "Bacaan Elektronik dengan tema-tema yang mendidik dan menghibur"
    },
    {
      images: { alt: "img-list", src: "icon_ic_selection_edukids_3.png" },
      title: "Music",
      desc: "Channel berbagai pilihan musik untuk anak-anak"
    },
  ]
  return (
    <div className="py-main bg-white animated fadeInUp delayp8">
      <div className="container">
        <div className="row row-4">
          <div className="col-md-6 col-12 mb-3">
            <div className="subheading">
              <p>Package</p>
            </div>
            <div className="list-group">
              {listPackage.map(function (value, index) {
                return (
                  <div key={index} className="list-group-item w-checkbox list-check" onClick={() => checkBox(index)}>
                    <img src={images.common[`${value.images.src}`]} className="img-fluid w-50px" alt={value.images.alt} />
                    <div className="list-group-item-content">
                      <h5 className="title">{value.title}</h5>
                      <span className="desc">{value.desc}</span>
                    </div>
                    <div className="form-group checkbox">
                      <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input check" />
                        <label className="custom-control-label"></label>
                      </div>
                    </div>
                  </div>
                )
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}


