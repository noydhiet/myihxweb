import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';

class AccessRequest extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(1);
    this.context.pageCase("w-bg");
  }
  render() {
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    return (
      <TextContext.Consumer>{(context)=>{
        const { registerText: { request }} = context;
        const { title, subtitle, btnHome } = request;
        return (
          <section>
            <div className="container container-xs">
              <div className="box box-main animated fadeInUp">
                <header className="header-light white icon-center animated fadeInUp delayp1">
                  <div className="header-actions">
                    <Link to="/register/otp" className="btn btn-link"><i className="fal fa-times"></i></Link>
                  </div>
                  <img className="header-img" src={images.common['ic_header_check.png']} alt="Icon Success" />
                </header>
                <section className="animated fadeInUp delayp2">
                  <h1>{title}</h1>
                  <p>{subtitle}</p>
                </section>
                <div className="btn-placeholder bottom animated fadeInUp delayp3">
                  <Link to="/" className="btn btn-primary btn-block"> {btnHome} </Link>
                </div>
              </div>
            </div>
          </section>
        )
      }}</TextContext.Consumer>
    )
  }
}

export default AccessRequest
