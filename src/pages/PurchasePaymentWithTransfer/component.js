import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const PurchasePaymentWithTransfer = () => {
    const { trigger, navCase, pageCase } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    const listBank = ["BCA", "BRI", "BNI", "Bank Mandiri", "Bank Permata", "CIMB Niaga", "OCBC NISP", "May Bank"];
    return (
      <section>
        <div className="container container-sm">
            <div className="box animated fadeInUp">
                <header>
                <div className="header-actions">
                    <Link to="/shop/internet/cloud-storage" className="btn btn-link">
                    <i className="fa fa-arrow-left"></i>
                    </Link>
                </div>
                </header>
                <section className="section-header-text">
                    <div className="heading">
                        <h1>Pilih Bank</h1>
                        <p>Pilih bank yang dituju</p>
                    </div>
                </section>
                <section className="py-main-sm pb-0">
                    <div className="list-group mb-0">
                        <div className="row">
                             {listBank.map(function(value, index){
                            return (
                                <div className="col-md-6 col-12">
                                    <Link to="/purchase/payment-method/payment-with-transfer/confirm" className="list-group-item pd-sm w-arrow" key={index}>
                                        <div className="list-group-item-content">
                                            <h5 className="title">{value}</h5>
                                        </div>
                                    </Link>
                                </div>
                            )
                            })}
                        </div> 
                    </div>
                </section>
            </div>
          </div>
        </section>
  )
}

export default PurchasePaymentWithTransfer;