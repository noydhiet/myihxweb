import React, { useEffect, useContext, useState } from "react";
import { Link } from "react-router-dom";
import { PageContext } from './../../context/PageContext';
import Skeleton from 'react-loading-skeleton';
import moment from 'moment';
import { Modal } from 'react-bootstrap'
import { closeInstallation } from '../../services';
import { getUserProfile } from '../../utils/storage';

const InstallationDetailWaiting = (props) => {
  const { history } = props;
  const { trigger, navCase, pageCase, brand, common, sample, modal } = useContext(PageContext);
  const [response, setResponse] = useState(undefined);
  const [technician, setTechnician] = useState({});
  const [installationDate, setInstallationDate] = useState({});
  const [modalInstallationStatus, setModalInstallationStatus] = useState(false);
  const [showDetail, setshowDetail] = useState(false);
  const [modalProcess, setModalProcess] = useState(false);
  useEffect(()=>{
      trigger(2);
      navCase("w-md-up-block");
      pageCase("w-bg");
      if(props.location.state) {
        const {appointments} = props.location.state
        setResponse(appointments);
        setTechnician(appointments.technician);
        setInstallationDate(appointments.timeSlot);
      } else {
        const { history } = props;
        history.push('/home')
      }
  }, [trigger, navCase, pageCase]);
  const closeBookingId = (e, nextPath) => {
    e.preventDefault();
    setModalInstallationStatus(false);
    if(response && response.bookingId) {
      if(nextPath === 'fail') {
        history.replace({ pathname: '/installation/detail-failure', state: { appointments: response ? response : '' } });
      } else if(nextPath === 'success') {
        history.replace('/installation/detail-success')
      }
      closeInstallation(response.bookingId).then(({data}) => {

      }).catch(error => {
        console.error(error);
      });
    }
  }
  const toggleInstallationDetailShow = event => {
    event.preventDefault();
    setshowDetail(!showDetail);
  }
  const triggerPage = (mode) => {
    switch (mode) {
      case "modalProcess":
        setModalProcess(true);
        break;

      case "close":
        setModalProcess(false)
        
        break;

      default:
        return null
    }
  }
  const images = { brand, common, sample };
  const date =  installationDate.date ? moment(`${installationDate.date} ${installationDate.time}`, 'DD-MM-YYYY HH:mm') : undefined;
  const enable_check_installation = (response && response.status === 'COMPLETED') ? '' : 'disabled';
  const technician_status = response && response.currentStatus ? response.currentStatus.title : ''
  const stepsArray = response ? response.currentStatus.steps.map((item, index) => {
    const flagState = item.active ? 'success' : '';
    const last_child = response.currentStatus.steps.length-1 === index ? 'last-child' : '';
    const learnMore = index === 1 ? 
    <Link to="#" className="" onClick={e => {e.preventDefault(); triggerPage("modalProcess");}}>Learn more about the installation process</Link>
    :
    (null)
     return(
      <div key={index} className={`milestone-item ${flagState} ${last_child}`}>
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">{item.title}</h5>
                  <span className="desc">{item.text}</span>
                  {learnMore}
                </div>
              </div>
    );
  })
  : (response === null ? null : <div className="d-flex">
  <div className="content-center-left ml-3">
    <div className="animated fadeInUp delayp1">
      <Skeleton width={150}/>
    </div> 
    <div className="animated fadeInUp delayp2">
      <Skeleton width={150}/>
    </div>
    <div className="animated fadeInUp delayp2">
      <Skeleton width={150}/>
    </div>
  </div>
</div>   )
  const appointments = props.location.state ? props.location.state.appointments : undefined;
  const userProfile = getUserProfile();
  const accounts = userProfile && userProfile.accounts.length > 0 ? userProfile.accounts[0] : null;
  const location = accounts && Object.keys(accounts.Location).length > 0 ? accounts.Location : null;
  return (
    <section>
      <div className="container container-sm">
        <div className="box position-relative animated fadeInUp">
          <header className="">
            <div className="header-actions">
              <Link to="/installation/detail" className="btn btn-link">
                <i className="fa fa-arrow-left" />
              </Link>
            </div>
            <div className="heading animated fadeInUp delayp1">
  <h1>Appointment Detail</h1>
            </div>
          </header>
          <section className="section-header-card py-main-sm animated fadeInUp delayp2">
            <div className="subheading">
              <p>Your Appointment</p>
            </div>
            <div className="list-group mb-0">
              <div className="list-group-item unclickable">
                <div className="badges badge-date dark">
                  <small>{ date ? date.format('ddd') : '' }</small>
                  <h3>{date ? date.format('DD') : ''}</h3>
                  <span>{date ? date.format('MMM') : ''}</span>
                </div>
                <div className="list-group-item-content">
                  <h5 className="title">New installation</h5>
                  <span className="desc mb-2">
                    Estimated arrival time {installationDate.slot ? installationDate.slot : ''}
                  </span>
                  <span className="font-weight-bold text-muted">
                    Reschedule
                  </span>
                </div>
              </div>
            </div>
            {showDetail 
            ?
             (<section className="section-header-card py-main-sm animated fadeInUp delayp2">
             <div className="subheading mb-2">
               <p>Package</p>
             </div>
             <Link
               to="/shop/internet/package/details"
               className="card card-packages w-arrow animated fadeInUp delayp2"
             >
               <div className="card-header">
                 <div className="badges-list">
                   <div className="badges badge-red left">
                     <h3>10</h3>
                     <span>Mbps</span>
                   </div>
                 </div>
                 <ul className="list-unstyled">
                   <li>Internet unlimited</li>
                   <li>203 USee TV channels</li>
                   <li>1000 minutes call</li>
                 </ul>
               </div>
               <div className="card-body w-btn">
                 <h3>
                   Rp180.000<small> / bln</small>
                 </h3>
                 <small className="help-block">One time payment</small>
                 <span className="btn btn-primary">Details</span>
               </div>
             </Link>
             <div className="form-group">
               <label>Address</label>
               <p>
               {location ? `${location.street}, ${location.district}, ${location.city}, ${location.province}, ${location.postalCode}` : ''}
               </p>
             </div>
           </section>)
            :(null)
            }
            
            <div className="text-center">
              <Link to="#" className="btn btn-link" onClick={toggleInstallationDetailShow}>
                {showDetail ? 'Hide Installation Detail' : 'View Installation Detail'}
              </Link>
            </div>
            <div className="subheading">
              <p>Your Technician</p>
            </div>
            <div className="list-group">
              <div
                to="/shop/internet/package-finder"
                className="list-group-item transparent"
              >
                <img
                  alt=""
                  src={images.common["sampleImg.jpg"]}
                  className="w-50px rounded-circle"
                />
                <div className="list-group-item-content">
                  <h5 className="title w-md-up-75 mb-0">{technician.displayname ? technician.displayname : 'Technician is yet to be assigned'}</h5>
                  <p className="desc">{technician_status}</p>
                </div>
                <div>
                  {
                    technician.email === ""
                    ?
                    <i className="fas fa-comment-alt-dots text-primary pr-3"></i>
                    :
                    <a href={`mailto:${technician.email}`}>
                      <i className="fas fa-comment-alt-dots text-primary pr-3"></i>
                    </a>
                  } 
                  {
                    technician.phone === ""
                    ?
                    <i className="fas fa-phone text-primary"></i> 
                    :
                    <a href={`tel:${technician.phone}`}>
                      <i className="fas fa-phone text-primary"></i>
                    </a>
                  }
                
                </div>
              </div>
            </div>
            <div className="py-main-sm milestone">
              {
                stepsArray
              }
              {/* <div className="milestone-item active">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Technician is on its way</h5>
                  <span className="desc">Technician is on its way</span>
                </div>
              </div> 
              <div className="milestone-item">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Installation is in progress</h5>
                  <span className="desc">
                    Technician is installing your package
                  </span>
                  <Link to="" className="">
                    Learn more about the installation process
                  </Link>
                </div>
              </div>
              <div className="milestone-item last-child">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Installation completed</h5>
                  <span className="desc">
                    Installation completed! Enjoy your IndiHome package!
                  </span>
                </div>
              </div>
              */}
              <Link to="#" className={`btn btn-primary ${enable_check_installation}`} onClick={e => { e.preventDefault(); setModalInstallationStatus(true)}}>
                Check Installation Status
              </Link>
            </div>
            <div className="subheading">
              <p>Good To Know</p>
            </div>
            <div className="milestone mb-3">
              <div className="milestone-item pb-0 active">
                <div className="milestone-item-no-line">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <p className="text">
                    Installation will take a least 3 hours. Installation may
                    take longer if there are complications. Make sure that you
                    or a contact person is available during that time.
                  </p>
                </div>
              </div>
              <div className="milestone-item pb-0 active">
                <div className="milestone-item-no-line">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <p className="text">
                    There may be additional charges once the installation
                    begins. These charges will be confirmed to you beforehand
                  </p>
                </div>
              </div>
              <div className="milestone-item pb-0 active">
                <div className="milestone-item-no-line">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <p className="text">
                    If the installation is not successful, we will reschedule
                    or issue a refund
                  </p>
                </div>
              </div>
            </div>
            <div hidden className="text-center">
              <p className="help-block mb-0">
                Are you having problems during the installation
              </p>
              <Link hidden to={{pathname: (response ? `/installation/detail-waiting/ticket/${response.bookingId}` : '#'), state : { appointments: appointments ? appointments : undefined }}} className="btn btn-link btn-lg">
                Report Issue
              </Link>
            </div>
          </section>
        </div>
      </div>
      {/* MODAL Installation Status / Technician Modal  */}
      <Modal show={modalInstallationStatus} onHide={() => setModalInstallationStatus(false)} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <header className="header-light white w-l-icon icon-center">
                <img className="header-img" src={images.common['graphic_find2.png']} alt="Icon Success" />
              </header>
              <section className="">
                <h1>Check Installation Status</h1>
                <p>Our technician has finished installing your new package. Please check and confirm if all your products are working correctly.</p>
                <div className="subheading">
                  <p>How To Check</p>
                </div>
                <div className="list-group mb-3">
                  <Link to="/shop/internet/package-finder/budget/1" className="list-group-item">
                    <img src={images.common["ic_selection_wifi.png"]} className="img-fluid w-50px" alt="Budget Icon" />
                    <div className="list-group-item-content">
                      <h5 className="title">Internet</h5>
                      <span className="desc">My devices can connect to WiFi and my connection speed is as fast as i expected.</span>
                    </div>
                  </Link>
                  <Link to="/shop/internet/package-finder/budget/1" className="list-group-item">
                    <img src={images.common["ic_selection_tv.png"]} className="img-fluid w-50px" alt="Budget Icon" />
                    <div className="list-group-item-content">
                      <h5 className="title">TV</h5>
                      <span className="desc">I can watch my purchased channels and there's no distortion or blank screens</span>
                    </div>
                  </Link>
                </div>
                <p>Are your products working?</p>
              </section>
              <div className="btn-placeholder inline bottom mt-0">
                <Link to="#" className="btn btn-transparent btn-block" onClick={e => closeBookingId(e, 'fail')}>No</Link>
                <Link to="#" className="btn btn-primary btn-block" onClick={e => closeBookingId(e, 'success')}>Yes</Link>
              </div>
            </div>
          </div>
        </Modal>
        <Modal show={modalProcess} onHide={() => triggerPage("close")} centered={true}>
        <div className="modal-body p-box pb-0">
          <Link to="#" className="close animated fadeInUp" onClick={ e => {e.preventDefault(); triggerPage('close');}}>
            <span><i className="far fa-times text-primary"></i></span>
          </Link>
          <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
            <h3>Installation Process</h3>
            <p>Your technician will do all the following process</p>
          </div>
          <section className="animated fadeInUp delayp2 mb-4">
            <div className="milestone mb-3">
              <div className="milestone-item">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Measure ODP distance to installation point</h5>
                </div>
              </div>
              <div className="milestone-item">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Installing cable &amp; internet</h5>
                </div>
              </div>
              <div className="milestone-item last-child">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Installing UseeTV hybrid box</h5>
                </div>
              </div>
            </div>
          </section>
        </div>
      </Modal>
    </section>
  );
}

export default InstallationDetailWaiting;
