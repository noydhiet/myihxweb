import React, { useState, useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { Modal, Form } from 'react-bootstrap'
import { PageContext } from './../../context/PageContext';

const FisikTicketStatusAssigned = () => {
  const { trigger, navCase, pageCase, brand, common, sample, modal } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  const [inputs, setInputs] = useState({
    notifReport: false,
  });

  const closeNotif = () => {
    setTimeout(() => {
      setInputs({
        notifReport: false
      })
    }, 2000);
  }
  const notifView = inputs.notifReport ? "active" : ""
  const images = { brand, common, sample };
  if (inputs.notifReport === true) {
    closeNotif();
  }
  return (
    <section>
      <div className="container container-sm">
        <div className="box position-relative animated fadeInUp">
          <header className="">
            <div className="header-actions">
              <Link to="/installation/fisik-ticket/status" className="btn btn-link"><i className="fa fa-arrow-left" /></Link>
            </div>
            <div className="heading animated fadeInUp delayp1">
              <h1>Issue Report</h1>
            </div>
          </header>
          <section className="section-header-card py-main-sm animated fadeInUp delayp2">
            <div className="subheading">
              <p>Your Appointment</p>
            </div>
            <div className="list-group mb-0">
              <div className="list-group-item unclickable">
                <div className="badges badge-date dark">
                  <small>Thu</small>
                  <h3>27</h3>
                  <span>July</span>
                </div>
                <div className="list-group-item-content">
                  <h5 className="title">New installation</h5>
                  <span className="desc mb-2">
                    Estimated arrival time 8:00 - 12:00
                                  </span>
                  <span className="font-weight-bold text-muted">
                    Reschedule
                                  </span>
                </div>
              </div>
            </div>
            <div className="subheading">
              <p>Your Ticket</p>
            </div>
            <div className="list-group">
              <Link
                to="/shop/internet/package-finder"
                className="list-group-item transparent w-shadow"
              >
                <div className="list-group-item-content">
                  <h5 className="title w-md-up-75 mb-0">#1234567</h5>
                  <p className='desc'>Internet has been slow for more than 7 days. Technician will visit to analyze the problem.</p>
                </div>
              </Link>
            </div>
            <div className="py-main-sm pb-0 pt-0">
              <div className="form-group">
                <label>Address</label>
                <p>Jl. Jurang Mangu Barat no. 8, Tangerang Selatan, Banten, 15223</p>
            </div>
            <div className="subheading">
                <p>Your Technician</p>
            </div>
            <div className="list-group">
              <Link
                to="/shop/internet/package-finder"
                className="list-group-item transparent w-shadow"
              >
                <img alt="" src={images.common["sampleImg.jpg"]} className="w-50px rounded-circle" />
                <div className="list-group-item-content">
                  <h5 className="title w-md-up-75 mb-0">Fajar Nugraha</h5>
                  <p className='desc'>On the way</p>
                </div>
                <div>
                  <i className="fas fa-comment-alt-dots text-primary pr-3"></i>
                  <i className="fas fa-phone text-primary"></i>
                </div>
              </Link>
            </div>
            <div className="milestone">
              <div className="milestone-item success">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Ticket Submitted!</h5>
                  <span className="desc">Ticket has been submitted. Our team is currently, processing your request</span>
                </div>
              </div>

              <div className="milestone-item success">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">In Progress</h5>
                  <span className="desc">Technician is on the way to you</span>
                </div>
              </div>
                <div className="milestone-item">
                    <div className="milestone-item-img">
                    <div className="dot"></div>
                    </div>
                    <div className="milestone-item-content">
                    <h5 className="title">Repair Complete</h5>
                    <span className="desc">Our technician has finished repairing the issue </span>
                    </div>
                </div>
                <div className="milestone-item last-child">
                    <div className="milestone-item-img">
                    <div className="dot"></div>
                    </div>
                    <div className="milestone-item-content">
                    <h5 className="title">Issue Solved</h5>
                    <span className="desc">Issue has been resolved!</span>
                    </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
      <div className={"card card-notif " + notifView}>
        <p className="notif-text">We're sorry you're having issues. We have received your report and will follow up within 1x24 hours</p>
      </div>
    </section>
  )
}

export default FisikTicketStatusAssigned