import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const HelpTelephone = () => {
  const { trigger, navCase, pageCase } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  return (
    <section>
      <div className="container container-sm">
        <div className="box position-relative animated fadeInUp">
          <header className="">
            <div className="header-actions">
              <Link to="/help" className="btn btn-link">
                <i className="fa fa-arrow-left" />
              </Link>
            </div>
            <div className="heading animated fadeInUp delayp1">
              <h1>Telephone</h1>
              <p>Help regarding Telephone connection issues</p>
            </div>
          </header>
          <section className="section-header-card py-main-sm animated fadeInUp delayp2">
            <div className="box box-sm box-white mb-0">
              <div className="list-group list-group-activity mb-0">
                <Link to="#" className="list-group-item w-arrow">
                  <div className="list-group-item-content">
                    <p className="title mb-0">I can't connect to internet</p>
                  </div>
                </Link>
                <Link to="#" className="list-group-item w-arrow">
                  <div className="list-group-item-content mb-0">
                    <p className="title mb-0">Internet is connected but the speed is slow</p>
                  </div>
                </Link>
                <Link to="#" className="list-group-item w-arrow">
                  <div className="list-group-item-content mb-0">
                    <p className="title mb-0">My WiFi signal is weak</p>
                  </div>
                </Link>
                <Link to="#" className="list-group-item w-arrow">
                  <div className="list-group-item-content mb-0">
                    <p className="title mb-0">Internet equiptment is broken</p>
                  </div>
                </Link>
                <Link to="#" className="list-group-item w-arrow">
                  <div className="list-group-item-content mb-0">
                    <p className="title mb-0">I can't connect to the internet</p>
                  </div>
                </Link>
                <Link to="#" className="list-group-item w-arrow">
                  <div className="list-group-item-content mb-0">
                    <p className="title mb-0">Internet equiptment is broken</p>
                  </div>
                </Link>
                <Link to="#" className="list-group-item w-arrow">
                  <div className="list-group-item-content mb-0">
                    <p className="title mb-0">I can't connect to the internet</p>
                  </div>
                </Link>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
  )
}

export default HelpTelephone;
