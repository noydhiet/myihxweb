import React, { Component } from "react";
import { Link } from 'react-router-dom';
import ShopTv from './../ShopTv';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import { getToken } from "../../utils/storage";

class ShopTvHybridBox extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    const { brand, common, sample, modal } = this.context;
    const images = { brand, common, sample };
    const urlConfirm = getToken() ? '/shop/tv/hybrid-box/confirm' : '#';
    return (
      <TextContext.Consumer>{(context)=>{
        const { shopText } = context;
        return (
          <div>
            <ShopTv hybridPage="active" shopPrice="Rp100.000" shopPriceFixed="Rp100.000" btnText="Subscribe" linkTo={urlConfirm} modal={modal} />
            <SectionWithBanner images={images} shopText={shopText}/>
            <SectionText shopText={shopText}/>
            <SectionFeatures images={images} />
          </div>
        )
      }}

      </TextContext.Consumer>
    )
  }
}
export default ShopTvHybridBox;

const SectionWithBanner = ({ images, shopText }) => {
  return (
    <div className="py-main bg-cover-section hybrid-box content-center">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-8 content-center-left">
            <div className="heading mb-2">
              <h3 className="animated fadeInUp delayp5">{shopText.shopTvHybridBoxBanner.title}</h3>
              <p className="animated fadeInUp delayp6">
              {shopText.shopTvHybridBoxBanner.desc}
                </p>
            </div>
          </div>
          <div className="col-6 d-none d-md-block content-center">
            <img
              src={images.common["banner_bnr_tv_hybridbox.png"]}
              className="img-fluid animated fadeInUp delayp7 rounded"
              alt="Speed Boost"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

const SectionText = ({shopText}) => {
  return (
    <div>
      <div className="py-main-sm bg-white animated fadeInUp delayp7">
        <div className="container">
          <div className="subheading">
            <p>About This Product</p>
          </div>
          <p className="mb-0 w-md-up-50">
          {shopText.shopTvHybridBoxBanner.about}
                </p>
        </div>
      </div>
    </div>
  );
};

const SectionFeatures = ({ images }) => {
  return (
    <div className="py-main bg-white animated fadeInUp delayp8">
      <div className="container">
        <div className="row row-4">
          <div className="col-md-6 col-12 mb-3">
            <div className="subheading">
              <p>Package</p>
            </div>
            <div className="list-group">
              <Link to="/shop/internet/package-finder/needs/1" className="list-group-item">
                <img src={images.common["grfx_grfx_line_hybridbox_2.png"]} className="img-fluid w-50px" alt="Need Icon" />
                <div className="list-group-item-content">
                  <h5 className="title">Turn any TV to a smart TV</h5>
                  <span className="desc">Browse internet, watch Youtube, watch and record TV shows on any TV.</span>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div >
    </div>
  );
};
