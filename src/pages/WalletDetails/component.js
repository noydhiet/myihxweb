import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';
import { getToken } from '../../utils/storage';

const WalletDetails = () => {
  const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
  const [cond, setCond] = useState({
    notifLinkAja: false
  })
  const images = { brand, common, sample };
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  if (!getToken()) {
    window.location.href = '/login';
    return
  }
  const closeNotif = (mode) => {
    switch (mode) {
      case "closeLinkAjaNotif":
        setTimeout(() => {
          setCond({
            notifLinkAja: false
          })
        }, 2000);
        break;

      default:
        return null;
    }
  }
  if (cond.notifLinkAja) {
    closeNotif("closeLinkAjaNotif");
  }
  const activNotifLinkAja = cond.notifLinkAja ? "active" : "";
  return (
    <section>
      <div className="container container-sm">
        <div className="box box-main animated fadeInUp">
          <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
            <img className="header-img" src={images.common['grfx_grfx_linkaja.png']} alt="Icon Success" />
          </header>
          <section className="animated fadeInUp delayp2">
            <div className="subheading">
              <p>Link Aja Account</p>
            </div>
            <h1>081234567890</h1>
          </section>
          <section className="py-main-sm">
            <div className="text-box rounded">
              <div className="subheading">
                <p className="mb-0">Balance</p>
              </div>
              <h5 className="title">Rp10.000.000</h5>
            </div>
          </section>
          <div className="btn-placeholder bottom text-center hidden">
            <p className="help-block mb-0">Want to stop connecting with this account?</p>
            <Link to="/" className="btn btn-link">Get Help</Link>
          </div>
          <div className="btn-placeholder bottom ">
            <Link to="/" className="btn btn-primary btn-block">Next</Link>
          </div>
        </div>
      </div>
      <div className={"card card-notif " + activNotifLinkAja}>
        <p className="notif-text">LinkAja account has been successfully connected</p>
      </div>
    </section>
  )
}

export default WalletDetails;