import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';
import { getToken } from '../../utils/storage';

const HelpInternetDetail4 = () => {
  const { trigger, navCase, pageCase, modal } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);

  return (
    <section>
      <div className="container container-sm">
        <div className="box position-relative animated fadeInUp">
          <header className="">
            <div className="header-actions">
              <Link to="/help/internet/detail/3" className="btn btn-link">
                <i className="fa fa-arrow-left" />
              </Link>
            </div>
          </header>
          <section className="section-header-text">
            <div className="heading">
              <h1>Internet is connected but the speed is slow</h1>
            </div>
          </section>
          <section className="py-main-sm  pb-0 animated fadeInUp delayp2">
            <div className="mb-5">
              <div className="heading">
                <p>Trying resetting your modem by pressing the button below. You will be disconnected from all indiHome services for a few minutes</p>
              </div>
              <div className="btn-placeholder">
                <button className="btn btn-primary btn-block">Reset Modem</button>
              </div>
            </div>
            <div>
              <div className="heading">
                <p>Did this solve your problem?</p>
              </div>
              <div className="list-group mb-0">
                <Link to="/help" onClick={() => modal('rating')} className="list-group-item w-arrow pd-sm">
                  <div className="list-group-item-content">
                    <span className="desc">Yes, problem is solved</span>
                  </div>
                </Link>
                <Link to={getToken() ? '/help/internet/detail/ticket' : '#'} onClick={() => { if (!getToken()) { modal('modalSubscribe') } }} className="list-group-item w-arrow pd-sm">
                  <div className="list-group-item-content">
                    <span className="desc">No, submit a ticket</span>
                  </div>
                </Link>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
  )
}

export default HelpInternetDetail4;
