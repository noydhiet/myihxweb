import React, { Component } from "react";
import { Link } from 'react-router-dom';
import ShopStorage from './../ShopStorage';
import { PageContext } from './../../context/PageContext';

class ShopStorageTv extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    return (
      <div>
        <ShopStorage tvStoragePage="active" shopPrice="Rp100.000" shopPriceFixed="Rp100.000" btnText="Subscribe" linkTo="/shop/storage/tv-storage/confirm" />
        <SectionWithBanner images={images} />
        <PackageListSection images={images} />
      </div>
    )
  }
}
export default ShopStorageTv;

const SectionWithBanner = ({ images }) => {
  return (
    <div className="py-main bg-cover-section indihome-cloud content-center">
      <div className="container">
        <div className="row row-4">
          <div className="col-md-6 col-8 content-center-left">
            <div className="heading mb-2">
              <h3 className="animated fadeInUp delayp5">Love to record and re-watch your favourite TV shows?</h3>
              <p className="mb-0 animated fadeInUp delayp6">Our TV storage feature have got you covered</p>
            </div>
            <div className="btn-placeholder mt-2 animated fadeInUp delayp7">
              <div className="row d-none d-md-block">
                <div className="col-lg-5">
                  <Link
                    to="/shop/more/cloud-storage"
                    className="btn btn-primary"
                  >
                    More Details
                              </Link>
                </div>
              </div>
              <Link
                to="/shop/more/cloud-storage"
                className="btn btn-link light d-md-none"
              >
                More Details
                          </Link>
            </div>
          </div>
          <div className="col-6 d-none d-md-block">
            <div className="content-center" style={{ height: "100%" }}>
              <img src={images.common["banner_bnr_more_cloud.png"]} className="img-fluid rounded animated fadeInUp delayp7" alt="Indonesia map" />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const PackageListSection = ({ images }) => {
  return (
    <section className="py-main bg-gray-50 bg-md-up-white ">
      <div className="container">
        <div className="list-group mb-0 pb-3">
          <div className="row">
            <div className="col-lg-4">
              <div className="list-group-item w-badge unclickable align-items-center animated fadeInUp delayp4">
                <div className="badges badge-red">
                  <h3>100</h3>
                  <span>Mbps</span>
                </div>
                <div className="list-group-item-content">
                  <h5 className="title mb-0">Record shows using recording and save in TV storage</h5>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="list-group-item w-badge unclickable align-items-center animated fadeInUp delayp5">
                <div className="badges badge-blue">
                  <h3>202</h3>
                  <span>Channel</span>
                </div>
                <div className="list-group-item-content">
                  <h5 className="title mb-0">Up to 50GB space</h5>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="list-group-item w-badge unclickable align-items-center animated fadeInUp delayp6">
                <div className="badges badge-green">
                  <h3>100</h3>
                  <span>mins</span>
                </div>
                <div className="list-group-item-content">
                  <h5 className="title mb-0"> 600mins recording for SD/240mins for HD quality </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};