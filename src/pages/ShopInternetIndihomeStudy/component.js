import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ShopInternet from './../ShopInternet';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';

class ShopInternetIndihomeStudy extends Component {
    static contextType = PageContext;
    componentDidMount() {
        this.context.trigger(2);
        this.context.navCase("w-md-up-block");
        this.context.pageCase("l-bg");
    }
    render() {
        const { brand, common, sample } = this.context;
        const images = { brand, common, sample };
        return (
          <TextContext.Consumer>{(context)=>{
            const { shopText } = context;
            return (
              <div>
                <ShopInternet indihomeStudyActive="active"/>
                <SectionWithBanner images={images}shopText={shopText}/>
                <SectionText shopText={shopText}/>
                <SectionFeatures images={images} />
            </div>
            )
          }}

          </TextContext.Consumer>
        )
    }
}

export default ShopInternetIndihomeStudy;

const SectionWithBanner = ({ images, shopText }) => {
  return (
    <div className="py-main bg-cover-section indihome-study content-center">
      <div className="container">
        <div className="row row-4">
          <div className="col-md-6 col-8 content-center-left">
            <div className="heading mb-2">
              <h3 className="animated fadeInUp delayp5">
              {shopText.shopInternetIndiHomeStudyBanner.title}
                </h3>
              <p className="mb-0 animated fadeInUp delayp6">
              {shopText.shopInternetIndiHomeStudyBanner.desc}
                </p>
            </div>
          </div>
          <div className="col-6 d-none d-md-block">
            <img
              src={images.common["banner_bnr_more_ihstudy.png"]}
              className="img-fluid rounded animated fadeInUp delayp7"
              alt="Indonesia map"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

const SectionText = ({shopText}) => {
  return (
    <div className="py-main bg-white">
      <div className="container">
        <div className="subheading">
          <p>About this service</p>
        </div>
        <div className="heading w-md-up-50">
          <p className="dark">
          {shopText.shopInternetIndiHomeStudyBanner.desc}
                    </p>
        </div>
      </div>
    </div>
  );
};

const SectionFeatures = ({ images }) => {
  return (
    <div className="py-main-sm pt-0 bg-white">
      <div className="container">
        <div className="row row-4">
          <div className="col-md-6 col-12">
            <div className="subheading">
              <p>How To Activate</p>
            </div>
            <div className="milestone">
              <div className="milestone-item active">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Download IndiStudy app</h5>
                  <span className="desc">Aplikasi IndiStudy tersedia di Play Store dan App Store</span>
                </div>
              </div>
              <div className="milestone-item active">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Login with myIndiHome e-mail and password</h5>
                  <span className="desc">Anda bisa menggunakan akun yang sama di IndiStudy dan myIndiHome</span>
                </div>
              </div>
              <div className="milestone-item active last-child">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">
                    Start Learning!
                    </h5>
                  <span className="desc">Enjoy unlimited access to ebooks and learning materials.</span>
                </div>
              </div>
            </div>
            <div className="btn-placeholder d-md-none">
              <Link to="/" className="btn btn-primary">Open IndiStudy</Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
