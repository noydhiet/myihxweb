import React from 'react';
import renderer from 'react-test-renderer';
import CheckCoverage from '../index';

jest.mock('react-router-dom');

const instance = props => renderer.create(<CheckCoverage {...props} />).getInstance();

describe('CheckCoverage', () => {
  test('render', () => {
    const result = instance().render();
    expect(result.type).toBe('div');
  });

  test('checkAccount', () => {
    const wrapper = instance();
    const e = { target: { id: 'test', value: 'test-value' } };
    wrapper.handleChange(e);
    expect(wrapper.state.test).toBe('test-value');
  });
});
