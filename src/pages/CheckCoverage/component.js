import React, { useEffect, useState, useContext } from "react";
import { Form, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import querystring from 'querystring';
import { PageContext } from './../../context/PageContext';
import { listProvinces, listCities, listDistricts, listStreets } from "../../services";
// import { Document, Page, Text, View, StyleSheet } from "@react-pdf/renderer";

const CheckCoverage = (props) => {
  const { trigger, navCase, pageCase, } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
    getProvinces();
  }, [trigger, navCase, pageCase]);
  const [inputs, setInputs] = useState({
    coverageAddress: '',
    coverageRtRw: '',
    coverageZipCode: '',
    packageId: querystring.parse(window.location.search.replace('?', '')).packageId,
  })
  const [selectedIndex, setSelectedIndex] = useState({
    province: '',
    city: '',
    district: '',
    street: '',
  })
  const [dropDown, setDropDown] = useState({
    provinces: [],
    cities: [],
    districts: [],
    streets: []
  })
  const [alerts, setAlerts] = useState({
    coverageAddress: '',
    province: '',
    city: '',
    district: '',
    street: '',
    coverageZipCode:''
  });
  const handleChange = (e, type) => {
    e.persist();
    // e.target.value is index of selected item
    switch (type) {
      case 'province':
        if(e.target.value) {
          setSelectedIndex({...selectedIndex, ['province']: e.target.value, ['city']: '', ['district']: '', ['street']: ''});
          setDropDown(d => ({ ...d, cities: 'LOADING' }));
          setInputs(i => ({ ...i, coverageZipCode: '' }));
          const distric_id = dropDown.provinces[e.target.value].id;
          distric_id && getCities(distric_id);
        } else {
          setSelectedIndex({...selectedIndex, ['province']: '', ['city']: '', ['district']: '', ['street']: ''});
          setInputs(i => ({ ...i, coverageZipCode: '' }));
        }
        handleBlur(e);
        break;
      case 'city':
        if(e.target.value) {
          setSelectedIndex({...selectedIndex, ['city']: e.target.value, ['district']: '', ['street']: ''});
          setDropDown(d => ({ ...d, districts: 'LOADING' }));
          setInputs(i => ({ ...i, coverageZipCode: '' }));
          const cit_legacyCode = dropDown.cities[e.target.value].legacyCode
          cit_legacyCode && getDistricts(cit_legacyCode);
        } else {
          setSelectedIndex({...selectedIndex, ['city']: '', ['district']: '', ['street']: ''});
          setInputs(i => ({ ...i, coverageZipCode: '' }));
        }
        handleBlur(e);
        break;
      case 'district':
        // {`${data.districtID}--${data.postalCode}`}
        if(e.target.value) {
          setSelectedIndex({...selectedIndex, ['district']: e.target.value, ['street']: ''});
          setDropDown(d => ({ ...d, streets: 'LOADING' }));
          const zipcode = dropDown.districts[e.target.value].postalCode;
          if (zipcode && zipcode.match(/^\d{5}$/)) {
            setInputs(i => ({ ...i, coverageZipCode: zipcode }));
          } else {
            setInputs(i => ({ ...i, coverageZipCode: '' }));
          }
          const legacyCodeCity = dropDown.cities[selectedIndex.city].legacyCode;
          const legacyCodeDistrict = dropDown.districts[e.target.value].legacyCode;
          legacyCodeCity && legacyCodeDistrict && getStreets(legacyCodeCity, legacyCodeDistrict);
        } else {
          setSelectedIndex({...selectedIndex, ['district']: '', ['street']: ''});
          setInputs(i => ({ ...i, coverageZipCode: '' }));
        }
        handleBlur(e);
        break;
      case 'street':
        if(e.target.value) {
          setSelectedIndex({...selectedIndex, ['street']: e.target.value});
        } else {
          setSelectedIndex({...selectedIndex, ['street']: ''});
        }
        handleBlur(e);
        break;
      case 'coverageRtRw':
        const [rt, rw] = e.target.value.split('/');
        if (e.target.value.match(/^\d\d\d\d$/)) setInputs(inputs => ({ ...inputs, coverageRtRw: `${e.target.value.slice(0, 3)}/${e.target.value.slice(-1)}` }));
        if (rt && rt.length > 3) break;
        if (rw && rw.length > 3) break;
        if (e.target.value.match(/[^0-9/]/)) break;
        else setInputs(inputs => ({ ...inputs, coverageRtRw: e.target.value }));
        break;

      default:
        setInputs(inputs => ({ ...inputs, [e.target.id]: e.target.value }));
        break;
    }
  }
  const handleBlur = e => {
    const { id, value } = e.target;
    const message = ((id, value) => {
      if (value) return '';
      switch(id) {
        case 'province':
          return 'Please select the province'
        case 'city':
          return 'Please select the city'
        case 'district':
          return 'Please select the district'
        case 'street':
          return 'Please select the street'
        case 'coverageAddress':
          return 'Please enter the address'
        case 'coverageZipCode':
          return 'Please enter the zip code'
      }
    })(id,value);
    setAlerts({ ...alerts, [id]: message });
  };
  const getProvinces = () => {
    setDropDown(dropDown => ({ ...dropDown, provinces: 'LOADING' }));
    listProvinces()
      .then(({ data }) => {
        console.log('listProvinces', data);
        data.ok && setDropDown(dropDown => ({ ...dropDown, provinces: data.data }));
      }).catch(e => {
        console.error(e);
      });
  }
  const getCities = (provinceId) => {
    listCities(provinceId)
      .then(({ data }) => {
        console.log('getCities', data);
        data.ok && setDropDown(dropDown => ({ ...dropDown, cities: data.data }));
      }).catch(e => {
        console.error(e);
      });
  }
  const getDistricts = (cityId) => {
    listDistricts(cityId)
      .then(({ data }) => {
        console.log('getDistricts', data);
        data.ok && setDropDown(dropDown => ({ ...dropDown, districts: data.data }));
      }).catch(e => {
        console.error(e);
      });
  }
  const getStreets = (cityId, districtId) => {
    listStreets(cityId, districtId)
      .then(({ data }) => {
        console.log('getStreets', data);
        data.ok && setDropDown(dropDown => ({ ...dropDown, streets: data.data }));
      }).catch(e => {
        console.error(e);
      });
  }
  const title = inputs.packageId ? 'Enter Installation Address' : 'Check IndiHome Coverage in Your Area';
  const goNextScreen = e => {
    e.preventDefault();
    const province = dropDown.provinces[selectedIndex.province] ? dropDown.provinces[selectedIndex.province].name : null;
    const provinceCode = dropDown.provinces[selectedIndex.province] ? dropDown.provinces[selectedIndex.province].id : null;
    const city = dropDown.cities[selectedIndex.city] ? dropDown.cities[selectedIndex.city].name : null;
    const cityCode = dropDown.cities[selectedIndex.city] ? dropDown.cities[selectedIndex.city].legacyCode : null;
    const district = dropDown.districts[selectedIndex.district] ? dropDown.districts[selectedIndex.district].districtName : null;
    const districtCode = dropDown.districts[selectedIndex.district] ? dropDown.districts[selectedIndex.district].legacyCode : null;
    const street = dropDown.streets[selectedIndex.street] ? dropDown.streets[selectedIndex.street].streetName : null;
    const streetCode = dropDown.streets[selectedIndex.street] ? dropDown.streets[selectedIndex.street].legacyCode : null;
    const postalCode = inputs.coverageZipCode;
    const rtrw = inputs.coverageRtRw;
    const addressDescription = inputs.coverageAddress;
    if(!province) {
      setAlerts({ ...alerts, ['province']: 'Please select the province' });
      return;
    } else if(!city) {
      setAlerts({ ...alerts, ['city']: 'Please select the city' });
      return;
    } else if(!district) {
      setAlerts({ ...alerts, ['district']: 'Please select the district' });
      return;
    } else if(!street) {
      setAlerts({ ...alerts, ['street']: 'Please select the street' });
      return;
    } else if(!addressDescription) {
      setAlerts({ ...alerts, ['coverageAddress']: 'Please enter the address' });
      return;
    } else if(!postalCode) {
      setAlerts({...alerts, ['coverageZipCode']: 'Please enter the zip code'});
      return;
    }
    const queryString = `province=${province}&city=${city}&district=${district}&street=${street}&addressDescription=${addressDescription}`
    const queryString2 = `&provinceCode=${provinceCode}&cityCode=${cityCode}&districtCode=${districtCode}&streetCode=${streetCode}&postalCode=${postalCode}&rtrw=${rtrw}`
    const nextLink = inputs.packageId ? `/check-coverage/map?packageId=${inputs.packageId}&${queryString}${queryString2}` : `/check-coverage/map?${queryString}${queryString2}`
    console.log('queryString', queryString);
    console.log('queryString2', queryString2);
    props.history.push(nextLink)
  }
  return (
    <div className="container container-sm">
      <div className="box animated fadeInUp">
        <header>
          <div className="header-actions">
            <Link to="/shop/internet/package" className="btn btn-link">
              <i className="fa fa-arrow-left"></i>
            </Link>
          </div>
        </header>
        <section className="section-header-text">
          <div className="heading">
            <h1>{title}</h1>
            <p>Make sure you have entered the right address and location pin point.</p>
          </div>
        </section>
        <section className="animated fadeInUp delayp2">
          <Form>
            <Form.Group>
              <Form.Label>Province</Form.Label>
              <Form.Control as="select"
                className={"custom-select" + (alerts.province && ' is-invalid')}
                id="province"
                onChange={e => handleChange(e, 'province')}
                onBlur={handleBlur}
              >
                {dropDown.provinces === 'LOADING' ? <option value=''>Loading...</option> : <option value=''>Choose Province</option>}
                {
                  dropDown.provinces && dropDown.provinces !== 'LOADING' && dropDown.provinces.length > 0 ? (dropDown.provinces.map((data, index) => (
                    <option key={index} value={index}>{data.name}</option>
                  ))) : (<option disabled>No data</option>)
                }
                >
              </Form.Control>
              <div className="invalid-feedback">{alerts.province}</div>
            </Form.Group>
            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>City</Form.Label>
                <Form.Control as="select"
                  className={"custom-select" + (alerts.city && ' is-invalid')}
                  id="city"
                  onChange={e => handleChange(e, 'city')}
                  onBlur={handleBlur}
                  disabled={!selectedIndex.province}
                >
                  {
                    dropDown.cities === 'LOADING' ? <option value=''>Loading...</option> :
                      (
                        <>
                          <option value=''>Choose City</option>
                          {
                            selectedIndex.province && dropDown.cities.length > 0 ? (dropDown.cities.map((data, index) => (
                              <option key={index} value={index}>{data.name}</option>
                            ))) : (<option disabled>No data</option>)
                          }
                        </>)}
                </Form.Control>
                <div className="invalid-feedback">{alerts.city}</div>
              </Form.Group>
              <Form.Group as={Col}>
                <Form.Label>District</Form.Label>
                <Form.Control as="select"
                  className={"custom-select" + (alerts.district && ' is-invalid')}
                  id="district"
                  onChange={(e) => handleChange(e, 'district')}
                  onBlur={handleBlur}
                  disabled={!(selectedIndex.province && selectedIndex.city)}
                >
                  {
                    dropDown.districts === 'LOADING' ? <option value=''>Loading...</option> :
                      (<>
                        <option value=''>Choose District</option>
                        {
                          selectedIndex.province && selectedIndex.city && dropDown.districts.length > 0 ? (dropDown.districts.map((data, index) => (
                            <option key={index} value={index}>{data.districtName}</option>
                          ))) : (<option disabled>No data</option>)
                        }
                      </>)
                    }
                </Form.Control>
                <div className="invalid-feedback">{alerts.district}</div>
              </Form.Group>
            </Form.Row>
            <Form.Group>
              <Form.Label>Nearest Address</Form.Label>
              <Form.Control as="select"
                className={"custom-select" + (alerts.street && ' is-invalid')}
                id="street"
                onChange={e => handleChange(e, 'street')}
                onBlur={handleBlur}
                disabled={!(selectedIndex.province && selectedIndex.city && selectedIndex.district)}
              >
                {
                  dropDown.streets === 'LOADING' ? <option value=''>Loading...</option> :
                  (
                    <>
                    <option value=''>Choose Street</option>
                    {selectedIndex.province && selectedIndex.city && selectedIndex.district && dropDown.streets.length > 0 ? (dropDown.streets.map((data, index) => (
                      <option key={index} value={index}>{data.streetName}</option>
                  ))) : (<option disabled>No data</option>
                  )}
                    </>
                  )
                }
                >
              </Form.Control>
              <div className="invalid-feedback">{alerts.street}</div>
            </Form.Group>
            <Form.Group>
              <Form.Label>Detailed Address</Form.Label>
              <Form.Control
                className={alerts.coverageAddress && 'is-invalid'}
                type="text"
                id="coverageAddress"
                value={inputs.coverageAddress}
                placeholder="Enter your address"
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <div className="invalid-feedback">{alerts.coverageAddress}</div>
            </Form.Group>
            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>Rt/Rw (Optional)</Form.Label>
                <Form.Control
                  className="form-control"
                  type="text"
                  id="coverageRtRw"
                  value={inputs.coverageRtRw}
                  placeholder="Enter Number"
                  onChange={(e) => handleChange(e, 'coverageRtRw')}
                />
              </Form.Group>
              <Form.Group as={Col}>
                <Form.Label>Zip Code</Form.Label>
                <Form.Control
                  className={alerts.coverageZipCode && ' is-invalid'}
                  type="number"
                  id="coverageZipCode"
                  value={inputs.coverageZipCode}
                  placeholder="Enter Number"
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <div className="invalid-feedback">{alerts.coverageZipCode}</div>
              </Form.Group>
            </Form.Row>
            <div className="btn-placeholder">
              {/* <Link to={{pathname: config.nextLink, state: { province: selectedValue.province, city: selectedValue.city, district: selectedValue.district, coverageAddress: inputs.coverageAddress, coverageZipCode: selectedValue.coverageZipCode }}} className={"btn btn-primary btn-block" + (disabled ? ' disabled' : '')}>next</Link> */}
              <Link to="#" className="btn btn-primary btn-block" onClick={e => goNextScreen(e)}>next</Link>
            </div>
          </Form>
        </section>
      </div>
      {/* <Document>
        <Page size="A4" style={styles.page}>
          <View style={styles.section}>
            <Text>Hello World!</Text>
          </View>
          <View style={styles.section}>
            <Text>We're inside a PDF!</Text>
          </View>
        </Page>
      </Document> */}
    </div>
  );
}

export default CheckCoverage;
