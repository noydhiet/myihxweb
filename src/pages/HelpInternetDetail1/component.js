import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const HelpInternetDetail1 = props => {
  const { trigger, navCase, pageCase, modal, checkToken } = useContext(PageContext);
  const [issue, setIssue] = useState('')
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
    const { location: { state } } = props;
    if(state) {
      setIssue(state.issue);
    }
  }, [trigger, navCase, pageCase]);
  
  const { history, location: { state }}  = props;
  if(!state) {
    history.push('/help')
    return(null)
  } 
  console.log('state', state)
  const title = (state && state.solutions.length > 0) ? state.solutions[state.solutionIndex].solution : 'Checking Connected Devices';
  const description = (state && state.solutions.length > 0) ? state.solutions[state.solutionIndex].steps: 'Check the other devices connected to the network. If too many devices are connected or on bandwidth heavy activities like video streaming or downloading large files, your internet speed can be affected';
  const btnNeedDifferentSolution = state && (state.solutions.length - 1 === state.solutionIndex)
  ?
  (null)
  :
  <Link to={{pathname: '/help/internet/detail/1', state: { solutionIndex : state.solutionIndex + 1, 
    solutions: state.solutions, 
    categoryId: state.categoryId,
    category: state.category,
    issueIndex: state.issueIndex,
    issuesArray: state.issuesArray } }} className="list-group-item w-arrow pd-sm">
    <div className="list-group-item-content">
      <span className="desc">No, I need a different solution</span>
    </div>
  </Link>
  return (
    <section>
      <div className="container container-sm">
        <div className="box position-relative animated fadeInUp">
          <header className="">
            <div className="header-actions">
              <Link to="#" className="btn btn-link" onClick={ e =>  { e.preventDefault(); history.goBack();} }>
                <i className="fa fa-arrow-left" />
              </Link>
            </div>
          </header>
          <section className="section-header-text">
            <div className="heading">
              <h1>{issue}</h1>
            </div>
          </section>
          <section className="py-main-sm pb-0 animated fadeInUp delayp2">
            <div className="mb-5">
              <div className="title">
                <h3>{title}</h3>
              </div>
                <div className="heading">
                <p>{description}</p>
                </div>
              {/* <div className="btn-placeholder">
                <button className="btn btn-primary btn-block">View Status</button>
              </div> */}
            </div>
            <div>
              <div className="heading">
                <p>Did this solve your problem?</p>
              </div>
              <div className="list-group mb-0">
                <Link to="/help" onClick={() => modal('rating')} className="list-group-item w-arrow pd-sm">
                  <div className="list-group-item-content">
                    <span className="desc">Yes, problem is solved</span>
                  </div>
                </Link>
                {
                  btnNeedDifferentSolution
                }
                <Link to={checkToken() ? { pathname: "/help/internet/detail/ticket", state: { categoryId: state.categoryId, issueIndex: state.issueIndex, issuesArray: state.issuesArray } } : "#"} onClick={e => { !checkToken() && modal('modalSubscribe'); }} className="list-group-item w-arrow pd-sm">
                  <div className="list-group-item-content">
                    <span className="desc">No, I want to submit a report</span>
                  </div>
                </Link>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
  )
}

export default HelpInternetDetail1;
