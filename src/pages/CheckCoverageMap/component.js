import React, { useEffect, useContext, useState, useCallback } from 'react';
import L from 'leaflet'
import { Map, TileLayer, Marker, Popup, withLeaflet } from 'react-leaflet'
import { ReactLeafletSearch } from 'react-leaflet-search'
import { Form, Modal } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import querystring from 'querystring';
import { PageContext } from './../../context/PageContext';
import { orderFeasibilityPackage, googlegeocoder, checkCoverage, guestUser } from '../../services';
import 'leaflet-control-geocoder';
import { getToken, getGuestToken, checkGuestTokenExpireTime, setGuestToken, setGuestTokenExpireTime } from '../../utils/storage';
const ReactLeafletSearchComponent = withLeaflet(ReactLeafletSearch);
const defaultCoordinate = {
  //Default Location Telkomsel Graha Merah Putih Gatot Subroto
  lat: -6.230032491437649,
  lng: 106.81871841195974,
};

const CheckCoverageMap = (props) => {
  const { history } = props;
  const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
  const [coordinate, setCoordinate] = useState(defaultCoordinate);
  const [errorDialog, setErrorDialog] = useState(false);
  const [errorTitle, setErrorTitle] = useState('');
  const [errorDes, setErrorDes] = useState('');
  const [locationName, setLocationName] = useState({ name: '', street: '', zip: '' });
  const [submitting, setSubmitting] = useState(false);
  const [mapState, setMapState] = useState({
    zoom: 15,
    modal: false,
    province: querystring.parse(window.location.search.replace('?', '')).province,
    provinceCode: querystring.parse(window.location.search.replace('?', '')).provinceCode,
    city: querystring.parse(window.location.search.replace('?', '')).city,
    cityCode: querystring.parse(window.location.search.replace('?', '')).cityCode,
    district: querystring.parse(window.location.search.replace('?', '')).district,
    districtCode: querystring.parse(window.location.search.replace('?', '')).districtCode,
    street: querystring.parse(window.location.search.replace('?', '')).street,
    streetCode: querystring.parse(window.location.search.replace('?', '')).streetCode,
    addressDescription: querystring.parse(window.location.search.replace('?', '')).addressDescription,
    coverageZipCode: querystring.parse(window.location.search.replace('?', '')).postalCode,
    rtrw: querystring.parse(window.location.search.replace('?', '')).rtrw,
    packageId: querystring.parse(window.location.search.replace('?', '')).packageId
  });

  const images = { brand, common, sample };
  const myIcon = L.icon({
    iconUrl: require("./../../assets/img/common/ic_location_pin.png"),
    //iconUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAYAAADAk4LOAAAFgUlEQVR4Aa1XA5BjWRTN2oW17d3YaZtr2962HUzbDNpjszW24mRt28p47v7zq/bXZtrp/lWnXr337j3nPCe85NcypgSFdugCpW5YoDAMRaIMqRi6aKq5E3YqDQO3qAwjVWrD8Ncq/RBpykd8oZUb/kaJutow8r1aP9II0WmLKLIsJyv1w/kqw9Ch2MYdB++12Onxee/QMwvf4/Dk/Lfp/i4nxTXtOoQ4pW5Aj7wpici1A9erdAN2OH64x8OSP9j3Ft3b7aWkTg/Fm91siTra0f9on5sQr9INejH6CUUUpavjFNq1B+Oadhxmnfa8RfEmN8VNAsQhPqF55xHkMzz3jSmChWU6f7/XZKNH+9+hBLOHYozuKQPxyMPUKkrX/K0uWnfFaJGS1QPRtZsOPtr3NsW0uyh6NNCOkU3Yz+bXbT3I8G3xE5EXLXtCXbbqwCO9zPQYPRTZ5vIDXD7U+w7rFDEoUUf7ibHIR4y6bLVPXrz8JVZEql13trxwue/uDivd3fkWRbS6/IA2bID4uk0UpF1N8qLlbBlXs4Ee7HLTfV1j54APvODnSfOWBqtKVvjgLKzF5YdEk5ewRkGlK0i33Eofffc7HT56jD7/6U+qH3Cx7SBLNntH5YIPvODnyfIXZYRVDPqgHtLs5ABHD3YzLuespb7t79FY34DjMwrVrcTuwlT55YMPvOBnRrJ4VXTdNnYug5ucHLBjEpt30701A3Ts+HEa73u6dT3FNWwflY86eMHPk+Yu+i6pzUpRrW7SNDg5JHR4KapmM5Wv2E8Tfcb1HoqqHMHU+uWDD7zg54mz5/2BSnizi9T1Dg4QQXLToGNCkb6tb1NU+QAlGr1++eADrzhn/u8Q2YZhQVlZ5+CAOtqfbhmaUCS1ezNFVm2imDbPmPng5wmz+gwh+oHDce0eUtQ6OGDIyR0uUhUsoO3vfDmmgOezH0mZN59x7MBi++WDL1g/eEiU3avlidO671bkLfwbw5XV2P8Pzo0ydy4t2/0eu33xYSOMOD8hTf4CrBtGMSoXfPLchX+J0ruSePw3LZeK0juPJbYzrhkH0io7B3k164hiGvawhOKMLkrQLyVpZg8rHFW7E2uHOL888IBPlNZ1FPzstSJM694fWr6RwpvcJK60+0HCILTBzZLFNdtAzJaohze60T8qBzyh5ZuOg5e7uwQppofEmf2++DYvmySqGBuKaicF1blQjhuHdvCIMvp8whTTfZzI7RldpwtSzL+F1+wkdZ2TBOW2gIF88PBTzD/gpeREAMEbxnJcaJHNHrpzji0gQCS6hdkEeYt9DF/2qPcEC8RM28Hwmr3sdNyht00byAut2k3gufWNtgtOEOFGUwcXWNDbdNbpgBGxEvKkOQsxivJx33iow0Vw5S6SVTrpVq11ysA2Rp7gTfPfktc6zhtXBBC+adRLshf6sG2RfHPZ5EAc4sVZ83yCN00Fk/4kggu40ZTvIEm5g24qtU4KjBrx/BTTH8ifVASAG7gKrnWxJDcU7x8X6Ecczhm3o6YicvsLXWfh3Ch1W0k8x0nXF+0fFxgt4phz8QvypiwCCFKMqXCnqXExjq10beH+UUA7+nG6mdG/Pu0f3LgFcGrl2s0kNNjpmoJ9o4B29CMO8dMT4Q5ox8uitF6fqsrJOr8qnwNbRzv6hSnG5wP+64C7h9lp30hKNtKdWjtdkbuPA19nJ7Tz3zR/ibgARbhb4AlhavcBebmTHcFl2fvYEnW0ox9xMxKBS8btJ+KiEbq9zA4RthQXDhPa0T9TEe69gWupwc6uBUphquXgf+/FrIjweHQS4/pduMe5ERUMHUd9xv8ZR98CxkS4F2n3EUrUZ10EYNw7BWm9x1GiPssi3GgiGRDKWRYZfXlON+dfNbM+GgIwYdwAAAAASUVORK5CYII=")marker-icon.png',
    iconSize: [20, 32],
    iconAnchor: [12.5, 41],
    popupAnchor: [0, -41],
    shadowUrl: null,
    shadowSize: null,
    shadowAnchor: null
  });
  // const config = {
  //   backLink: mapState.packageId ? `/check-coverage?packageId=${mapState.packageId}` : '/check-coverage',
  // };

  const _handleMarkerMove = ({ target }) => {
    const { zoom } = mapState;
    const geocoder = L.Control.Geocoder.nominatim();
    const location = target.getLatLng();
    console.log('123')
    geocoder.reverse(location, zoom, _setLocationName);
    setCoordinate(location);
  }

  const _handleSearchMove = useCallback(e => {
    const { zoom } = mapState;
    const geocoder = L.Control.Geocoder.nominatim();
    const location = { lat: e[0], lng: e[1] };

    geocoder.reverse(location, zoom, _setLocationName);
    console.log('456')
    setCoordinate(location);
  }, [mapState])

  const _handleZoom = ({ target }) => setMapState({ ...mapState, zoom: target.getZoom() });

  const _setLocationName = res => {
    if (!res[0]) return null;

    const { name, properties } = res[0];
    const { city, postcode, road } = properties.address;

    setLocationName({
      name: name.split(',')[0],
      street: [road, city].filter(Boolean).join(', '),
      zip: postcode,
    });
    setMapState({...mapState, ['addressDescription']: `${name.split(',')[0]}, ${[road, city].filter(Boolean).join(', ')}, ${postcode}`})
  }

  const handleConfirm = () => {
    const { lat, lng } = coordinate;

    setSubmitting(true);
    setMapState({ ...mapState, modal: false });
    const jsonParams  = {
      "latitude": lat + '',
      "longitude": lng + '',
      "province": mapState.province,
      "provinceCode": mapState.provinceCode,
      "city": mapState.city,
      "cityCode": mapState.cityCode,
      "district": mapState.district,
      "districtCode": mapState.districtCode,
      "street": mapState.street,
      "streetCode": mapState.streetCode,
      "postalCode": mapState.coverageZipCode,
      "rtrw": mapState.rtrw,
      "addressDescription": mapState.addressDescription
      };
      console.log('jsonParams',jsonParams)
      console.log('getToken', getToken(),  getGuestToken())
      if ((!getToken() || getToken() === 'null') && checkGuestTokenExpireTime()) {
        console.log('guest token expired')
        guestUser()
          .then(({ data }) => {
            if(data.ok) {
              setGuestToken(data.data.token)
              const guestData = JSON.parse(atob(data.data.token.split('.')[1]));
              setGuestTokenExpireTime(guestData.exp)
              funCheckCoverage(jsonParams)
            }
          });
      } else {
        funCheckCoverage(jsonParams)
      }
  }

  const funCheckCoverage = (jsonParams) => {
    checkCoverage(jsonParams)
    .then(({ data }) => {
      console.log('data',data)
      if(data.status === 200 || data.status === 201) {
        const { status } = data.data;
        if (status === 'PT1' && mapState.packageId) {
          history.push('/personal-data');
        } else if (status === 'PT1') {
          history.push('/check-coverage/result-pt1');
        } else if (status === 'PT2') {
          history.push('/check-coverage/result-pt2');
        } else if (status === 'PT3') {
          history.push('/check-coverage/result-pt3');
        }
      } else if(data.status === 503) {
        setErrorTitle('ODP is not available')
        setErrorDes('Please select another address')
        setErrorDialog(true)
      } else {
        setErrorTitle('Service Error')
        setErrorDes('A service error has occured while processing your request')
        setErrorDialog(true)
      }
      setSubmitting(false);
    }).catch((e) => {
      setErrorTitle('Service Error')
      setErrorDes('A service error has occured while processing your request')
      setErrorDialog(true)
      console.error(e); setSubmitting(false)
    });
  }
  
  const onClickSubmit = e => {
    e.preventDefault();
    mapState.coverageZipCode === locationName.zip ? handleConfirm(e) : modal();
  };

  const modal = () => setMapState({ ...mapState, modal: !mapState.modal });

  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
    
    if(mapState.province) {
      const geocoder = L.Control.Geocoder.nominatim();
      // const searchAddress = `${mapState.district}, ${mapState.city}, ${mapState.province}, ${mapState.street}, ${mapState.coverageZipCode}`
      const searchAddress = `${mapState.district}, ${mapState.city}, ${mapState.province}, ${mapState.coverageZipCode}`
      setLocationName({
        name: mapState.street,
        street: `${mapState.province}, ${mapState.district}`,
        zip: `${mapState.city} - ${mapState.coverageZipCode}`,
      });
      console.log('searchAddress', searchAddress)
      geocoder.geocode(searchAddress, res => {
        if(res && res.length > 0) {
          // locationName, setLocationName] = useState({ name: '', street: '', zip: '' });
          setLocationName({...locationName, ['name']: mapState.province, ['street']: ` ${mapState.district}, ${mapState.city}`, ['zip']: mapState.coverageZipCode})
          const {lat, lng} = res[0].center
          setCoordinate({lat: lat, lng: lng})
        } else if (navigator.geolocation) {
          setErrorTitle('Location not found')
          setErrorDes('Unable to pin point the location. Please verify your given address')
          setErrorDialog(true)
          // navigator.geolocation.getCurrentPosition(
          //   ({ coords }) => _handleSearchMove([coords.latitude, coords.longitude]),
          //   () => _handleSearchMove([defaultCoordinate.lat, defaultCoordinate.lng]),
          // );
        }
        console.log('resres', res);
      })
      // googlegeocoder(`key=AIzaSyCCXjZ-LU5FN6PSz7GnJIubchS4uLV6-4U&inputtype=textquery&input=${mapState.coverageZipCode}, ${mapState.province}, ${mapState.district}, ${mapState.city}&fields=formatted_address,geometry,place_id`)
      // .then(response => {
      //   const { data } = response;
      //   if(response.status === 200 && data.candidates.length > 0) {
      //     setCoordinate({lat: data.candidates[0].geometry.location.lat, lng: data.candidates[0].geometry.location.lng})
      //   }
      // }).catch(error => {
      //   console.error(error);
      // });
    } 
  }, [trigger, navCase, pageCase, mapState.province]);
  // ?key=AIzaSyCCXjZ-LU5FN6PSz7GnJIubchS4uLV6-4U&inputtype=textquery&input=80852, Bali, Ababi, Amlapura&fields=formatted_address,geometry,place_id
  console.log('locationName', locationName)
  return (
    <>
      <div className="container container-md">
        <div className="box">
          <header className="header-light-w-text">
            <div className="header-actions">
              <Link to="#" className="btn btn-link" onClick={e => { e.preventDefault(); history.goBack(); }}>
                <i className="fa fa-arrow-left" />
              </Link>
            </div>
            <div className="heading delayp1">
              <h1>Place In Point</h1>
            </div>
          </header>
          <section>
            <Map
              center={coordinate}
              zoom={mapState.zoom}
              style={{ height: "600px", marginBottom: "1.5rem" }}
              zoomControl
              scrollWheelZoom={false}
              touchZoom
              dragging
              trackResize
              onZoomEnd={_handleZoom}
            >
              <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoic25kYW50aWtvZGUiLCJhIjoiY2p6cnZ0NTcxMWM1YzNtbnBlZmJ2NzlkcyJ9.dZd20RU0nnsrWS7yprgGyw"
                id="mapbox.streets"
                accessToken="pk.eyJ1Ijoic25kYW50aWtvZGUiLCJhIjoiY2p6cnZ0NTcxMWM1YzNtbnBlZmJ2NzlkcyJ9.dZd20RU0nnsrWS7yprgGyw"
              />
              {(
                <Marker draggable={!submitting} position={coordinate} icon={myIcon} onMoveEnd={_handleMarkerMove}>
                  <Popup>{locationName.name}</Popup>
                </Marker>
              )}
              {submitting || <ReactLeafletSearchComponent
                markerIcon={myIcon}
                inputPlaceholder="Enter Location"
                openSearchOnLoad
                closeResultsOnClick
                className="form-control"
                mapStateModifier={_handleSearchMove}
              />}
            </Map>
            <Form className="form-map-coverage">
              <Form.Group>
                <div className="coverage-pin-point active mb-0">
                  <img
                    src={images.common["bg_location-pin.png"]}
                    className="img-fluid bg_pin invisible"
                    alt="Location"
                  />
                  <img
                    src={images.common["ic_location_pin.png"]}
                    className="ic_location-pin"
                    alt="Location"
                  />
                  <div className="coverage-pin-point-info">
                    <h3>{locationName.name}</h3>
                    <p>{locationName.street}</p>
                    <p>{locationName.zip}</p>
                  </div>
                </div>
              </Form.Group>
              <Form.Group>
                <div className="btn-placeholder" style={{ marginTop: "1rem" }}>
                  <button className="btn btn-primary btn-block" disabled={submitting} onClick={onClickSubmit}>{submitting ? 'Checking...' : 'Confirm'}</button>
                </div>
              </Form.Group>
            </Form>
          </section>
        </div>
      </div>
      {/* MODAL  */}
      <Modal centered={true} show={mapState.modal} onHide={modal}>
        <div className="box box-main mb-0">
          <section>
            <h1>Are you sure this is the right location pin point?</h1>
            {locationName.zip && <p>Your chosen location is placed within {locationName.zip} zip code area</p>}
          </section>
          <div className="btn-placeholder inline">
            <button className="btn btn-block" onClick={modal}> no </button>
            <button className="btn btn-primary btn-block" onClick={handleConfirm}> yes </button>
          </div>
        </div>
      </Modal>
      {/* MODAL  */}
      {/* Address not found */}
      <Modal centered={true} show={errorDialog} onHide={() => setErrorDialog(false)}>
        <div className="box box-main mb-0">
          <section>
              <h1>{errorTitle}</h1>
              <p>{errorDes}</p>
          </section>
          <div className="btn-placeholder inline">
            <button className="btn btn-primary btn-block" onClick={e => {e.preventDefault(); setErrorDialog(false)}}> ok </button>
          </div>
        </div>
      </Modal>
    </>
  );
}
export default CheckCoverageMap;