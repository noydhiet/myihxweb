import React from 'react';
import renderer from 'react-test-renderer';
import CheckCoverageMap from '../index';
import PageContextProvider from '../../../context/PageContext';

jest.mock('react-leaflet', () => ({
  Map: () => null,
  TileLayer: () => null,
  Marker: () => null,
  Popup: () => null,
  withLeaflet: () => () => null,
}));
jest.mock('react-leaflet-search', () => ({
  ReactLeafletSearch: null,
}));
jest.mock('react-bootstrap');
jest.mock('react-router-dom');
jest.mock('../../../utils/requireContext');

const wrapper = props => renderer.create(
  <PageContextProvider>
    <CheckCoverageMap {...props} />
  </PageContextProvider>);

describe('CheckCoverageMap', () => {
  test('render', () => {
    const result = wrapper().toJSON();
    expect(result.type).toBe('div');
  });
});
