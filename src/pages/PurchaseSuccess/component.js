import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const PaymentSuccess = () => {
    const { trigger, navCase, pageCase, brand, common, sample, imgSignature } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    const images = { brand, common, sample } ;
    const coverStyle = {
        background: `url(${images.common["bnr_blue_2nd.png"]}) no-repeat center`,
        border: "none"
    };
    const localImgSignature = localStorage.getItem('imgSignature');

    return (
            <section>
                <div className="container container-sm">
                    <div className="box box-main animated fadeInUp">
                        <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                            <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
                        </header>
                        <section className="animated fadeInUp delayp2">
                            <h1>Transaction Success</h1>
                            <p className="mb-0 help-block">Monday, 10th June 2019</p>
                            <p className="help-block">Order ID 123412341234</p>
                        </section>
                        <section className="py-main-sm pb-0">
                            <div className="subheading animated fadeInUp delayp3">
                                <p className="text-left">What's Next?</p>
                            </div>
                            {/* <div className="list-group list-group-banner animated fadeInUp delayp4">
                                <Link
                                to="/installation/schedule"
                                className="list-group-item light w-arrow w-bg-cover"
                                style={coverStyle}
                                >
                                <img src={images.common["graphic_calendar.png"]} className="w-75px" />
                                <div className="list-group-item-content">
                                    <h5 className="title w-md-up-75 mb-0">Please set a schedule with a technician to activate your package</h5>
                                </div>
                                </Link>
                            </div> */}
            {
              localImgSignature ? (
                <div className="list-group list-group-banner animated fadeInUp delayp4">
                  <Link
                    to="/installation/schedule"
                    className="list-group-item light w-arrow w-bg-cover"
                    style={coverStyle}
                  >
                    <img alt="" src={images.common["ic_selection_schedule.png"]} className="w-75px" />
                    <div className="list-group-item-content">
                      {/* <h5 className="title w-md-up-75 mb-0">Upload your KTP and signature to finalize the order</h5> */}
                      <h5 className="title w-md-up-75 mb-0">Please set a schedule with a technician to activate your package</h5>
                    </div>
                  </Link>
                </div>
              ) : (
                <div className="list-group list-group-banner animated fadeInUp delayp4">
                  <Link
                    to="/personal-data"
                    className="list-group-item light w-arrow w-bg-cover"
                    style={coverStyle}
                  >
                    <img alt="" src={images.common["icon_ic_selection_id.png"]} className="w-75px" />
                    <div className="list-group-item-content">
                      {/* <h5 className="title w-md-up-75 mb-0">Upload your KTP and signature to finalize the order</h5> */}
                      <h5 className="title w-md-up-75 mb-0">Upload your KTP to finalize the order</h5>
                    </div>
                  </Link>
                </div>
              )
            }
            <div className="subheading animated fadeInUp delayp5">
              <p className="text-left">Package</p>
            </div>
            <Link
              to="#"
              className="card card-packages w-arrow animated fadeInUp delayp6"
            >
              <div className="card-header">
                <div className="badges-list">
                  <div className="badges badge-red left">
                    <h3>100</h3>
                    <span>Mbps</span>
                  </div>
                </div>
                <ul className="list-unstyled">
                  <li>Internet unlimited</li>
                  <li>203 USee TV channels</li>
                  <li>1000 minutes call</li>
                </ul>
              </div>
              <div className="card-body w-btn">
                <h3>
                  Rp1.890.000<small> / bln</small>
                </h3>
                <small className="help-block">Billing starts the month after installation</small>
                <span className="btn btn-primary">Details</span>
              </div>
            </Link>
            <div className="subheading animated fadeInUp delayp7">
              <p className="text-left">Your Information</p>
            </div>
            <div className="row">
              <div className="col-md-6">
                <div className="form-group animated fadeInUp delayp8">
                  <label>Full Name</label>
                  <p className="mb-0 ml-0 text-left">John Soetanto</p>
                </div>
              </div>
              <div className="col-md-6">
                <div className="form-group animated fadeInUp delayp8">
                  <label>KTP Number</label>
                  <p className="mb-0 ml-0 text-left">123123123123123</p>
                </div>
              </div>
            </div>
            <div className="form-group animated fadeInUp delayp8">
              <label>Address</label>
              <p className="mb-0 ml-0 text-left">Jl. Jurang Mangu Barat no. 8, Tangerang Selatan, Banten, 15223</p>
            </div>
            <div className="btn-placeholder  animated fadeInUp delayp9">
              <Link to="/home" className="btn btn-primary btn-block">Back To Home</Link>
            </div>
          </section>
        </div>
      </div>
      <div className={"card card-notif "}>
        <p className="notif-text">Appointment successfully added to calendar</p>
      </div>
    </section>
  )
}
export default PaymentSuccess;
