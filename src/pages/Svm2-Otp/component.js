import React from 'react';
import PropTypes from 'prop-types';
import { CardNotif } from '../../include/CardPackage';
import ContainerBase from '../../include/ContainerBase';
import OtpInput from '../../include/OtpInput';
import { verifyOtp, sendOtp } from '../../services';
import { setExpireTime, setToken, setUserData } from '../../utils/storage';
import { PageContext } from './../../context/PageContext';

export default class Svm2Otp extends React.Component {
  static contextType = PageContext;
  state = {
    alert: false,
    attempt: 3,
    mobile: localStorage.getItem('mobile') || '',
    email: localStorage.getItem('email') || '',
    otpChannel: localStorage.getItem('otpChannel') || '',
    timer: '179',
  }

  componentDidMount() {
    this.context.trigger(1);
    this.context.pageCase("w-bg");
    this.myInterval = setInterval(() => {
      this.setState({ timer: this.state.timer - 1 })
    }, 1000);
  }

  validateOTP(otp) {
    const { otpChannel, mobile, email } = this.state;

    verifyOtp(otpChannel, mobile, email, otp)
      .then(({ data }) => {
        if (data.ok) {
          const userData = JSON.parse(atob(data.data.token.split('.')[1]));
          userData.userName = data.data.userName;
          
          setToken(data.data.token);
          setUserData(userData);
          setExpireTime(userData.exp);
          this.trigger('returnSuccess');          
        } else {
          this.trigger('returnFailed');
        }
      })
      .catch(() => { this.trigger('returnFailed'); });
  }

  resendOtp() {
    const { otpChannel, mobile, email } = this.state;

    sendOtp(otpChannel, mobile, email)
      .then(({ data }) => { if (!data.ok) this.setState({ notif: data.message }); })
      .catch(({ message }) => { this.setState({ notif: message }); });
  }

  pathSuffix = () => {
    const { match: { params } } = this.props;
    
    if (!params.pathSuffix) return '';
    return params.pathSuffix.replace(/--/g, '/').substring(1);
  }

  trigger = (mode) => {
    const { history } = this.props;

    switch (mode) {
      case 'returnFailed':
        this.setState({ alert: true, attempt: this.state.attempt - 1 });
        break;

      case 'returnSuccess':
        history.push(`/${this.pathSuffix()}`);
        break;

      case 'stopTimer':
        clearInterval(this.myInterval);
        break;

      case 'startTimer':
        this.resendOtp();
        this.setState({ timer: 179 });
        this.myInterval = setInterval(() => {
          this.setState({ timer: this.state.timer - 1 })
        }, 1000);
        break;

      default:
        break;
    }
  }

  render() {
    const { alert, attempt, mobile, notif, timer } = this.state;
    let displayTimer = true
    let displayResend = false
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    //Timer
    if (timer === 0) {
      this.trigger('stopTimer');
      displayTimer = false
      displayResend = true
    }

    const mobile4Digits = mobile.slice(mobile.length - 4, mobile.length);
    const config = {
      alert: attempt > 0 ? 'The code you entered is incorrect.' : 'You have exceeded max allowed incorrect code input. Please request after 1 hour.',
      displayTimer: displayTimer && attempt > 0 ? 'block' : 'none',
      displayResend: displayResend && attempt > 0 ? 'block' : 'none',
      displayInvalid: {
        display: alert ? 'block' : 'none'
      },
      colorAlert: alert ? '#ee3124' : '#000000',
      notifActive: notif ? 'active' : '',
      min: `${Math.floor(timer / 60)}`.padStart(2, '0'),
      sec: `${timer % 60}`.padStart(2, '0'),
    };
    const containerProps = {
      image: { alt: 'Icon OTP', src: images.common['ic_header_verification.png'], },
      subtitle: `Enter the code from the voice call to your IndiHome home number ****1234`,
      title: 'Enter Code',
    };

    return (
      <>
        <ContainerBase {...containerProps}>
          <form className="needs-validation" id="form">
            <div className="otp-code-container animated fadeInUp delayp2">
              <OtpInput names={['otp1', 'otp2', 'otp3', 'otp4']} onSubmit={this.validateOTP.bind(this)} />
              <div className="invalid-feedback text-center" style={config.displayInvalid}>{config.alert}</div>
            </div>
          </form>
          <div className="countdown-timer text-center animated fadeInUp delayp3">
            <div className="btn btn-link btn-resend-timer disabled" style={{ display: config.displayTimer }}>Resend code in <span id="timer"> {config.min}:{config.sec} </span></div>
            <div className="btn-placeholder btn-resend">
              <button className="btn btn-link" style={{ display: config.displayResend, margin: 'auto' }} onClick={() => this.trigger("startTimer")}>Resend Code</button>
            </div>
          </div>
        </ContainerBase>
        <CardNotif active={config.notifActive} textTop={notif} />
      </>
    )
  }
}

Svm2Otp.defaultProps = {
  pageCase: () => {},
  trigger: () => {},
};

Svm2Otp.propTypes = {
  history: PropTypes.object,
  pageCase: PropTypes.func,
  trigger: PropTypes.func,
};
