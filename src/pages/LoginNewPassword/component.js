import React from 'react';
import { Form, Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ContainerBase from '../../include/ContainerBase';
import { login } from '../../services';
import { setExpireTime, setToken, setUserData } from '../../utils/storage';
import { PageContext } from './../../context/PageContext';

export default class LoginNewPassword extends React.Component {
  static contextType = PageContext;
  state = {
    icon: false,
    password: '',
    email: localStorage.getItem('email'),
    alert: '',
    modal: false,
  }

  componentDidMount() {
    this.context.trigger(1);
    this.context.pageCase("w-bg");
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  trigger = (type) => () => {
    this.setState({
      [type]: !this.state[type]
    })
  }

  submit = (e) => {
    e.preventDefault();

    const { history } = this.props;
    const { email, password } = this.state;
    login({ email, password })
      .then(({ data }) => {
        if (data.ok) {
          const userData = JSON.parse(atob(data.data.token.split('.')[1]));
          userData.userName = data.data.userName;

          setToken(data.data.token);
          setUserData(userData);
          setExpireTime(userData.exp);
          history.push(`/${this.pathSuffix()}`)
        } else if (data.status === 404) {
          this.trigger('modal')();
        } else {
          this.setState({ alert: 'Wrong password' });
        }
      })
      .catch(() => { this.setState({ alert: 'Wrong password' }); });
  }

  pathSuffix = () => {
    const { match: { params } } = this.props;

    if (!params.pathSuffix) return '';
    return params.pathSuffix.replace(/--/g, '/').substring(1);
  }

  render() {
    const { icon, modal, password, alert } = this.state
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    const config = {
      disabledBtn: password ? '' : 'disabled',
      iconPassword: icon ? "fas fa-eye" : "fas fa-eye-slash",
      typePassword: icon ? "text" : "password",
      alert: alert ? 'block' : 'none',
      justifyIcon: alert ? '35%' : '18%'
    }
    const containerProps = {
      image: { alt: 'Icon Login Email', src: images.common['ic_header_password.png'], },
      title: 'New Password',
      subtitle: 'For your security, please create a new password to protect your account'
    };

    return (
      <>
        <ContainerBase {...containerProps}>
          <Form onSubmit={this.submit}>
            <Form.Group className="form-custom w-preppend">
              <Form.Label> New Password </Form.Label>
              <Form.Control
                autoFocus
                type={config.typePassword}
                name="password"
                id="password"
                value={password}
                onChange={this.handleChange}
                placeholder="Enter Your New Password"
              />
              <div className="invalid-feedback" style={{ display: config.alert }}>{alert}</div>
              <span onClick={this.trigger('icon')} className="preppend-icon" style={{ bottom: config.justifyIcon }}><i className={config.iconPassword}></i> </span>
              
            </Form.Group>
            <small className="help-block">Min. 6 characters with a mix of letters and number</small>
            <div className="btn-placeholder">
              <button type="submit" className={"btn btn-primary btn-block animated fadeInUp delayp4 " + config.disabledBtn} disabled={config.disabledBtn} onClick={this.submit}>Create New Password</button>
            </div>
          </Form>
        </ContainerBase>
        {/* MODAL  */}
        <Modal show={modal} onHide={this.trigger('modal')} className="modal-centered">
          <div className="box box-main mb-0">
            <section>
              <h1>Account not yet registered</h1>
              <p>Looks like this account is not yet registered! Do you want to register instead?</p>
            </section>
            <div className="btn-placeholder inline bottom">
              <button className="btn btn-block w-border" onClick={this.trigger('modal')}> try again </button>
              <Link to="/register/account-details" className="btn btn-primary btn-block"> register </Link>
            </div>
          </div>
        </Modal>
        {/* MODAL  */}
      </>
    )
  }
}

LoginNewPassword.defaultProps = {
  pageCase: () => { },
  trigger: () => { },
};
