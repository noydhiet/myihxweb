import React, { Component, useState } from 'react';
import { Link } from 'react-router-dom';
import { Modal, Form } from 'react-bootstrap';
import { NavMobile } from './../../include/NavMobile'
import { PageContext } from './../../context/PageContext';
import { getToken } from '../../utils/storage';
import { userSupportTicket, supportTicketCategories, supportTicketIssues, ticketSolution } from '../../services';

class Help extends Component {
  static contextType = PageContext;
  state = {
    modalGamas: false,
    modalIsolir: false,
    modalBill: false,
    modalReportIssues: false,
    modalTicketHistory: false,
    listTicketHistory: [],
    categoryIndex: "",
    categoriesArray: undefined,
    issueIndex: "",
    issuesArray: undefined,
    isLoadingTickets: false,
    isLoadingSolution: false,
    modalServiceUnavailable: false
  }
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.footerCase("w-nav-bottom");
    this.context.pageCase("w-bg");
    const {history, location: {state}} = this.props;
    if(history.action !== 'POP' && state && state.redirect === 'fisik_detail') {
      this.getTicketHistory();
    }
    this.fetchCategory()
  }

  fetchCategory = () => {
    supportTicketCategories()
    .then( response => {
      if(response.status === 200) {
        this.setState({
          categoriesArray: response.data.data
        })
      } else {
        this.setState({
          categoriesArray: null
        })
      }
      console.log('category', response)
    })
    .catch( error => {
      this.setState({
        categoriesArray: null
      })
      console.error(error)
    });
  }

  fetchIssues = categoryId => {
    supportTicketIssues(categoryId)
    .then( response => {
      if(response.status === 200) {
        this.setState({
          issuesArray: response.data.data
        })
      } else {
        this.setState({
          issuesArray: null
        })
      }
      console.log('fetchIssues', response)
    }).catch( error => {
      this.setState({
        issuesArray: null
      })
      console.error(error)
    });
  }

  componentWillUnmount() {
    this.context.footerCase("");
  }
  reportIssue = (e) => {
    e.preventDefault();
    const { categoriesArray, categoryIndex, issueIndex, issuesArray, isLoadingSolution } = this.state;
    if(!isLoadingSolution) {
      this.setState({isLoadingSolution: true});
      const { history } = this.props;
      if(issuesArray[issueIndex].solutions.length > 0) {
        const solutionIdArray = { solutionIds: issuesArray[issueIndex].solutions };
        ticketSolution(solutionIdArray)
        .then( response => {
          console.log('responsewqwq', response)
          if(response.data.status === 200) {
            if(response.data.data.length > 0) {
              const payload = {
                solutionIndex: 0,
                solutions: response.data.data,
                categoryId: categoriesArray[categoryIndex].categoryId,
                issue: issuesArray[issueIndex].issue,
                issueIndex: issueIndex,
                issuesArray: issuesArray
              };
              history.push({ pathname: '/help/internet/detail/1', state: payload });
            } else {
              const payload = { 
                categoryId: categoriesArray[categoryIndex].categoryId,
                issueIndex: issueIndex,
                issuesArray: issuesArray
              };
              history.push({ pathname: '/help/internet/detail/ticket', state: payload });
            }
          } else {
            this.setState({modalServiceUnavailable: true})
          }
          this.setState({isLoadingSolution: false});
        })
        .catch( error => {
          this.setState({modalServiceUnavailable: true})
          this.setState({isLoadingSolution: false});
          console.error(error)
        });
      } else {
        const payload = { 
          categoryId: categoriesArray[categoryIndex].categoryId,
          issueIndex: issueIndex,
          issuesArray: issuesArray
        };
        history.push({ pathname: '/help/internet/detail/ticket', state: payload });
      }
    }
  }
  handleInputChange = (e) => {
    if(e.target.value !== '' && 'categoryIndex' === e.target.id) {
      this.setState({
        [e.target.id]: e.target.value, issueIndex: '', issuesArray: undefined
      });
      const { categoriesArray } = this.state;
      this.fetchIssues(categoriesArray[e.target.value].categoryId);
    } else {
      this.setState({
        [e.target.id]: e.target.value
      });
    }
  }
  getTicketHistory = () => {
    if (getToken() && !this.state.isLoadingTickets) {
        this.setState({isLoadingTickets: true, modalTicketHistory: true}, () => {
            userSupportTicket(true).then(({ data }) => { 
              if(data.ok) {
                console.log('data.data.data', data.data.data)
                this.setState({ listTicketHistory: data.data.data, isLoadingTickets: false }, () => this.triggerPage('ticketHistory'));
              } else {
                this.setState({ isLoadingTickets: false })
              }
            }).catch(err => {
                this.setState({isLoadingTickets: false});
                console.error(err.response);
            })
        });
    }
  }
  triggerPage = (mode) => {
    switch (mode) {
      case "gamas":
        this.setState({
          modalGamas: !this.state.modalGamas
        });
        break;

      case "isolir":
        this.setState({
          modalIsolir: !this.state.modalIsolir
        });
        break;

      case "bill":
        this.setState({
          modalBill: !this.state.modalBill
        });
        break;

      case "reportIssues":
        if (getToken()) {
          this.setState({
            modalReportIssues: !this.state.modalReportIssues
          });  
        } else {
          this.context.modal('modalSubscribe');
        }
        break;

      case "ticketHistory":
        const { listTicketHistory } = this.state;
        const { history } = this.props;
        console.log('hiii', listTicketHistory)
        const ticket = listTicketHistory && listTicketHistory.length ? listTicketHistory[0] : '';
        listTicketHistory && listTicketHistory.length === 1 
        ? 
        history.push({pathname: `/history/ticket-1`, state: {ticket: ticket}})
        :
        this.setState({ modalTicketHistory: true }) ;
        break;
      case "close":
        this.setState({
          modalGamas: false,
          modalIsolir: false,
          modalBill: false,
          modalReportIssues: false,
          modalTicketHistory: false
        });
        break;

      default:
        return null

    }
  }
  _handleStatus = (status) => {
    return 'review';
    // switch (status) {
    //   case 'RECEIVED':
    //     return 'review';
    //   case 'IN_PROGRESS':
    //     return 'on_progress';
    //   case 'SOLVED':
    //     return 'done';
    //   default:
    //     return '';
    // }
  }

  closeAlert = () => {
    setTimeout(() => {
      this.context.pageCase("submitHelpTicket")
    }, 2000) 
  }

  render() {
    const { modalGamas, modalIsolir, modalBill, modalReportIssues, issueIndex, categoryIndex, modalTicketHistory, listTicketHistory, isLoadingTickets, categoriesArray, issuesArray, isLoadingSolution, modalServiceUnavailable } = this.state;
    const { common, brand, sample, condition, notifProblemResolved, notifHelpTicket, checkToken } = this.context;
    const images = { common, brand, sample };
    const disabledSubmit = issueIndex.length === 0 || categoryIndex.length === 0 || isLoadingSolution ? "disabled" : "";
    const disabledIssue = categoryIndex.length < 1 ? "disabled" : "";
    const notifViewProblemResolved = notifProblemResolved ? "active" : "";
    const notifViewHelpTicket = notifHelpTicket ? "active" : "";
    const Loading = ({ images }) => {
      return (
        <span><i className="fa fa-circle-notch fa-spin"></i></span>
      )
    }
    (notifHelpTicket && this.closeAlert())
    const ticketHistory = <Link to="#" onClick={(e) => { e.preventDefault(); this.getTicketHistory(); }} className="nav-link nav-link-help w-notif">
    {" "}
   <i className="far fa-ballot-check"></i>
    {" "}
  </Link>
    
    return (
      <>
        <div>
          <NavMobile condition={condition} helpPage="active" />
          <div className="cover cover-sm cover-bg-white-sm cover-help">
            <div className="header-actions d-block d-md-none">
              <Link
                to="/shop/"
                className="btn btn-link text-black"
              >
                <i className="fa fa-arrow-left"></i>
              </Link>
            </div>
            <div className="container">
              <nav aria-label="breadcrumb" className="d-none d-md-block">
                <ol className="breadcrumb animated fadeInUp">
                  <li className="breadcrumb-item"><Link to="/home"><i className="fa fa-arrow-left mr-1"></i>Home</Link></li>
                </ol>
              </nav>
              <div className="cover-content-w-links">
                <div className="cover-content">
                  <h1 className="cover-title animated fadeInUp delayp1">Help</h1>
                  <p className="cover-text animated fadeInUp delayp2">Having problems? Report your issue and our team will be there to help you</p>
                  <div className="btn-placeholder d-none d-md-block animated fadeInUp delayp3">
                    <Link to="#" className="btn btn-primary mr-4" onClick={() => this.triggerPage("reportIssues")}>Report Issues</Link>
                    <Link to="#" className="btn btn-primary" >Reset Modem</Link>
                  </div>
                </div>
                <div>

                  <ul className="help-nav ml-auto">
                    <li className="nav-item">
                      <Link to="#" className="nav-link nav-link-help">
                        {" "}
                        <i className="fas fa-search"></i>{" "}
                      </Link>
                    </li>
                    {
                      checkToken() && 
                      (<li className="nav-item">
                        {ticketHistory}
                      </li>)
                    }
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <SectionContent images={images} />
          <Modal show={modalReportIssues} onHide={() => this.triggerPage('close')} centered={true}>
            <div className="modal-body p-box">
              <Link to="#" className="close animated fadeInUp" onClick={() => this.triggerPage('close')}>
                <span><i className="far fa-times text-primary"></i></span>
              </Link>
              <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
                <h3>Report Issues</h3>
                <p>We're sorry to hear that you're having difficulties. Report your issue here and our team wil get in contact with you to fix.</p>
              </div>
              <Form>
                <Form.Group>
                  <Form.Label>Category</Form.Label>
                  <Form.Control as="select"
                    className="custom-select"
                    id="categoryIndex"
                    value={categoryIndex}
                    onChange={this.handleInputChange}
                  >
                    {
                    categoriesArray === undefined
                    ?
                    <option value="" disabled>Loading...</option>
                    :
                    (categoriesArray === null ? <option disabled>No response data</option> : <option value="">Choose One</option>)
                    }
                    {
                      (categoriesArray && categoriesArray.length > 0)
                      ?
                      categoriesArray.map( (item, index) => <option key={index} value={index}>{item.category}</option> )
                      :
                      (null)
                    }
                  </Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Issue</Form.Label>
                  <Form.Control as="select"
                    className="custom-select"
                    id="issueIndex"
                    value={issueIndex}
                    onChange={this.handleInputChange}
                    disabled={ disabledIssue === 'disabled' ? true : false } >
                    {
                      issuesArray === undefined && categoryIndex.length > 0
                      ?
                      <option value="" disabled>Loading...</option>
                      :
                      (issuesArray === null ? <option disabled>No response data</option> : <option value="">Choose One</option>)
                    }
                    {
                      issuesArray
                      ?
                      issuesArray.map( (item, index) => <option key={index} value={index}>{item.issue}</option> )
                      :
                      (null)
                    }
                  </Form.Control>
                </Form.Group>
                <div className="btn-placeholder">
                  <Link onClick={e => (this.reportIssue(e))} to="#" className={'btn btn-primary btn-block ' + disabledSubmit}> { isLoadingSolution ? <Loading images={images}/> : 'Next' } </Link>
                </div>
              </Form>
            </div>
          </Modal>
          <Modal show={modalGamas} onHide={() => this.triggerPage('close')}>
            <div className="modal-body p-box">
              <Link to="#" className="close animated fadeInUp" onClick={() => this.triggerPage('close')}>
                <span><i className="far fa-times text-primary"></i></span>
              </Link>
              <div className="heading animated text-center fadeInUp delayp1 pt-sm-down-4">
                <img alt="grfx_warning" src={images.common["grfx_warning.png"]} className="w-100px" />
                <h3>Saat ini area Anda sedang mengalami gangguan massal</h3>
                <p>Hal ini menyebabkan koneksi Anda menjadi lambat dan sedang dalam penanganan tim teknis kami dengan estimasi waktu sampai waktu dengan 20 September 2019. Mohon maaf atas ketidaknyamanannya.</p>
              </div>
              <div className="btn-placeholder">
                <button className="btn btn-primary btn-block" onClick={() => this.triggerPage('close')}>Ok</button>
              </div>
            </div>
          </Modal>
          <Modal show={modalIsolir} onHide={() => this.triggerPage('close')}>
            <div className="modal-body p-box">
              <Link to="#" className="close animated fadeInUp" onClick={() => this.triggerPage('close')}>
                <span><i className="far fa-times text-primary"></i></span>
              </Link>
              <div className="heading animated text-center fadeInUp delayp1 pt-sm-down-4">
                <img alt="grfx_warning" src={images.common["grfx_warning.png"]} className="w-100px" />
                <h3>Saat ini layanan Anda terisolir</h3>
                <p>Agar dapat digunakan kembali, silahkan melakukan pembayaran tagihan.</p>
              </div>
              <div className="row">
                <div className="col-6">
                  <div className="btn-placeholder">
                    <button className="btn btn-transparent btn-block" onClick={() => this.triggerPage('close')}>Cancel</button>
                  </div>
                </div>
                <div className="col-6">
                  <div className="btn-placeholder">
                    <button className="btn btn-primary btn-block" onClick={() => this.triggerPage('close')}>Bayar</button>
                  </div>
                </div>
                <div className="col-12">
                  <div className="btn-placeholder mt-3">
                    <button className="btn btn-block" onClick={() => this.triggerPage('close')}>Bantuan</button>
                  </div>
                </div>
              </div>
            </div>
          </Modal>
          <Modal show={modalBill} onHide={() => this.triggerPage('close')}>
            <div className="modal-body p-box">
              <Link to="#" className="close animated fadeInUp" onClick={() => this.triggerPage('close')}>
                <span><i className="far fa-times text-primary"></i></span>
              </Link>
              <div className="heading animated text-center fadeInUp delayp1 pt-sm-down-4">
                <img alt="grfx_warning" src={images.common["grfx_warning.png"]} className="w-100px" />
                <h3>Anda belum melakukan pembayaran tagihan</h3>
                <p>Agar layanan dapat digunakan kembali, silahkan melakukan pembayaran tagihan.</p>
              </div>
              <div className="row">
                <div className="col-6">
                  <div className="btn-placeholder">
                    <button className="btn btn-transparent btn-block" onClick={() => this.triggerPage('close')}>Tutup</button>
                  </div>
                </div>
                <div className="col-6">
                  <div className="btn-placeholder">
                    <button className="btn btn-primary btn-block" onClick={() => this.triggerPage('close')}>Bayar</button>
                  </div>
                </div>
                <div className="col-12">
                  <div className="btn-placeholder mt-3">
                    <button className="btn btn-block" onClick={() => this.triggerPage('close')}>Bantuan</button>
                  </div>
                </div>
              </div>
            </div>
          </Modal>
          <Modal show={modalTicketHistory} onHide={() => this.triggerPage('close')} centered={true}>
            {
              isLoadingTickets
              ?
            <div className="btn btn-white btn-block">
                <Loading images={images}/>
              </div>
              : 
              <div className="modal-body p-box">
                <Link to="#" className="close animated fadeInUp" onClick={() => this.triggerPage('close')}>
                  <span><i className="far fa-times text-primary"></i></span>
                </Link>
                <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
                  <h3>Active Issue</h3>
                </div>
                {
                  listTicketHistory && listTicketHistory.length > 1 ? (
                    listTicketHistory.map((ticket, index) => {
                      const status = ticket.ticketStatus.status;
                      return (
                        <div key={index} className="list-group list-group-activity mb-0">
                          <Link
                            to={{pathname: `/history/ticket-1`, state: {ticket: ticket}}}
                            className="list-group-item pr-0 animated fadeInUp delayp2"
                          >
                            <div className="list-group-item-content mb-0">
                              <small className="caption">{ticket.EXTTransactionID}</small>
                              <div className="list-group-item-content-w-status">
                                <p className="title">
                                { 
                              ticket.incidentList && ticket.incidentList.incident.length > 0
                              ?
                              (ticket.incidentList.incident[0].description)
                              :
                              (null)
                              }
                                </p>
                                {/* {ticket.category} */}
                                <p className={`text-status ${this._handleStatus(status)}`}>
                                  {status.replace("_"," ")}
                                </p>
                              </div>
                              {/* { 
                              ticket.incidentList && ticket.incidentList.incident.length > 0
                              ?
                              (<small className="caption">{`${ticket.incidentList.incident[0].description}`}</small>)
                              :
                              (null)
                              } */}
                            </div>
                          </Link>
                        </div>
                      )})
                  ) : (
                    <div className="blank-state animated fadeInUp delayp1">
                      <img alt="" src={images.common["grfx_blank_history.png"]} />
                      <div className="heading">
                        <p className="dark">
                          You don't have any issue report. <br /> Your issue reports will show here when you have one.
                        </p>
                      </div>
                    </div>
                  )
                }
                
                {/* <div className="list-group list-group-activity mb-0">
                  <Link
                    to="/history/activity/problem-report"
                    className="list-group-item pr-0 animated fadeInUp delayp2"
                  >
                    <div className="list-group-item-content mb-0">
                      <small className="caption">#12345667</small>
                      <div className="list-group-item-content-w-status">
                        <p className="title">Billing Issue Report</p>
                        <p className="text-status on_progress">On Progress</p>
                      </div>
                    </div>
                  </Link>
                  <Link
                    to="/history/activity/problem-report"
                    className="list-group-item pr-0 animated fadeInUp delayp3"
                  >
                    <div className="list-group-item-content mb-0">
                      <small className="caption">#1234567</small>
                      <div className="list-group-item-content-w-status">
                        <p className="title">Technical Issue Report</p>
                        <p className="text-status on_progress">On Progress</p>
                      </div>
                    </div>
                  </Link>
                </div> */}

              </div>
            }
            
          </Modal>
          {/* Modal Session Time Out */}
        <Modal show={modalServiceUnavailable} onHide={() => this.setState({modalServiceUnavailable: false})} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <section>
                <h1>Service Error</h1>
                <p>A service error has occured while processing your request</p>
              </section>
              <div className="btn-placeholder inline bottom">
                <Link to="#" className="btn btn-primary btn-block" onClick={e => {e.preventDefault(); this.setState({modalServiceUnavailable: false});}}>Ok</Link>
              </div>
            </div>
          </div>
        </Modal>
        </div>
        {/* <SectionTopic images={images} />
            <SectionFAQ /> */}
        <div className={"card card-notif " + notifViewProblemResolved}>
          <p className="notif-text">We're happy to hear that you resolved your issue!</p>
        </div>
        <div className={"card card-notif " + notifViewHelpTicket}>
          <p className="notif-text">Your ticket has been submitted! Our team will respond to your ticket within 1x24 hours</p>
          <Link to="/">View Progress</Link>
        </div>
      </>
    );
  }
}

export default Help;

const SectionContent = ({ images }) => {
  const [cond, setCond] = useState({
    activeTab: "all"
  })
  const handleClick = arg => {
    setCond({
      activeTab: arg
    })
  }
  const active = {
    all: cond.activeTab === "all" ? "active" : "",
    internet: cond.activeTab === "internet" ? "active" : "",
    tv: cond.activeTab === "tv" ? "active" : "",
    telephone: cond.activeTab === "telephone" ? "active" : "",
  }
  const helpContent = {
    all: {
      title: "All",
      content: ["Why is my internet connection slow?", "I can't access some websites", "How do i buy other channels?", "I got billed more than i'm supposed to", "How to pay my bills?"]
    },
    internet: {
      title: "Internet",
      content: ["Why is my internet connection slow?", "I can't access some websites", "How do i buy other channels?", "I got billed more than i'm supposed to", "How to pay my bills?"]
    },
    tv: {
      title: "Tv",
      content: ["Why is my internet connection slow?", "I can't access some websites", "How do i buy other channels?", "I got billed more than i'm supposed to", "How to pay my bills?"]
    },
    telephone: {
      title: "Telephone",
      content: ["Why is my internet connection slow?", "I can't access some websites", "How do i buy other channels?", "I got billed more than i'm supposed to", "How to pay my bills?"]
    }
  }
  let returnContentHelp;
  switch(cond.activeTab) {
    case "all":
        returnContentHelp = 
        <>
          <div className="subheading">
            <p className="animated fadeInUp delayp1">{helpContent.all.title}</p>
          </div>
          <div className="list-group list-group-activity mb-0">
          {helpContent.all.content.map((value, index)=>{
              const sumDelay = index+1;
            return (
            <Link to="#" className={"list-group-item w-arrow animated fadeInUp delayp" + sumDelay} key={index}>
              <div className="list-group-item-content mb-0">
                <p className="title">{value}</p>
              </div>
            </Link>
            )
          })}
          </div>
        </>
        ;
        break;

      case "internet":
          returnContentHelp = 
          <>
            <div className="subheading">
              <p className="animated fadeInUp delayp1">{helpContent.internet.title}</p>
            </div>
            <div className="list-group list-group-activity mb-0">
            {helpContent.internet.content.map((value, index)=>{
              const sumDelay = index+1;
              return (
              <Link to="#" className={"list-group-item w-arrow animated fadeInUp delayp" + sumDelay} key={index}>
                <div className="list-group-item-content mb-0">
                  <p className="title">{value}</p>
                </div>
              </Link>
              )
            })}
            </div>
          </>
          ;
          break;

        case "tv":
            returnContentHelp = 
            <>
              <div className="subheading">
                <p className="animated fadeInUp delayp1">{helpContent.tv.title}</p>
              </div>
              <div className="list-group list-group-activity mb-0">
              {helpContent.tv.content.map((value, index)=>{
                const sumDelay = index+1;
                return (
                <Link to="#" className={"list-group-item w-arrow animated fadeInUp delayp" + sumDelay} key={index}>
                  <div className="list-group-item-content mb-0">
                    <p className="title">{value}</p>
                  </div>
                </Link>
                )
              })}
              </div>
            </>
            ;
            break;

        case "telephone":
            returnContentHelp = 
            <>
              <div className="subheading">
                <p className="animated fadeInUp delayp1">{helpContent.telephone.title}</p>
              </div>
              <div className="list-group list-group-activity mb-0">
              {helpContent.telephone.content.map((value, index)=>{
                const sumDelay = index+1;
                return (
                <Link to="#" className={"list-group-item w-arrow animated fadeInUp delayp" + sumDelay} key={index}>
                  <div className="list-group-item-content mb-0">
                    <p className="title">{value}</p>
                  </div>
                </Link>
                )
                // history.push({ pathname: '/help/internet/detail/ticket', state: { category: category, issue: issue} });
              })}
              </div>
            </>
            ;
            break;
      
      default:
        return null
  }
  return (
    <div className="container py-main pt-0">
      <div className="row">
        <div className="col-md-4 d-none d-md-block">
          <div className="box box-in-cover box-transparent">
            <div className="list-group mb-0">
              {/* <Link to="/history/activity" className="list-group-item bg-black"> */}
              <Link to="#" className={"list-group-item bg-black " + active.all} onClick={()=>handleClick("all")}>
                <div className="list-group-item-content mb-0">
                  <p className="title mb-0">All</p>
                </div>
              </Link>
              <Link to="#" className={"list-group-item bg-red " + active.internet} onClick={()=>handleClick("internet")}>
                <div className="list-group-item-content mb-0">
                  <p className="title mb-0">Internet</p>
                </div>
                <i className="fas fa-wifi"></i>
              </Link>
              <Link to="#" className={"list-group-item bg-blue " + active.tv} onClick={()=>handleClick("tv")}>
                <div className="list-group-item-content mb-0">
                  <p className="title mb-0">TV</p>
                </div>
                <i className="fas fa-tv"></i>
              </Link>
              <Link to="#" className={"list-group-item bg-green " + active.telephone} onClick={()=>handleClick("telephone")}>
                <div className="list-group-item-content mb-0">
                  <p className="title mb-0">Telephone</p>
                </div>
                <i className="far fa-phone"></i>
              </Link>
            </div>
          </div>
        </div>
        <div className="col-md-8 col-12">
          <div className="box box-in-cover mb-3 mt-sm-down-4 min-h-auto animated fadeInUp">
            {returnContentHelp}
          </div>
         {/* Start - Blank State (Hidden) */}
         <div className="box min-h-auto animated fadeInUp hidden">
            <div className="row">
              <div className="col-md-6 col-12 mx-auto">
                <div className="blank-state">
                  <img alt="" src={images.common["ic_header_failed.png"]} />
                  <div className="heading">
                    <p className="dark">
                      You don't have any active subscription. <br /> Buy your
                      first package to start enjoying IndiHome!
                      </p>
                  </div>
                  <div className="btn-placeholder">
                    <Link
                      to="/shop/internet/package"
                      className="btn btn-primary"
                    >
                      Browse Package
                      </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* End - Blank State */}
          {/* <div className="box box-in-cover mb-3 mt-sm-down-4 min-h-auto animated fadeInUp">
              <div className="subheading">
                <p>Most Frequently Asked Questions</p>
              </div>
              <div className="list-group list-group-activity mb-0">
                <Link to="/help/internet" className="list-group-item w-arrow">
                  <div className="list-group-item-content mb-0">
                    <p className="title">Why is my internet connection slow?</p>
                  </div>
                </Link>
                <Link to="/help/internet" className="list-group-item w-arrow">
                  <div className="list-group-item-content mb-0">
                    <p className="title">I can't access some websites</p>
                  </div>
                </Link>
                <Link to="/help/internet" className="list-group-item w-arrow">
                  <div className="list-group-item-content mb-0">
                    <p className="title">How do i buy other channels?</p>
                  </div>
                </Link>
                <Link to="/help/internet" className="list-group-item w-arrow">
                  <div className="list-group-item-content mb-0">
                    <p className="title">I got billed more than i'm supposed to</p>
                  </div>
                </Link>
                <Link to="/help/internet" className="list-group-item w-arrow">
                  <div className="list-group-item-content mb-0">
                    <p className="title">How to pay my bills?</p>
                  </div>
                </Link>
              </div>
            </div> */}
        </div>
      </div>
    </div>
  );
};
