import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { Modal, Form } from 'react-bootstrap';
import Rewards from './../Rewards';
import Slider from 'react-slick';
import { PageContext } from './../../context/PageContext';

class RewardsVoucher extends Component {
  static contextType = PageContext;
    componentDidMount() {
        this.context.trigger(2);
        this.context.navCase("w-md-up-block");
        this.context.pageCase("l-bg");
    }
    render() {
        const { brand, common, sample } = this.context;
        const images = { brand, common, sample };
        return (
            <div>
                <Rewards vouchersPage="active"/>
                <SectionPackages images={images} triggerPage={this.triggerPage}/>
            </div>
        )
    }
}
export default RewardsVoucher;


class SectionPackages extends Component {
    state = {
        modalVoucherDetail: false,
        modalVoucherDetailWithCode: false,
        modalVoucherDetailWithQrCode: false,
        modalConfirmOrder: false,
        modalVoucherDetailUseVoucher: false,
        modalUseVoucher: false,
        modalOpenQrCode: false,
        modalContent: 1,


        notifRedeem: false,
        voucherCoppied: false,
        voucherRedeemSuccess: false
    }
    triggerPage = (mode) => {
        switch(mode) {
            case "openVoucherDetail":
                this.setState({
                    modalVoucherDetail: !this.state.modalVoucherDetail
                })
                break;

            case "openVoucherDetailUseVoucher":
                    this.setState({
                        modalVoucherDetailUseVoucher: !this.state.modalVoucherDetailUseVoucher
                    })
                    break;

            case "openUseVoucher":
                    this.setState({
                        modalVoucherDetailUseVoucher: false,
                        modalUseVoucher: !this.state.modalUseVoucher
                    })
                    break;

            case "openVoucherDetailWithCode":
                    this.setState({
                        modalVoucherDetailWithCode: !this.state.modalVoucherDetailWithCode
                    })
                    break;

            case "openVoucherDetailWithQrCode":
                    this.setState({
                        modalVoucherDetailWithQrCode: !this.state.modalVoucherDetailWithQrCode
                    })
                    break;

            case "openQrCode":
                    this.setState({
                        modalVoucherDetailWithQrCode: false,
                        modalOpenQrCode: !this.state.modalOpenQrCode
                    })
                    break;

            case "openConfirmOrder":
                this.setState({
                    modalVoucherDetail: false,
                    modalConfirmOrder: !this.state.modalConfirmOrder
                });
                break;

            case "close":
                this.setState({
                    modalVoucherDetail: false,
                    modalConfirmOrder: false,
                    modalVoucherDetailWithCode: false,
                    modalVoucherDetailWithQrCode: false,
                    modalVoucherDetailUseVoucher: false,
                    modalOpenQrCode: false,
                    modalUseVoucher: false
                })
                break;

            case "detailVoucher":
                this.setState({
                    modalContent: 1
                })
                break;

            case "tnc":
                this.setState({
                    modalContent: 2
                })
                break;

            case "RedeemSuccess":
                this.setState({
                    notifRedeem: true,
                    modalConfirmOrder: false
                });
            break;
    
            case "VoucherCoppied":
                this.setState({
                voucherCoppied: true,
                modalVoucherDetailWithCode: false
                });
                break;
    
            case "VoucherRedeemSuccess":
                this.setState({
                voucherRedeemSuccess: true,
                modalUseVoucher: false
                });
                break;
    
            case "closeNotif":
            setTimeout(()=>{
                this.setState({
                notifRedeem: false,
                voucherCoppied: false,
                voucherRedeemSuccess: false
                })
            }, 1000);
            break;

            default:
                return null
        }
    }
    render() {
        const config = {
            carouselTab: {
                className: "tab-list",
                dots: false,
                infinite: false,
                arrows: false,
                speed: 500,
                slidesToShow: 2,
                slidesToScroll: 2,
                responsive: [
                    {
                      breakpoint: 991,
                      settings: {
                        dots: false,
                        slidesToShow: 2,
                        slidesToScroll: 2
                      }
                    },
                    {
                      breakpoint: 575,
                      settings: {
                        dots: false,
                        slidesToShow: 2,
                        slidesToScroll: 2
                      }
                    },
                    {
                      breakpoint: 324,
                      settings: {
                        dots: false,
                        slidesToShow: 2,
                        slidesToScroll: 2
                      }
                    }
                ]
            }
        }
        const { images } = this.props;
        const ReturnTab = this.state.modalContent === 1 ? <DetailVoucher /> : <Tnc />;
        const ReturnTabUseVoucher = this.state.modalContent === 1 ? <DetailVoucherUseVoucher /> : <Tnc />;
        const ReturnTabWithCode = this.state.modalContent === 1 ? <DetailVoucherWithCode triggerPage={this.triggerPage} images={images}/> : <Tnc />;
        const ReturnTabWithQrCode = this.state.modalContent === 1 ? <DetailVoucherWithQrCode images={images} triggerPage={this.triggerPage}/> : <Tnc />;
                
        const configNotif = {
            redeemSuccess: this.state.notifRedeem ? "active" : "",
            voucherCoppied: this.state.voucherCoppied ? "active" : "",
            voucherRedeemSuccess: this.state.voucherRedeemSuccess ? "active" : ""
            }
        if(this.state.notifRedeem === true || this.state.voucherCoppied === true || this.state.voucherRedeemSuccess === true) {
            this.triggerPage("closeNotif");
            }
        return (
            <section className="py-main">
                <div className="container">
                    <div className="row animated fadeInUp delayp5">
                        <div className="col-md-4 col-12 mb-3" onClick={()=>this.triggerPage("openVoucherDetail")}>
                            <div className="img-voucher">
                                <img src={images.common["content_banner_placeholder.png"]} className="img-fluid" alt="voucher" />
                            </div>
                        </div>
                        <div className="col-md-4 col-12 mb-3" onClick={()=>this.triggerPage("openVoucherDetailWithCode")}>
                            <div className="img-voucher">
                                <img src={images.common["content_banner_placeholder.png"]} className="img-fluid" alt="voucher" />
                            </div>
                        </div>
                        <div className="col-md-4 col-12 mb-3">
                            <div className="img-voucher reedemed">
                                <img src={images.common["content_banner_placeholder.png"]} className="img-fluid" alt="voucher" />
                            </div>
                        </div>
                        <div className="col-md-4 col-12 mb-3">
                            <div className="img-voucher expired">
                                <img src={images.common["content_banner_placeholder.png"]} className="img-fluid" alt="voucher" />
                            </div>
                        </div>
                        <div className="col-md-4 col-12 mb-3" onClick={()=>this.triggerPage("openVoucherDetailUseVoucher")}>
                            <div className="img-voucher">
                                <img src={images.common["banner_chattime.png"]} className="img-fluid" alt="voucher" />
                            </div>
                        </div>
                        <div className="col-md-4 col-12 mb-3" onClick={()=>this.triggerPage("openVoucherDetailWithQrCode")}>
                            <div className="img-voucher">
                                <img src={images.common["banner_w_qr.png"]} className="img-fluid" alt="voucher" />
                            </div>
                        </div>
                    </div>
                </div>
                <Modal show={this.state.modalVoucherDetail} onHide={() =>this.triggerPage('close')}>
                    <div className="modal-body p-box">
                        <Link to="#" className="close animated fadeInUp" onClick={() =>this.triggerPage('close')}>
                            <span><i className="far fa-times text-primary"></i></span>
                        </Link>
                        <div className="img-placeholder">
                            <img src={images.common["content_banner_placeholder.png"]} className="img-fluid" alt="voucher" />
                        </div>
                        <section className="shop-tab-list in-modal mt-4 animated fadeInUp delayp3">
                            <Slider {...config.carouselTab}>
                                <div className={this.state.modalContent === 1 ? "tab-item active" : "tab-item"} onClick={()=>this.triggerPage("detailVoucher")}> 
                                    <Link to="#">Detail Voucher</Link> 
                                </div>
                                <div className={this.state.modalContent === 2 ? "tab-item active" : "tab-item"} onClick={()=>this.triggerPage("tnc")}> 
                                    <Link to="#">Terms & Condition</Link>
                                </div>
                            </Slider>
                        </section>
                        {ReturnTab}
                        <div className="btn-placeholder">
                            <button className="btn btn-primary btn-block" onClick={()=>this.triggerPage("openConfirmOrder")}>Redeem with 200 pts</button>
                        </div>
                    </div>
                </Modal>
                <Modal show={this.state.modalVoucherDetailUseVoucher} onHide={() =>this.triggerPage('close')}>
                    <div className="modal-body p-box">
                        <Link to="#" className="close animated fadeInUp" onClick={() =>this.triggerPage('close')}>
                            <span><i className="far fa-times text-primary"></i></span>
                        </Link>
                        <div className="img-placeholder">
                            <img src={images.common["banner_chattime.png"]} className="img-fluid" alt="voucher" />
                        </div>
                        <section className="shop-tab-list in-modal mt-4 animated fadeInUp delayp3">
                            <Slider {...config.carouselTab}>
                                <div className={this.state.modalContent === 1 ? "tab-item active" : "tab-item"} onClick={()=>this.triggerPage("detailVoucher")}> 
                                    <Link to="#">Detail Voucher</Link> 
                                </div>
                                <div className={this.state.modalContent === 2 ? "tab-item active" : "tab-item"} onClick={()=>this.triggerPage("tnc")}> 
                                    <Link to="#">Terms & Condition</Link>
                                </div>
                            </Slider>
                        </section>
                        {ReturnTabUseVoucher}
                        <div className="btn-placeholder">
                            <button className="btn btn-primary btn-block" onClick={()=>this.triggerPage("openUseVoucher")}>Use Voucher</button>
                            <button className="btn btn-orange btn-block disabled" onClick={()=>this.triggerPage("close")}>Expired</button>
                            <button className="btn btn-green btn-block disabled" onClick={()=>this.triggerPage("close")}>Reedemed</button>
                        </div>
                    </div>
                </Modal>
                <Modal show={this.state.modalUseVoucher} onHide={() =>this.triggerPage('close')}>
                    <div className="modal-body p-box">
                        <Link to="#" className="close animated fadeInUp" onClick={() =>this.triggerPage('close')}>
                            <span><i className="far fa-times text-primary"></i></span>
                        </Link>
                        <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
                            <h3>Enter Store Code</h3>
                            <p>Ask your cashier to input the store code to reedem this voucher.</p>
                        </div>
                        <form>
                            <Form.Group>
                                <Form.Control
                                type="text"
                                placeholder="Store Code"
                                />
                            </Form.Group>
                        </form>
                        <div className="btn-placeholder">
                            <button className="btn btn-primary btn-block" onClick={()=>this.triggerPage("VoucherRedeemSuccess")}>Reedem</button>
                        </div>
                    </div>
                </Modal>
                <Modal show={this.state.modalVoucherDetailWithCode} onHide={() =>this.triggerPage('close')}>
                    <div className="modal-body p-box">
                        <Link to="#" className="close animated fadeInUp" onClick={() =>this.triggerPage('close')}>
                            <span><i className="far fa-times text-primary"></i></span>
                        </Link>
                        <div className="img-placeholder">
                            <img src={images.common["banner_2_lines.png"]} className="img-fluid" alt="voucher" />
                        </div>
                        <section className="shop-tab-list in-modal mt-4 animated fadeInUp delayp3">
                            <Slider {...config.carouselTab}>
                                <div className={this.state.modalContent === 1 ? "tab-item active" : "tab-item"} onClick={()=>this.triggerPage("detailVoucher")}> 
                                    <Link to="#">Detail Voucher</Link> 
                                </div>
                                <div className={this.state.modalContent === 2 ? "tab-item active" : "tab-item"} onClick={()=>this.triggerPage("tnc")}> 
                                    <Link to="#">Terms & Condition</Link>
                                </div>
                            </Slider>
                        </section>
                        {ReturnTabWithCode}
                    </div>
                </Modal>
                <Modal show={this.state.modalVoucherDetailWithQrCode} onHide={() =>this.triggerPage('close')}>
                    <div className="modal-body p-box">
                        <Link to="#" className="close animated fadeInUp" onClick={() =>this.triggerPage('close')}>
                            <span><i className="far fa-times text-primary"></i></span>
                        </Link>
                        <div className="img-placeholder">
                            <img src={images.common["banner_w_qr.png"]} className="img-fluid" alt="voucher" />
                        </div>
                        <section className="shop-tab-list in-modal mt-4 animated fadeInUp delayp3">
                            <Slider {...config.carouselTab}>
                                <div className={this.state.modalContent === 1 ? "tab-item active" : "tab-item"} onClick={()=>this.triggerPage("detailVoucher")}> 
                                    <Link to="#">Detail Voucher</Link> 
                                </div>
                                <div className={this.state.modalContent === 2 ? "tab-item active" : "tab-item"} onClick={()=>this.triggerPage("tnc")}> 
                                    <Link to="#">Terms & Condition</Link>
                                </div>
                            </Slider>
                        </section>
                        {ReturnTabWithQrCode}
                    </div>
                </Modal>
                <Modal show={this.state.modalOpenQrCode} onHide={() =>this.triggerPage('close')}>
                    <div className="modal-body p-box">
                        <Link to="#" className="close animated fadeInUp" onClick={() =>this.triggerPage('close')}>
                            <span><i className="far fa-times text-primary"></i></span>
                        </Link>
                        <div className="img-placeholder content-center">
                            <img src={images.common["QR-Code-Standard.jpg"]} className="img-fluid w-200px" alt="voucher" />
                        </div>
                    </div>
                </Modal>
                <Modal show={this.state.modalConfirmOrder} onHide={() =>this.triggerPage('close')}>
                    <div className="modal-body p-box">
                        <Link to="#" className="close animated fadeInUp" onClick={() =>this.triggerPage('close')}>
                            <span><i className="far fa-times text-primary"></i></span>
                        </Link>
                        <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
                            <h3>Confirm Order</h3>
                            <p>Confirm your selection and order</p>
                        </div>
                        <div className="subheading">
                            <p>Product</p>
                        </div>
                        <Link
                            to="#"
                            className="card card-packages w-arrow animated fadeInUp delayp2"
                            >
                            <div className="card-header">
                                <div className="badges-list">
                                <div className="badges badge-voucher left">
                                </div>
                                </div>
                                <div className="heading mb-0">
                                <h6 className="title">3 Kupon Undian Semarak Kebahagiaan</h6>
                                <p className="mb-0">Internet unlimited, 92 USee TV channels, 100 minutes voice call</p>
                                </div>
                            </div>
                            <div className="card-body w-btn">
                                <h3>
                                200 points
                                </h3>
                            </div>
                        </Link>
                        <div className="btn-placeholder animated fadeInUp delayp5">
                            <div className="row">
                                <div className="col-6">
                                    <button className="btn btn-transparent btn-block" onClick={()=>this.triggerPage("close")}>Cancel</button>
                                </div>
                                <div className="col-6">
                                    <button className="btn btn-primary btn-block" onClick={()=>this.triggerPage("RedeemSuccess")}>Confirm</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal>
                <div className={"card card-notif " + configNotif.redeemSuccess}>
                <p className="notif-text">Point redeemed successfully!</p>
                </div>
                <div className={"card card-notif " + configNotif.voucherCoppied}>
                <p className="notif-text">Voucher code copied!</p>
                </div>
                <div className={"card card-notif active " + configNotif.voucherRedeemSuccess}>
                <p className="notif-text">Voucher redeemed successfully</p>
                </div>
            </section>
        )

    }
}

const DetailVoucher = () => { 
    return (
        <>
            <div className="mt-4 animated fadeInUp delayp4">
                <h4>3 Kupon Undian Semarak Kebahagiaan</h4>
                <small className="help-block mb-3">200 points</small>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
            </div>
        </>
    )
}

const Tnc = () => {
    return (
        <>
            <div className="mt-4 animated fadeInUp delayp4">
                <h4>Term N Condition</h4>
                <small className="help-block">200 points</small>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
            </div>
        </>
    )
}

const DetailVoucherUseVoucher = () => {
    return (
        <>
            <div className="mt-4 animated fadeInUp delayp4">
                <h4>Free Chatime MilkTea</h4>
                <small className="help-block mb-3">Valid until 10 january 2020</small>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
            </div>
        </>
    )
}

const DetailVoucherWithCode = ({images, triggerPage}) => {
    return (
        <>
            <div className="mt-4 animated fadeInUp delayp4">
                <h4>Gratis Saldo LinkAja Rp100.000</h4>
                <small className="help-block mb-3">Valid until 10 january 2020</small>
                <div className="list-group code-placeholder">
                    <Link to="#" className="list-group-item">
                        <div className="list-group-item-content">
                            <h5 className="title">Kode Voucher</h5>
                            <span className="desc">3279812ASD2</span>
                        </div>
                        <img alt="icon" src={images.common["ic_copy.png"]} className="" onClick={()=>triggerPage("VoucherCoppied")}/>
                    </Link>
                    <Link to="#" className="list-group-item reedemed hidden">
                        <div className="list-group-item-content">
                            <h5 className="title">Kode Voucher</h5>
                            <span className="desc">3279812ASD2</span>
                        </div>
                        <img alt="icon" src={images.common["ic_copy.png"]} className="" />
                    </Link>
                    <Link to="#" className="list-group-item expired hidden">
                        <div className="list-group-item-content">
                            <h5 className="title">Kode Voucher</h5>
                            <span className="desc">3279812ASD2</span>
                        </div>
                        <img alt="icon" src={images.common["ic_copy.png"]} className="" />
                    </Link>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
            </div>
        </>
    )
}

const DetailVoucherWithQrCode = ({images, triggerPage}) => {
    return (
        <>
            <div className="mt-4 animated fadeInUp delayp4">
                <h4>Sapphire Lounge Access</h4>
                <small className="help-block mb-3">Valid until 10 january 2020</small>
                <div className="list-group code-placeholder">
                    <Link to="#" className="list-group-item hidden">
                        <div className="list-group-item-content">
                            <h5 className="title">Kode Voucher</h5>
                            <span className="desc">3279812ASD2</span>
                        </div>
                        <img alt="icon" src={images.common["ic_qr.png"]} className="" onClick={()=>triggerPage("openQrCode")}/>
                    </Link>
                    <Link to="#" className="list-group-item reedemed">
                        <div className="list-group-item-content">
                            <h5 className="title">Kode Voucher</h5>
                            <span className="desc">3279812ASD2</span>
                        </div>
                        <img alt="icon" src={images.common["ic_qr.png"]} className="" />
                    </Link>
                    <Link to="#" className="list-group-item expired hidden">
                        <div className="list-group-item-content">
                            <h5 className="title">Kode Voucher</h5>
                            <span className="desc">3279812ASD2</span>
                        </div>
                        <img alt="icon" src={images.common["ic_qr.png"]} className="" />
                    </Link>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
            </div>
        </>
    )
}
