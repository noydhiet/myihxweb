import React from 'react';
import renderer from 'react-test-renderer';
import Login from '../index';

jest.mock('react-router-dom');

const instance = props => renderer.create(<Login images={{ common: {} }} {...props} />).getInstance();

describe('Login', () => {
  test('render', () => {
    const result = instance().render();
    expect(result.type).toBe(React.Fragment);
  });

  test('onChange', () => {
    const wrapper = instance();
    const e = { target: { id: 'name', value: 'test' } };
    wrapper.onChange(e);
    expect(wrapper.state.name).toBe('test');
  });

  test('handleSubmit', () => {
    const history = { push: jest.fn() };
    const wrapper = instance({ history });
    const e = { preventDefault: jest.fn() };
    wrapper.trigger = jest.fn();
    wrapper.handleCheck = jest.fn();

    wrapper.handleSubmit(e);
    expect(e.preventDefault).toHaveBeenCalled();
    expect(wrapper.trigger).toHaveBeenCalled();

    wrapper.state.loginID = 'test@telkom.co.id';
    wrapper.handleSubmit(e);
    expect(wrapper.handleCheck).toHaveBeenCalledWith('email');

    wrapper.state.loginID = '081212341234';
    wrapper.handleSubmit(e);
    expect(wrapper.handleCheck).toHaveBeenCalledWith('mobile');
  });
});
