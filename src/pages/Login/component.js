import React from 'react';
import { Link } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import { CardNotif } from '../../include/CardPackage';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import { checkUserExist } from '../../services';
import ContainerBase from '../../include/ContainerBase';
import { NUMBER_BLOCKED, OTP_BLOCKED_TIME } from '../../utils/storage'

export default class Login extends React.Component {
  static contextType = PageContext;
  state = {
    alert: '',
    loginID: '',
    cardNotif: false,
    modal: false,
    isSubmitting: false
  }

  componentDidMount(){
    this.context.trigger(1);
    this.context.pageCase("w-bg");
  }

  onChange = ({ target }) => {
    const { id, value } = target;
    const remail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const renumber = /^(\+62|62|0)+[0-9]+$/;
    const mobileCheck = v => {
      return /^((?:\+62|62)|0)[8]{1}[0-9]{6,12}$/.test(v)
      // return !(/\s/g.test(v) || !/^[+0-9]+$/i.test(v)) && ((v.startsWith('0') && v.length < 15 && v.length > 7 ) || (v.startsWith('62') && v.length < 16 && v.length > 8) || (v.startsWith('+62') && v.length < 17 && v.length > 9))
    };
    const alert = remail.test(value) || mobileCheck(value) ? '' : 'invalid';

    this.setState({
      [id]: value,
      alert,
    });
  }

  trigger = () => {
    this.setState({
      modal: !this.state.modal
    })
  }

  handleCheckUserExist = (type, value) => {
    const { history } = this.props;
    const { match: { params } } = this.props;
    this.setState({isSubmitting: true}, () => {
      checkUserExist(type, value)
      .then(({ data }) => {
        if (data.status === 200) {
          localStorage.setItem('mobile', data.data.mobile);
          localStorage.setItem('email', data.data.email);
          if (type === 'mobile') {
            localStorage.removeItem(NUMBER_BLOCKED);
            localStorage.removeItem(OTP_BLOCKED_TIME);
            history.push(`/login/mobile/verification${ params.pathSuffix || '' }`);
          } else {
            history.push(`/login/email/password${ params.pathSuffix || '' }`);
          }
        } else {
          this.trigger();
        }
        this.setState({isSubmitting:false});
      })
      .catch(() => { this.trigger(); this.setState({isSubmitting:false}); });
    });
  }


  handleSubmit = (e) => {
    e.preventDefault();
    const { loginID } = this.state;
    const renumber = /^((?:\+62|62)|0)[8]{1}[0-9]{6,12}$/;
    const remail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      
    if (remail.test(loginID)) {
      this.handleCheck('email');
    } else if (renumber.test(loginID)) {
      this.handleCheck('mobile');
    } else {
      this.trigger();
    }
  }

  handleCheck = type => {
    const { loginID } = this.state;

    if (type === 'email') {
      this.handleCheckUserExist(type, loginID);
    } else if (type === 'mobile') {
      this.handleCheckUserExist(type, loginID.replace(/^\+*62/, '0'));
    }
  }

  render() {
    const { alert, modal, cardNotif, loginID, isSubmitting } = this.state;
    const { brand, common, sample, notifRecoveryPassword, closeNotif } = this.context;
    const images = { brand, common, sample };
    const remail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const mobileCheck = v => {
      return /^((?:\+62|62)|0)[8]{1}[0-9]{6,12}$/.test(v)
      // let mobileNumber;
      // if(v.startsWith('0')) {
      //   mobileNumber = v.substring(1);
      // } else if(v.startsWith('62')) {
      //   mobileNumber = v.substring(2);
      // } else if(v.startsWith('+62')) {
      //   mobileNumber = v.substring(3);
      // } else {
      //   return false;
      // }
      // return !(/\s/g.test(mobileNumber) || !/^[0-9]+$/i.test(mobileNumber)) && ((mobileNumber.length < 14 && mobileNumber.length > 6 ))
    };
    const config = {
      activeNotif: cardNotif ? 'active' : '',
      alert: alert ? 'block' : 'none',
      disabled: !isSubmitting && (remail.test(loginID) || mobileCheck(loginID)) ? '' : 'disabled'
    };
    (notifRecoveryPassword && closeNotif('recoveryPassword'))
    const Loading = () => {
      return (
        <span><i className="fa fa-circle-notch fa-spin"></i></span>
      )
    }
    console.log('config.disabled', config)
    console.log('isSubmitting', isSubmitting )
    console.log('thrd', ((/^(\+62|62|0)+[0-9]+$/.test(loginID) && loginID.length < 15) || !remail.test(loginID)) )
    return (
      <>
        <TextContext.Consumer>{(context)=>{
          const { loginText } = context;
          const alertMsg = this.state.alert === "invalid" && loginText.login.alert;
          const containerProps = {
            image: { alt: 'Icon Success', src: images.common['ic_header_login.png'], },
            title: loginText.login.title,
          };
          return (
            <ContainerBase {...containerProps}>
              <form className="needs-validation" noValidate onSubmit={this.handleSubmit}>
                <div className="form-group animated fadeInUp delayp2">
                  <label htmlFor="loginID">{loginText.login.label}</label>
                  <input autoFocus type="text" className="form-control" id="loginID" placeholder={loginText.login.placeholder} onChange={this.onChange} />
                  <div className="invalid-feedback" style={{ display: config.alert }}>{alertMsg}</div>
                </div>
                <div className="btn-placeholder">
                <Link to="#" className={"btn btn-primary btn-block animated fadeInUp delayp3 " + config.disabled} id='verification' onClick={this.handleSubmit}> {isSubmitting ? <Loading /> : <span>{loginText.login.button}</span> } </Link>
                </div>
              </form>
            </ContainerBase>
          )
        }}
        </TextContext.Consumer>
        {/* MODAL  */}
        <Modal show={modal} onHide={this.trigger} className="modal-centered" centered={true}>
          <div className="box box-main mb-0">
            <section>
              <h1>Account not yet registered</h1>
              <p>Looks like this account is not yet registered! Do you want to register instead?</p>
            </section>
            <div className="btn-placeholder inline bottom">
              <button className="btn btn-block w-border" onClick={this.trigger}> try again </button>
              <Link to="/register/account-details" className="btn btn-primary btn-block"> register </Link>
            </div>
          </div>
        </Modal>
        {/* MODAL  */}
        <CardNotif
          active={config.activeNotif}
          textTop="Password has been reset successfully. You can now login"
        />
      </>
    )
  }
}

Login.defaultProps = {
  pageCase: () => {},
  trigger: () => {},
};
