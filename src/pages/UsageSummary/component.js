import React, { useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { ProgressBar } from 'react-bootstrap';
import { PageContext } from './../../context/PageContext';
const UsageSummary = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = { brand, common, sample } ;
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
          <div className="container container-sm">
              <div className="box animated fadeInUp">
                  <header>
                  <div className="header-actions">
                      <Link to="/shop/internet/speed-on-demand" className="btn btn-link">
                      <i className="fa fa-arrow-left"></i>
                      </Link>
                  </div>
                  </header>
                  <section className="section-header-text">
                      <div className="heading heading-progress">
                          <h1>July Usage Summary</h1>
                      </div>
                  </section>
                  <section className="py-main-sm my-main-progrees">
                      <div className="subheading">
                          <p>Main Package</p>
                      </div>
                      <div className="box box-sm box-progress mb-5">
                          <h2>IndiHome Gamer 20Mpbs</h2>
                          <div className="list-group list-group-transparent">
                              <div className="list-group-item pl-0 pr-0">
                                  <img alt="icon" src={images.common["ic_internet.png"]} className="w-50px"/>
                                  <div className="list-group-item-content">
                                      <p className="mb-2"><b>250</b> GB used</p>
                                    <ProgressBar variant="danger" now={50} />
                                  </div>
                              </div>
                              <div className="list-group-item pl-0 pr-0">
                                  <img alt="icon" src={images.common["ic_call.png"]} className="w-50px"/>
                                  <div className="list-group-item-content">
                                      <p className="mb-2"><b>100</b> out of 1000 minutes used</p>
                                    <ProgressBar variant="success" now={20} />
                                  </div>
                              </div>
                          </div>
                          <div className="btn-placeholder">
                              <Link to="/" className="btn btn-primary btn-block">Upgrade</Link>
                              <Link to="/" className="btn btn-transparent btn-block">View Package Details</Link>
                          </div>
                      </div>
                      <div className="subheading">
                          <p>Add On</p>
                      </div>
                      <div className="list-group mb-0">
                        <Link to="/shop/internet/package-finder/budget/1" className="list-group-item w-arrow">
                            <img src={images.common["icon_ic_usage_tv.png"]} className="img-fluid w-50px" alt="Budget Icon" />
                            <div className="list-group-item-content">
                                <h5 className="title">IndiSport 1</h5>
                                <span className="desc">View MiniPack Details</span>
                            </div>
                        </Link>
                        <Link to="/shop/internet/package-finder/needs/1" className="list-group-item w-arrow">
                            <img src={images.common["ic_internet.png"]} className="img-fluid w-50px" alt="Need Icon" />
                            <div className="list-group-item-content">
                                <h5 className="title">Wifi.id seamless</h5>
                                <span className="desc">Manage wifi.id account</span>
                            </div>
                        </Link>
                    </div>
                  </section>
              </div>
          </div>
      </section>
    )
}

export default UsageSummary;
