import React from 'react';
import renderer from 'react-test-renderer';
import CheckCoverageAvailable from '../';
import PageContextProvider from '../../../context/PageContext';

jest.mock('react-router-dom');
jest.mock('../../../utils/requireContext');

const wrapper = props => renderer.create(
  <PageContextProvider>
    <CheckCoverageAvailable {...props} />
  </PageContextProvider>);

describe('CheckCoverageAvailable', () => {
  test('render', () => {
    const result = wrapper().toJSON();
    expect(result.type).toBe('section');
  });
});
