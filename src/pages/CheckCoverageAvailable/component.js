import React, { useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { PageContext } from './../../context/PageContext';

const CVAvailable = () => {
  const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
  const images = { brand, common, sample };
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  return (
    <section>
      <div className="container container-sm">
        <div className="box box-main animated fadeInUp">
          <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
            <div className="header-actions">
              <Link to="/upgrade-fiber/confirm" className="btn btn-link"><i className="fa fa-arrow-left"></i></Link>
            </div>
            <img className="header-img" src={images.common['ic_header_check.png']} alt="Icon Success" />
          </header>
          <section className="animated fadeInUp delayp2">
            <h1>IndiHome service is available in tour area!</h1>
            <p>Congratulation! Your location is within IndiHome are coverage. Browse packages and activate it in a few easy steps!</p>
          </section>
          <div className="btn-placeholder bottom animated fadeInUp delayp3">
            <Link to="/speed-on-demand" className="btn btn-primary btn-block">Browse Package</Link>
            <Link to="/speed-on-demand" className="btn btn-block">find the best package for me</Link>
          </div>
        </div>
      </div>
    </section>
  )
}

export default CVAvailable;
