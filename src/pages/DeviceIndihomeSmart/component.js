import React, { Component } from "react";
import { Link } from 'react-router-dom';
import Device from './../Device';
import { PageContext } from './../../context/PageContext';

class DeviceIndihomeSmart extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    return (
      <div>
        <Device indihomeSmartPage="active" />
        <SectionWithBanner images={images} />
        <SectionPackage images={images} />
        <SectionSingleItem images={images} />
      </div>
    )
  }
}
export default DeviceIndihomeSmart;

const SectionWithBanner = ({ images }) => {
  return (
    <div className="py-main bg-cover-section apps content-center">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-8 content-center-left">
            <div className="heading mb-2">
              <h3 className="animated fadeInUp delayp5">Control and secure your house with IndiHome Smart</h3>
              <p className="animated fadeInUp delayp6">
                Monitor and control your house security system, electricity usage, and other things from your phone
                </p>
            </div>
          </div>
          <div className="col-6 d-none d-md-block content-center">
            <img
              src={images.common["banner_bnr_tv_apps.png"]}
              className="img-fluid animated fadeInUp delayp7 rounded"
              alt="Speed Boost"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

const SectionPackage = ({ images }) => {
  return (
    <section className="py-main bg-white">
      <div className="container">
        <div className="heading text-md-center d-none d-md-block">
          <h2 className="animated fadeInUp delayp8">Packages</h2>
        </div>
        <div className="subheading d-block d-md-none">
          <p className="animated fadeInUp delayp8">Packages</p>
        </div>
        <section className="animated fadeInUp delayp9">
          <div className="row">
            <div className="col-lg-4">
              <Link
                to="/shop/indihome-smart/smart-monitoring"
                className="card card-packages card-packages-lg animated fadeInUp delayp2"
              >
                <div className="card-header">
                  <div className="badges-list">
                    <div className="badges badge-red left">
                      <h3>10</h3>
                      <span>Mbps</span>
                    </div>
                    <div className="badges badge-green middle">
                      <h3>92</h3>
                      <span>Channels</span>
                    </div>
                    <div className="badges badge-blue right">
                      <h3>100</h3>
                      <span>Minutes</span>
                    </div>
                  </div>
                  <div className="header-text">
                    <h3>Smart Home Camera</h3>
                    <p>Include cameras and maintenance</p>
                  </div>
                </div>
                <div className="card-body w-btn">
                  <div className="card-body-top">
                    <div className="subheading">
                      <p>Triple Play Value</p>
                    </div>
                    <h3>
                      Rp711.000<small> / bln</small>
                    </h3>
                    <div className="content">
                      <p>
                        Perfect for everyday internet users with light daily
                        use
                            </p>
                    </div>
                  </div>
                  <div className="card-body-bottom">
                    <div className="subheading">
                      <p>Bonus Subscriptions</p>
                    </div>
                    <div className="content">
                      <p>
                        Get bonus subscriptions from various apps and games!
                            </p>
                    </div>
                    <img
                      src={images.common["logo_add-ons.jpg"]}
                      className="img-fluid"
                      alt="Logo Add-Ons"
                    />
                  </div>
                  <span className="btn btn-primary">View Detail</span>
                </div>
              </Link>
            </div>
            <div className="col-lg-4">
              <Link
                to="/shop/indihome-smart/smart-monitoring"
                className="card card-packages card-packages-lg animated fadeInUp delayp3"
              >
                <div className="card-header">
                  <div className="badges-list">
                    <div className="badges badge-red left">
                      <h3>20</h3>
                      <span>Mbps</span>
                    </div>
                    <div className="badges badge-green middle">
                      <h3>92</h3>
                      <span>Channels</span>
                    </div>
                    <div className="badges badge-blue right">
                      <h3>100</h3>
                      <span>Minutes</span>
                    </div>
                  </div>
                  <div className="header-text">
                    <h3>Smart Monitoring</h3>
                    <p>Include cameras, and door & windows sensors</p>
                  </div>
                </div>
                <div className="card-body w-btn">
                  <div className="card-body-top">
                    <div className="subheading">
                      <p>IndiHome Gamer</p>
                    </div>
                    <h3>
                      Rp2.019.999<small> / bln</small>
                    </h3>
                    <div className="content">
                      <p>
                        Paket dengan berbagai keuntungan di game favorit
                        Anda!
                            </p>
                    </div>
                  </div>
                  <div className="card-body-bottom">
                    <div className="subheading">
                      <p>Bonus Subscriptions</p>
                    </div>
                    <div className="content">
                      <p>
                        Get bonus subscriptions from various apps and games!
                            </p>
                    </div>
                    <img
                      src={images.common["logo_games.jpg"]}
                      className="img-fluid"
                      alt="Logo Games"
                    />
                  </div>
                  <span className="btn btn-primary">View Detail</span>
                </div>
              </Link>
            </div>
            <div className="col-lg-4">
              <Link
                to="/shop/indihome-smart/smart-monitoring"
                className="card card-packages card-packages-lg animated fadeInUp delayp4"
              >
                <div className="card-header">
                  <div className="badges-list">
                    <div className="badges badge-red left">
                      <h3>50</h3>
                      <span>Mbps</span>
                    </div>
                    <div className="badges badge-blue right">
                      <h3>201</h3>
                      <span>Channels</span>
                    </div>
                    <div className="badges badge-green middle">
                      <h3>1000</h3>
                      <span>Minutes</span>
                    </div>
                  </div>
                  <div className="header-text">
                    <h3>Smart Safety</h3>
                    <p>Include flammable gas detector and smoke detector</p>
                  </div>
                </div>
                <div className="card-body w-btn">
                  <div className="card-body-top">
                    <div className="subheading">
                      <p>IndiHome Premium</p>
                    </div>
                    <h3>
                      Rp2.091.999<small> / bln</small>
                    </h3>
                    <div className="content">
                      <p>
                        Perfect for everyday internet users with light daily
                        use
                            </p>
                    </div>
                  </div>
                  <div className="card-body-bottom">
                    <div className="subheading">
                      <p>Bonus Subscriptions</p>
                    </div>
                    <div className="content">
                      <p>
                        Get bonus subscriptions from various apps and games!
                            </p>
                    </div>
                    <img
                      src={images.common["logo_add-ons.jpg"]}
                      className="img-fluid"
                      alt="Logo Add-Ons"
                    />
                  </div>
                  <span className="btn btn-primary">View Detail</span>
                </div>
              </Link>
            </div>
          </div>
        </section>
      </div>
    </section>
  )
}

const SectionSingleItem = ({ images }) => {
  return (
    <section className="py-main bg-white">
      <div className="container">
        <div className="heading text-md-center d-none d-md-block">
          <h2 className="animated fadeInUp delayp8">Single Item</h2>
        </div>
        <div className="subheading d-block d-md-none">
          <p className="animated fadeInUp delayp8">Single Item</p>
        </div>
        <section className="animated fadeInUp delayp9">
          <div className="row">
            <div className="col-lg-4">
              <Link
                to="/shop/indihome-smart/infrared-motion-detector"
                className="card card-packages card-packages-lg animated fadeInUp delayp2"
              >
                <div className="card-header">
                  <div className="badges-list">
                    <div className="badges badge-red left">
                      <h3>10</h3>
                      <span>Mbps</span>
                    </div>
                    <div className="badges badge-green middle">
                      <h3>92</h3>
                      <span>Channels</span>
                    </div>
                    <div className="badges badge-blue right">
                      <h3>100</h3>
                      <span>Minutes</span>
                    </div>
                  </div>
                  <div className="header-text">
                    <h3>Infrared Motion Detector</h3>
                  </div>
                </div>
                <div className="card-body w-btn">
                  <div className="card-body-top">
                    <div className="subheading">
                      <p>Triple Play Value</p>
                    </div>
                    <h3>
                      Rp711.000<small> / bln</small>
                    </h3>
                    <div className="content">
                      <p>
                        Perfect for everyday internet users with light daily
                        use
                            </p>
                    </div>
                  </div>
                  <div className="card-body-bottom">
                    <div className="subheading">
                      <p>Bonus Subscriptions</p>
                    </div>
                    <div className="content">
                      <p>
                        Get bonus subscriptions from various apps and games!
                            </p>
                    </div>
                    <img
                      src={images.common["logo_add-ons.jpg"]}
                      className="img-fluid"
                      alt="Logo Add-Ons"
                    />
                  </div>
                  <span className="btn btn-primary">View Detail</span>
                </div>
              </Link>
            </div>
            <div className="col-lg-4">
              <Link
                to="/shop/indihome-smart/infrared-motion-detector"
                className="card card-packages card-packages-lg animated fadeInUp delayp3"
              >
                <div className="card-header">
                  <div className="badges-list">
                    <div className="badges badge-red left">
                      <h3>20</h3>
                      <span>Mbps</span>
                    </div>
                    <div className="badges badge-green middle">
                      <h3>92</h3>
                      <span>Channels</span>
                    </div>
                    <div className="badges badge-blue right">
                      <h3>100</h3>
                      <span>Minutes</span>
                    </div>
                  </div>
                  <div className="header-text">
                    <h3>Emergency Alarm/Siren</h3>
                  </div>
                </div>
                <div className="card-body w-btn">
                  <div className="card-body-top">
                    <div className="subheading">
                      <p>IndiHome Gamer</p>
                    </div>
                    <h3>
                      Rp2.019.999<small> / bln</small>
                    </h3>
                    <div className="content">
                      <p>
                        Paket dengan berbagai keuntungan di game favorit
                        Anda!
                            </p>
                    </div>
                  </div>
                  <div className="card-body-bottom">
                    <div className="subheading">
                      <p>Bonus Subscriptions</p>
                    </div>
                    <div className="content">
                      <p>
                        Get bonus subscriptions from various apps and games!
                            </p>
                    </div>
                    <img
                      src={images.common["logo_games.jpg"]}
                      className="img-fluid"
                      alt="Logo Games"
                    />
                  </div>
                  <span className="btn btn-primary">View Detail</span>
                </div>
              </Link>
            </div>
            <div className="col-lg-4">
              <Link
                to="/shop/internet/package/details"
                className="card card-packages card-packages-lg animated fadeInUp delayp4"
              >
                <div className="card-header">
                  <div className="badges-list">
                    <div className="badges badge-red left">
                      <h3>50</h3>
                      <span>Mbps</span>
                    </div>
                    <div className="badges badge-blue right">
                      <h3>201</h3>
                      <span>Channels</span>
                    </div>
                    <div className="badges badge-green middle">
                      <h3>1000</h3>
                      <span>Minutes</span>
                    </div>
                  </div>
                  <div className="header-text">
                    <h3>Smart Safety</h3>
                  </div>
                </div>
                <div className="card-body w-btn">
                  <div className="card-body-top">
                    <div className="subheading">
                      <p>IndiHome Premium</p>
                    </div>
                    <h3>
                      Rp2.091.999<small> / bln</small>
                    </h3>
                    <div className="content">
                      <p>
                        Perfect for everyday internet users with light daily
                        use
                            </p>
                    </div>
                  </div>
                  <div className="card-body-bottom">
                    <div className="subheading">
                      <p>Bonus Subscriptions</p>
                    </div>
                    <div className="content">
                      <p>
                        Get bonus subscriptions from various apps and games!
                            </p>
                    </div>
                    <img
                      src={images.common["logo_add-ons.jpg"]}
                      className="img-fluid"
                      alt="Logo Add-Ons"
                    />
                  </div>
                  <span className="btn btn-primary">View Detail</span>
                </div>
              </Link>
            </div>
          </div>
        </section>
        <section className="text-center animated fadeInUp delayp16 pt-3 pb-4">
          <p className="mb-0">Belum menemukan yang Anda cari?</p>
          <Link to="/shop/internet/package/all" className="btn btn-link btn-lg">Lihat Semua Paket <i className="far fa-angle-right ml-1"></i></Link>
        </section>
      </div>
    </section>
  )
}