import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';
import { getToken } from '../../utils/storage';

const PurchasePaymentMethod = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    if (!getToken()) {
        window.location.href = '/login';
        return
    }
    const images = { brand, common, sample } ;
    return (
            <section>
                <div className="container container-sm">
                    <div className="box position-relative animated fadeInUp">
                        <header className="">
                            <div className="header-actions">
                                <Link to="/home" className="btn btn-link">
                                <i className="fa fa-arrow-left" />
                                </Link>
                            </div>
                            <div className="heading animated fadeInUp delayp1">
                                <h1>Payment</h1>
                                <p>Select your payment method</p>
                            </div>
                        </header>
                        <section className="section-header-card py-main-sm animated fadeInUp delayp2">
                            <div className="subheading">
                                <p>Total Due</p>
                            </div>
                            <div className="form-group">
                                <h3>Rp100.000</h3>
                                <small className="help-block">One time installation fee.</small>
                            </div>
                            <div className="py-main-sm pb-0">
                                <div className="subheading">
                                    <p>Payment Method</p>
                                </div>
                                <div className="list-group mb-0">
                                    {/* <div className="list-group-item">
                                        <img src={images.common["ic_selection_linkaja.png"]} className="img-fluid w-50px" />
                                        <div className="list-group-item-content">
                                            <h5 className="title">LinkAja</h5>
                                            <span className="desc">Balance Rp250.000</span>
                                        </div>
                                    </div>  */}
                                    <Link to="/purchase/payment-method/payment-with-linkaja" className="list-group-item">
                                        <img alt="icon" src={images.common["ic_selection_linkaja.png"]} className="img-fluid w-50px" />
                                        <div className="list-group-item-content">
                                            <h5 className="title">LinkAja</h5>
                                        </div>
                                    </Link> 
                                    <Link to="/purchase/payment-method/payment-with-card" className="list-group-item">
                                        <img alt="icon" src={images.common["ic_selection_card.png"]} className="img-fluid w-50px" />
                                        <div className="list-group-item-content">
                                            <h5 className="title">Debit/Credit Card</h5>
                                            <span className="desc">Visa/Master Card</span>
                                        </div>
                                    </Link> 
                                    <div className="list-group-item">
                                        <img alt="icon" src={images.common["icon_ic_selection_trf.png"]} className="img-fluid w-50px" />
                                        <div className="list-group-item-content">
                                            <h5 className="title">Bank Transfer</h5>
                                            <span className="desc">Transfer to rekening bank</span>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </section>
                    </div>
                  </div>
                </section>
  )
}

export default PurchasePaymentMethod;
