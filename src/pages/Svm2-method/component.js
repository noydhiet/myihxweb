import React from 'react';
import { Form } from 'react-bootstrap';
import ContainerBase from '../../include/ContainerBase';
import { Link } from 'react-router-dom'
import { PageContext } from '../../context/PageContext';

export default class Svm2Method extends React.Component {
  static contextType = PageContext;
  componentDidMount(){
    this.context.trigger(1);
    this.context.pageCase("w-bg");
  }

  render() {
    const { brand, common, sample, notifForgetPassword, closeNotif } = this.context;
    const images = { brand, common, sample };
    const containerProps = {
      image: { alt: 'Choose Verification Method', src: images.common['ic_header_verification.png'], },
      title: "Choose Verification Method"
    }; 

    return (
      <ContainerBase {...containerProps}>
          <div className="list-group">
              <div className="list-group-item w-arrow">
                  <img src={images.common["ic_selection_wifi.png"]} alt="wifi" className="img-fluid w-50px" />
                  <div className="list-group-item-content">
                        <p className="title">Internet</p>
                        <p className="desc">Connect to your IndiHome network to automatically verify your identity</p>
                  </div>
              </div>
              <div className="list-group-item w-arrow">
                  <img src={images.common["ic_shop_call.png"]} alt="wifi" className="img-fluid w-50px" />
                  <div className="list-group-item-content">
                        <p className="title">Internet</p>
                        <p className="desc">Connect to your IndiHome network to automatically verify your identity</p>
                  </div>
              </div>
          </div>
      </ContainerBase>
    )
  }
}
