import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ShopInternet from './../ShopInternet';
import Slider from 'react-slick';
import { PageContext } from './../../context/PageContext';
import TrackVisibility from "react-on-screen";

class ShopInternetCloudStorage extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    return (
      <div>
        <ShopInternet cloudActive="active" />
        <SectionBenefits />
        <TrackVisibility once offset={250}> 
          <SectionPackages />
        </TrackVisibility>
      </div>
    )
  }
}

export default ShopInternetCloudStorage;

const SectionBenefits = () => {
  const feature = [
    {
      bgColor: "red",
      title: "Safe Digital Storage",
      desc: "Lorem Ipsum"
    },
    {
      bgColor: "blue",
      title: "Easy Access",
      desc: "Lorem Ipsum"
    },
    {
      bgColor: "green",
      title: "Simple Sharing",
      desc: "Lorem Ipsum"
    },
  ]
  return (
    <div className="py-main bg-gray-50">
      <div className="container">
        <div className="row row-1 row-md-up-2">
          {feature.map((value, index)=>{
            const sumDelay = index+1;
            return (
              <div className={"col-4 animated fadeInUp delayp" + sumDelay } key={index}>
                <div className={"card card-feature " + value.bgColor}>
                  <h5>{value.title}<i className="fal fa-angle-right"></i> </h5>
                  <p>{value.desc}</p>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}

const SectionPackages = ({isVisible}) => {
  const carousel = {
    className: "primary-carousel overflow-inherit mb-3",
    dots: false,
    infinite: false,
    arrows: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2.2,
          slidesToScroll: 2.2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1.5,
          slidesToScroll: 1.5
        }
      }
    ]
  }
  const packages = [
    {
      badges: {
        color: "badge-red",
        title: "20",
        desc: "Mbps"
      },
      title: "Rp40.000",
      desc: "3 x 24 hours"
    },
    {
      badges: {
        color: "badge-red",
        title: "20",
        desc: "Mbps"
      },
      title: "Rp40.000",
      desc: "3 x 24 hours"
    },
    {
      badges: {
        color: "badge-red",
        title: "20",
        desc: "Mbps"
      },
      title: "Rp40.000",
      desc: "3 x 24 hours"
    },
    {
      badges: {
        color: "badge-red",
        title: "20",
        desc: "Mbps"
      },
      title: "Rp40.000",
      desc: "3 x 24 hours"
    },
  ]
  const effect = isVisible ? "fadeInUp" : "";
  return (
    <div className="py-main bg-gray-50 bg-md-up-white">
      <div className="container">
        <div className="subheading">
          <p className={"animated " + effect + " delayp1"}>Packages</p>
        </div>
        <Slider {...carousel}>
          {packages.map((value, index)=>{
            let sumDelay = index+1;
            return (
            <div className="carousel-item" key={index}>
              <Link
                to="/shop/internet/cloud-storage/confirm"
                className={"card card-packages card-packages-sm animated " + effect + " delayp" + sumDelay}
              >
                <div className="card-header">
                  <div className="badges-list">
                    <div className={"badges " + value.badges.color}>
                      <h3>{value.badges.title}</h3>
                      <span>{value.badges.desc}</span>
                    </div>
                  </div>
                </div>
                <div className="card-body">
                  <h3>
                    {value.title}<small> / bln</small>
                  </h3>
                  <p>{value.desc}</p>
                  <span className="btn btn-primary">Buy</span>
                </div>
              </Link>
            </div>
            )
          })}
        </Slider>
      </div>
    </div>
  )
}