import React, { Component } from "react";
import { Modal } from "react-bootstrap";
import { Link } from "react-router-dom";
import { NavMobile } from "./../../include/NavMobile";
import { PageContext } from './../../context/PageContext';
import { clearStorages, getUserData, getToken, getFcmToken, setUserProfile } from '../../utils/storage';
import { userProfile, deleteFcmTokenApi } from '../../services'

class Profile extends Component {
  static contextType = PageContext;
  constructor() {
    super();

    this.state = {
      logOutPopUp: false,
      userData: getUserData(),
      profileData: undefined
    }
  }

  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
    this.context.footerCase("d-none d-md-block");
    if(getToken()) {
      userProfile()
      .then( ({data}) => {
        console.log('response---', data)
        if(data.status === 200 || data.status === 201) {
          this.setState({profileData: data.data})
          setUserProfile(data.data)
        }
      })
      .catch( error => {console.error(error)})
    }
  }

  componentWillUnmount() {
    this.context.footerCase("");
  }
  
  triggerPage = mode => {
    switch (mode) {
      case 'logOutPopUp':
        this.setState({
          logOutPopUp: !this.state.logOutPopUp
        });
        break;

      case 'logOutAggree':
        const payload = { fcmToken: getFcmToken() }
        console.log('payload', payload)
        console.log('getToken', getToken())
        deleteFcmTokenApi(payload).then(response => console.log('deleteFcmTokenApi', response)).catch(error => console.log(error.response))
        clearStorages();
        window.location.href = '/';
        break;

      case 'closelogOutPopUp':
        this.setState({
          logOutPopUp: false
        });
        break;

      default:
        return null;
    }
  };

  render() {
    const { logOutPopUp, userData, profileData } = this.state;
    const { common, brand, sample, condition } = this.context;
    const images = { common, brand, sample };
    const coverStyle = {
      background: `url(${images.common["bnr_blue.png"]}) no-repeat center`,
      backgroundSize: "cover",
      border: "none"
    };
    return (
      <div>
        <NavMobile condition={condition} profilePage="active" />
        <div className="profile-cover py-main">
          <img
            src={images.common["bg_bg_landing_corner.png"]}
            alt="bg"
            className="profile-cover-bg"
          />
          <div className="container container-sm">
            <div className="list-group list-group-profile animated fadeInUp">
              <Link to="" className="list-group-item min-h-auto pr-3">
                <div className="img-profile w-text ">
                  <span>{userData.userName.charAt(0)}</span>
                </div>
                <div className="list-group-item-content">
                  <p className="mid-text dark">{userData.userName}</p>
                  <small className="bottom-text dark">{ profileData && profileData.accounts.length > 0 ? profileData.accounts[0].indiHomeNum : ''}</small>
                </div>
              </Link>
            </div>

            <div className="row row-2">
              <Link className="col-6"
              to="/wallet/details">
                <div className="list-group animated fadeInUp delayp1">
                  <div className="list-group-item min-h-auto w-arrow">
                    <img src={images.common["ic_selection_linkaja.png"]} alt="link-aja" className="img-fluid w-50px" />
                    <div className="list-group-item-content mb-0">
                      <small className="help-block">LinkAja Balance</small>
                      <p className="title">
                        <strong>Rp10.000.000</strong>
                      </p>
                    </div>
                  </div>
                </div>
              </Link>
              <Link className="col-6"
                to="/points">
                <div className="list-group animated fadeInUp delayp2">
                  <div className="list-group-item min-h-auto w-arrow">
                    <div className="list-group-item-content mb-0">
                      <small className="help-block">IndiHome Points</small>
                      <p className="title">
                        <strong>10.000</strong>
                      </p>
                    </div>
                  </div>
                </div>
              </Link>
            </div>

            <div className="list-group">
              <Link
                to="/profile/completion-task"
                className="list-group-item w-arrow light w-bg-cover animated fadeInUp delayp3"
                style={coverStyle}
              >
                <img
                  src={images.common["grfx_action_profile.png"]}
                  className="w-50px"
                  alt="grfx"
                />
                <div className="list-group-item-content">
                  <h5 className="title">Your profile is 80% complete</h5>
                  <span className="desc">
                    Complete your profile to get 10% off from you next bill.{" "}
                  </span>
                </div>
              </Link>
            </div>

            <div className="box box-profile animated fadeInUp delayp4">
              <div className="list-group list-group-activity mb-0">
                <Link to="/profile/edit" className="list-group-item w-arrow">
                  <div className="list-group-item-content">
                    <p className="title mb-0">Edit Profile</p>
                  </div>
                </Link>
                <Link
                  to="/profile/change-password"
                  className="list-group-item w-arrow border-none"
                >
                  <div className="list-group-item-content">
                    <p className="title mb-0">Change Password</p>
                  </div>
                </Link>
                <Link
                  to="/profile/change-phone-number"
                  className="list-group-item w-arrow border-none"
                >
                  <div className="list-group-item-content">
                    <p className="title mb-0">Change phone number</p>
                  </div>
                </Link>
                <Link
                  to="/profile/change-email"
                  className="list-group-item w-arrow border-none"
                >
                  <div className="list-group-item-content">
                    <p className="title mb-0">Change email</p>
                  </div>
                </Link>
                <button className="btn btn-primary mt-3 w-100" onClick={() => this.triggerPage("logOutPopUp")}>Log Out</button>
              </div>
            </div>
          </div>
        </div>

        <Modal
          show={logOutPopUp}
          onHide={() => this.triggerPage("logOutPopUp")}
          centered={true}>
          <div className="modal-body p-box">
            <div className="heading text-center">
              <h3>Are you sure you want to log out from myIndiHome?</h3>
              <p>
                You will need to login again next time you open myIndiHome app.
                </p>
            </div>
            <section>
              <div className="row">
                <div className="col-6">
                  <Link
                    to="/home"
                    className="btn btn-transparent btn-block"
                    onClick={() => this.triggerPage('logOutAggree')}
                  >
                    Yes
                    </Link>
                </div>
                <div className="col-6">
                  <button
                    className="btn btn-primary btn-block"
                    onClick={() => this.triggerPage('closelogOutPopUp')}
                  >
                    No
                    </button>
                </div>
              </div>
            </section>
          </div>
        </Modal>
        <div className={"card card-notif"}>
          <p className="notif-text">Mobile Number Successfully Changed</p>
        </div>
        <div className={"card card-notif"}>
          <p className="notif-text">Email Successfully Changed</p>
        </div>
        <div className={"card card-notif"}>
          <p className="notif-text">Password has been Successfully Changed</p>
        </div>
        <div className={"card card-notif"}>
          <p className="notif-text">Profile has been Successfully Changed</p>
        </div>
      </div>
    );
  }
}

export default Profile;

// const SectionContent = ({images}) => {
//     const coverStyle = {
//         background: `url(${images.common["bnr_blue.png"]}) no-repeat center`,
//         backgroundSize: "cover",
//         border: "none"
//     };
//     return (
//         <div className="home-product bg-gray-50 py-main pb-main pt-0">
//             <div className="container">
//                 <section className="animated fadeInUp delayp4">
//                     <div className="list-group">
//                         <Link
//                             to="#"
//                             className="list-group-item w-arrow light w-bg-cover"
//                             style={coverStyle}
//                         >
//                             <img src={images.common["grfx_grfx_action_profile.png"]} className="w-50px" />
//                             <div className="list-group-item-content">
//                             <h5 className="title">Your profile is 80% complete</h5>
//                             <span className="desc">
//                             Complete your profile to get 10% off from you next bill. {" "}
//                             </span>
//                             </div>
//                         </Link>
//                     </div>
//                     <div className="subheading">
//                         <p>Account</p>
//                     </div>
//                     <div className="list-group list-group-activity mb-0">
//                         <div className="row">
//                             <div className="col-6">
//                                 <Link to="/history/payment" className="list-group-item w-arrow">
//                                     <div className="list-group-item-content">
//                                     <p className="title mb-0">Edit Profile</p>
//                                     </div>
//                                 </Link>
//                             </div>
//                             <div className="col-6">
//                                 <Link to="/history/purchase" className="list-group-item w-arrow">
//                                     <div className="list-group-item-content mb-0">
//                                     <p className="title mb-0">Change Password</p>
//                                     </div>
//                                 </Link>
//                             </div>
//                         </div>
//                     </div>
//                 </section>
//             </div>
//         </div>
//     )
// }
