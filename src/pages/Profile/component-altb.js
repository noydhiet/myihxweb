import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { NavMobile } from './../../include/NavMobile'
import { CardBasic, CardContent, CardContentStacked } from './../../include/CardPackage'
import Slider from "react-slick";
import TrackVisibility from "react-on-screen";

class Profile extends Component {
  state = {
    logOutPopUp: false,
    logOutAggree: false
  }
  componentDidMount() {
    this.props.trigger(2);
    this.props.navCase("w-md-up-block");
    this.props.pageCase("l-bg");
  }
  triggerPage = (mode) => {
    switch (mode) {
      case "logOutPopUp":
        this.setState({
          logOutPopUp: !this.state.logOutPopUp
        });
        break;

      case "logOutAggree":
        this.setState({
          logOutAggree: true,
          logOutPopUp: false
        });
        break;

      case "closelogOutPopUp":
        this.setState({
          logOutPopUp: false
        });

      default:
        return null
    }
  }
  render() {
    const { logOutPopUp } = this.state;
    const { images, condition } = this.props;
    const coverStyle = {
      background: `url(${images.common["bnr_blue.png"]}) no-repeat center`,
      backgroundSize: "cover",
      border: "none"
    };
    return (
      <div>
        <NavMobile condition={condition} profilePage="active" />
        <div className="profile-cover py-main">
          <img
            src={images.common["bg_bg_landing_corner.png"]}
            alt="bg"
            className="profile-cover-bg"
          />
          <div className="container">
            <div className="row">
              <div className="col-md-4 col-12">
                <div className="box box-profile animated fadeInUp delayp1">
                  <div className="list-group list-group-profile animated fadeInUp">
                    <Link to="" className="list-group-item or-3">
                      <div className="img-profile w-text ">
                        <span>M</span>
                      </div>
                      <div className="list-group-item-content">
                        <p className="mid-text dark">Michelle</p>
                        <small className="bottom-text dark">0212384795</small>
                      </div>
                    </Link>
                  </div>
                  <div className="list-group list-group-activity mb-0">
                    <Link to="/profile/edit" className="list-group-item w-arrow">
                      <div className="list-group-item-content">
                        <p className="title mb-0">Edit Profile</p>
                      </div>
                    </Link>
                    <Link to="/profile/change-password" className="list-group-item w-arrow">
                      <div className="list-group-item-content">
                        <p className="title mb-0">Change Password</p>
                      </div>
                    </Link>
                  </div>
                </div>
              </div>
              <div className="col-md-8 col-12">
                <div className="list-group">
                  <Link
                    to="#"
                    className="list-group-item w-arrow light w-bg-cover animated fadeInUp delayp2"
                    style={coverStyle}
                  >
                    <img src={images.common["grfx_grfx_action_profile.png"]} className="w-50px" />
                    <div className="list-group-item-content">
                      <h5 className="title">Your profile is 80% complete</h5>
                      <span className="desc">
                        Complete your profile to get 10% off from you next bill. {" "}
                      </span>
                    </div>
                  </Link>
                </div>
                <div className="box animated fadeInUp delayp3 min-h-auto mb-3">
                  <div className="list-group list-group-activity mb-0">
                    <div className="list-group-item w-arrow">
                      <div className="list-group-item-content mb-0">
                        <p className="title">LinkAja Balance</p>
                        <small className="help-block">
                          Rp10.000.000
                                            </small>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="box animated fadeInUp delayp4">
                  <div className="subheading">
                    <p>IndiHome Points</p>
                  </div>
                  <div className="list-group list-group-activity mb-0">
                    <p className="help-block mb-0">10,000</p>
                    <Link
                      to="/history/activity/problem-report"
                      className="list-group-item pr-0"
                    >
                      <div className="list-group-item-content mb-0">
                        <small className="caption">16 June 2019</small>
                        <div className="list-group-item-content-w-status">
                          <p className="title">Redeem voucher</p>
                          <p className="text-status">-15.000</p>
                        </div>
                      </div>
                    </Link>
                    <Link
                      to="/history/activity/problem-report"
                      className="list-group-item pr-0"
                    >
                      <div className="list-group-item-content mb-0">
                        <small className="caption">16 June 2019</small>
                        <div className="list-group-item-content-w-status">
                          <p className="title">Subscribe Minipack</p>
                          <p className="text-status done">+159</p>
                        </div>
                      </div>
                    </Link>
                    <Link
                      to="/history/activity/problem-report"
                      className="list-group-item pr-0"
                    >
                      <div className="list-group-item-content mb-0">
                        <small className="caption">16 June 2019</small>
                        <div className="list-group-item-content-w-status">
                          <p className="title">Subscribe Minipack</p>
                          <p className="text-status done">+159</p>
                        </div>
                      </div>
                    </Link>
                    <Link
                      to="/history/activity/problem-report"
                      className="list-group-item pr-0"
                    >
                      <div className="list-group-item-content mb-0">
                        <small className="caption">16 June 2019</small>
                        <div className="list-group-item-content-w-status">
                          <p className="title">Redeem voucher</p>
                          <p className="text-status">-15.000</p>
                        </div>
                      </div>
                    </Link>
                    <Link
                      to="/history/activity/problem-report"
                      className="list-group-item pr-0"
                    >
                      <div className="list-group-item-content mb-0">
                        <small className="caption">16 June 2019</small>
                        <div className="list-group-item-content-w-status">
                          <p className="title">Subscribe Minipack</p>
                          <p className="text-status done">+159</p>
                        </div>
                      </div>
                    </Link>
                    <Link
                      to="/history/activity/problem-report"
                      className="list-group-item pr-0"
                    >
                      <div className="list-group-item-content mb-0">
                        <small className="caption">16 June 2019</small>
                        <div className="list-group-item-content-w-status">
                          <p className="title">Subscribe Minipack</p>
                          <p className="text-status done">+159</p>
                        </div>
                      </div>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <Link to="" className="nav-link-message w-notif">
              <i className="far fa-envelope"></i>
            </Link>
            {/* <div className="list-group list-group-profile animated fadeInUp">
                            <Link to="" className="list-group-item or-3">
                            <div className="img-profile w-text ">
                                <span>M</span>
                            </div>
                            <div className="list-group-item-content">
                                <p className="mid-text dark">Michelle</p>
                                <small className="bottom-text dark">0212384795</small>
                            </div>
                            </Link>
                        </div>
                        <Link to="" className="nav-link-message w-notif">
                            <i className="far fa-envelope"></i>
                        </Link> */}
            {/* <div className="row">
                            <div className="col-6">
                                <div className="list-group">
                                    <Link to="/shop/internet/package-finder/needs/2" className="list-group-item w-arrow">
                                        <div className="list-group-item-content">
                                            <span className="desc">LinkAja Balance</span>
                                            <h5 className="title">Rp10.000</h5>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                            <div className="col-6">
                                <div className="list-group">
                                    <Link to="/shop/internet/package-finder/needs/2" className="list-group-item w-arrow">
                                        <div className="list-group-item-content">
                                            <span className="desc">IndiHome Points</span>
                                            <h5 className="title">10,000</h5>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </div> */}
          </div>
        </div>
        {/* <TrackVisibility once offset={354}>
                    <SectionContent images={images} />
                </TrackVisibility> */}

        <Modal show={logOutPopUp} onHide={() => this.triggerPage('logOutPopUp')}>
          <div className="modal-body p-box">
            <div className="heading text-center animated fadeInUp delayp1">
              <h3>Are you sure you want to log out from myIndiHome?</h3>
              <p>You will need to login again next time you open myIndiHome app.</p>
            </div>
            <section>
              <div className="row">
                <div className="col-6">
                  <button className="btn btn-transparent btn-block" onClick={() => this.triggerPage("logOutAggree")}>Yes</button>
                </div>
                <div className="col-6">
                  <button className="btn btn-primary btn-block" onClick={() => this.triggerPage("closelogOutPopUp")}>No</button>
                </div>
              </div>
            </section>
          </div>
        </Modal>
      </div>
    )
  }
}

export default Profile;

// const SectionContent = ({images}) => {
//     const coverStyle = {
//         background: `url(${images.common["bnr_blue.png"]}) no-repeat center`,
//         backgroundSize: "cover",
//         border: "none"
//     };
//     return (
//         <div className="home-product bg-gray-50 py-main pb-main pt-0">
//             <div className="container">
//                 <section className="animated fadeInUp delayp4">
//                     <div className="list-group">
//                         <Link
//                             to="#"
//                             className="list-group-item w-arrow light w-bg-cover"
//                             style={coverStyle}
//                         >
//                             <img src={images.common["grfx_grfx_action_profile.png"]} className="w-50px" />
//                             <div className="list-group-item-content">
//                             <h5 className="title">Your profile is 80% complete</h5>
//                             <span className="desc">
//                             Complete your profile to get 10% off from you next bill. {" "}
//                             </span>
//                             </div>
//                         </Link>
//                     </div>
//                     <div className="subheading">
//                         <p>Account</p>
//                     </div>
//                     <div className="list-group list-group-activity mb-0">
//                         <div className="row">
//                             <div className="col-6">
//                                 <Link to="/history/payment" className="list-group-item w-arrow">
//                                     <div className="list-group-item-content">
//                                     <p className="title mb-0">Edit Profile</p>
//                                     </div>
//                                 </Link>
//                             </div>
//                             <div className="col-6">
//                                 <Link to="/history/purchase" className="list-group-item w-arrow">
//                                     <div className="list-group-item-content mb-0">
//                                     <p className="title mb-0">Change Password</p>
//                                     </div>
//                                 </Link>
//                             </div>
//                         </div>
//                     </div>
//                 </section>
//             </div>
//         </div>
//     )
// }