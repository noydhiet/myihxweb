import React from 'react';
import renderer from 'react-test-renderer';
import CheckCoverageResultPt2 from '../';
import PageContextProvider from '../../../context/PageContext';

jest.mock('react-router-dom');
jest.mock('../../../utils/requireContext');

const wrapper = props => renderer.create(
  <PageContextProvider>
    <CheckCoverageResultPt2 {...props} />
  </PageContextProvider>);

describe('CheckCoverageResultPt2', () => {
  test('render', () => {
    const result = wrapper().toJSON();
    expect(result.type).toBe('div');
  });
});
