import React, { useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { Form } from 'react-bootstrap'
import { PageContext } from './../../context/PageContext';
import { getToken } from './../../utils/storage';

const PT2 = () => {
  const isLoggedIn = Boolean(getToken());
  const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
  const images = { brand, common, sample };
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);

  return (
    <section>
      <div className="container container-sm">
        <div className="box box-main animated fadeInUp">
          <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
            <div className="header-actions">
              <Link to="/check-coverage/map" className="btn btn-link"><i className="fa fa-arrow-left"></i></Link>
            </div>
            <img className="header-img" src={images.common["ic_header_failed.png"]} alt="Icon Success" />
          </header>
          <section className="animated fadeInUp delayp2">
            <h1>Sorry, IndiHome is not yet available in your area</h1>
            <p>IndiHome may be available in 1 - 3 months. Please Enter your information to stay updated!</p>
          </section>
          <div className="btn-placeholder bottom animated fadeInUp delayp3">
            {isLoggedIn || (<Form>
              <div className="form-group w-icon-left">
                <div className="input-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text" id="inputGroupPrepend"><i className="far fa-envelope"></i></span>
                  </div>
                  <input type="text" className="form-control" id="validationCustomUsername" placeholder="Email Or Phone Number" aria-describedby="inputGroupPrepend" required />
                  <div className="invalid-feedback">Please Enter Email or Phone Number</div>
                </div>
              </div>
            </Form>)}
            {
              isLoggedIn ?
                (
                  <>
                    <Link to="/home" className="btn btn-primary btn-block">Back to Home</Link>
                    <Link to="/check-coverage" className="btn btn-block">Check Other Address</Link>
                  </>
                ) :
                (
                  <>
                    <Link to="#" className="btn btn-primary btn-block">Update Me</Link>
                    <Link to="/home" className="btn btn-block">Back to Home</Link>
                  </>
                )
              }
          </div>
        </div>
      </div>
    </section>
  )
}

export default PT2;
