import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Form } from 'react-bootstrap';
import { PageContext } from './../../context/PageContext';
const ByNeeds2 = ({ history }) => {
  const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  const images = { brand, common, sample };
  const checkBox = (e) => {
    const check = document.getElementsByClassName("check");
    for (let i = 0; i < check.length; i++) {
      if (i === e) {
        if (check[i].checked) {
          check[i].checked = false
        } else {
          check[i].checked = true
        }
      }
    }
  }
  const handleSubmit = (e) => {
    e.preventDefault();
    history.push("/shop/internet/package-finder/needs/3");
  }
  return (
    <section>
      <div className="container container-sm">
        <div className="box animated fadeInUp">
          <header>
            <div className="header-actions">
              <Link to="/shop/internet/package-finder/needs/1" className="btn btn-link">
                <i className="fa fa-arrow-left"></i>
              </Link>
              <ul className="btn wizard-finder">
                <li className="active"></li>
                <li className="active"></li>
                <li></li>
              </ul>
            </div>
            <div className="heading animated fadeInUp delayp1">
              <h1>What do you usually use the internet for?</h1>
            </div>
          </header>
          <Form onSubmit={handleSubmit}>
            <section className="section-header-card header-card-sm animated fadeInUp delayp2">
              <div className="list-group">
                <div className="list-group-item w-checkbox list-check" onClick={() => checkBox(0)}>
                  <img src={images.common["ic_selection_call.png"]} className="img-fluid w-50px" alt="Phone Icon" />
                  <div className="list-group-item-content">
                    <h5 className="title">Browsing</h5>
                    <span className="desc">News, articles, other websites.</span>
                  </div>
                  <div className="form-group checkbox">
                    <div className="custom-control custom-checkbox">
                      <input type="checkbox" className="custom-control-input check" />
                      <label className="custom-control-label"></label>
                    </div>
                  </div>
                </div>
                <div className="list-group-item w-checkbox list-check" onClick={() => checkBox(1)}>
                  <img src={images.common["ic_selection_call.png"]} className="img-fluid w-50px" alt="Phone Icon" />
                  <div className="list-group-item-content">
                    <h5 className="title">Social Media</h5>
                    <span className="desc">Facebook, Instagram, Twitter, WhatsApp, etc.</span>
                  </div>
                  <div className="form-group checkbox">
                    <div className="custom-control custom-checkbox">
                      <input type="checkbox" className="custom-control-input check" />
                      <label className="custom-control-label"></label>
                    </div>
                  </div>
                </div>
                <div className="list-group-item w-checkbox list-check" onClick={() => checkBox(2)}>
                  <img src={images.common["ic_selection_call.png"]} className="img-fluid w-50px" alt="Phone Icon" />
                  <div className="list-group-item-content">
                    <h5 className="title">Video & Streaming</h5>
                    <span className="desc">Youtube, HOOQ, iflix and other streaming platform</span>
                  </div>
                  <div className="form-group checkbox">
                    <div className="custom-control custom-checkbox">
                      <input type="checkbox" className="custom-control-input check" />
                      <label className="custom-control-label"></label>
                    </div>
                  </div>
                </div>
                <div className="list-group-item w-checkbox list-check" onClick={() => checkBox(3)}>
                  <img src={images.common["ic_selection_call.png"]} className="img-fluid w-50px" alt="Phone Icon" />
                  <div className="list-group-item-content">
                    <h5 className="title">Music</h5>
                    <span className="desc">Spotify, Joox, Langitmusik and other music platform</span>
                  </div>
                  <div className="form-group checkbox">
                    <div className="custom-control custom-checkbox">
                      <input type="checkbox" className="custom-control-input check" />
                      <label className="custom-control-label"></label>
                    </div>
                  </div>
                </div>
              </div>
              <div className="btn-placeholder">
                <button className="btn btn-primary btn-block" type="submit" value="submit">Next</button>
              </div>
            </section>
          </Form>
        </div>
      </div>
    </section>
  )
}

export default ByNeeds2
