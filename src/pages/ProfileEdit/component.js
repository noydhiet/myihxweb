import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Form } from "react-bootstrap";
import DatePicker from "react-datepicker";
import { PageContext } from '../../context/PageContext';
import { getUserProfile } from '../../utils/storage';

class ProfileEdit extends Component {
  static contextType = PageContext;
  state = {
    startDate: new Date(),
    fullName: "Adhitia Putra Herawan",
    phoneNumber: "0812345678",
    address: "Jl. Jurangmangu Barat no. 8. Tanggerang",
    emailAddress: "adhitia.putra@gmail.com",
    message: {
      fullName: '',
      phoneNumber: '',
      address: '',
      emailAddress: '',
    },
  };

  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("w-bg");
    const userProfile = getUserProfile();
    const accounts = userProfile && userProfile.accounts.length > 0 ? userProfile.accounts[0] : null;
    const location = accounts && Object.keys(accounts.Location).length > 0 ? accounts.Location : null;
    const address = location ? `${location.street}, ${location.district}, ${location.city}, ${location.province}, ${location.postalCode}` : '';
    this.setState({address})
  }

  handleChange = date => {
    this.setState({ startDate: date });
  };

  handleInputChange = ({ target }) => {
    const { id, value } = target;
    const { message } = this.state;

    this.setState({
      [id]: value,
      message: { ...message, [id]: this.setMessage(id, value) },
    });
  }

  handleSubmit = e => {
    const { history } = this.props;

    e.preventDefault();
    history.push('/profile');
  }

  gendeSelect = mode => {
    const listCheck = document.getElementsByClassName("list-group-item");
    const male = document.getElementById("male");
    const female = document.getElementById("female");
    switch (mode) {
      case "0":
        male.checked = true;
        female.checked = false;
        listCheck[0].classList.add("active");
        listCheck[1].classList.remove("active");
        break;

      case "1":
        female.checked = true;
        male.checked = false;
        listCheck[0].classList.remove("active");
        listCheck[1].classList.add("active");
        break;

      default:
        return null;
    }
  };

  setMessage(id, value) {
    const reMail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    switch (id) {
      case 'fullName':
        if (!value.length) return 'Please enter your full name';
        if (value.length > 30) return 'Max. 30 characters';
        if (/[^-'A-Za-z\s]/.test(value)) return 'Please enter a valid fullname';
        return '';
      case 'phoneNumber':
        return !/^(\+62|62|0)+[0-9]+$/.test(value) ? 'Please enter a valid phone number' : '';
      case 'emailAddress':
        return !reMail.test(value) ? 'Please enter a valid email address' : '';
      default:
        return '';
    }
  }

  _renderInput(label, id) {
    const value = this.state[id];
    const message = this.state.message[id];

    return (
      <div className="col-md-6 col-12">
        <Form.Group>
          <Form.Label>{label}</Form.Label>
          <Form.Control
            className={message ? 'is-invalid' : ''}
            id={id}
            value={value}
            onChange={this.handleInputChange}
            disabled={id === 'address'}
          />
          <div className="invalid-feedback">{message}</div>
        </Form.Group>
      </div>
    );
  }

  render() {
    const { message } = this.state;
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    const config = {
      disabled: Object.values(message).filter(Boolean).length > 0 ? 'disabled' : '',
    };

    return (
      <section>
        <div className="container container-sm">
          <div className="box show-out-box animated fadeInUp">
            <header>
              <div className="header-actions">
                <Link to="/profile" className="btn btn-link">
                  <i className="fa fa-arrow-left"></i>
                </Link>
              </div>
            </header>
            <section className="section-header-text">
              <div className="heading">
                <h1>Edit Profile</h1>

                {/* <img className="header-img" src={images.common['sampleImg.jpg']} alt="Icon Success" className="rounded-circle w-75px" /> */}
              </div>
            </section>
            <section className="py-main-sm pt-0 pb-0">
              <div className="img-profile w-text mb-4">
                <span>M</span>
              </div>
              <Form>
                <Form.Row className="row-2">
                  {this._renderInput('Full Name', 'fullName')}
                  {this._renderInput('Phone Number', 'phoneNumber')}
                  {this._renderInput('Address', 'address')}
                  {this._renderInput('E-mail Address', 'emailAddress')}
                  <div className="col-md-6 col-12">
                    <Form.Group className="form-custom w-icon-right">
                      <Form.Label>Birthdate</Form.Label>
                      <DatePicker
                        selected={this.state.startDate}
                        onChange={this.handleChange}
                        className="form-control"
                        tabIndex={1}
                        maxDate={new Date()}
                        id="startDate"
                      />
                      {/* <DayPickerInput style={{display: "block"}} component={props => <input {...props} className="form-control" placeholder="MM/YY" />} /> */}
                      <label htmlFor="startDate" className="preppend-icon" style={{ marginBottom: 0 }}>
                        <img
                          alt=""
                          src={images.common["icon_ic_cal.png"]}
                          className="img-fluid w-20px"
                        />
                      </label>
                    </Form.Group>
                  </div>
                  <div className="col-md-6 col-12">
                    <Form.Group>
                      <Form.Label>Gender</Form.Label>
                      <div className="row row-1">
                        <div className="col-6">
                          <div className="list-group list-group-xs mb-0">
                            <div
                              className="list-group-item pd-sm"
                              onClick={() => this.gendeSelect("0")}
                            >
                              <div className="list-group-item-content">
                                <p className="mb-0">Male</p>
                                <div className="custom-control custom-radio d-none">
                                  <input
                                    type="radio"
                                    className="custom-control-input"
                                    id="male"
                                    name="radio-stacked"
                                    required
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-6">
                          <div className="list-group list-group-xs">
                            <div
                              className="list-group-item pd-sm"
                              onClick={() => this.gendeSelect("1")}
                            >
                              <div className="list-group-item-content">
                                <p className="mb-0">Female</p>
                                <div className="custom-control custom-radio d-none">
                                  <input
                                    type="radio"
                                    className="custom-control-input"
                                    id="female"
                                    name="radio-stacked"
                                    required
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                          {/* <Form.Control
                            placeholder="Female"
                          /> */}
                        </div>
                      </div>
                    </Form.Group>
                  </div>
                  {/* <div className="col-6 list-group">
                    <div className="row">
                      <div className="col-md-6 col-6">
                        <div className="list-group-item pd-sm" onClick={() => this.gendeSelect("0")}>
                          <div className="list-group-item-content">
                            <h5 className="title mb-0">Male</h5>
                            <div className="custom-control custom-radio d-none">
                              <input
                                type="radio"
                                className="custom-control-input"
                                id="male"
                                name="radio-stacked"
                                required
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div className="list-group-item pd-sm" onClick={() => this.gendeSelect("1")}>
                          <div className="list-group-item-content">
                            <h5 className="title mb-0">Female</h5>
                            <div className="custom-control custom-radio d-none">
                              <input
                                type="radio"
                                className="custom-control-input"
                                id="female"
                                name="radio-stacked"
                                required
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> */}
                  <div className="col-12">
                    <div className="btn-placeholder">
                      <button onClick={this.handleSubmit} className={"btn btn-primary btn-block " + config.disabled} disabled={config.disabled}>
                        Save Changes
                      </button>
                    </div>
                  </div>
                </Form.Row>
              </Form>
            </section>
          </div>
        </div>
      </section>
    );
  }
}

export default ProfileEdit;
