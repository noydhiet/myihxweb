import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const HistoryActivityInstallationReport = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    const images = { brand, common, sample };
    return (
      <section>
          <div className="container container-sm">
              <div className="box animated fadeInUp">
                  <header>
                  <div className="header-actions">
                      <Link to="/history/activity" className="btn btn-link">
                      <i className="fa fa-arrow-left"></i>
                      </Link>
                  </div>
                  </header>
                  <section className="section-header-text">
                      <div className="heading">
                          <h1>Installation Detail</h1>
                      </div>
                      <div className="row">
                          <div className="col-6">
                              <div className="heading">
                                  <p className="caption mb-0">Date</p>
                                  <p className="title sm">12 March 2019</p>
                              </div>
                          </div>
                          <div className="col-6">
                              <div className="heading">
                                  <p className="caption mb-0">Status</p>
                                  <p className="title sm text-primary">Failed</p>
                              </div>
                          </div>
                      </div>
                  </section>
                  <section className="py-main-sm pb-0">
                      <div className="subheading">
                          <p>Package</p>
                      </div>
                      <div className="list-group mb-0">
                          <Link to="/shop/internet/package-finder/budget/1" className="list-group-item w-arrow">
                              <div className="badges-list">
                                  <div className="badges badge-red left">
                                  <h3>100</h3>
                                  <span>Mbps</span>
                                  </div>
                              </div>
                              <div className="list-group-item-content">
                                  <ul className="list-unstyled mb-0">
                                      <li>Internet unlimited</li>
                                      <li>203 USee TV channels</li>
                                      <li>1000 minutes call</li>
                                  </ul>
                              </div>
                          </Link>
                      </div>
                      <div className="form-group">
                          <label>Address</label>
                          <p>
                          Jl. Jurang Mangu Barat no. 8, Tangerang Selatan, Banten, 15223
                          </p>
                      </div>
                      <div className="subheading">
                          <p>Technician</p>
                      </div>
                      <div className="list-group">
                          <Link
                          to="/shop/internet/package-finder"
                          className="list-group-item transparent w-shadow w-status"
                          >
                              <img alt="icon" src={images.common["sampleImg.jpg"]} className="w-50px rounded-circle" />
                              <div className="list-group-item-content">
                                  <h5 className="title w-md-up-75 mb-0">Fajar Nugraha</h5>
                                  <p className='desc'>On the way</p>
                              </div>
                              <div>
                                  <img alt="icon" src={images.common["icon_ic_chat.png"]} />
                                  <img alt="icon" src={images.common["icon_ic_call.png"]} />
                              </div>
                          </Link>
                      </div>
                      <Link to="/" className="text-primary font-weight-bold">Lear more about the installation process</Link>
                  </section>
                  <section className="py-main-sm">
                      <div className="subheading">
                              <p>Good To Know</p>
                          </div>
                          <div className="milestone mb-3">
                              <div className="milestone-item pb-0 active">
                                  <div className="milestone-item-no-line">
                                      <div className="dot"></div>
                                  </div>
                                  <div className="milestone-item-content">
                                      <p className="text">Installation will take a least 3 hours. Installation may take longer if there are complications. Make sure that you or a contact person is available during that time.</p>
                                  </div>
                              </div>
                              <div className="milestone-item pb-0 active">
                                  <div className="milestone-item-no-line">
                                  <div className="dot"></div>
                                  </div>
                                  <div className="milestone-item-content">
                                      <p className="text">There may be additional charges once the installation begins. These charges will be confirmed to you beforehand</p>
                                  </div>
                              </div>
                              <div className="milestone-item pb-0 active">
                                  <div className="milestone-item-no-line">
                                  <div className="dot"></div>
                                  </div>
                                  <div className="milestone-item-content">
                                      <p className="text">If the installation is not successful, we will reschedule or issue a refund</p>
                                  </div>
                              </div>
                          </div>
                  </section>
                  <div className="btn-placeholder mt-0 text-center">
                      <p className="mb-0">Are you having problems during the installation?</p>
                      <Link to="/history/activity/installation-report" className="btn btn-link">Report Issue</Link>
                  </div>
              </div>
          </div>
      </section>
  )
}

export default HistoryActivityInstallationReport;