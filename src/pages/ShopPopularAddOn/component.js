import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';
import { CardContentStacked } from './../../include/CardPackage'

const ShopPopularAddOn = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    const images = { brand, common, sample } ;
    return (
            <section>
                <div className="container container-sm">
                    <div className="box position-relative animated fadeInUp">
                        <header className="">
                            <div className="header-actions">
                                <Link to="/help/internet" className="btn btn-link">
                                <i className="fa fa-arrow-left" />
                                </Link>
                            </div>
                        </header>
                        <section className="section-header-text pt-4">
                            <div className="heading">
                                <h1>Popular Add-Ons</h1>
                            </div>
                        </section>
                        <section className="py-main-sm pb-0 animated fadeInUp delayp2">
                            <div className="row">
                                <div className="col-6 mb-3">
                                    <CardContentStacked
                                        className=""
                                        img="offer1.png"
                                        mainText="Rp250.000/month"
                                        images={images}
                                        caption="TV Channels"
                                        title="IndiSport"
                                        
                                    />
                                </div>
                                <div className="col-6 mb-3">
                                    <CardContentStacked
                                        className=""
                                        img="offer1.png"
                                        mainText="Rp63.000/month"
                                        images={images}
                                        caption="App"
                                        title="HOOQ"
                                    />
                                </div>
                                <div className="col-6">
                                    <CardContentStacked
                                        className=""
                                        img="offer1.png"
                                        mainText="Start from Rp17.500/month"
                                        images={images}
                                        caption="TV"
                                        title="Edukids"
                                    />
                                </div>
                                <div className="col-6">
                                    <CardContentStacked
                                        className=""
                                        img="offer1.png"
                                        mainText="Rp63.000/month"
                                        images={images}
                                        caption="TV Channels"
                                        title="IndiKorea"
                                    />
                                </div>
                            </div>
                        </section>
                    </div>
                  </div>
            </section>
  )
}

export default ShopPopularAddOn;
