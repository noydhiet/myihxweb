import React from 'react';
import renderer from 'react-test-renderer';
import PersonalDataReviewSignature from '../index';
import PageContextProvider from '../../../context/PageContext';

jest.mock('react-router-dom');
jest.mock('../../../utils/requireContext');
jest.mock('../../../utils/storage', () => ({
  getToken: () => true,
}));

const wrapper = props => renderer.create(
  <PageContextProvider>
    <PersonalDataReviewSignature {...props} />
  </PageContextProvider>);

describe('PersonalDataReviewSignature', () => {
  test('render', () => {
    const result = wrapper().toJSON();
    expect(result.type).toBe('section');
  });
});
