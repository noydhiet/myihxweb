import React, { useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { PageContext } from './../../context/PageContext';
import { getToken } from '../../utils/storage';
const ReviewSignature = () => {
  const { trigger, navCase, pageCase, imgSignature } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  if (!getToken()) {
    window.location.href = '/login';
    return
  }
  const localImgSignature = localStorage.getItem('imgSignature');

  return (
    <section>
      <div className="container container-sm">
        <div className="box box-main animated fadeInUp">
          <header className="header-light white icon-center mb-3 animated fadeInUp delayp1">
            <div className="header-actions">
              <Link to="/personal-data" className="btn btn-link"><i className="fa fa-arrow-left"></i> </Link>
            </div>
            <div className="heading animated fadeInUp delayp1">
              <h1>Review Signature Image</h1>
              <p>Please make sure your signature picture is clear.</p>
            </div>
            <img className="header-img fluid" src={localImgSignature} alt="Icon Success" />
          </header>
          <section className="animated fadeInUp delayp2">

          </section>
          <div className="btn-placeholder inline bottom animated fadeInUp delayp3">
            <Link to="/personal-data" className="btn btn-transparent btn-block"> Reupload </Link>
            <Link to="/personal-data" className="btn btn-primary btn-block"> Confirm  </Link>
          </div>
        </div>
      </div>
    </section>
  )
}

export default ReviewSignature
