import React, { useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { PageContext } from './../../context/PageContext';
const BillDetails = () => {
    const { trigger, navCase, pageCase } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
          <div className="container container-sm">
              <div className="box animated fadeInUp">
                  <header>
                  <div className="header-actions">
                      <Link to="/shop/internet/speed-on-demand" className="btn btn-link">
                      <i className="fa fa-arrow-left"></i>
                      </Link>
                  </div>
                  </header>
                  <section className="section-header-text">
                      <div className="heading">
                          <h1>Your Bill</h1>
                          <p>Make sure your billing detail is correct</p>
                      </div>
                  </section>
                  <section className="py-main-sm pb-0">
                      <div className="row">
                          <div className="col-6">
                            <div className="form-group">
                                <label className="help-block">Billing Period</label>
                                <p>July 2019</p>
                            </div>
                          </div>
                          <div className="col-6">
                            <div className="form-group">
                                <label className="help-block">Due Date</label>
                                <p>21 July 2019</p>
                            </div>
                          </div>
                      </div>
                      <div className="list-group list-group-transparent">
                          <div className="list-group-item pl-0 pr-0">
                              <div className="list-group-item-content">
                                  <div className="subheading mb-0">
                                      <p>Total</p>
                                  </div>
                                  <h1>Rp2.650.000</h1>
                                  <p className="help-block">Includes 10% tax</p>
                              </div>
                              <label className="label-primary">Belum Dibayar</label>
                          </div>
                      </div>
                      <div className="subheading">
                          <p>Bill Detail</p>
                      </div>
                      <div className="list-group list-group-activity mb-0">
                        <div className="list-group-item w-arrow">
                            <div className="list-group-item-content mb-0">
                            <p className="title">10 Mbps IndiGamer TriplePlay</p>
                            <small className="help-block">
                                10Mbps + 92 channels + 100 minutes
                            </small>
                            <Link
                                to="/history/purchase/details"
                                className="btn btn-link px-0 py-0"
                            >
                                Lihat Detail
                            </Link>
                            </div>
                        </div>
                        <div className="list-group-item w-arrow">
                            <div className="list-group-item-content mb-0">
                            <p className="title">30 Mbps Extra Speed</p>
                            <small className="help-block">
                                3 days package * 1-3 July 2019
                            </small>
                            </div>
                        </div>
                            <div className="list-group-item w-arrow">
                                <div className="list-group-item-content mb-0">
                                <p className="title">Minipack IndiKids Lite</p>
                                <small className="help-block">3 channels</small>
                                <Link
                                    to="/history/purchase/details"
                                    className="btn btn-link px-0 py-0"
                                >
                                    Lihat Detail
                                </Link>
                                </div>
                            </div>
                        </div>
                        <div className="btn-placeholder mb-4">
                            <Link to="/bill/success" className="btn btn-primary btn-block">Pay</Link>
                        </div>
                        <div className="text-center">
                            <p className="help-block mb-0">Have questions about this bill?</p>
                            <Link to="/help" className="btn btn-link">Get Help</Link>
                        </div>
                  </section>
              </div>
          </div>
      </section>
    )
}

export default BillDetails;
