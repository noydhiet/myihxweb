import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Slider from 'react-slick'
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import { getToken } from '../../utils/storage';

class ShopInternet extends Component {
  static contextType = PageContext;
  render() {
    const { paketActive, extraSpeedActive, upgradeToFiberActive, wifiIdActive, wifiExtenderActive, upgradeSpeedActive, cloudActive, indihomeStudyActive, shopPrice, shopPriceFixed, btnText, linkTo } = this.props;
    const { modal } = this.context;
    const config = {
      carouselTab: {
        className: "tab-list",
        dots: false,
        infinite: false,
        arrows: false,
        speed: 500,
        slidesToShow: 7,
        slidesToScroll: 7,
        responsive: [
          {
            breakpoint: 991,
            settings: {
              dots: false,
              slidesToShow: 4.5,
              slidesToScroll: 4.5
            }
          },
          {
            breakpoint: 575,
            settings: {
              dots: false,
              slidesToShow: 3.5,
              slidesToScroll: 3.5
            }
          },
          {
            breakpoint: 324,
            settings: {
              dots: false,
              slidesToShow: 2.55,
              slidesToScroll: 2.55
            }
          }
        ]
      }
    }
    const shopPriceView = shopPrice === undefined ? null :
      <div className="shop-price animated fadeInUp delayp4 d-none d-md-block">
        <div className="container">
          <div className="shop-price-info">
            <h3>
              {shopPrice} <small>/month</small>
            </h3>
            <Link to="#" className="">
              View Pricing Detail <i className="fal fa-angle-right"></i>
            </Link>
          </div>
          {getToken()
            ? (<Link to={linkTo} className="btn btn-primary">{btnText}</Link>)
            : (<button className="btn btn-primary" onClick={() => modal('modalSubscribe')}>{btnText}</button>)}
        </div>
      </div>;
    const shopPriceFixedView = shopPriceFixed === undefined ? null :
      <div
        className="shop-price fixed-bottom animated fadeInUp delayp8"
        id="shop-price"
      >
        <div className="container">
          <div className="shop-price-info">
            <h3>
              {shopPriceFixed}<small>/month</small>
            </h3>
            <Link to="#" className="">
              View Pricing Detail <i className="fal fa-angle-right"></i>
            </Link>
          </div>
          {getToken()
            ? (<Link to={linkTo} className="btn btn-primary">{btnText}</Link>)
            : (<button className="btn btn-primary" onClick={() => modal('modalSubscribe')}>{btnText}</button>)}
        </div>
      </div>;

    return (
      <TextContext.Consumer>{(context)=>{
        const { shopText } = context;
        return (
          <div>
          <div className="cover cover-sm cover-bg-white-sm bg-red-gradient">
            <div className="header-actions d-block d-md-none">
              <Link
                to="/shop/"
                className="btn btn-link"
              >
                <i className="fa fa-arrow-left"></i>
              </Link>
            </div>
            <div className="container">
              <nav aria-label="breadcrumb" className="d-none d-md-block">
                <ol className="breadcrumb animated fadeInUp">
                  <li className="breadcrumb-item"><Link to="/shop"><i className="fa fa-arrow-left mr-1"></i>{shopText.coverShopLinkText}</Link></li>
                </ol>
              </nav>
              <div className="cover-content">
                <h1 className="cover-title animated fadeInUp delayp1">{shopText.coverShopInternetTextTitle}</h1>
                <p className="cover-text animated fadeInUp delayp2">{shopText.coverShopInternetTextDesc}</p>
              </div>
              {shopPriceView}
            </div>
          </div>
          <section className="shop-tab-list animated fadeInUp delayp3">
            <div className="container">
              <Slider {...config.carouselTab}>
                <div className={"tab-item " + paketActive}>
                  <Link to="/shop/internet/package">Paket</Link>
                </div>
                <div className={"tab-item " + upgradeToFiberActive}>
                  <Link to="/shop/internet/upgrade-to-fiber">Upgrade To Fiber</Link>
                </div>
                <div className={"tab-item " + extraSpeedActive}>
                  <Link to="/shop/internet/speed-on-demand">Extra Speed</Link>
                </div>
                <div className={"tab-item " + wifiIdActive}>
                  <Link to="/shop/internet/wifi-id-seamless">wifi.id</Link>
                </div>
                <div className={"tab-item " + wifiExtenderActive}>
                  <Link to="/shop/internet/wifi-extender">wifi Extender</Link>
                </div>
                <div className={"tab-item " + upgradeSpeedActive}>
                  <Link to="/shop/internet/upgrade-speed">Upgrade Speed</Link>
                </div>
                <div className={"tab-item " + cloudActive}>
                  <Link to="/shop/internet/cloud-storage">Cloud</Link>
                </div>
                <div className={"tab-item " + indihomeStudyActive}>
                  <Link to="/shop/internet/indihome-study">IndiHome Study</Link>
                </div>
              </Slider>
            </div>
          </section>
          {shopPriceFixedView}
        </div>
        )
      }}

      </TextContext.Consumer>
    )
  }
}


export default ShopInternet
