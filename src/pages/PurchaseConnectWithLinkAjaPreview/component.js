import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';
import { Form } from 'react-bootstrap';
import { getToken } from '../../utils/storage';

const PurchaseConnectWithLinkAjaPreview = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const [ cond, setCond ] = useState({
        mobileNumber: ""
    });
    const images = { brand, common, sample } ;
    const handleInputChange = (e) => {
        e.persist();
        setCond(inputs => ({...inputs, [e.target.name]:e.target.value}));
    }
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    const disabled = cond.mobileNumber.length < 1 ? "disabled" : "";
    return (
        <div className="container container-xs">
            <div className="box animated fadeInUp">
                <header className="header-light white w-icon">
                <div className="header-actions">
                    <Link to="/purchase/payment-method/payment-with-linkaja" className="btn btn-link"><i className="fa fa-arrow-left"></i></Link>
                    <Link to="/" className="btn btn-link"><i className="fa fa-question-circle"></i></Link>
                </div>
                <img className="header-img" src={images.common["ic_header_verification.png"]} alt="img" />
                <div className="heading animated fadeInUp delayp1">
                    <h1>Connect LinkAja Account</h1>
                    <p>Enter your LinkAja mobile number to connect</p>
                </div>
                </header>

                <Form>
                <section className="animated fadeInUp delayp2">
                        <Form.Group>
                            <Form.Label>Mobile Number</Form.Label>
                            <Form.Control
                                type="number"
                                name="mobileNumber"
                                value={cond.mobileNumber}
                                onChange={handleInputChange}
                            />
                        </Form.Group>
                </section>

                    <div className="btn-placeholder bottom">
                        {/* <button className={"btn btn-primary btn-block " + disabled} type="submit" value="submit">Login</button> */}
                        <Link to="/purchase/link-aja/otp" className={"btn btn-primary btn-block " + disabled}>Login</Link>
                    </div>
                </Form>
            </div>
        </div>
    )
}

export default PurchaseConnectWithLinkAjaPreview;