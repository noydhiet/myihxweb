import React from 'react';
import renderer from 'react-test-renderer';
import CheckCoverageResultPt3 from '../index';
import PageContextProvider from '../../../context/PageContext';

jest.mock('react-router-dom');
jest.mock('../../../utils/requireContext');

const wrapper = props => renderer.create(
  <PageContextProvider>
    <CheckCoverageResultPt3 {...props} />
  </PageContextProvider>);

describe('CheckCoverageResultPt3', () => {
  test('render', () => {
    const result = wrapper().toJSON();
    expect(result.type).toBe('section');
  });
});
