import React, { Component } from 'react';
import Promo from '../Promo';
import { PageContext } from '../../context/PageContext';

class PromoDetail extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    const { common, brand, sample  } = this.context;
    const images = { common, brand, sample };
    return (
      <div>
        <Promo promoDetail="active"/>
        <SectionPopular />
        <SectionMenu images={images}/>
      </div>
    )
  }
}

export default PromoDetail;


const SectionPopular = () => {
    return (
        <section className="py-main">
            <div className="container">
                <div className="row">
                    <div className="col-md-10 col-12 mx-auto animated fadeInUp delayp4">
                        <div className="heading">
                            <h4>Most Popular for Family of 4:Unlimited 50Mbps Internet & USee TV</h4>
                            <small className="help-block">21 June 2019, 10:00</small>
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </section>
    )
}

const SectionMenu = ({images}) => {
    return (
        <section className="py-main bg-white">
            <div className="container">
                <div className="row row-4 animated fadeInUp delayp5">
                    <div className="col-md-6 col-12 order-md-last content-center mb-3">
                        <img src={images.common['img_placeholder_rectangle.jpg']} className="img-fluid" alt="TV Channels"/>
                    </div>
                    <div className="col-md-6 col-12 order-md-first content-center-left mb-3">
                        <div className="heading">
                            <h4>MiniPack IndiKids</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                        <div className="btn-placeholder mt-0">
                            <button className="btn btn-primary btn-block">Get Offer</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}