import React, { useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { PageContext } from './../../context/PageContext';

const PackageFinder = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = { brand, common, sample } ;
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
      <section>
        <div className="container container-sm">
            <div className="box box-main animated fadeInUp">
                <header className="header-light white w-icon-lg animated fadeInUp delayp1">
                <div className="header-actions">
                    <Link to="/shop/internet/package"  className="btn btn-link"> <i  className="fa fa-arrow-left"></i> </Link>
                </div>
                    <img className="header-img" src={images.common["graphic_find.png"]} alt="Icon Success" />
                </header>
                <section className="animated fadeInUp delayp2">
                    <div className="heading">
                        <h1>Find a package based on</h1>
                    </div>
                    <div className="list-group mb-0">
                        <Link to="/shop/internet/package-finder/budget/1" className="list-group-item w-arrow">
                            <img src={images.common["ic_selection_budget.png"]} className="img-fluid w-50px" alt="Budget Icon" />
                            <div className="list-group-item-content">
                                <h5 className="title">My Budget</h5>
                                <span className="desc">Find the best package for your budget</span>
                            </div>
                        </Link>
                        <Link to="/shop/internet/package-finder/needs/1" className="list-group-item w-arrow">
                            <img src={images.common["ic_selection_needs.png"]} className="img-fluid w-50px" alt="Need Icon" />
                            <div className="list-group-item-content">
                                <h5 className="title">My Needs</h5>
                                <span className="desc">Find the best package for your need</span>
                            </div>
                        </Link>
                    </div>
                </section>
            </div>
          </div>
        </section>
  )
}
export default PackageFinder
