import React, { Component } from "react";
import { Link } from "react-router-dom";
import { PageContext } from './../../context/PageContext';
import { CardContent } from './../../include/CardPackage'
import Slider from "react-slick";

class ShopTvAppsIflixDetails extends Component {
    static contextType = PageContext;
    componentDidMount() {
        this.context.trigger(2);
        this.context.navCase("w-md-up-block");
        this.context.pageCase("w-bg");
    }
    render() {
        const { common, brand, sample } = this.context;
        const images = { common, brand, sample };
        const config = {
            carousel: {
              className: "primary-carousel overflow-inherit",
              dots: false,
              infinite: false,
              arrows: false,
              speed: 500,
              slidesToShow: 4,
              slidesToScroll: 4,
              responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: false,
                    swipe: false,
                    dots: false
                  }
                },
                {
                  breakpoint: 736,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: false,
                    dots: false,
                  }
                },
                {
                  breakpoint: 575,
                  settings: {
                    slidesToShow: 3.2,
                    slidesToScroll: 3.2,
                    infinite: false,
                  }
                }
              ]
            }
        };
        return (
            <section>
              <div className="container container-sm">
                <div className="box animated fadeInUp">
                  <header>
                    <div className="header-actions">
                      <Link to="/shop/tv/apps" className="btn btn-link">
                        <i className="fa fa-arrow-left"></i>
                      </Link>
                    </div>
                  </header>
                  <section className="section-header-text pt-4">
                    <div className="heading">
                      <h1>iFlix</h1>
                    </div>
                  </section>
                  <section className="py-main-sm pb-0">
                    <div className="subheading">
                      <p>Watch All These on iFlix</p>
                    </div>
                    <Slider {...config.carousel}>
                      <div className="carousel-item">
                        <CardContent
                          link="#"
                          img="poster1.png"
                          images={images}
                        />
                      </div>
                      <div className="carousel-item">
                        <CardContent
                          link="#"
                          img="poster2.png"
                          images={images}
                        />
                      </div>
                      <div className="carousel-item">
                        <CardContent
                          link="#"
                          img="poster3.png"
                          images={images}
                        />
                      </div>
                      <div className="carousel-item">
                        <CardContent
                          link="#"
                          img="poster4.png"
                          images={images}
                        />
                      </div>
                      <div className="carousel-item">
                        <CardContent
                          link="#"
                          img="poster5.png"
                          images={images}
                        />
                      </div>
                    </Slider>
                  </section>
                  <section className="py-main-sm pb-0">
                    <div className="subheading">
                        <p>Promo</p>
                    </div>
                    <p>Gratis untuk semua pelanggan IndiHome, for a limited time!</p>
                    <div className="subheading">
                        <p>About this Service</p>
                    </div>

                    <p>Dengan iFlix, Anda bisa menonton tayangan favorit mulai dari serial TV hollywood, film-film terbaik dari seluruh dunia; acara TV lokal; sitkom; kartun, sampai drama korea, semuanya bisa Anda nikmati di iflix kapan saja, dan di mana saja!</p>
                    <p>Untuk menonton tayangan iflix, Anda dapat mengunduh konten-kontennya terlebih dahulu hingga ke lima perangkat dan menontonnya secara offline. Tidak hanya itu saja, tontonan iflix dapat disaksikan di dua perangkat secara bersamaan. Layanan Add-on iflix dapat digunakan di: televisi (via Hybrid Box IndiHome), PC/laptop, atau tablet/smartphone.</p>

                    <div className="subheading">
                        <p>Watch On</p>
                    </div>
                    <div className="row mb-3">
                        <div className="col-4 content-center">
                            <div>
                              <img alt="icon" src={images.common["icon_ic_selection_device_tv_b.png"]} className="img-fluid w-100px" />
                              <p className="text-center">TV</p>
                            </div>
                        </div>
                        <div className="col-4 content-center">
                            <div>
                              <img alt="icon" src={images.common["icon_ic_selection_device_computer_b.png"]} className="img-fluid w-100px" />
                              <p className="text-center">Komputer</p>
                            </div>
                        </div>
                        <div className="col-4 content-center">
                            <div>
                              <img alt="icon" src={images.common["icon_ic_selection_device_mobile_b.png"]} className="img-fluid w-100px" />
                              <p className="text-center">Handphone & Tablet</p>
                            </div>
                        </div>
                    </div>
                    <div className="btn-placeholder">
                      <Link to="/shop/tv/apps/iflix/redirect" className="btn btn-primary btn-block">Subscribe</Link>
                    </div>
                  </section>
                </div>
              </div>
            </section>
        )
    }
}
 

export default ShopTvAppsIflixDetails;
