import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Modal, Form } from "react-bootstrap";
import Slider from "react-slick";
import { PageContext } from './../../context/PageContext';
import { scheduleService, technicianTicket, userProfile, actionSupportTicket } from '../../services';
import moment from 'moment';
import { getToken, getUserProfile, setUserProfile } from '../../utils/storage';

class FisikTicket extends Component {
  constructor(props) {
    super(props)
    if(!getToken()) {
      window.location.href='/login';
      return	
    }
    if(!props.location.state) {
      props.history.push('/');
    }
  }
  static contextType = PageContext;
  state = {
    modalContact: false,
    modalSessionRoutes: false,
    status: false,
    name: '',
    mobileNumber: '',
    time: 10 * 60,
    dateSlots: undefined,
    cardMorningAfternoon: [{ show: false, slotId: '' }, { show: false, slotId: '' }, { show: false, slotId: '' }, { show: false, slotId: '' }],
    morning_1: false,
    morning_2: false,
    evening_1: false,
    evening_2: false,
    selectedDate: undefined,
    selectedDateIndex: undefined,
    selectedCard: undefined,
    modelForbiddenInstalltion:false,
    isButtonLoading: false,
    attemptExhausted:false,
    modalServiceUnavailable: false
  };

  tick = () => {
    const { time } = this.state;

    if (time === 0) {
      clearInterval(this.intervalHandle);
      this.setState({ modalSessionRoutes: true });
    }
    else {
      this.setState({ time: time - 1 });
    }
  }

  componentDidMount() {
    //this.context.loggedIn();
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("w-bg");
    //Countdown
    this.intervalHandle = setInterval(this.tick, 1000);
    scheduleService().then(response => {
      console.log('scheduleService', response);
      if(response.statusText === 'OK' && response.data.data.availableTimes) {
          var tempDate = undefined;
          const groupedDateSlots = response.data.data.availableTimes.length === 0 ? null : response.data.data.availableTimes.reduce((groupedData, { availability, date, bookingId, crewId, dateTime, information, timeSlot }) => {
            const formattedDate = moment(dateTime, 'DD-MM-YYYY HH:mm').format('DD-MM-YYYY')
            if(!tempDate) tempDate = formattedDate
            if (!groupedData[formattedDate]) groupedData[formattedDate] = [];
            groupedData[formattedDate].push({bookingId, crewId, information, timeSlot, availability, date});
            return groupedData;
          }, {});
          console.log('groupedDateSlots', groupedDateSlots);
          if(groupedDateSlots) {
            this.setState({
              dateSlots: groupedDateSlots
            }, () => { this.setSchedule(0, groupedDateSlots, tempDate) });
          }
      } else {
        this.setState({
          dateSlots: null
        });
      }
    }).catch(error => {
      this.setState({
        dateSlots: null
      });
      console.error(error);
    })
  }

  setSchedule = (mode, localDateSlots, formattedDate) => {
    const cardIndexSelect = [ false, false, false, false ];
    const { cardMorningAfternoon } = this.state;
    cardMorningAfternoon[0].show = false;
    cardMorningAfternoon[1].show = false;
    cardMorningAfternoon[2].show = false;
    cardMorningAfternoon[3].show = false;
    for(const [index, item] of localDateSlots[formattedDate].entries()) {
      if(item.timeSlot.slotId === 'morning-1' && item.availability === 1) {
        cardIndexSelect[0] = true;
        cardMorningAfternoon[0].show = true;
        cardMorningAfternoon[0].slotId = item.bookingId;
      }
      if(item.timeSlot.slotId === 'morning-2' && item.availability === 1) {
        cardIndexSelect[1] = true;
        cardMorningAfternoon[1].show = true;
        cardMorningAfternoon[1].slotId = item.bookingId;
      }
      if(item.timeSlot.slotId === 'evening-1' && item.availability === 1) {
        cardIndexSelect[2] = true;
        cardMorningAfternoon[2].show = true;
        cardMorningAfternoon[2].slotId = item.bookingId;
      }
      if(item.timeSlot.slotId === 'evening-2' && item.availability === 1) {
        cardIndexSelect[3] = true;
        cardMorningAfternoon[3].show = true;
        cardMorningAfternoon[3].slotId = item.bookingId;
      }
    }
    this.setState({
      cardMorningAfternoon: cardMorningAfternoon, selectedDate: formattedDate, selectedDateIndex: mode
    });
    if(cardIndexSelect[0]) {
      this.estimatedArrival("0")
    } else if(cardIndexSelect[1]) {
      this.estimatedArrival("1")
    } else if(cardIndexSelect[2]) {
      this.estimatedArrival("2")
    } else if(cardIndexSelect[3]) {
      this.estimatedArrival("3")
    } else {
      this.estimatedArrival(undefined)
    }
  }

  estimatedArrival = (mode) => {
    switch (mode) {
      case "0":
        this.setState({
          selectedCard: "0", morning_1: true, morning_2: false, evening_1: false, evening_2: false
        });
        break;
      case "1":
        this.setState({
          selectedCard: "1", morning_1: false, morning_2: true, evening_1: false, evening_2: false
        });
        break;
      case "2":
        this.setState({
          selectedCard: "2", morning_1: false, morning_2: false, evening_1: true, evening_2: false
        });
      break;
      case "3":
        this.setState({
          selectedCard: "3", morning_1: false, morning_2: false, evening_1: false, evening_2: true
        });
      break;
      default:
        this.setState({
          selectedCard: undefined, morning_1: false, morning_2: false, evening_1: false, evening_2: false
        });
      break;
    }
  };
  scheduleTechnician = e => {
    e.preventDefault();
    const { dateSlots, selectedDate, selectedCard, isButtonLoading } = this.state;
    const { history, location: { state } } = this.props;
    if(!isButtonLoading) {
      this.setState({
        isButtonLoading: true
      });
      if(state.ticketId) {
        let bookingId = undefined;
        let time = undefined;
        for(const [index, item] of dateSlots[selectedDate].entries()) {
          if(item.availability === 1) {
            console.log('selectedCard=>>', selectedCard, item)
            if((selectedCard === "0" && item.timeSlot.slotId === "morning-1") || (selectedCard === "1" && item.timeSlot.slotId === "morning-2")
            || (selectedCard === "2" && item.timeSlot.slotId === "evening-1") || (selectedCard === "3" && item.timeSlot.slotId === "evening-2")) {
              bookingId = item.bookingId;
              time = item.timeSlot.slot;
            }
          }
        }
        const { ticketId } = state;
        const body = {
          "bookingId": bookingId
        }
        console.log('body', body, ticketId)
        actionSupportTicket(ticketId, 'REOPEN', body).then(response => {
          console.log('responsexz', response)
          if(response.data.status === 200 || response.data.status === 201) {
            const { history } = this.props;
            history.push('/installation/fisik-ticket/re-open/success/' + selectedDate + '--' + time)
          } else if(response.data.status === 4006) {
            this.setState({attemptExhausted: true})
          } else {
            this.setState({modalServiceUnavailable: true})
          }
          this.setState({
            isButtonLoading: false
          });
        }).catch(error => {
          console.error('errorzx', error)
          this.setState({
            isButtonLoading: false, modalServiceUnavailable: true
          });
        })
      } else {
        userProfile().then(({data}) => {
          if(data.status === 200 || data.status === 201) {
            setUserProfile(data.data);
            if(data.data.accounts.length > 0) {
              let bookingId = undefined;
              let time = undefined;
              for(const [index, item] of dateSlots[selectedDate].entries()) {
                if(item.availability === 1) {
                  console.log('selectedCard=>>', selectedCard, item)
                  if((selectedCard === "0" && item.timeSlot.slotId === "morning-1") || (selectedCard === "1" && item.timeSlot.slotId === "morning-2")
                  || (selectedCard === "2" && item.timeSlot.slotId === "evening-1") || (selectedCard === "3" && item.timeSlot.slotId === "evening-2")) {
                    bookingId = item.bookingId;
                    time = item.timeSlot.slot;
                  }
                }
              }
              const queryParam = `bookingId=${bookingId}&selectedIndiHomeNum=${data.data.accounts[0].indiHomeNum}`
              // console.log('state.data', state.data, queryParam)
              console.log('state payload', state.payload)
              technicianTicket(state.payload, queryParam)
              .then(({data}) => {
                console.log('technicianTicket', data);
                if(data.status === 200 || data.status === 201) {
                  history.push(`/installation/fisik-ticket/scheduled:${selectedDate}--${time}`);
                } else {
                  this.setState({modalServiceUnavailable: true})
                }
                this.setState({
                  isButtonLoading: false
                })
              }).catch(error => {
                if(error.response.status === 403) {
                  this.setState({modelForbiddenInstalltion: true})
                } else {
                  this.setState({modalServiceUnavailable: true})
                }
                this.setState({
                  isButtonLoading: false
                })
                console.error(error)
              });
            } else {
              console.log('My indihome number is empty')
            }
          } else {
            this.setState({modalServiceUnavailable: true})
          }
        }).catch( error => {this.setState({modalServiceUnavailable: true});console.error(error)});
      }
    }
  }
//   estimatedArrival = mode => {
//     const listCheck = document.getElementsByClassName("list-check");
//     const morning = document.getElementById("morning");
//     const afternoon = document.getElementById("afternoon");
//     switch (mode) {
//       case "0":
//         morning.checked = true;
//         afternoon.checked = false;
//         listCheck[0].classList.add("active");
//         listCheck[1].classList.remove("active");
//         break;

//       case "1":
//         afternoon.checked = true;
//         morning.checked = false;
//         listCheck[0].classList.remove("active");
//         listCheck[1].classList.add("active");
//         break;

//       default:
//         return null;
//     }
//   };

  modal = (mode) => {
    switch (mode) {
      case "modalContact":
        this.setState({ modalContact: !this.state.modalContact });
        break;

      case "retrySession":
        this.intervalHandle = setInterval(this.tick, 1000);
        this.setState({
          modalSessionRoutes: !this.state.modalSessionRoutes,
          time: 10 * 60
        });
        break;

      case "save":
        this.setState({
          modalContact: !this.state.modalContact,
          status: !this.state.false,
          name: "",
          mobileNumber: ""
        });
        break;

      case "close":
        this.setState({
          modalContact: false,
          modalSessionRoutes: false,
          modelForbiddenInstalltion:false,
          attemptExhausted:false,
          modalServiceUnavailable: false
        });
        break;

      default:
        return null
    }
  };
  //   save = () => {
  //     this.setState({
  //       modalContact: !this.state.modalContact,
  //       status: !this.state.false,
  //       name: "",
  //       mobileNumber: ""
  //     });
  //   };
  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  render() {
    const { modalContact, modalSessionRoutes, status, name, mobileNumber, time, dateSlots, cardMorningAfternoon, morning_1, morning_2, evening_1, evening_2, selectedDateIndex, selectedCard, modelForbiddenInstalltion, isButtonLoading, attemptExhausted, ticketId, modalServiceUnavailable } = this.state;
    const { history } = this.props;
    const recommend = ["bnr_recommend01.png", "bnr_recommend02.png", "bnr_recommend03.png"];
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    const carousel = {
      className: "primary-carousel overflow-inherit mb-4",
      dots: false,
      infinite: false,
      arrows: false,
      speed: 500,
      slidesToShow: 5.3,
      slidesToScroll: 5.3,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 7,
            slidesToScroll: 7
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 5.5,
            slidesToScroll: 5.5
          }
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 4.5,
            slidesToScroll: 4.5
          }
        },
        {
          breakpoint: 320,
          settings: {
            slidesToShow: 3.5,
            slidesToScroll: 3.5
          }
        }
      ]
    };
    const userProfile = getUserProfile();
    const accounts = userProfile && userProfile.accounts.length > 0 ? userProfile.accounts[0] : null;
    const location = accounts && Object.keys(accounts.Location).length > 0 ? accounts.Location : null;
    const coverStyle = {
      background: `url(${images.common["bnr_blue_2nd.png"]}) no-repeat center`,
      backgroundSize: "cover",
      border: "none"
    };
    const disabled =
      name.length < 1 || mobileNumber.length < 1 ? "disabled" : "";
    const viewContact = status ? (
      <div className="list-group">
        <Link
          to="/shop/internet/package-finder"
          className="list-group-item transparent w-arrow"
        >
          <img alt="" src={images.common["ic_location_pin.png"]} className="wx-25px" />
          <div className="list-group-item-content">
            <h5 className="title w-md-up-75 mb-0">Siti Hasanah</h5>
            <span className="desc">08123456789</span>
            <p className="text-primary mb-0">Edit Contact</p>
          </div>
        </Link>
      </div>
    ) : (

        <div className="list-group list-group-banner">
          <Link
            to="#"
            className="list-group-item light w-arrow bg-cover"
            style={coverStyle}
            onClick={() => this.modal("modalContact")}
          >
            <img alt="" src={images.common["ic_call.png"]} className="w-50px" />
            <div className="list-group-item-content">
              <h5 className="title w-md-up-75 mb-0">Add Secondary Contact</h5>
              <p className='desc'>Not home during the installation? <br /> Add a second contact person</p>
            </div>
          </Link>
        </div>
      );
      const carousalCards = !dateSlots
      ?
      <div key={0}
          className="carousel-item-sm">
          <div className={`badges badge-date active selector`}>
            <small>No slots available</small>
            <h3></h3>
            <span></span>
          </div>
      </div>
      :
      Object.keys(dateSlots).map((formattedDate, index) => {
        const date = moment(formattedDate, 'DD-MM-YYYY');
        return(
         <div key={index}
              className="carousel-item-sm"
              onClick={() => this.setSchedule(index, dateSlots, formattedDate)} >
              <div className={`badges badge-date ${index === selectedDateIndex ? 'active' : ''} selector`}>
                <small>{date.format('ddd')}</small>
                <h3>{date.format('DD')}</h3>
                <span>{date.format('MMM')}</span>
              </div>
          </div>
        );
      })
    const Loading = ({ images }) => {
      return (
        <span><i className="fa fa-circle-notch fa-spin"></i></span>
      )
    }
    return (
      <section>
        <div className="container container-sm">
          <div className="box position-relative animated fadeInUp">
            <div className="countdown-timer block">
              <p>
                <i className="fal fa-clock"></i> {`${Math.floor(time / 60)}`.padStart(2, '0')}:{`${time % 60}`.padStart(2, '0')}
              </p>
            </div>
            <header className="">
              <div className="header-actions">
                <Link to="#" onClick={ e => { e.preventDefault(); history.goBack(); }} className="btn btn-link">
                  <i className="fa fa-arrow-left" />
                </Link>
              </div>
              <div className="heading animated fadeInUp delayp1">
                <h1>{ticketId ? `Reschedule Service` : `Schedule Service`}</h1>
                <p>
                  Finish your schedule before the timer runs out to book a
                  technician.
                </p>
              </div>
            </header>
            <section className="section-header-card py-main-sm animated fadeInUp delayp2">
              <div className="subheading mb-2">
                <p>Issue Summary</p>
              </div>
              <div className="list-group">
                  <div className="list-group-item">
                      <div className="list-group-item-content">
                          <p className="desc">Internet has been slow for more than 7 days. Technician will visit to analyze the problem</p>
                      </div>
                  </div>
              </div>
              <div className="form-group">
                <label>Address</label>
                <p>
                  {location ? `${location.street}, ${location.district}, ${location.city}, ${location.province}, ${location.postalCode}` : ''}
                </p>
              </div>
            </section>
            <section className="section-fluid bg-gray-50 animated fadeInUp delayp4">
              <div className="subheading mb-3">
                <p>Set Schedule</p>
              </div>
              {
                dateSlots === undefined
              ? 
              <>
                <Slider {...carousel}>
                  {recommend.map((value, index)=>{
                    let sumDelay = index+1;
                    return (
                      <div className={"carousel-item animated fadeInUp delayp" + sumDelay} key={index}>
                        <section className="card-skeleton">
                          <figure className="card-image loading"></figure>
                        </section>
                      </div>
                    )
                  })}
                </Slider>
                </>
              :
              <Slider {...carousel}>
                {carousalCards}
              </Slider>
              }
              <div className="subheading mb-3">
                <p>Estimated Arrival</p>
              </div>
              <div className="list-group">
                <div className="row row-1 mb-0">
                  <div className="col-6">
                    <button
                      className={`btn btn-block list-group-item bg-green-color list-check ${morning_1 ? 'active' : ''} inherit-height${cardMorningAfternoon[0].show === false ? ' disabled': ''}`}
                      disabled={cardMorningAfternoon[0].show === false ? true : false}
                      onClick={() => this.estimatedArrival("0")}
                    >
                      <div className="list-group-item-content">
                        <h5 className="title text-center">08:00 - 10:00</h5>
                        <div className="custom-control custom-radio d-none">
                          <input
                            type="radio"
                            className="custom-control-input"
                            id="morning"
                            name="radio-stacked"
                            required
                          />
                        </div>
                        {/* <label className="input-check-card">
                                        <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                    </label> */}
                      </div>
                    </button>
                  </div>
                  <div className="col-6">
                    <button
                      className={`btn btn-block list-group-item bg-green-color list-check ${morning_2 ? 'active' : ''} inherit-height${cardMorningAfternoon[1].show === false ? ' disabled': ''}`}
                      disabled={cardMorningAfternoon[1].show === false ? true : false}
                      onClick={() => this.estimatedArrival("1")}
                    >
                      <div className="list-group-item-content">
                        <h5 className="title text-center">10:00 - 12:00</h5>
                        <div className="custom-control custom-radio d-none">
                          <input
                            type="radio"
                            className="custom-control-input"
                            id="afternoon"
                            name="radio-stacked"
                            required
                          />
                        </div>
                        {/* <label className="input-check-card">
                                        <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                    </label> */}
                      </div>
                    </button>
                  </div>
                  <div className="col-6">
                    <button
                      className={`btn btn-block list-group-item bg-green-color list-check ${evening_1 ? 'active' : ''} inherit-height${cardMorningAfternoon[2].show === false ? ' disabled': ''}`}
                      disabled={cardMorningAfternoon[2].show === false ? true : false}
                      onClick={() => this.estimatedArrival("2")}
                    >
                      <div className="list-group-item-content">
                        <h5 className="title text-center">13:00 - 15:00</h5>
                        <div className="custom-control custom-radio d-none">
                          <input
                            type="radio"
                            className="custom-control-input"
                            id="afternoon"
                            name="radio-stacked"
                            required
                          />
                        </div>
                        {/* <label className="input-check-card">
                                        <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                    </label> */}
                      </div>
                    </button>
                  </div>
                  <div className="col-6">
                    <button
                      className={`btn btn-block list-group-item bg-green-color list-check ${evening_2 ? 'active' : ''} inherit-height${cardMorningAfternoon[3].show === false ? ' disabled': ''}`}
                      disabled={cardMorningAfternoon[3].show === false ? true : false}
                      onClick={() => this.estimatedArrival("3")}
                    >
                      <div className="list-group-item-content">
                        <h5 className="title text-center">15:00 - 17:00</h5>
                        <div className="custom-control custom-radio d-none">
                          <input
                            type="radio"
                            className="custom-control-input"
                            id="afternoon"
                            name="radio-stacked"
                            required
                          />
                        </div>
                        {/* <label className="input-check-card">
                                        <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                    </label> */}
                      </div>
                    </button>
                  </div>
                </div>
              </div>
              <p className="help-block">
                Time range is the Estimated is the estimated arrival time. You
                will receive a notification when the technician is heading to
                your location
              </p>
            </section>
            <section className="py-main-sm pb-0">
              <div className="subheading">
                <p>Secondary Contact</p>
              </div>
              {viewContact}
              <div className="btn-placeholder">
                <div className="row row-2">
                  <div className="col-6">
                    <Link to="" className="btn btn-transparent btn-block">
                      Cancel
                    </Link>
                  </div>
                  <div className="col-6">
                    <Link
                      className={`btn btn-primary btn-block ${!selectedCard || isButtonLoading ? 'disabled' : ''}`}
                      to="#"
                      onClick={e => this.scheduleTechnician(e)} >
                      {isButtonLoading ? <Loading images={images}/> : "Next"}
                    </Link>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
        {/* Modal */}
        <Modal show={modalContact} onHide={() => this.modal('close')} centered={true}>
          <div className="modal-body p-box pb-0">
            <div className="heading pt-sm-down-4">
              <h3>Add a second contact person</h3>
              <p>
                Our technician will contact this person if you're not available
              </p>
            </div>
            <section className="mb-4">
              <Form>
                <Form.Group>
                  <Form.Label>Full Name</Form.Label>
                  <Form.Control
                    type="text"
                    id="name"
                    placeholder="Contact's full name"
                    value={name}
                    onChange={this.handleChange}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Mobile Number</Form.Label>
                  <Form.Control
                    type="text"
                    id="mobileNumber"
                    placeholder="Enter mobile number"
                    value={mobileNumber}
                    onChange={this.handleChange}
                  />
                </Form.Group>
                <div className="btn-placeholder">
                  <div className="row row-2">
                    <div className="col-6">
                      <Link
                        to="#"
                        className="btn btn-transparent btn-block"
                        onClick={() => this.modal("close")}
                      >
                        Cancel
                      </Link>
                    </div>
                    <div className="col-6">
                      <Link
                        to="#"
                        className={"btn btn-primary btn-block " + disabled}
                        onClick={() => this.modal("save")}
                      >
                        Save
                      </Link>
                    </div>
                  </div>
                </div>
              </Form>
            </section>
          </div>
        </Modal>
        {/* Modal Session Time Out */}
        <Modal show={modalSessionRoutes} onHide={() => this.modal("retrySession")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <section>
                <h1>Session Time Out</h1>
                <p>Sorry your session is expired. Retry and try to finish your schedule before the timer expires.</p>
              </section>
              <div className="btn-placeholder inline bottom">
                <Link to="#" className="btn btn-primary btn-block" onClick={() => this.modal("retrySession")}>Retry</Link>
              </div>
            </div>
          </div>
        </Modal>
        {/* Model for booking not allowed */}
        <Modal show={modelForbiddenInstalltion} onHide={() => this.modal("close")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <section>
                <h1>Sorry you have a pending ticket</h1>
                <p>At the moment you can only have one ticket at a time</p>
              </section>
              <Link to="#" className="btn btn-primary btn-block mb-3" onClick={() => this.modal("close")}>Close</Link>
                <Link to="/installation/fisik-ticket/status" className="btn btn-transparent btn-block">See my pending ticket</Link>
            </div>
          </div>
        </Modal>
        <Modal show={attemptExhausted} onHide={() =>  this.modal("close")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                <img className="header-img" src={images.common['grfx_grfx_status_failed.png']} alt="Icon Failed" />
              </header>
              <section>
                <h1>Sorry you can't reschedule anymore</h1>
                <p>You have exceeded reschedule installation limit. Please contact our customer service for further information</p>
              </section>
              <Link to="#" className="btn btn-primary btn-block mb-3" onClick={e => { e.preventDefault();  this.modal("close")}}>Close</Link>
                <a href="tel:121" className="btn btn-transparent btn-block" onClick={e => {  this.modal("close")}}>Contact Us</a>
            </div>
          </div>
        </Modal>
        {/* Modal Session Time Out */}
        <Modal show={modalServiceUnavailable} onHide={() => this.modal("close")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <section>
                <h1>Service Error</h1>
                <p>A service error has occured while processing your request</p>
              </section>
              <div className="btn-placeholder inline bottom">
                <Link to="#" className="btn btn-primary btn-block" onClick={e => {e.preventDefault(); this.modal("close")}}>Ok</Link>
              </div>
            </div>
          </div>
        </Modal>
      </section>
    );
  }
}

export default FisikTicket;
