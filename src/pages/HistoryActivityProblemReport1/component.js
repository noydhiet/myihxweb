import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const HistoryActivityProblemReport1 = () => {
    const { trigger, navCase, pageCase, modal } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
            <div className="container container-sm">
                <div className="box animated fadeInUp">
                    <header>
                    <div className="header-actions">
                        <Link to="/history/activity" className="btn btn-link">
                            <i className="fa fa-arrow-left"></i>
                        </Link>
                    </div>
                    </header>
                    <section className="section-header-text">
                        <div className="heading">
                            <h1>Problem Report</h1>
                            <small className="help-block">Ticket Number</small>
                            <h5>#12345</h5>
                        </div>
                    </section>
                    <section className="py-main-sm pb-0">
                        <div className="heading">
                            <small className="caption">Topic</small>
                            <p className="dark">Internet Connection</p>
                        </div>
                        <div className="heading">
                            <small className="caption">Comments</small>
                            <p className="dark">Lorem Ipsum Dolor Sit Ahmet, Lorem Ipsum Dolor Sit Ahmet Lorem Ipsum Dolor Sit Ahmet Lorem Ipsum Dolor Sit AhmetLorem Ipsum Dolor Sit AhmetLorem Ipsum Dolor Sit Ahmet</p>
                        </div>
                    </section>

          {/* Problem Report 1 */}
          <section className="py-main-sm hidden">
            <div className="subheading">
              <p>Progress Tracking</p>
            </div>
            <div className="milestone">
              <div className="milestone-item check">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Appointment set</h5>
                  <span className="desc">Our technician will visit you on Friday, 20 July 2019 between 9.00 - 12.00</span>
                </div>
              </div>
              <div className="milestone-item">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Schedule a technician visit</h5>
                  <span className="desc">
                    set a schedule for technician visit
                                    </span>
                  <Link to="/history/activity/installation-report" className="btn btn-transparent">
                    set schedule
                                    </Link>
                </div>
              </div>
              <div className="milestone-item last-child">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">In Review</h5>
                  <span className="desc">
                    IndiHome customer service will contact you for confirmation
                                    </span>
                </div>
              </div>
            </div>
          </section>
          <div className="btn-placeholer">
            <p className="text-center">Problem has already solved?</p>
            <Link to="/help" onClick={() => modal('rating')} className="btn btn-primary btn-block">Problem Solved</Link>
          </div>

          {/* Problem Report 2 */}
          <section className="py-main-sm">
            <div className="subheading">
                <p>Progress Tracking</p>
            </div>
            <div className="milestone">
                <div className="milestone-item success">
                    <div className="milestone-item-img">
                        <div className="dot"></div>
                    </div>
                    <div className="milestone-item-content">
                        <h5 className="title">Issue solved successfully</h5>
                    </div>
                </div>
                <div className="milestone-item success">
                    <div className="milestone-item-img">
                        <div className="dot"></div>
                    </div>
                    <div className="milestone-item-content">
                        <h5 className="title">Technician Visit</h5>
                    </div>
                </div>
                <div className="milestone-item check">
                    <div className="milestone-item-img">
                        <div className="dot"></div>
                    </div>
                    <div className="milestone-item-content">
                        <h5 className="title">Appointment set</h5>
                        <span className="desc">
                        Our technician will visit you on Friday, 20 July 2019 between 9.00 - 12.00
                        </span>
                    </div>
                </div>
                <div className="milestone-item">
                    <div className="milestone-item-img">
                        <div className="dot"></div>
                    </div>
                    <div className="milestone-item-content">
                        <h5 className="title">Schedule a technician visit</h5>
                        <span className="desc">
                        Set a schedule for technician visit
                        </span>
                    </div>
                </div>
                <div className="milestone-item last-child">
                    <div className="milestone-item-img">
                        <div className="dot"></div>
                    </div>
                    <div className="milestone-item-content">
                        <h5 className="title">In Review</h5>
                        <span className="desc">
                        IndiHome customer service will contact you for confirmation 
                        </span>
                    </div>
                </div>
            </div>
        </section>
        <div className="btn-placeholder text-center">
            <p className="mb-0">Are you having problems during the installation?</p>
            <Link to="/history/activity" className="btn btn-link">Report Issue</Link>
        </div>
        </div>
      </div>
    </section>
  )
}

export default HistoryActivityProblemReport1;
