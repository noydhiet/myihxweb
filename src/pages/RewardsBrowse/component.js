import React, { Component } from "react";
import Rewards from './../Rewards';
import { PageContext } from './../../context/PageContext';
import Slider from 'react-slick';

class RewardsBrowse extends Component {
  static contextType = PageContext;

    state = {
        notifRedeem: false,
        voucherCoppied: false,
        voucherRedeemSuccess: false
    }
    componentDidMount() {
        this.context.trigger(2);
        this.context.navCase("w-md-up-block");
        this.context.pageCase("l-bg");
    }
    triggerPage = (mode) => {
        switch(mode) {
          case "RedeemSuccess":
            this.setState({
              notifRedeem: true
            });
            break;
  
          case "VoucherCoppied":
              this.setState({
                voucherCoppied: true
              });
              break;
  
          case "VoucherRedeemSuccess":
              this.setState({
                voucherRedeemSuccess: true
              });
              break;
  
          case "closeNotif":
            setTimeout(()=>{
              this.setState({
                notifRedeem: false,
                voucherCoppied: false,
                voucherRedeemSuccess: false
              })
            }, 1000);
            break;
  
          default:
            return null
        }
      }
    render() {
        if(this.state.notifRedeem === true || this.state.voucherCoppied === true || this.state.voucherRedeemSuccess === true) {
        this.triggerPage("closeNotif");
        }
        const { brand, common, sample } = this.context;
        const images = { brand, common, sample };

        const configNotif = {
          redeemSuccess: this.state.notifRedeem ? "active" : "",
          voucherCoppied: this.state.voucherCoppied ? "active" : "",
          voucherRedeemSuccess: this.state.voucherRedeemSuccess ? "active" : ""
        }
        return (
            <div>
                <Rewards browseRewards="active"/>
                <SectionPackages images={images} />
                <div className={"card card-notif  " + configNotif.redeemSuccess}>
                <p className="notif-text">Point redeemed successfully!</p>
                </div>
                <div className={"card card-notif  " + configNotif.voucherCoppied}>
                <p className="notif-text">Voucher code copied!</p>
                </div>
                <div className={"card card-notif  " + configNotif.voucherRedeemSuccess}>
                <p className="notif-text">Voucher redeemed successfully</p>
                </div>
            </div>
        )
    }
}
export default RewardsBrowse;

const SectionPackages = ({images}) => {
    const config = {
        carousel: {
            className: "primary-carousel",
            dots: false,
            infinite: false,
            arrows: false,
            speed: 500,
            slidesToShow: 7,
            slidesToScroll: 7,
            responsive: [
                {
                  breakpoint: 991,
                  settings: {
                    dots: false,
                    slidesToShow: 2,
                    slidesToScroll: 2
                  }
                },
                {
                  breakpoint: 575,
                  settings: {
                    dots: false,
                    slidesToShow: 4,
                    slidesToScroll: 4
                  }
                },
                {
                  breakpoint: 324,
                  settings: {
                    dots: false,
                    slidesToShow: 4,
                    slidesToScroll: 4
                  }
                }
            ]
        }
    }
    const voucherType = [
        {
            img: "ic_voucher_all.png",
            desc: "All",
            color: "light"
        },
        {
            img: "ic_voucher_recommended.png",
            desc: "Recommended",
            color: "dark"
        },
        {
            img: "ic_voucher_automotive.png",
            desc: "Automotive",
            color: "dark"
        },
        {
            img: "ic_voucher_ecommerce.png",
            desc: "E-Commerce",
            color: "dark"
        },
        {
            img: "ic_voucher_recommended.png",
            desc: "Recommended",
            color: "dark"
        },
        {
            img: "ic_voucher_automotive.png",
            desc: "Automotive",
            color: "dark"
        },
        {
            img: "ic_voucher_ecommerce.png",
            desc: "E-Commerce",
            color: "dark"
        }
    ]
    const voucherCard = ["content_banner_placeholder.png", "banner_2_lines.png", "content_banner_placeholder.png", "banner_2_lines.png", "content_banner_placeholder.png", "banner_2_lines.png"];
    return (
        <section className="py-main">
            <div className="container">
                 <div className="filter-category py-main pt-0">
                    <Slider {...config.carousel}>
                     {voucherType.map((value, index)=>{
                        const sumDelay = index+1;
                         return (
                            <div className={"carousel-item animated fadeInUp delayp" + sumDelay} key={index}>
                                <div className="content-center">
                                    <div className="card card-category rounded">
                                        <div className={"card-header " + value.color}>
                                            <img alt="icon" src={images.common[`${value.img}`]} className="w-35px" />
                                        </div>
                                        <p className="text-center">{value.desc}</p>
                                    </div>
                                </div>
                            </div> 
                         )
                     })}
                    </Slider>
                </div>
                <div className="row">
                    {voucherCard.map((value, index)=>{
                        const sumDelay = index+1;
                        return (
                            <div className={"col-md-4 col-12 mb-3 animated fadeInUp delayp" + sumDelay} key={index}>
                                <img src={images.common[`${value}`]} className="img-fluid" alt="voucher" />
                            </div>
                        )
                    })}
                </div>
            </div>
        </section>
    )
}