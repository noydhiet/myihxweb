import React, { Component, useState } from "react";
import { Link } from "react-router-dom";
import { Modal } from 'react-bootstrap';
import History from "./../History";
import { NavMobile } from "./../../include/NavMobile";
import { PageContext } from './../../context/PageContext';

class HistoryBill extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
    this.context.footerCase("w-nav-bottom");
  }
  render() {
    const { common, brand, sample, condition } = this.context;
    const images = { common, brand, sample };
    return (
      <div>
        <NavMobile condition={condition} historyPage="active" />
        <History billActive="active" />
        <SectionContent images={images} />
      </div>
    );
  }
}

export default HistoryBill;

const SectionContent = ({ images }) => {
  const [cond, setCond] = useState({
    modalBillHistory: false,
    alertEmail: false,
    alertDownload: false
  });
  const [checkboxes, setCheckboxes] = useState([]);
  const triggerPage = (mode) => {
    switch (mode) {
      case "downloadBillHistory":
        setCond({
          modalBillHistory: !cond.modalBillHistory
        });
        break;

      case "sendToEmail":
        setCond({
          alertEmail: true
        });
        break;

      case "download":
        setCond({
          alertDownload: true
        });
        break;

      case "close":
        setCond({
          modalBillHistory: false
        });
        break;

      default:
        return null
    }
  }

  const closeAlert = () => {
    setTimeout(() => {
      setCond({
        alertEmail: false,
        alertDownload: false
      })
    }, 2000)
  }
  const checkBox = idx => {
    if (checkboxes.includes(idx)) setCheckboxes(checkboxes.filter(i => i !== idx));
    else setCheckboxes([...checkboxes, idx]);
  }
  const checkboxesLabel = ['June 2019', 'May 2019', 'April 2019'];
  const alertView = {
    download: cond.alertDownload ? "active" : "",
    email: cond.alertEmail ? "active" : ""
  }
  if (cond.alertDownload === true || cond.alertEmail === true) {
    closeAlert();
  }
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-4 d-none d-md-block">
          <div className="box box-in-cover py-3">
            <div className="list-group list-group-activity mb-0">
              <Link to="/history/bill" className="list-group-item w-arrow">
                <div className="list-group-item-content">
                  <p className="title active mb-0">Bill</p>
                </div>
              </Link>
              <Link to="/history/purchase" className="list-group-item w-arrow">
                <div className="list-group-item-content">
                  <p className="title mb-0">Purchase</p>
                </div>
              </Link>
              <Link to="/history/activity" className="list-group-item w-arrow">
                <div className="list-group-item-content">
                  <p className="title mb-0">Activity</p>
                </div>
              </Link>
            </div>
          </div>
        </div>
        <div className="col-md-8 col-12">
          <div className="box box-in-cover mt-sm-down-4 animated fadeInUp">
            <div className="subheading w-links">
              <p>Your Payments</p>
              <Link to="#" className="btn btn-link pr-0" onClick={() => triggerPage("downloadBillHistory")}><i className="far fa-arrow-to-bottom" style={{ fontSize: "1.25rem" }}></i></Link>
            </div>
            <div className="list-group list-group-activity mb-0">
              <Link to="/history/bill/details" className="list-group-item pr-0">
                <div className="list-group-item-content mb-0">
                  <small className="caption">5 June 2019</small>
                  <div className="list-group-item-content-w-status">
                    <p className="title">June 2019</p>
                    <p className="text-status">Rp2.060.000</p>
                  </div>
                </div>
              </Link>
              <Link to="/history/bill/details" className="list-group-item pr-0">
                <div className="list-group-item-content mb-0">
                  <small className="caption">5 May 2019</small>
                  <div className="list-group-item-content-w-status">
                    <p className="title">May 2019</p>
                    <p className="text-status">Rp2.040.000</p>
                  </div>
                </div>
              </Link>
              <Link to="/history/bill/details" className="list-group-item pr-0">
                <div className="list-group-item-content mb-0">
                  <small className="caption">5 May 2019</small>
                  <div className="list-group-item-content-w-status">
                    <p className="title">April 2019</p>
                    <p className="text-status">Rp2.040.000</p>
                  </div>
                </div>
              </Link>
            </div>
            {/* Start - Blank State (Hidden) */}
            <div className="blank-state hidden">
              <img alt="grfx_blank_history" src={images.common["grfx_blank_history.png"]} />
              <div className="heading">
                <p className="dark">
                  You don't have any bill history. <br /> Subscribe to start using IndiHome
                </p>
              </div>
              <div className="btn-placeholder">
                <Link to="/shop/internet/package" className="btn btn-primary">
                  Browse Package
                </Link>
              </div>
            </div>
            {/* End - Blank State */}

            {/* Start - Blank State (Hidden) */}
            <div className="blank-state hidden">
              <img alt="grfx_blank_history" src={images.common["grfx_blank_history.png"]} />
              <div className="heading">
                <p className="dark">
                  No history recorded from January 2019 until February 2019
                </p>
              </div>
            </div>
            {/* End - Blank State */}


            {/* Start - Modal Download Bill History */}
            <Modal show={cond.modalBillHistory} onHide={() => triggerPage("close")}>
              <div className="modal-body p-box">
                <Link to="#" className="close animated fadeInUp" onClick={() => triggerPage("close")}>
                  <span><i className="far fa-times text-primary"></i></span>
                </Link>
                <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
                  <h3>Download bill history</h3>
                  <p>Your technician will do all the following process</p>
                </div>
                <section className="animated fadeInUp delayp2 mb-4">
                  <div className="list-group">
                    {checkboxesLabel.map((i, idx) => (
                      <div className="list-group-item w-checkbox" key={idx} onClick={() => checkBox(idx)}>
                        <div className="list-group-item-content">
                          <span className="desc">{i}</span>
                        </div>
                        <div className="form-group checkbox">
                          <div className="custom-control custom-checkbox">
                            <input checked={checkboxes.includes(idx)} onChange={() => checkBox(idx)} type="checkbox" className="custom-control-input check" />
                            <label className="custom-control-label"></label>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </section>
                <div className="row">
                  <div className="col-6">
                    <button className={"btn btn-transparent btn-block" + (checkboxes.length ? '' : ' disabled')} disabled={!checkboxes.length} onClick={() => triggerPage("sendToEmail")}>Send to E-mail</button>
                  </div>
                  <div className="col-6">
                    <button className={"btn btn-primary btn-block" + (checkboxes.length ? '' : ' disabled')} disabled={!checkboxes.length} onClick={() => triggerPage("download")}>Download</button>
                  </div>
                </div>
              </div>
            </Modal>
            {/* End - Modal Download Bill History */}
          </div>
        </div>
      </div>
      <div className={"card card-notif " + alertView.download}>
        <p className="notif-text">Bill Statement has successfully downloaded</p>
      </div>

      <div className={"card card-notif " + alertView.email}>
        <p className="notif-text">Bill Statement has successfully sent to john.doe@gmail.com</p>
      </div>
    </div>
  );
};
