import React from 'react';
import { Modal } from 'react-bootstrap';
import renderer from 'react-test-renderer';
import Register from '../index';

jest.mock('react-router-dom');
jest.mock('../../../services', () => ({
  checkAccount: (number) => new Promise((resolve, reject) => {
    if (number === '123') resolve();
    else reject({ message: 'error'});
  }),
}));

const instance = props => renderer.create(<Register {...props} />).getInstance();

describe('Register', () => {
  test('render', () => {
    const result = instance().render();
    expect(result.type).toBe(React.Fragment);
  });

  test('checkAccount', () => {
    const wrapper = instance();

    wrapper.state.indiHomeAccNo = '000';
    wrapper.checkAccount();
    expect(wrapper.state.loading).toBe(true);
  });

  test('handleSubmit', () => {
    const wrapper = instance();
    const e = { preventDefault: jest.fn() };
    const trigger = jest.fn();
    wrapper.trigger = (e) => () => trigger(e);

    wrapper.handleSubmit(e);
    expect(e.preventDefault).toHaveBeenCalledTimes(1);

    wrapper.state.doActive = true;
    wrapper.handleSubmit(e);
    expect(trigger).toHaveBeenCalledTimes(1);
    expect(trigger).toHaveBeenCalledWith('modal');
  });

  test('trigger', () => {
    const wrapper = instance();
    wrapper.refs = { inner: { clientHeight: 0 } };
    wrapper.checkAccount = jest.fn();

    wrapper.trigger('do')();
    expect(wrapper.state.doActive).toBe(true);
    wrapper.trigger('dont')();
    expect(wrapper.state.dontActive).toBe(true);

    wrapper.trigger('modal')();
    expect(wrapper.checkAccount).toHaveBeenCalled();
    wrapper.state.modal = true;
    expect(wrapper.trigger('modal')()).toBe(null);
    expect(wrapper.state.modal).toBe(false);
    wrapper.state.disabledBtn = true;
    expect(wrapper.trigger('modal')()).toBe(null);

    expect(wrapper.trigger()()).toBe(null);
  });

  test('onChange', () => {
    const wrapper = instance();
    wrapper.onChange({ target: { id: 'name', value: 'test' } });
    expect(wrapper.state.name).toBe('test');
    expect(wrapper.state.disabledBtn).toBe(true);
  });

  test('_renderModal', () => {
    const wrapper = instance();
    wrapper.state.response.email = 'test@telkom.co.id';
    expect(wrapper._renderModal().type).toBe(Modal);
  });
});
