import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { Button, Modal } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import ContainerBase from '../../include/ContainerBase';
import { checkAccount } from '../../services';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';

export default class Register extends Component {
  static contextType = PageContext;
  state = {
    doActive: false,
    dontActive: false,
    doHeight: 0,
    disabledBtn: true,
    indiHomeAccNo: '',
    loading: false,
    modal: false,
    response: {},
    message: '',
  }

  componentDidMount(){
    this.context.trigger(1);
    this.context.pageCase("w-bg");
}

  checkAccount = () => {
    const { doHeight, indiHomeAccNo, message } = this.state

    this.setState({ loading: true });
    //TODO handle the indihome number exist and don't exist redirection here 1.2
    checkAccount(indiHomeAccNo)
      .then(({ data }) => {
        console.log('checkAccount', data)
        if(data.status === 200 || data.status === 201) {
          this.setState({
            loading: false,
            doHeight: message ? doHeight - 36 : doHeight,
            modal: true,
            response: { ...data.data },
            message: '',
          });
        }
        
      })
      .catch(() => {
        this.setState({
          loading: false,
          doHeight: message ? doHeight : doHeight + 36,
          message: 'notFound',
        });
      });
  }

  handleSubmit = e => {
    const { doActive } = this.state;
    e.preventDefault();
    if (doActive) this.trigger('modal')();
  }

  trigger = (mode) => () => {
    const { disabledBtn, indiHomeAccNo, modal } = this.state

    switch (mode) {
      case 'do':
        this.setState({
          doActive: true,
          dontActive: false,
          disabledBtn: indiHomeAccNo.length <= 1,
        })
        break;

      case 'dont':
        this.setState({
          dontActive: true,
          doActive: false,
          disabledBtn: false,
          indiHomeAccNo: ''
        })
        break;

      case "modal":
        if (modal) {
          this.setState({ modal: false });
          this.props.history.push('/help')
          return null;
        }
        if (disabledBtn) return null;

        this.checkAccount();
        break;

      default:
        return null
    }
  }

  onChange = ({ target }) => {
    const { id, value } = target;
    const message = this.setMessage(value);

    this.setState({
      [id]: value,
      message,
      disabledBtn: Boolean(message),
    });
  }

  setMessage(value) {
    const mobileCheck = v => {
      return !(/\s/g.test(v) || !/^[+0-9]+$/i.test(v)) && ((v.startsWith('0') && v.length < 15 && v.length > 7 ) || (v.startsWith('62') && v.length < 16 && v.length > 8) || (v.startsWith('+62') && v.length < 17 && v.length > 9))
    };
    // if (value.length > 12) return 'maxNo';
    if (!mobileCheck(value)) return 'invNo';
    return '';
  }

  _renderModal() {
    const { indiHomeAccNo, modal, response } = this.state;

    if (!response.email) return null;

    const email = `***${response.email.slice(response.email.search('@') - 3, response.email.length)}`;
    const mobileNumber = response.mobileNumber.slice(-3).padStart(response.mobileNumber.length - 3, '*');

    return (
      <TextContext.Consumer>{(context=>{
        const { registerText } = context;
        return (
          <Modal show={modal} onHide={this.trigger('modal')} centered={true}>
            <div className="modal-body p-box pb-0">
              <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
                <h3 className="text-left">IndiHome account number has already been registered</h3>
                <p className="text-left">IndiHome account number {indiHomeAccNo} has been connected with the email {email} / mobile number {mobileNumber}</p>
              </div>
              <section className="animated fadeInUp delayp2 mb-4">
                <div className="btn-placeholder">
                  <Link to="/login" className="btn btn-primary btn-block mb-3">Login</Link>
                  <button className="btn btn-transparent btn-block mb-3" onClick={this.trigger('modal')}>Not My Email/Mobile Number</button>
                </div>
              </section>
            </div>
          </Modal>
        )
      })}</TextContext.Consumer>
    );
  }

  render() {
    const { doActive, dontActive, disabledBtn, indiHomeAccNo, loading, message } = this.state
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    const config = {
      alert: message ? 'block' : 'none',
      doActive: doActive ? 'active' : '',
      dontActive: dontActive ? 'active' : '',
      disabledBtn: loading || disabledBtn ? 'disabled' : '',
      styleDoContent: {
        height: doActive ? 'auto' : '0px',
        transition: 'all .3s',
        overflow: 'hidden',
        padding: doActive ? '' : '0',
        margin: doActive ? '' : '0',
        opacity: doActive ? '1' : '0'
      }
    };

    return (
      <TextContext.Consumer>{(context)=>{
        const { registerText } = context;
        const containerProps = {
          image: { alt: 'Icon Success', src: images.common['ic_header_register_new.png'], },
          // subtitle: 'IndiHome account number is the number stated in your bill and usually start with 12.',
          title: registerText.register.title,
        };
        const alertMsg = () => {
          const { message } = this.state;
          switch (message) {
            case "maxNo":
              return registerText.register.alertMaxNo;
            case "invNo":
              return registerText.register.alertInvNo;
            case "notFound":
              return registerText.register.alertNotFound;
            default:
              break;
          }
        };
        
        return (
          <>
            <ContainerBase {...containerProps}>
              <form className="needs-validation" id="form" noValidate onSubmit={this.handleSubmit}>
                <div className="list-group list-group-register">
                  <div className={"list-group-item " + config.doActive} onClick={this.trigger('do')}>
                    <div className={"list-group-item-header " + config.doActive}>{registerText.register.btnYes} <i className="fal fa-check check-list"></i></div>
                    <div className="list-group-item-content" style={config.styleDoContent}>
                      <div className="form-group mb-0">
                        <label htmlFor="indiHomeAccNo">{registerText.register.labelAccNo}</label>
                        <input type="text" className="form-control" id="indiHomeAccNo" placeholder={registerText.register.placeholderIndiNo} value={indiHomeAccNo} onChange={this.onChange} />
                        <div className="invalid-feedback" style={{ display: config.alert }}>{alertMsg()}</div>
                      </div>
                    </div>
                  </div>
                  <div className={"list-group-item " + config.dontActive} onClick={this.trigger('dont')}>
                    <div className={"list-group-item-header " + config.dontActive} id="headingTwo">{registerText.register.btnNo}<i className="fal fa-check check-list"></i></div>
                  </div>
                </div>
                <div className="btn-placeholder">
                  {dontActive
                    ? (<Link to="/register/success" className={"btn btn-primary btn-block " + config.disabledBtn}> <span>{registerText.register.btnNext}</span> </Link>)
                    : (<Button className={"btn btn-primary btn-block " + config.disabledBtn} onClick={this.trigger('modal')}> {loading ? 'checking..' : registerText.register.btnNext} </Button>)
                  }
                </div>
              </form>
            </ContainerBase>
            {this._renderModal()}
          </>
        )
      }}
      </TextContext.Consumer>
    )
  }
}

Register.defaultProps = {
  images: { brand: {}, common: {}, sample: {} },
  pageCase: () => { },
  trigger: () => { },
};

Register.propTypes = {
  images: PropTypes.shape({ brand: PropTypes.object, common: PropTypes.object, sample: PropTypes.object }),
  pageCase: PropTypes.func,
  trigger: PropTypes.func,
};
