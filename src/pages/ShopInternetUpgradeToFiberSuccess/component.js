import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const ShopInternetUpgradeToFiberSuccess = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = { brand, common, sample } ;
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
            <div className="container container-sm">
                <div className="box box-main animated fadeInUp">
                    <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                    <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
                    </header>
                    <section className="animated fadeInUp delayp2">
                        <h1>Upgrade to Fiber successfully ordered!</h1>
                        <p>Next, schedule an activation to get your internet upgrade to fiber</p>
                    </section>
                    <div className="btn-placeholder bottom animated fadeInUp delayp3">
                        <Link to="/installation/schedule" className="btn btn-primary btn-block">Schedule an Activation</Link>
                        <Link to="/home" className="btn btn-transparent btn-block">Back To Home</Link>
                    </div>
                </div>
            </div>
        </section> 
    )
}

export default ShopInternetUpgradeToFiberSuccess;