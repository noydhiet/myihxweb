import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';
import moment from 'moment';
import { technicianAvailability, reScheduleTechnician, technicianAvailabilityFeasibility } from '../../services'
import Slider from "react-slick";
import { Modal } from 'react-bootstrap'

const InstallationDetailFailure = (props) => {
  const {history} = props;
  const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
    const {history, location: {state}} = props;
    if(!state) {
      history.push('/home');
    }
  }, [trigger, navCase, pageCase]);
  const images = { brand, common, sample };
  const [dateSlots, setDateSlots] = useState(undefined);
  const [selectedDate, setSelectedDate] = useState(undefined);
  const [isFetchingSlots, setIsFetchingSlots] = useState(false);
  const [attemptExhausted, setAttemptExhausted] = useState(false);
  const [selectedCard, setSelectedCard] = useState(undefined);
  const [modalReschedule, setModalReschedule] = useState(false);
  const [isButtonLoading, setButtonLoading] = useState(false);
  const [showConfirmationReschedule, setShowConfirmationReschedule] = useState(false);
  const [modalNotReschedule, setModalNotReschedule] = useState({ reason: '', message: '', show: false });
  const [cardMorningAfternoon, setCardMorningAfternoon] = useState([{ show: false, text: '', timeSlot: '' }, { show: false, text: '', timeSlot: '' }]);
  const [selectedDateIndex, setSelectedDateIndex] = useState(undefined);
  const [morningCheck, setMorningCheck] = useState(false);
  const [afternoonCheck, setAfternoonCheck] = useState(false);
  const {location: {state}} = props;
  const Loading = ({ images }) => {
    return (
      <span><i className="fa fa-circle-notch fa-spin"></i></span>
    )
  }
  const  carouselDate = {
    className: "primary-carousel overflow-inherit mb-4",
    dots: false,
    infinite: false,
    arrows: false,
    speed: 500,
    slidesToShow: 5.3,
    slidesToScroll: 5.3,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 5.5,
          slidesToScroll: 5.5
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3.5,
          slidesToScroll: 3.5
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 4.5,
          slidesToScroll: 4.5
        }
      },
      {
        breakpoint: 320,
        settings: {
          slidesToShow: 3.5,
          slidesToScroll: 3.5
        }
      }
    ]
  };
  const fetchSlots = (e) => {
    e.preventDefault();
    setModalReschedule(true);
    if(!isFetchingSlots) {
      setIsFetchingSlots(true);
      technicianAvailabilityFeasibility()
      .then(response => {
        console.log(response)
        if(response.data.ok) {
          if(response.data.data.rescheduleAvailable) {
            technicianAvailability().then((data) => {
              if(data.statusText === 'OK' && data.data.data.availableTimes) {
                  var tempDate = undefined;
                  const groupedDateSlots = data.data.data.availableTimes.length === 0 ? null : data.data.data.availableTimes.reduce((groupedData, { availability, bookingId, crewId, dateTime, information, timeSlot }) => {
                    const formattedDate = moment(dateTime, 'DD-MM-YYYY HH:mm').format('DD-MM-YYYY')
                    if(!tempDate) tempDate = formattedDate
                    if (!groupedData[formattedDate]) groupedData[formattedDate] = [];
                    groupedData[formattedDate].push({ availability, bookingId, crewId, information, timeSlot });
                    return groupedData;
                  }, {});
                  if(groupedDateSlots) {
                    setDateSlots(groupedDateSlots);
                    setSchedule(0, groupedDateSlots, tempDate);
                  }
                  setIsFetchingSlots(false);
                  
              } else {
                setDateSlots(null)
                setIsFetchingSlots(false);
              }
            }).catch((error) => {
              setIsFetchingSlots(false);
              setDateSlots(null)
              console.error(error);
            });
          } else {
            setModalReschedule(false);
            //important to set undefined here otherwise next fetch will show no slots for a while and load it later
            setDateSlots(undefined)
            setIsFetchingSlots(false);
            setModalNotReschedule({ ...modalNotReschedule, ['reason']: response.data.data.reason, ['message']: response.data.data.message, ['show']: true })
          }
        }
      }).catch(error => {
        console.error(error)
      });
      
    }
  }
  const setSchedule = (mode, localDateSlots, formattedDate) => {
    setSelectedDate(formattedDate.toString());
    setSelectedDateIndex(mode)
    const cardIndexSelect = [ false, false ];
    for(const [index, item] of localDateSlots[formattedDate.toString()].entries()) {
      const indexToUpdate = item.timeSlot.slotId === 'Afternoon' ? 1 : 0;
      if(item.availability === 1) {
        cardIndexSelect[indexToUpdate] = true;
        setCardMorningAfternoon(oldArray => ({...oldArray, [indexToUpdate]: {'show': true, 'text': item.timeSlot.slotId, 'timeSlot': item.timeSlot.slot}}));
      } else {
        setCardMorningAfternoon(oldArray => ({...oldArray, [indexToUpdate]: {'show': false, 'text': item.timeSlot.slotId, 'timeSlot': 'Unavailable'}}));
      }
    }
    if(cardIndexSelect[0]) {
      estimatedArrival("0");
    } else if(cardIndexSelect[1]) {
      estimatedArrival("1");
    } else {
      estimatedArrival(undefined);
    }
  };
  const modal = (mode) => {
    switch (mode) {

      case "close":
          setModalReschedule(false)
          setAttemptExhausted(false)
          setModalNotReschedule({...modalNotReschedule, ['show']: false})
        break;

      default:
        return null
    }
  };

  const estimatedArrival = mode => {
    switch (mode) {
      case "0":
        setSelectedCard("0");
        setMorningCheck(true);
        setAfternoonCheck(false);
        break;
      case "1":
        setSelectedCard("1");
        setMorningCheck(false);
        setAfternoonCheck(true);
        break;
      default:
        setSelectedCard(undefined)
        setMorningCheck(false);
        setAfternoonCheck(false);
        return null;
    }
  };
  const reScheduleTechnicianClick = (e) => {
    e.preventDefault();
    setModalReschedule(true);
    setShowConfirmationReschedule(false);
    if(!isButtonLoading) {
      setButtonLoading(true);
      const { history } = props;
      let bookingId = undefined;
      let time = undefined;
      console.log('dateSlotse', dateSlots)
      console.log('dateSlotseselectedDate', selectedDate)
      for(const [index, item] of dateSlots[selectedDate].entries()) {
          if(item.availability === 1) {
            console.log('bookingId2121', selectedCard)
            if((selectedCard === "0" && item.timeSlot.slotId === "Morning") || (selectedCard === "1" && item.timeSlot.slotId === "Afternoon")) {
              console.log('bookingId2121cccc')
              bookingId = item.bookingId;
              time = item.timeSlot.slot;
              break;
            }
        }
      } 
      console.log('bookingId', bookingId)
      if(bookingId) {
        reScheduleTechnician(bookingId).then((data) => {
          if(data.data.ok) {
            history.push('/installation/reschedule/success/' + selectedDate + '--' + time)
          }
          setButtonLoading(false);
        }).catch((error) => {
          if(error.response && error.response.status === 400) {
            console.log('error.response', error.response)
            setModalReschedule(false)
            setAttemptExhausted(true)
          }
          setButtonLoading(false);
          console.log('error.response', error.response)
        });
      } else {
        setButtonLoading(false);
        console.error('Error', bookingId)
      }
    }
    setSelectedCard(undefined);
  }
  const carousel = {
    className: "primary-carousel overflow-inherit mb-4",
    dots: false,
    infinite: false,
    arrows: false,
    speed: 500,
    slidesToShow: 5.3,
    slidesToScroll: 5.3,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 7,
          slidesToScroll: 7
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 5.5,
          slidesToScroll: 5.5
        }
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 4.5,
          slidesToScroll: 4.5
        }
      },
      {
        breakpoint: 320,
        settings: {
          slidesToShow: 3.5,
          slidesToScroll: 3.5
        }
      }
    ]
  };
  const recommend = ["bnr_recommend01.png", "bnr_recommend02.png", "bnr_recommend03.png"];
  const carousalCards = !dateSlots 
  ?
    <div key={0}
        className="carousel-item-sm">
        <div className={`badges badge-date active selector`}>
          <small>No slots available</small>
          <h3></h3>
          <span></span>
        </div>
    </div>
      :
      Object.keys(dateSlots).map((formattedDate, index) => {
        const date = moment(formattedDate, 'DD-MM-YYYY');
        return(
         <div key={index}
              className="carousel-item-sm"
              onClick={() => setSchedule(index, dateSlots, formattedDate)} >
              <div className={`badges badge-date ${selectedDateIndex === index ? 'active' : ''} selector`}>
                <small>{date.format('ddd')}</small>
                <h3>{date.format('DD')}</h3>
                <span>{date.format('MMM')}</span>
              </div>
          </div>
          );
      })
  const reportIssueClick = e => {
    e.preventDefault();
    history.replace({pathname: `/installation/detail-waiting/ticket/${state ? state.appointments.bookingId : ''}`, state: { appointments: state ? state.appointments : '' }})
  }    
  const rescheduleBtn = <Link to="#" hidden onClick={e => fetchSlots(e)} className="btn btn-transparent btn-block">reschedule installation</Link>
      {/* /installation/schedule */}

  return (
    <section>
      <div className="container container-sm">
        <div className="box box-main animated fadeInUp">
          <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
            <img className="header-img" src={images.common['grfx_grfx_sad.png']} alt="Icon Failed" />
          </header>
          <section className="animated fadeInUp delayp2">
            <h1>We're sorry to hear that you're having difficulties</h1>
            <p>Report an issue or reschedule your installation so our technician can visit you to fix the problem</p>
          </section>
          <div className="btn-placeholder bottom animated fadeInUp delayp3">
            <Link to="#" onClick={reportIssueClick} className="btn btn-primary btn-block">report issue</Link>
            {rescheduleBtn}
          </div>
        </div>
      </div>
      {/* Modal Set Reschedule */}
      <Modal show={modalReschedule} onHide={() => setModalReschedule(false)} centered={true}>
          <div className="modal-body p-box pb-0">
            <div className="heading pt-sm-down-4">
              <h3>Reschedule Installation</h3>
              <p>Select a new time and date. You only have 2 chance to reschedule your installation</p>
            </div>
            <section className="mb-4">
              <div className="subheading mb-3">
                <p>Set Schedule</p>
              </div>
              {
            dateSlots === undefined
              ? 
              <>
                <Slider {...carousel}>
                  {recommend.map((value, index)=>{
                    let sumDelay = index+1;
                    return (
                      <div className={"carousel-item animated fadeInUp delayp" + sumDelay} key={index}>
                        <section className="card-skeleton">
                          <figure className="card-image loading"></figure>
                        </section>
                      </div>
                    )
                  })}
                </Slider>
                </>
              :
              <Slider {...carouselDate}>
                {carousalCards}
              </Slider>
            }
              <div className="subheading mb-3">
                <p>Estimated Arrival</p>
              </div>
              <div className="list-group">
                <div className="row row-1 mb-0">
                  <div className="col-6">
                    <div id="morningContainer" className={`list-group-item list-check ${morningCheck ? 'active' : ''} ${cardMorningAfternoon[0].show ? '' : 'disabled'}`} onClick={() => { if(cardMorningAfternoon[0].show) estimatedArrival("0") }}>
                      <div className="list-group-item-content">
                        <h5 className="title">{cardMorningAfternoon[0].show ? cardMorningAfternoon[0].text : 'Morning'}</h5>
                        <span className="desc">{cardMorningAfternoon[0].show ? `Technician will come around ${cardMorningAfternoon[0].timeSlot}`: 'Unavailable'}</span>
                        <div className="custom-control custom-radio d-none">
                          <input type="radio" className="custom-control-input" id="morning" name="radio-stacked" required />
                          <label className="custom-control-label" htmlFor="customControlValidation2">morning</label>
                        </div>
                        {/* <label className="input-check-card">
                                          <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                      </label> */}
                      </div>
                    </div>
                  </div>
                  <div className="col-6">
                    <div id="afternoonContainer" className={`list-group-item list-check ${afternoonCheck ? 'active' : ''} ${cardMorningAfternoon[1].show ? '' : 'disabled'}`} onClick={() => { if(cardMorningAfternoon[1].show) estimatedArrival("1") }}>
                      <div className="list-group-item-content">
                        <h5 className="title">{cardMorningAfternoon[1].show ? cardMorningAfternoon[1].text : 'Afternoon'}</h5>
                        <span className="desc"> {cardMorningAfternoon[1].show ? `Technician will come around ${cardMorningAfternoon[1].timeSlot}`: 'Unavailable'}</span>
                        <div className="custom-control custom-radio d-none">
                          <input type="radio" className="custom-control-input" id="afternoon" name="radio-stacked" required />
                          <label className="custom-control-label" htmlFor="customControlValidation3">afternoon</label>
                        </div>
                        {/* <label className="input-check-card">
                                          <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                      </label> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <p className="help-block">Time range is the Estimated is the estimated arrival time. You will receive a notification when the technician is heading to your location</p>
              <div className="btn-placeholder inline">
                <Link to="#" className="btn btn-transparent btn-block" onClick={() => setModalReschedule(false)}>Cancel</Link>
                <Link to="#" className={`btn btn-primary btn-block ${selectedCard === "0" || selectedCard === "1" ? '' : 'disabled'}`} onClick={e => { e.preventDefault(); if(!isButtonLoading) {setModalReschedule(false); setShowConfirmationReschedule(true); }}} >{isButtonLoading ? <Loading images={images}/> : "Reschedule"}</Link>
              </div>
            </section>
          </div>
        </Modal>
        {/* Model for not reschedulable */}
      <Modal show={modalNotReschedule.show} onHide={() => modal("close")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                <img className="header-img" src={images.common['grfx_grfx_status_failed.png']} alt="Icon Failed" />
              </header>
              <section>
                <h1>{modalNotReschedule.reason}</h1>
                <p>{modalNotReschedule.message}</p>
              </section>
              <Link to="#" className="btn btn-primary btn-block mb-3" onClick={e => { e.preventDefault(); modal("close"); }}>Close</Link>
                <a href="tel:121" className="btn btn-transparent btn-block" onClick={() => modal("close")}>Contact Us</a>
            </div>
          </div>
        </Modal>

        {/* Reschedule confirmation dialog */}
        <Modal show={showConfirmationReschedule} centered={true} onHide={() => this.trigger('modal')} className="modal-centered">
        <div className="box box-main mb-0">
          <section>
            <h1>Confirm Reschedule</h1>
            <p>Are you sure to reschedule the installation?</p>
          </section>
          <section>
              <div className="row">
                <div className="col-6">
                    <button className="btn btn-primary btn-block" onClick={(e) => reScheduleTechnicianClick(e)}>Yes</button>
                  </div>
                <div className="col-6">
                  <button className="btn btn-transparent btn-block" onClick={() => {setModalReschedule(true); setShowConfirmationReschedule(false);}}>No</button>
                </div>
              </div>
            </section>
        </div>
        </Modal>
        {/* Model for booking exhausted */}
        <Modal show={attemptExhausted} onHide={() => modal("close")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                <img className="header-img" src={images.common['grfx_grfx_status_failed.png']} alt="Icon Failed" />
              </header>
              <section>
                <h1>Sorry you can't reschedule anymore</h1>
                <p>You have exceeded reschedule installation limit. Please contact our customer service for further information</p>
              </section>
              <Link to="#" className="btn btn-primary btn-block mb-3" onClick={() => modal("close")}>Close</Link>
                <a href="tel:121" className="btn btn-transparent btn-block">Contact Us</a>
            </div>
          </div>
        </Modal>
    </section>
  )
}

export default InstallationDetailFailure
