import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Form } from 'react-bootstrap';
import { PageContext } from './../../context/PageContext';

const ShopInternetPackageWifiIdSeamlessRegisterDevice = ({history}) => {
    const { trigger, navCase, pageCase } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    },[trigger, navCase, pageCase]);
    const handleSubmit = (e) => {
        // if(e) {
        //     e.preventDefault();
        // }
        history.push("/shop/internet/wifi-id-seamless/connect-wifi-id");
    }
    return (
        <section>
            <div className="container container-sm">
                <div className="box animated fadeInUp">
                    <header>
                        <div className="header-actions">
                            <Link to="/shop/internet/package" className="btn btn-link">
                            <i className="fa fa-arrow-left"></i>
                            </Link>
                        </div>
                    </header>
                    <section className="section-header-text">
                        <div className="heading">
                            <h1>Register Device</h1>
                            <p>Register the device that will connect to wifi.id hotspot</p>
                        </div>
                    </section>

                    <Form onSubmit={handleSubmit}>
                    <section className="animated fadeInUp delayp2">
                            <Form.Group>
                                <Form.Label>Mac Address</Form.Label>
                                <Form.Control
                                    name="macAddress"
                                    type="text"
                                />
                                <small className="text-primary font-weight-bold">How do i find my MAC Address?</small>
                            </Form.Group>
                    </section>

                    <div className="btn-placeholder bottom">
                        <button className="btn btn-primary btn-block" type="submit" value="submit">Next</button>
                    </div>
                    </Form>
                </div>
            </div>
        </section>
    )
}

export default ShopInternetPackageWifiIdSeamlessRegisterDevice;