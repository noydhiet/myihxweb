import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const ShopInternetPackageWifiExtenderSuccess = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = { brand, common, sample } ;
    useEffect(()=>{
       trigger(2);
       navCase("w-md-up-block");
       pageCase("w-bg"); 
    }, [trigger, navCase, pageCase]);
    const coverStyle = {
        background: `url(${images.common["bnr_blue_2nd.png"]}) no-repeat center`,
        border: "none"
    };
    return (
      <section>
        <div className="container container-sm">
            <div className="box box-main animated fadeInUp">
                <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
                </header>
                <section className="animated fadeInUp delayp2">
                    <h1>Wifi Extender has already ordered</h1>
                    <p>Please set a schedule with a technician to install your wifi extender</p>
                </section>
                <section className="py-main-sm pb-0">
                    <div className="subheading">
                        <p className="text-left">What's Next?</p>
                    </div>
                    <div className="list-group list-group-banner animated fadeInUp delayp4">
                        <Link
                        to="/installation/schedule"
                        className="list-group-item light w-arrow w-bg-cover"
                        style={coverStyle}
                        >
                        <img alt="icon" src={images.common["graphic_calendar.png"]} className="w-75px" />
                        <div className="list-group-item-content">
                            <h5 className="title w-md-up-75 mb-0">Please set a schedule with a technician to install your wifi extender</h5>
                        </div>
                        </Link>
                    </div>
                </section>
                <div className="btn-placeholder bottom animated fadeInUp delayp3">
                    <Link to="/home" className="btn btn-primary btn-block">Back To Home</Link>
                </div>
            </div>
        </div>
      </section>
  )
}

export default ShopInternetPackageWifiExtenderSuccess;
