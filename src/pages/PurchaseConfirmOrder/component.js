import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';    
import { Form } from 'react-bootstrap';
import { PageContext } from './../../context/PageContext';
import { getToken } from '../../utils/storage';

const PaymentPurchaseOrder = () => {
    const { trigger, navCase, pageCase, history } = useContext(PageContext);
    let [ check, setCheck ] = useState(false);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    if (!getToken()) {
      window.location.href = '/login';
      return
    }
    const checkBox = (e) => {
      let check = document.getElementsByClassName("check");
      for (let i = 0; i < check.length; i++) {
          if (i === e) {
              if (check[i].checked) {
              check[i].checked = false;
              setCheck(check = false);
              } else {
              check[i].checked = true;
              setCheck(check = true);
              }
          }
      }
    }
    const handleSubmit = (e) => {
        // if(e) {
        //     e.preventDefault();
        // }
        history.push("/purchase/success");
    }
    const config = {
        disabled: check ? "" : "disabled"
    }
    return (
    <section>
      <div className="container container-sm">
        <div className="box animated fadeInUp">
          <header>
            <div className="header-actions">
              <Link to="/personal-data" className="btn btn-link">
                <i className="fa fa-arrow-left"></i>
              </Link>
            </div>
          </header>
          <section className="section-header-text">
            <div className="heading">
              <h1>Confirm order</h1>
              <p>Confirm your package selection and order</p>
            </div>
          </section>
          <section className="py-main-sm pb-0">
            <div className="subheading">
              <p>Package</p>
            </div>
            <Link
              to="#"
              className="card card-packages w-arrow animated fadeInUp delayp2"
            >
              <div className="card-header">
                <div className="badges-list">
                  <div className="badges badge-red left">
                    <h3>100</h3>
                    <span>Mbps</span>
                  </div>
                </div>
                <ul className="list-unstyled">
                  <li>Internet unlimited</li>
                  <li>203 USee TV channels</li>
                  <li>1000 minutes call</li>
                </ul>
              </div>
              <div className="card-body w-btn">
                <h3>
                  Rp1.890.000<small> / bln</small>
                </h3>
                <small className="help-block">Billing starts the month after installation</small>
                <span className="btn btn-primary">Details</span>
              </div>
            </Link>
            <div className="subheading">
              <p>Your Information</p>
            </div>
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label>Full Name</label>
                  <p>John Soetanto</p>
                </div>
              </div>
              {/* <div className="col-md-6">
                            <div className="form-group">
                                <label>KTP Number</label>
                                <p>123123123123123</p>
                            </div>
                        </div> */}
              <div className="col-md-6">
                <div className="form-group">
                  <label>Address</label>
                  <p>Jl. Address St.123</p>
                </div>
              </div>
            </div>
            <div className="subheading mt-3">
              <p>Total Due</p>
            </div>
            <div className="form-group">
              <h4>Rp100.000</h4>
              <small className="help-block">One time installation fee. If the installation is cancelled, your installation fee
                        will be fully refunded. </small>
            </div>
            <Form onSubmit={handleSubmit}>
              <div className="form-group">
                <div className="custom-control custom-checkbox" onClick={() => checkBox(0)}>
                  <input type="checkbox" className="custom-control-input check" required />
                  <label className="custom-control-label" htmlFor="customControlValidation1">I agree to the <Link to="/purchase"> terms &amp; conditions </Link></label>
                </div>
              </div>
              <div className="btn-placeholder">
                <div className="row">
                  <div className="col-6">
                    <Link to="/personal-data" className="btn btn-transparent btn-block"> cancel </Link>
                  </div>
                  <div className="col-6">
                    <Link to="/purchase/payment-method" className={"btn btn-primary btn-block " + config.disabled}> <span> next </span> </Link>
                  </div>
                </div>
              </div>
            </Form>
          </section>
        </div>
      </div>
    </section>
  )
}

export default PaymentPurchaseOrder;
