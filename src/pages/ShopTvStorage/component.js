import React, { Component } from "react";
import ShopTv from '../ShopTv';
import { PageContext } from '../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import { getToken } from "../../utils/storage";

class ShopTvStorage extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    const { brand, common, sample, modal } = this.context;
    const images = { brand, common, sample };
    const urlConfirm = getToken() ? '/shop/tv/tv-storage/confirm' : '#';
    return (
      <TextContext.Consumer>{(context)=>{
        const { shopText } = context;
        return (
          <div>
            <ShopTv tvStoragePage="active" shopPrice="Rp100.000" shopPriceFixed="Rp100.000" btnText="Subscribe" linkTo={urlConfirm} modal={modal} />
            <SectionWithBanner images={images} shopText={shopText}/>
            <SectionText shopText={shopText}/>
          </div>
        )
      }}

      </TextContext.Consumer>
    )
  }
}
export default ShopTvStorage;

const SectionWithBanner = ({ images, shopText }) => {
  return (
    <div className="py-main bg-cover-section tv-storage content-center">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-8 content-center-left">
            <div className="heading mb-2">
              <h3 className="animated fadeInUp delayp5">{shopText.shopTvStorageBanner.title}</h3>
              <p className="animated fadeInUp delayp6">
              {shopText.shopTvStorageBanner.desc}
                </p>
            </div>
          </div>
          <div className="col-6 d-none d-md-block content-center">
            <img
              src={images.common["banner_bnr_tv_storage.png"]}
              className="img-fluid animated fadeInUp delayp7 rounded"
              alt="Speed Boost"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

const SectionText = ({shopText}) => {
  return (
    <div className="py-main-sm bg-white animated fadeInUp delayp7">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-12 mb-3">
            <div className="subheading">
              <p>About This Product</p>
            </div>
            <p className="mb-0 w-md-up-50">
            {shopText.shopTvStorageBanner.about}
                      </p>
          </div>
        </div>
      </div>
    </div>
  );
};

