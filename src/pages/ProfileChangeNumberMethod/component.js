import React from 'react';
import ContainerBase from '../../include/ContainerBase';
// import { validateMobile } from '../../services';
import { PageContext } from './../../context/PageContext';

class ProfileChangeNumberMethod extends React.Component {
  static contextType = PageContext;
  state = {
    mobileNumber: localStorage.getItem('mobile'),
  }

  componentDidMount() {
    this.context.trigger(1);
    this.context.pageCase("w-bg");
  }

  smsActivation() {
    const { history } = this.props;
    // const { mobileNumber } = this.state;

    // validateMobile(mobileNumber);
    history.push('/register/otp');
  }

  render() {
    const { common, brand, sample } = this.context;
    const { mobileNumber } = this.state;
    const images = { common, brand, sample };
    const containerProps = {
      image: { alt: 'Icon Verification', src: images.common['ic_header_verification.png'], },
      subtitle: 'Choose one of available method to receive account activation code',
      title: 'Choose Verification Method',
    };

    return (
      <ContainerBase {...containerProps}>
        <div className="list-group">
          <button onClick={() => this.smsActivation()} className="list-group-item w-arrow animated fadeInUp delayp2">
            <img src={images.common['ic_whatsapp.png']} className="img-fluid w-25px" alt="Icon WA" />
            <span>By <strong>Whatsapp</strong> to {mobileNumber}</span>
          </button>
          <button onClick={() => this.smsActivation()} className="list-group-item w-arrow animated fadeInUp delayp2">
            <img src={images.common['ic_form_sms.png']} className="img-fluid w-25px" alt="Icon SMS" />
            <span>By <strong>SMS</strong> to {mobileNumber}</span>
          </button>
        </div>
      </ContainerBase>
    )
  }
}

export default ProfileChangeNumberMethod;