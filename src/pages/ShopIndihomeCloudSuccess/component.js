import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const ShopIndihomeCloudSuccess = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = { brand, common, sample } ;
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
            <div className="container container-sm">
                <div className="box box-main animated fadeInUp">
                    <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                    <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
                    </header>
                    <section className="animated fadeInUp delayp2">
                        <h1>Your cloud storage is now activate!</h1>
                        <p>Download IndiHome Cloud app and login with the same e-mail as your myIndiHome account to start managing your storage</p>
                    </section>
                    <div className="btn-placeholder bottom animated fadeInUp delayp3">
                        <Link to="/home" className="btn btn-primary btn-block">Next</Link>
                    </div>
                </div>
            </div>
        </section> 
    )
}

export default ShopIndihomeCloudSuccess;