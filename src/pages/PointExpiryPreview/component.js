import React, { Component } from 'react';
import { Link } from "react-router-dom";
import PointsPreview from "./../PointsPreview";
import { NavMobile } from './../../include/NavMobile'
import { PageContext } from './../../context/PageContext';

class PointExpiryPreview extends Component {
    static contextType = PageContext;
    componentDidMount() {
        this.context.trigger(2);
        this.context.navCase("w-md-up-block");
        this.context.pageCase("l-bg");
        this.context.footerCase("w-nav-bottom");
    }
    componentWillUnmount() {
      this.context.footerCase("");
    }
    render() {
        const { common, brand, sample, condition } = this.context;
        const images = { common, brand, sample };
        return (
        <div>
            <NavMobile condition={condition} profilePage="active" />
            <PointsPreview expiryActive="active" />
            <SectionContent images={images} />
        </div>
        );
    }
}

export default PointExpiryPreview;

const SectionContent = () => {
    return (
        <div className="container">
            <div className="row">
                <div className="col-md-4 d-none d-md-block">
                    <div className="box box-in-cover py-3">
                        <div className="list-group list-group-activity mb-0">
                            <Link to="/points/history-preview" className="list-group-item w-arrow">
                                <div className="list-group-item-content">
                                <p className="title  mb-0">Point History</p>
                                </div>
                            </Link>
                            <Link to="/points/expiry-preview" className="list-group-item w-arrow">
                                <div className="list-group-item-content">
                                <p className="title active mb-0">Point Expiry</p>
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
                <div className="col-md-8 col-12">
                    <div className="box box-in-cover mt-sm-down-4 animated fadeInUp">
                        <div className="list-group list-group-activity mb-0">
                            <Link
                                    to="/history/activity/problem-report"
                                    className="list-group-item pr-0"
                                >
                                <div className="list-group-item-content mb-0">
                                <small className="caption">16 June 2019</small>
                                <div className="list-group-item-content-w-status">
                                    <p className="title">Redeem Voucher</p>
                                </div>
                                </div>
                            </Link>
                            <Link
                                to="/history/activity/problem-report"
                                className="list-group-item pr-0"
                            >
                                <div className="list-group-item-content mb-0">
                                <small className="caption">16 June 2019</small>
                                <div className="list-group-item-content-w-status">
                                    <p className="title">Subscribe Minipack</p>
                                </div>
                                </div>
                            </Link>
                            <Link
                                to="/history/activity/problem-report"
                                className="list-group-item pr-0"
                            >
                                <div className="list-group-item-content mb-0">
                                <small className="caption">16 June 2019</small>
                                <div className="list-group-item-content-w-status">
                                    <p className="title">Subscribe Minipack</p>
                                </div>
                                </div>
                            </Link>
                            <Link
                                    to="/history/activity/problem-report"
                                    className="list-group-item pr-0"
                                >
                                <div className="list-group-item-content mb-0">
                                <small className="caption">16 June 2019</small>
                                <div className="list-group-item-content-w-status">
                                    <p className="title">Redeem Voucher</p>
                                </div>
                                </div>
                            </Link>
                            <Link
                                to="/history/activity/problem-report"
                                className="list-group-item pr-0"
                            >
                                <div className="list-group-item-content mb-0">
                                <small className="caption">16 June 2019</small>
                                <div className="list-group-item-content-w-status">
                                    <p className="title">Subscribe Minipack</p>
                                </div>
                                </div>
                            </Link>
                            <Link
                                to="/history/activity/problem-report"
                                className="list-group-item pr-0"
                            >
                                <div className="list-group-item-content mb-0">
                                <small className="caption">16 June 2019</small>
                                <div className="list-group-item-content-w-status">
                                    <p className="title">Subscribe Minipack</p>
                                </div>
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}