import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';
const HistoryPurchaseDetails = () => {
  const { trigger, navCase, pageCase } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  return (
    <section>
      <div className="container container-sm">
        <div className="box animated fadeInUp">
          <header>
            <div className="header-actions">
              <Link to="/history/purchase" className="btn btn-link">
                <i className="fa fa-arrow-left"></i>
              </Link>
            </div>
          </header>
          <section className="section-header-text">
            <div className="heading">
              <h1>Subscription</h1>
              <br />
            </div>
          </section>
          <section className="py-main-sm pb-0">
            <div className="subheading animated fadeInUp delayp1">
              <p>Package</p>
            </div>
            <Link
              to="#"
              className="card card-packages w-arrow animated fadeInUp delayp2"
            >
              <div className="card-header">
                <div className="badges-list">
                  <div className="badges badge-red left">
                    <h3>100</h3>
                    <span>Mbps</span>
                  </div>
                </div>
                <div className="heading mb-0">
                  <h6 className="title">Paket BUMN</h6>
                  <p className="mb-0">Internet unlimited, 92 USee TV channels, 100 minutes voice call</p>
                </div>
              </div>
              <div className="card-body w-btn">
                <h3>
                  Rp480.000<small> /month</small>
                </h3>
                <span className="btn btn-primary">Details</span>
              </div>
            </Link>
            <div className="py-main-sm animated fadeInUp delayp3">
              <div className="subheading">
                <p>Detail Langganan</p>
              </div>
              <div className="row">
                <div className="col-6">
                  <div className="form-group">
                    <label>Start Form</label>
                    <p>10 January 2019</p>
                  </div>
                </div>
                <div className="col-6">
                  <div className="form-group">
                    <label>Valid Until</label>
                    <p>10 December 2019</p>
                  </div>
                </div>
                <div className="col-6">
                  <div className="form-group">
                    <label>Code</label>
                    <p>IH100MB</p>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div className="btn-placeholder">
            <Link to="/history/purchase" className="btn btn-primary btn-block">Ok</Link>
          </div>
        </div>
      </div>
    </section>
  )
}

export default HistoryPurchaseDetails;