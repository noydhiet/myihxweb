import React, { useState, useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { Form } from "react-bootstrap";
import { PageContext } from './../../context/PageContext';

const PersonalDataReviewEKTP = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = {brand, common, sample};
    useEffect(() => {
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
      }, [trigger, navCase, pageCase]);
    const [inputs, setInputs] = useState({
        ktpNumber: null,
        dob: null,
        name: null
    });
    const handleInputChange = (e) => {
        e.persist();
        setInputs(inputs => ({...inputs, [e.target.name]:e.target.value}));
    }
    return (
    <section>
        <div className="container container-sm">
              <div className="box box-main animated fadeInUp">
                  <header className="header-light white icon-center mb-3 animated fadeInUp delayp1">
                      <div className="header-actions">
                          <Link to="/personal-data" className="btn btn-link"><i className="fa fa-arrow-left"></i> </Link>
                      </div>
                      <div className="heading animated fadeInUp delayp1">
                          <h1>Review e-KTP Photo</h1>
                          <p>Please make sure your e-KTP picture is clear and show complete information</p>
                      </div>
                      <img className="header-img fluid" src={images.common["img_kyc_ktp.jpg"]} alt="Icon Success" />
                  </header>
                  <section className="animated fadeInUp delayp2">
                      <Form>
                          <Form.Group>
                          <Form.Label> Ktp Number </Form.Label>
                          <Form.Control
                              type="text"
                              name="ktpNumber"
                              value={inputs.ktpNumber}
                              placeholder="Enter your ktp number"
                              onChange={handleInputChange}
                          />
                          </Form.Group>
                          <Form.Group>
                          <Form.Label> Date of Birth </Form.Label>
                              <Form.Control
                                  type="date"
                                  name="dob"
                                  value={inputs.dob}
                                  placeholder="Enter Date of Birth"
                                  onChange={handleInputChange}
                              />
                        </Form.Group>
                        <Form.Group>
                          <Form.Label> Name </Form.Label>
                              <Form.Control
                                  type="text"
                                  name="name"
                                  value={inputs.name}
                                  placeholder="Enter Your Name"
                                  onChange={handleInputChange}
                              />
                        </Form.Group>
                      </Form>
          </section>
          <div className="btn-placeholder inline animated fadeInUp delayp3">
            <Link to="/" className="btn btn-transparent btn-block"> Reupload </Link>
            <Link to="/purchase/confirm-order" className="btn btn-primary btn-block"> Confirm  </Link>
          </div>
        </div>
      </div>
    </section>
    )
}

export default PersonalDataReviewEKTP;