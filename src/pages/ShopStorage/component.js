import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';

class ShopStorage extends Component {
    render() {
        const { cloudPage, tvStoragePage, shopPrice, shopPriceFixed, btnText, linkTo } = this.props;
        const config = {
            carouselTab: {
                className: "tab-list",
                dots: false,
                infinite: false,
                arrows: false,
                speed: 500,
                slidesToShow: 2,
                slidesToScroll: 2,
                responsive: [
                    {
                      breakpoint: 991,
                      settings: {
                        dots: false,
                        slidesToShow: 2,
                        slidesToScroll: 2
                      }
                    },
                    {
                      breakpoint: 575,
                      settings: {
                        dots: false,
                        slidesToShow: 2,
                        slidesToScroll: 2
                      }
                    },
                    {
                      breakpoint: 324,
                      settings: {
                        dots: false,
                        slidesToShow: 2,
                        slidesToScroll: 2
                      }
                    }
                ]
            }
          }
          const shopPriceView = shopPrice === undefined ? null : 
            <div className="shop-price animated fadeInUp delayp4 d-none d-md-block">
                <div className="container">
                    <div className="shop-price-info">
                    <h3>
                        {shopPrice} <small>/month</small>
                    </h3>
                    <Link to="#" className="">
                        View Pricing Detail <i className="fal fa-angle-right"></i>
                    </Link>
                    </div>
                    <Link to={linkTo} className="btn btn-primary">
                    {btnText}
                    </Link>
                </div>
            </div>;
        const shopPriceFixedView = shopPriceFixed === undefined ? null :
            <div
            className="shop-price fixed-bottom animated fadeInUp delayp8"
            id="shop-price"
            >
            <div className="container">
                <div className="shop-price-info">
                <h3>
                    {shopPriceFixed}<small>/month</small>
                </h3>
                <Link to="#" className="">
                    View Pricing Detail <i className="fal fa-angle-right"></i>
                </Link>
                </div>
                <Link to={linkTo} className="btn btn-primary">
                {btnText}
                </Link>
            </div>
            </div>;
        return (
        <div>
            <div className="cover cover-sm cover-bg-white-sm  bg-red-gradient">
                <div className="header-actions d-block d-md-none">
                    <Link
                    to="/shop/"
                    className="btn btn-link"
                    >
                    <i className="fa fa-arrow-left"></i>
                    </Link>
                </div>
                <div className="container">
                    <nav aria-label="breadcrumb" className="d-none d-md-block">
                    <ol className="breadcrumb animated fadeInUp">
                        <li className="breadcrumb-item"><Link to="/shop"><i className="fa fa-arrow-left mr-1"></i>Home</Link></li>
                    </ol>
                    </nav>
                    <div className="cover-content">
                    <h1 className="cover-title animated fadeInUp delayp1">Storage</h1>
                    {/* <p className="cover-text animated fadeInUp delayp2">Mulai berlangganan paket internet, TV dan telepon mulai dari <strong> Rp150.000 </strong> per bulan <sup>*</sup></p> */}
                    </div>
                    {shopPriceView}
                </div>
            </div>
            <section className="shop-tab-list animated fadeInUp delayp3">
                <div className="container">
                <Slider {...config.carouselTab}>
                    <div className={"tab-item " + cloudPage}> 
                        <Link to="/shop/storage/cloud">Cloud</Link> 
                    </div>
                    <div className={"tab-item " + tvStoragePage}> 
                        <Link to="/shop/storage/tv-storage">TV Storage</Link>
                    </div>
                </Slider>
                </div>
            </section>
            {shopPriceFixedView}
        </div>
    )
  }
}

export default ShopStorage;