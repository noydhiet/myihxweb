import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';
import moment from 'moment';

const FisikTicketScheduled = (props) => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    useEffect(() => {
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
        window.onpopstate = onBackButtonEvent;
    }, [trigger, navCase, pageCase]);
    const images = { brand, common, sample };
    const { match: { params } } = props;
    const onBackButtonEvent = (e) => {
        e.preventDefault();
        props.history.replace('/help');
    }
    const elements = params.pathSuffix.split('--', 2);
    const selectedDate = moment(elements[0].replace(':',''), 'DD-MM-YYYY').format('dddd, DD MMMM YYYY');
    const time = elements[1];
    console.log('params.pathSuffix', params.pathSuffix)
    return (
        <section>
        <div className="container container-sm">
            <div className="box box-main animated fadeInUp">
            <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
            </header>
            <section className="animated fadeInUp delayp2">
                <h1>All set! Your service is scheduled</h1>
                <p>Our technician will visit on you</p>
            </section>
            <div className="text-box mb-0">
                <h5 className="title">{selectedDate}</h5>
                <p className="desc">Estimated arrival time {time}</p>
            </div>
            <div className="btn-placeholder bottom animated fadeInUp delayp3">
                <small className="help-block mb-3 text-center">Adding appointment to calendar will not automatically update it if any changes appear.</small>
                {/* <Link to="/installation/schedule" className="btn btn-primary btn-block">Add To Calendar</Link> */}
                <Link to={{pathname: `/help`, state: {redirect: 'fisik_detail'}}} className="btn btn-transparent btn-block">View Report Progress</Link>
            </div>
            </div>
        </div>
        </section>
    )
}

export default FisikTicketScheduled;