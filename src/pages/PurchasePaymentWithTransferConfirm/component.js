import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Accordion } from 'react-bootstrap';
import { PageContext } from './../../context/PageContext';

const PurchasePaymentWithTransferConfirm = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = { brand, common, sample } ;
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
      <section>
        <div className="container container-sm">
            <div className="box animated fadeInUp">
                <header>
                <div className="header-actions">
                    <Link to="/payment-with-transfer" className="btn btn-link">
                    <i className="fa fa-arrow-left"></i>
                    </Link>
                </div>
                </header>
                <section className="section-header-text">
                    <div className="heading">
                        <h1>BCA</h1>
                    </div>
                </section>
                <section className="py-main-sm pb-0">
                    <Link
                        to="#"
                        className="card card-packages animated fadeInUp delayp2"
                        >
                        <div className="card-header w-button">
                            <div className="card-header-content">
                                <p>No Rekening</p>
                                <h3>123412341234</h3>
                            </div>
                            <Link to="/" className="btn btn-primary">Salin</Link>
                        </div>
                        <div className="card-body">
                            <div className="card-body-content mb-3">
                                <p>Jumlah Transfer</p>
                                <h3>
                                    Rp85.000
                                </h3>
                            </div>
                            <div className="card-body-content">
                                <p>Kode Pembayaran</p>
                                <h3>
                                    #412394
                                </h3>
                            </div>
                        </div>
                    </Link>
                    <div className="list-group mb-0">
                        <Link to="/shop/internet/package-finder/budget/1" className="list-group-item">
                            <img src={images.common["icon_ic_selection_time.png"]} className="img-fluid w-50px" alt="Budget Icon" />
                            <div className="list-group-item-content">
                                <div className="subheading mb-0">
                                    <p>Selesaikan Pembayaran Dalam</p>
                                </div>
                                <h3 className="text-primary">20:33:11</h3>
                            </div>
                        </Link>
                    </div>
                </section>
                <section className="py-main-sm pb-0">
                    <div className="subheading">
                        <p>Lihat Cara Pembayaran</p>
                    </div>
                    <Accordion defaultActiveKey="0">
                        <div className="list-group">
                            <div className="list-group-item">
                                <div className="list-group-item-content">
                                    <Accordion.Toggle eventKey="0" style={{ width: "100%", background: "transparent", border: "none", paddingLeft: "0", paddingRight: "0", height: "40px"}}>
                                        <div className="heading heading-w-link mb-0">
                                            <p className="caption caption-lg">ATM</p>
                                            <p className="text-primary"><i className="fas fa-chevron-down arrow"></i></p>
                                        </div>
                                    </Accordion.Toggle>
                                    <div>
                                    <Accordion.Collapse eventKey="0">
                                        <div className="py-main-sm milestone">
                                            <div className="milestone-item active">
                                                <div className="milestone-item-img">
                                                <div className="dot"></div>
                                                </div>
                                                <div className="milestone-item-content">
                                                    <h5 className="title">Lorem Ipsum Dolor Sit Ahmet</h5>
                                                    <span className="desc">Corabitur lobortis id lorem bibendum. Ut id consectur magna. Quesque volut</span>
                                                </div>
                                            </div>
                                            <div className="milestone-item active">
                                                <div className="milestone-item-img">
                                                <div className="dot"></div>
                                                </div>
                                                <div className="milestone-item-content">
                                                    <h5 className="title">Lorem Ipsum Dolor Sit Ahmet</h5>
                                                    <span className="desc">Corabitur lobortis id lorem bibendum. Ut id consectur magna. Quesque volut</span>
                                                </div>
                                            </div>
                                            <div className="milestone-item active last-child">
                                                <div className="milestone-item-img">
                                                <div className="dot"></div>
                                                </div>
                                                <div className="milestone-item-content">
                                                    <h5 className="title">Lorem Ipsum Dolor Sit Ahmet</h5>
                                                    <span className="desc">Corabitur lobortis id lorem bibendum. Ut id consectur magna. Quesque volut</span>
                                                </div>
                                            </div>
                                        </div>
                                    </Accordion.Collapse>
                                    </div>
                                </div>
                            </div>
                            <div className="list-group-item">
                                <div className="list-group-item-content">
                                    <Accordion.Toggle eventKey="1" style={{ width: "100%", background: "transparent", border: "none", paddingLeft: "0", paddingRight: "0", height: "40px"}}>
                                        <div className="heading heading-w-link mb-0">
                                            <p className="caption caption-lg">Mobile Banking</p>
                                            <p className="text-primary"><i className="fas fa-chevron-down arrow"></i></p>
                                        </div>
                                    </Accordion.Toggle>
                                    <div>
                                    <Accordion.Collapse eventKey="1">
                                        <div className="py-main-sm milestone">
                                            <div className="milestone-item active">
                                                <div className="milestone-item-img">
                                                <div className="dot"></div>
                                                </div>
                                                <div className="milestone-item-content">
                                                    <h5 className="title">Lorem Ipsum Dolor Sit Ahmet</h5>
                                                    <span className="desc">Corabitur lobortis id lorem bibendum. Ut id consectur magna. Quesque volut</span>
                                                </div>
                                            </div>
                                            <div className="milestone-item active">
                                                <div className="milestone-item-img">
                                                <div className="dot"></div>
                                                </div>
                                                <div className="milestone-item-content">
                                                    <h5 className="title">Lorem Ipsum Dolor Sit Ahmet</h5>
                                                    <span className="desc">Corabitur lobortis id lorem bibendum. Ut id consectur magna. Quesque volut</span>
                                                </div>
                                            </div>
                                            <div className="milestone-item active last-child">
                                                <div className="milestone-item-img">
                                                <div className="dot"></div>
                                                </div>
                                                <div className="milestone-item-content">
                                                    <h5 className="title">Lorem Ipsum Dolor Sit Ahmet</h5>
                                                    <span className="desc">Corabitur lobortis id lorem bibendum. Ut id consectur magna. Quesque volut</span>
                                                </div>
                                            </div>
                                        </div>
                                    </Accordion.Collapse>
                                    </div>
                                </div>
                            </div>
                            <div className="list-group-item">
                                <div className="list-group-item-content">
                                    <Accordion.Toggle eventKey="2" style={{ width: "100%", background: "transparent", border: "none", paddingLeft: "0", paddingRight: "0", height: "40px"}}>
                                        <div className="heading heading-w-link mb-0">
                                            <p className="caption caption-lg">Internet Banking</p>
                                            <p className="text-primary"><i className="fas fa-chevron-down arrow"></i></p>
                                        </div>
                                    </Accordion.Toggle>
                                    <div>
                                    <Accordion.Collapse eventKey="2">
                                        <div className="py-main-sm milestone">
                                            <div className="milestone-item active">
                                                <div className="milestone-item-img">
                                                <div className="dot"></div>
                                                </div>
                                                <div className="milestone-item-content">
                                                    <h5 className="title">Lorem Ipsum Dolor Sit Ahmet</h5>
                                                    <span className="desc">Corabitur lobortis id lorem bibendum. Ut id consectur magna. Quesque volut</span>
                                                </div>
                                            </div>
                                            <div className="milestone-item active">
                                                <div className="milestone-item-img">
                                                <div className="dot"></div>
                                                </div>
                                                <div className="milestone-item-content">
                                                    <h5 className="title">Lorem Ipsum Dolor Sit Ahmet</h5>
                                                    <span className="desc">Corabitur lobortis id lorem bibendum. Ut id consectur magna. Quesque volut</span>
                                                </div>
                                            </div>
                                            <div className="milestone-item active last-child">
                                                <div className="milestone-item-img">
                                                <div className="dot"></div>
                                                </div>
                                                <div className="milestone-item-content">
                                                    <h5 className="title">Lorem Ipsum Dolor Sit Ahmet</h5>
                                                    <span className="desc">Corabitur lobortis id lorem bibendum. Ut id consectur magna. Quesque volut</span>
                                                </div>
                                            </div>
                                        </div>
                                    </Accordion.Collapse>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Accordion>
                </section>
                <div className="btn-placeholder">
                    <Link to="/" className="btn btn-primary btn-block">Tutup</Link>
                </div>
            </div>
          </div>
        </section>
  )
}

export default PurchasePaymentWithTransferConfirm;