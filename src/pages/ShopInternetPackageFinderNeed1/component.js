import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const ByNeeds1 = () => {
  const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
  const images = { brand, common, sample };
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  return (
    <section>
      <div className="container container-sm">
        <div className="box animated fadeInUp">
          <header>
            <div className="header-actions">
              <Link to="/shop/internet/package-finder" className="btn btn-link">
                <i className="fa fa-arrow-left"></i>
              </Link>
              <ul className="btn wizard-finder">
                <li className="active"></li>
                <li></li>
                <li></li>
              </ul>
            </div>
            <div className="heading animated fadeInUp delayp1">
              <h1>How many people will use the internet?</h1>
            </div>
          </header>
          <section className="section-header-card header-card-sm animated fadeInUp delayp2">
            <div className="list-group mb-0">
              <Link to="/shop/internet/package-finder/needs/2" className="list-group-item w-arrow">
                <img src={images.common["ic_selection_person.png"]} className="img-fluid w-50px" alt="People Icon" />
                <div className="list-group-item-content">
                  <h5 className="title">1-2 People</h5>
                  <span className="desc">Internet for an individual or a couple</span>
                </div>
              </Link>
              <Link to="/shop/internet/package-finder/needs/2" className="list-group-item w-arrow">
                <img src={images.common["ic_selection_person.png"]} className="img-fluid w-50px" alt="People Icon" />
                <div className="list-group-item-content">
                  <h5 className="title">3-10 People</h5>
                  <span className="desc">A small family or team</span>
                </div>
              </Link>
              <Link to="/shop/internet/package-finder/needs/2" className="list-group-item w-arrow">
                <img src={images.common["ic_selection_person.png"]} className="img-fluid w-50px" alt="People Icon" />
                <div className="list-group-item-content">
                  <h5 className="title">11-20 People</h5>
                  <span className="desc">A start up or a small company</span>
                </div>
              </Link>
              <Link to="/shop/internet/package-finder/needs/2" className="list-group-item w-arrow">
                <img src={images.common["ic_selection_person.png"]} className="img-fluid w-50px" alt="People Icon" />
                <div className="list-group-item-content">
                  <h5 className="title">21-30 People</h5>
                  <span className="desc">A start up or a medium sized company</span>
                </div>
              </Link>
            </div>
          </section>
        </div>
      </div>
    </section>
  )
}

export default ByNeeds1
