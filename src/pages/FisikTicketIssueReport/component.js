import React, { useState, useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { Modal, Form } from 'react-bootstrap'
import { PageContext } from './../../context/PageContext';

const FisikTicketIssueReport = () => {
  const { trigger, navCase, pageCase, brand, common, sample, modal } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  const [inputs, setInputs] = useState({
    notifReport: false,
  });

  const closeNotif = () => {
    setTimeout(() => {
      setInputs({
        notifReport: false
      })
    }, 2000);
  }
  const notifView = inputs.notifReport ? "active" : ""
  const images = { brand, common, sample };
  if (inputs.notifReport === true) {
    closeNotif();
  }
  return (
    <section>
      <div className="container container-sm">
        <div className="box position-relative animated fadeInUp">
          <header className="">
            <div className="header-actions">
              <Link to="/installation/fisik-ticket" className="btn btn-link"><i className="fa fa-arrow-left" /></Link>
            </div>
            <div className="heading animated fadeInUp delayp1">
              <h1>Issue Report</h1>
            </div>
          </header>
          <section className="section-header-card py-main-sm animated fadeInUp delayp2">
            <div className="subheading">
              <p>Your Report</p>
            </div>
            <div className="list-group">
              <Link
                to="/shop/internet/package-finder"
                className="list-group-item transparent w-shadow"
              >
                <div className="list-group-item-content">
                  <h5 className="title w-md-up-75 mb-0">#1234567</h5>
                  <p className='desc'>Internet has been slow for more than 7 days. Technician will visit to analyze the problem.</p>
                </div>
              </Link>
            </div>
            <div className="py-main-sm pt-0 pb-0">
                <div className="subheading">
                <p>Report Process</p>
                </div>
                <div className="milestone mb-3">
                    <div className="milestone-item success">
                        <div className="milestone-item-img">
                        <div className="dot"></div>
                        </div>
                        <div className="milestone-item-content">
                        <h5 className="title">Report received</h5>
                        <span className="desc">We are trying to solve this issue.</span>
                        </div>
                    </div>
                    <div className="milestone-item">
                        <div className="milestone-item-img">
                        <div className="dot"></div>
                        </div>
                        <div className="milestone-item-content">
                        <h5 className="title">Repair in Progress</h5>
                        <span className="desc">We have assigned a special team to resolve this issue</span>
                        </div>
                    </div>
                    <div className="milestone-item last-child">
                        <div className="milestone-item-img">
                        <div className="dot"></div>
                        </div>
                        <div className="milestone-item-content">
                        <h5 className="title">Issue Solved</h5>
                        <span className="desc">Issue has been resolved</span>
                        </div>
                    </div>
                </div>
                <div className="btn-placeholder">
                    <small className="help-block text-center mb-3">Problem has already solved?</small>
                    <button className="btn btn-primary btn-block">Problem Resolved</button>
                </div>
            </div>
          </section>
        </div>
      </div>
    </section>
  )
}

export default FisikTicketIssueReport