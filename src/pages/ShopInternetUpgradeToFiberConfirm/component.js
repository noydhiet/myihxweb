import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const ShopInternetUpgradeToFiberConfirm = () => {
  const { trigger, navCase, pageCase, loggedIn } = useContext(PageContext);
  useEffect(() => {
    loggedIn();
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [loggedIn, trigger, navCase, pageCase]);
  
  return (
    <section>
      <div className="container container-sm">
        <div className="box animated fadeInUp">
          <header>
            <div className="header-actions">
              <Link to="/shop/internet/upgrade-to-fiber" className="btn btn-link">
                <i className="fa fa-arrow-left"></i>
              </Link>
            </div>
          </header>
          <section className="section-header-text">
            <div className="heading">
              <h1>Confirm order</h1>
              <p>Confirm your selection and order</p>
            </div>
          </section>
          <section className="py-main-sm pb-0">
            <div className="subheading">
              <p>Product</p>
            </div>
            <Link
              to="#"
              className="card card-packages w-arrow animated fadeInUp delayp2"
            >
              <div className="card-header">
                <div className="badges-list">
                  <div className="badges badge-red left">
                    <i className="fas fa-wifi" style={{ fontSize: "2.5rem", color: "#ffffff" }}></i>
                  </div>
                </div>
                <div className="heading mb-0">
                  <h6 className="title mb-1">Upgrade Fiber Optic</h6>
                  <p className="mb-0">Upgrade internet network to fiber optic</p>
                </div>
              </div>
              <div className="card-body w-btn">
                <h3>
                  Rp100.000<small>/month</small>
                </h3>
              </div>
            </Link>
          </section>
          <section className="py-main-sm pb-0">
            <div className="subheading">
              <p>Installation Information</p>
            </div>
            <div className="form-group">
              <label>Address</label>
              <p>Jl. Jurang Manggu Barat no.8, Pondok Aren, Tanggerang Selatan, Indonesia, 15123</p>
            </div>
            <div className="form-group">
              <label>Total</label>
              <h4>Rp100.000</h4>
              <small className="help-block">Amount will be charged to your next bill. Includes 15% tax</small>
            </div>
            <div className="btn-placeholder">
              <div className="row">
                <div className="col-6">
                  <Link to="/shop/internet/upgrade-to-fiber" className="btn btn-transparent btn-block">Cancel</Link>
                </div>
                <div className="col-6">
                  <Link to="/shop/internet/upgrade-to-fiber/success" className="btn btn-primary btn-block"> <span>Confirm</span> </Link>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
  )
}

export default ShopInternetUpgradeToFiberConfirm;