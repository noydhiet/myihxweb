import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const BillSuccess = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    const images = { brand, common, sample } ;
    return (
        <section>
            <div className="container container-sm">
                <div className="box box-main text-left animated fadeInUp">
                    <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                        <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
                    </header>
                    <section className="mb-2 animated fadeInUp delayp2">
                        <h1>Transaction Successful</h1>
                        <p className="small-text">Monday, 10th June 2019</p>
                        <p className="small-text">Transaction ID 12341234</p>
                    </section>
                    <section className="py-main-sm pb-0">
                        <div className="subheading">
                            <p>Paymeny Details</p>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-12">
                                <div className="form-group">
                                    <label className="help-block">Billing Period</label>
                                    <p>July 2019</p>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="form-group">
                                    <label className="help-block">Payment Method</label>
                                    <p>LinkAja</p>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="py-main-sm pb-0">
                        <div className="subheading mb-0">
                            <p>Billing Details</p>
                        </div>
                        <div className="list-group list-group-activity mb-0">
                            <div className="list-group-item w-arrow">
                                <div className="list-group-item-content mb-0">
                                <p className="title">10 Mbps IndiGamer TriplePlay</p>
                                <small className="help-block">
                                    10Mbps + 92 channels + 100 minutes
                                </small>
                  <Link
                    to="/history/purchase/details"
                    className="btn btn-link px-0 py-0"
                  >
                    Lihat Detail
                                </Link>
                </div>
              </div>
              <div className="list-group-item w-arrow">
                <div className="list-group-item-content mb-0">
                  <p className="title">30 Mbps Extra Speed</p>
                  <small className="help-block">
                    3 days package * 1-3 July 2019
                                </small>
                </div>
              </div>
              <div className="list-group-item w-arrow">
                <div className="list-group-item-content mb-0">
                  <p className="title">Minipack IndiKids Lite</p>
                  <small className="help-block">3 channels</small>
                  <Link
                    to="/history/purchase/details"
                    className="btn btn-link px-0 py-0"
                  >
                    Lihat Detail
                                </Link>
                </div>
              </div>
            </div>
            <div className="list-group list-group-transparent">
              <div className="list-group-item p-0">
                <div className="list-group-item-content">
                  <div className="subheading mb-0">
                    <p>Total Paid</p>
                  </div>
                  <h2>Rp2.650.000</h2>
                  <small className="help-block">Includes 10% tax</small>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
  )
}

export default BillSuccess;