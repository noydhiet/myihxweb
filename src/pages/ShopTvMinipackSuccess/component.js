import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const ShopTvMinipackSuccess = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = { brand, common, sample } ;
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
            <div className="container container-sm">
                <div className="box box-main animated fadeInUp">
                    <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                    <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
                    </header>
                    <section className="animated fadeInUp delayp2">
                        <h1>You have subcribed to Dinasty 2 Minipack!</h1>
                        <p>Your new channels will be accessible to watch in USeeTV within 1x24 hours</p>
                        <p>Please contact us if you have any questions or concerns</p>
                    </section>
                    <div className="btn-placeholder bottom animated fadeInUp delayp3">
                        <Link to="/home" className="btn btn-primary btn-block">Back To Home</Link>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default ShopTvMinipackSuccess;