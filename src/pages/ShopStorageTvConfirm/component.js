import React, { useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { PageContext } from './../../context/PageContext';
const ShopStorageTvConfirm = () => {
  const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  const images = { brand, common, sample };
  return (
    <section>
      <div className="container container-sm">
        <div className="box animated fadeInUp">
          <header>
            <div className="header-actions">
              <Link to="/shop/storage/tv-storage" className="btn btn-link">
                <i className="fa fa-arrow-left"></i>
              </Link>
            </div>
          </header>
          <section className="section-header-text">
            <div className="heading">
              <h1>Confirm Order</h1>
              <p>Please make sure your package details are right before confirming</p>
            </div>
          </section>
          <section className="py-main-sm pb-0">
            <div className="subheading">
              <p>Product</p>
            </div>
            <Link
              to="#"
              className="card card-packages w-arrow animated fadeInUp delayp2"
            >
              <div className="card-header">
                <div className="badges-list">
                  <div className="badges badge-gray left">
                    <img alt="icon" src={images.common["ic_shop_tv_white.png"]} className="w-75px"/>
                  </div>
                </div>
                <div className="heading mb-0">
                  <h6 className="title">TV Storage</h6>
                  <p className="mb-0">Up to 50 GB Storage.</p>
                </div>
              </div>
              <div className="card-body w-btn">
                <h3>
                  Rp80.000<small> /month</small>
                </h3>
              </div>
            </Link>
          </section>
          <section className="py-main-sm pb-0">
            <div className="subheading">
              <p>Fee Detail</p>
            </div>
            <div className="form-group">
              <label>Total</label>
              <h4>Rp80.000</h4>
              <small className="help-block">Include 10% tax. This amount will be billed every month starting from 30th July 2019</small>
            </div>
            <div className="btn-placeholder bottom">
              <div className="row">
                <div className="col-6">
                  <Link to="/shop/storage/tv-storage" className="btn btn-transparent btn-block"> cancel </Link>
                </div>
                <div className="col-6">
                  <Link to="/shop/storage/tv-storage/success" className="btn btn-primary btn-block"> <span> Confirm </span> </Link>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
  )
}

export default ShopStorageTvConfirm;
