import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const ShopTvAppsRedirectConfirm = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = { brand, common, sample } ;
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
            <div className="container container-sm">
                <div className="box box-main animated fadeInUp">
                    <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                    <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
                    </header>
                    <section className="animated fadeInUp delayp2">
                        <h1>You will be redirected to iflix for registration</h1>
                        <p>To activate your free iflix membership, sign up or login in the next screen!</p>
                    </section>
                    <div className="btn-placeholder bottom animated fadeInUp delayp3">
                        <Link to="/shop/tv/apps/success" className="btn btn-primary btn-block">Next</Link>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default ShopTvAppsRedirectConfirm;