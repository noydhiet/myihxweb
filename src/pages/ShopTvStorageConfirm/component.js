import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { PageContext } from './../../context/PageContext';
import { getToken } from '../../utils/storage';

class ShopTvStorageConfirm extends Component {
  static contextType = PageContext;
  constructor(props){
    super(props)
    if (!getToken()) {
      window.location.href = '/login:';
      return
    }
  }
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("w-bg");
  }
  render() {
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    return (
      <section>
        <div className="container container-sm">
          <div className="box animated fadeInUp">
            <header>
              <div className="header-actions">
                <Link to="/shop/tv/tv-storage" className="btn btn-link">
                  <i className="fa fa-arrow-left"></i>
                </Link>
              </div>
            </header>
            <section className="section-header-text">
              <div className="heading">
                <h1>Confirm Subscription</h1>
                <p>Confirm subscription to continue</p>
              </div>
            </section>
            <section className="py-main-sm pb-0">
              <div className="subheading">
                <p>Product</p>
              </div>
              <Link
                to="#"
                className="card card-packages w-arrow animated fadeInUp delayp2"
              >
                <div className="card-header">
                  <div className="badges-list">
                    <div className="badges badge-gray left">
                      <img alt="icon" src={images.common["ic_shop_tv_white.png"]} className="w-75px"/>
                    </div>
                  </div>
                  <div className="heading mb-0">
                    <h6 className="title">Extra TV Storage</h6>
                    <p className="mb-0">Additional TV storage up to 10GB.</p>
                  </div>
                </div>
                <div className="card-body w-btn">
                  <h3>
                    Rp15.000<small> /month</small>
                  </h3>
                  <p className="help-block mb-0">Billing starts next month</p>
                </div>
              </Link>
            </section>
            <section className="py-main-sm pb-0">
              <div className="subheading">
                <p>Total</p>
              </div>
              <div className="form-group">
                <h4>Rp15.000</h4>
                <small className="help-block">Amount will be charged to your next bill. Includes 15% tax.</small>
              </div>
              <div className="btn-placeholder bottom">
                <div className="row">
                  <div className="col-6">
                    <Link to="/shop/tv/tv-storage" className="btn btn-transparent btn-block"> cancel </Link>
                  </div>
                  <div className="col-6">
                    <Link to="/shop/tv/tv-storage/success" className="btn btn-primary btn-block"> <span> next </span> </Link>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
    )
  }
}

export default ShopTvStorageConfirm;
