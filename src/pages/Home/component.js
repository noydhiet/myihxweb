import React, { Component } from "react";
import { Link } from "react-router-dom";
import { NavMobile } from './../../include/NavMobile'
import { CardBasic, CardContent, CardContentStacked } from './../../include/CardPackage'
import { Modal, Form } from 'react-bootstrap'
import Slider from "react-slick";
import TrackVisibility from "react-on-screen";
import { getUserData, getInstallationFlag, getUserProfile, setUserProfile } from '../../utils/storage';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import Skeleton from 'react-loading-skeleton';
import { installationStatus, userProfile } from '../../services'
import moment from 'moment';

export default class Home extends Component {
  static contextType = PageContext;
  constructor() {
    super();

    this.state = {
      userData: getUserData(),
      load: false,
      cardsArray: [],
    }
  }

  componentDidMount() {
    this.context.trigger(2);
    // this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
    this.context.footerCase("w-nav-bottom");
    this.context.navView(0);
    setTimeout(() => {
      this.setState({
        load: true
      })
    }, 3000);
    this.notificationCards();
  }

  notificationCards = () => {
    if(this.state.userData) {
      const userProfileData = getUserProfile();
      if(userProfileData && userProfileData.accounts && userProfileData.accounts.length > 0 && Object.keys(userProfileData.accounts[0].Location).length > 0) {
        this.fetchNotificationCards();
        console.log('notificationCardsiffff')
      } else {
        console.log('notificationCardselse')
        userProfile().then( ({ data }) => {
          console.log('userProfile', data)
          if(data.status === 200 || data.status === 201) {
            setUserProfile(data.data);
            if(data.data.accounts && data.data.accounts.length > 0 && Object.keys(data.data.accounts[0].Location).length > 0) {
              this.fetchNotificationCards();
            } else {
              this.setState({cardsArray: null})
            }
          } else {
            this.setState({cardsArray: null})
          }
      })
      .catch( error => {this.setState({cardsArray: null}); console.error(error)})
      }
    }
  }

  fetchNotificationCards = () => {
    installationStatus().then(response => {
      console.log('fetchNotificationCards', response)
      if(response.status === 200) {
        const { location, history } = this.props;
        const { state } = location;
        const { cardsArray } = this.state;
        const { brand, common, sample } = this.context;
        const images = { brand, common, sample };
        const coverStyle = {
          background: `url(${images.common["bnr_blue.png"]}) no-repeat center`,
          backgroundSize: "cover",
          border: "none"
        };
        const appointments = response.data.data.appointments;
        if(appointments) {
          if(appointments.length === 0) {
            cardsArray.push(
                  <div key={0} className="carousel-item">
                      <div className="list-group">
                        <Link
                          to="/installation/schedule"
                          className="list-group-item w-arrow light"
                          style={coverStyle}
                        >
                          <img alt="img" src={images.common["graphic_calendar.png"]} className="w-50px" />
                          <div className="list-group-item-content">
                            <span className="caption">Order ID 1234567890</span>
                            <h5 className="title">Schedule Installation</h5>
                            <span className="desc">
                              Schedule an installation before 3 August 2019
                              </span>
                          </div>
                        </Link>
                      </div>
                    </div>
            );
            this.setState({...cardsArray, [cardsArray]: cardsArray})
          } else if(appointments.length > 0 && appointments[0].status === 'BOOKED') {
            console.log('redirect1', state)
            if(state) {
              history.push({pathname: '/installation/detail', state: {appointments: appointments[0]}});
              return(null);
            } else {
              cardsArray.push(
                <div key={1} className="carousel-item">
                  <div className="list-group">
                    <Link
                      to={{pathname: '/installation/detail', state: {appointments: appointments[0]}}}
                      className="list-group-item w-arrow light"
                      style={coverStyle}
                    >
                      <img alt="img" src={images.common["grfx_service.png"]} className="w-50px" />
                      <div className="list-group-item-content">
                        <span className="caption">Order ID {appointments[0].transactionId}</span>
                        <h5 className="title">New Installation</h5>
                        <span className="desc">
                          {
                            appointments[0].timeSlot && Object.keys(appointments[0].timeSlot).length > 0 
                            ?
                            `You are scheduled to meet our technician on ${moment(appointments[0].timeSlot.date, 'DD-MM-YYYY').format('D MMMM YYYY')}.`
                            :
                            ''
                          }
                        </span>
                      </div>
                    </Link>
                  </div>
                </div>
              );
              this.setState({...cardsArray, [cardsArray]: cardsArray})
            }
          } else if(appointments.length > 0 && (appointments[0].status === 'ON_THE_WAY' || appointments[0].status === 'IN_PROGRESS' || appointments[0].status === 'COMPLETED')) {
            console.log('redirect2', state)
            if(state) {
              history.push({pathname: '/installation/detail', state: {appointments: appointments[0]}})
              return(null);
            } else {
              cardsArray.push(
                <div key={1} className="carousel-item">
                  <div className="list-group">
                    <Link
                      to={{pathname: '/installation/detail-waiting', state: {appointments: appointments[0]}}}
                      className="list-group-item w-arrow light"
                      style={coverStyle}
                    >
                      <img alt="img" src={images.common["grfx_service.png"]} className="w-50px" />
                      <div className="list-group-item-content">
                        <span className="caption">Order ID {appointments[0].transactionId}</span>
                        <h5 className="title">{appointments[0].currentStatus ? appointments[0].currentStatus.title : ''}</h5>
                        <span className="desc">
                          {appointments[0].currentStatus ? appointments[0].currentStatus.text : ''}
                        </span>
                      </div>
                    </Link>
                  </div>
                </div>
              );
              this.setState({...cardsArray, [cardsArray]: cardsArray})
            }
          } else {
            this.setState({cardsArray: null});
          }
        }
      }
    }).catch(error => {
      console.error(error);
    });
  }

  componentWillUnmount() {
    this.context.footerCase("");
  }

  _renderUsername() {
    const { userData } = this.state;

    return (
      <div>
        {this.state.load ?
          <Link to="/profile" className="list-group-item or-3">
            <div className="img-profile w-text ">
              <span>{userData.userName.charAt(0)}</span>
            </div>
            <div className="list-group-item-content">
              <small className="top-text">Selamat Datang</small>
              <p className="mid-text">
                {userData.userName}&nbsp;
              <small><i className="far fa-angle-right text-white" /></small>
              </p>
              <small className="bottom-text">0 pts</small>
            </div>
          </Link>
          :
          <div className="d-flex">
            <Skeleton circle={true} height={50} width={50} />
            <div className="content-center-left ml-3">
              <div className="animated fadeInUp delayp1">
                <Skeleton width={100} />
              </div>
              <div className="animated fadeInUp delayp2">
                <Skeleton width={150} />
              </div>
            </div>
          </div>
        }
      </div>
    );
  }

  _renderWelcome() {
    return (
      <div className="list-group-item or-3">
        <div className="list-group-item-content">
          {this.state.load ?
            <>
              <small className="bottom-text animated fadeInUp delayp2">Welcome to</small>
              <p className="mid-text animated fadeInUp delayp3">myIndiHome X</p>
            </>
            :
            <>
              <div className="animated fadeInUp delayp1">
                <Skeleton width={100} />
              </div>
              <div className="animated fadeInUp delayp2">
                <Skeleton width={150} />
              </div>
            </>
          }
        </div>
      </div>
    );
  }

  render() {
    const { condition, brand, common, sample } = this.context;
    const images = { brand, common, sample };
    const { userData, cardsArray } = this.state;
    const config = {
      carousel: {
        className: "primary-carousel overflow-inherit",
        dots: false,
        infinite: false,
        arrows: false,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 736,
            settings: {
              slidesToShow: 1.2,
              slidesToScroll: 1.2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1.1,
              slidesToScroll: 1.1
            }
          }
        ]
      }
    };
    return (
      <TextContext.Consumer>{(context)=>{
        const { generalText } = context;
        return (
          <div>
          <NavMobile condition={condition} homePage="active" />
          <div className="home-cover bg-gray-50 py-main">
            <img
              src={images.common["bg_home.jpg"]}
              alt="bg"
              className="home-cover-bg"
            />
            <div className="container">
              <div className="list-group list-group-home-profile animated fadeInUp">
                {userData ? this._renderUsername() : this._renderWelcome()}
              </div>
              <Link to="" className="nav-link-message w-notif">
                <i className="far fa-envelope"></i>
              </Link>
              {userData && (<Slider {...config.carousel}>
                <div className="carousel-item">
                  <CardBasic
                    link="/shop/internet/package/details"
                    className="animated fadeInUp delayp2"
                    caption="Tagihan bulan ini"
                    mainText="RP1.890.000"
                    label="Belum Dibayar"
                    footerLeftNotes="Tanggal jatuh tempo"
                    footerRightNotes="30 Juni 2019"
                    images={images}
                  />
                </div>
                <div className="carousel-item">
                  <CardBasic
                    link="/shop/internet/package/details"
                    className="animated fadeInUp delayp2"
                    caption="Penggunaan bulan ini"
                    footerLeftNotes="Unlimited 30 Mbps + 42 channel"
                    footerRightNotes=""
                    usedPackage={true}
                    mainUsedTextLeft="250"
                    mainUsedTextRight="450"
                    images={images}
                  />
  
                </div>
              </Slider>)}
              {
                this.state.load ?
                  <div className="row">
                    <div className={"col-md-6 col-12 mb-3" + (userData ? ' hidden' : '')}>
                      <div className="card card-packages card-home-package animated fadeInUp delayp5">
                        <div className="card-header">
                          <div className="card-header-content">
                            <h3>Unlimited connectivity with IndiHome. Get Yours now</h3>
                            <p>Get unlimited internet from as low as <span className="text-green">Rp320.000/mo</span></p>
                          </div>
                          <img alt="" src={images.common["ic_shop_internet.png"]} className="w-75px" />
                        </div>
                        <div className="card-body">
                          <Link to="/shop/internet/package" className="btn btn-primary btn-block">Browse Package</Link>
                        </div>
                      </div>
                    </div>
                  </div>
                  :
                  <div>
                    <div className="row">
                      <div className={"col-md-6 col-12 mb-3" + (userData ? ' hidden' : '')}>
                        <section className="card-skeleton animated fadeInUp delayp5">
                          <figure className="card-image loading"></figure>
                        </section>
                      </div>
                    </div>
                  </div>
              }
  
            </div>
          </div>
          <TrackVisibility once offset={400}>
            <ProductsSection images={images} userData={userData} load={this.state.load} cardsArray={cardsArray} generalText={generalText}/>
          </TrackVisibility>
          <TrackVisibility once offset={400}>
            <SubscribeWatchSection images={images} load={this.state.load} generalText={generalText}/>
          </TrackVisibility>
        </div>
        )
      }}
      </TextContext.Consumer>
    );
  }
}

class ProductsSection extends Component {
  state = {
    modalAlert: false,
    modalReschedule: false,
    modalContact: false,
    modalSetReschedule: false,
    modalConfirmReschedule: false,
    modalEarlyVisit: false,
    modalConfirmEarlyVisit: false,
    modalRefunds: false,
    modalRefundsDetail: false,
    status: false,
    name: "",
    mobileNumber: "",
    bank: "",
    noRek: "",
    notif: false,
    notifEarlyNo: false,
    notifEarlyYes: false
  }
  modal = (mode) => {
    switch (mode) {
      case "alert":
        this.setState({
          modalAlert: !this.state.modalSetReschedule
        });
        break;

      case "reschedule":
        this.setState({
          modalReschedule: !this.state.modalReschedule
        });
        break;

      case "addContact":
        this.setState({
          modalReschedule: false,
          modalContact: !this.state.modalContact
        });
        break;

      case "setReschedule":
        this.setState({
          modalReschedule: !this.state.modalReschedule,
          modalSetReschedule: !this.state.modalSetReschedule
        });
        break;

      case "confirmAddContact":
        this.setState({
          status: !this.state.status,
          modalContact: !this.state.modalContact,
          notif: true
        });
        break;

      case "confirmReschedule":
        this.setState({
          modalSetReschedule: !this.state.modalSetReschedule,
          modalConfirmReschedule: !this.state.modalConfirmReschedule
        });
        break;

      case "earlyvisit":
        this.setState({
          modalEarlyVisit: !this.state.modalEarlyVisit
        });
        break;

      case "confirmEarlyvisit":
        this.setState({
          modalEarlyVisit: !this.state.modalEarlyVisit,
          modalConfirmEarlyVisit: !this.state.modalConfirmEarlyVisit
        });
        break;

      case "earlyVisitNo":
        this.setState({
          modalEarlyVisit: !this.state.modalEarlyVisit,
          notifEarlyNo: true
        });
        break;

      case "earlyVisitYes":
        this.setState({
          modalConfirmEarlyVisit: !this.state.modalConfirmEarlyVisit,
          notifEarlyYes: true
        });
        break;

      case "refunds":
        this.setState({
          modalRefunds: !this.state.modalRefunds
        });
        break;

      case "refundsdetail":
        this.setState({
          modalRefundsDetail: !this.state.modalRefundsDetail
        });
        break;

      case "close":
        this.setState({
          modalReschedule: false,
          modalContact: false,
          modalSetReschedule: false,
          modalConfirmReschedule: false,
          modalEarlyVisit: false,
          modalConfirmEarlyVisit: false,
          modalRefunds: false,
          modalRefundsDetail: false
        });
        break;

      default:
        return null
    }
    this.setState({
      modal: !this.state.modal
    })
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }
  setSchedule = (mode) => {
    const badgesCheck = document.getElementsByClassName("badge-date");
    for (let i = 0; i < badgesCheck.length; i++) {
      if (badgesCheck[i] === badgesCheck[mode]) {
        badgesCheck[i].classList.add("active");
      } else {
        badgesCheck[i].classList.remove("active");
      }
    }
  }
  estimatedArrival = (mode) => {
    const listCheck = document.getElementsByClassName("list-check");
    const morning = document.getElementById("morning");
    const afternoon = document.getElementById("afternoon");
    switch (mode) {
      case "0":
        morning.checked = true;
        afternoon.checked = false;
        listCheck[0].classList.add("active");
        listCheck[1].classList.remove("active");
        break;

      case "1":
        afternoon.checked = true;
        morning.checked = false;
        listCheck[0].classList.remove("active");
        listCheck[1].classList.add("active");
        break;

      default:
        return null
    }
  }
  closeNotif = () => {
    setTimeout(() => {
      this.setState({
        notif: false
      })
    }, 2000)
  }
  closeNotifEarlyNo = () => {
    setTimeout(() => {
      this.setState({
        notifEarlyNo: false
      })
    }, 2000)
  }
  closeNotifEarlyYes = () => {
    setTimeout(() => {
      this.setState({
        notifEarlyYes: false
      })
    }, 2000)
  }
  render() {
    const { modalAlert, modalReschedule, modalContact, modalSetReschedule, modalConfirmReschedule, modalEarlyVisit, modalConfirmEarlyVisit, modalRefunds, modalRefundsDetail, notifEarlyNo, notifEarlyYes, name, mobileNumber, notif, bank, noRek } = this.state
    const { images, userData, isVisible, cardsArray, generalText } = this.props
    if (notif === true) {
      this.closeNotif();
    }
    if (notifEarlyNo === true) {
      this.closeNotifEarlyNo();
    }
    if (notifEarlyYes === true) {
      this.closeNotifEarlyYes();
    }
    const config = {
      carousel1st: {
        className: "primary-carousel overflow-inherit",
        dots: false,
        infinite: false,
        arrows: false,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              initialSlide: 3
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1.1,
              slidesToScroll: 1.1,
              initialSlide: 3
            }
          },
          {
            breakpoint: 320,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      },
      carousel2item: {
        className: "primary-carousel view-carousel",
        dots: false,
        infinite: false,
        arrows: false,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              initialSlide: 3
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1.1,
              slidesToScroll: 1.1,
              initialSlide: 3
            }
          },
          {
            breakpoint: 320,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      },
      carousel4th: {
        className: "primary-carousel view-carousel w-text",
        dots: false,
        infinite: false,
        arrows: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1.5,
              slidesToScroll: 1.5
            }
          }
        ]
      },
      carousel3rd: {
        className: "primary-carousel view-carousel mb-3 animated fadeInUp delayp7",
        dots: false,
        infinite: false,
        arrows: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 3
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1.1,
              slidesToScroll: 1.1
            }
          }
        ]
      },
      carouselDate: {
        className: "primary-carousel overflow-inherit mb-4",
        dots: false,
        infinite: false,
        arrows: false,
        speed: 500,
        slidesToShow: 5.3,
        slidesToScroll: 5.3,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 5.5,
              slidesToScroll: 5.5
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 3.5,
              slidesToScroll: 3.5
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 4.5,
              slidesToScroll: 4.5
            }
          },
          {
            breakpoint: 320,
            settings: {
              slidesToShow: 3.5,
              slidesToScroll: 3.5
            }
          }
        ]
      }
    };
    const coverStyle = {
      background: `url(${images.common["bnr_blue.png"]}) no-repeat center`,
      backgroundSize: "cover",
      border: "none",
      minHeight: "105px"
    };
    const notifView = notif ? "active" : "";
    const notifViewEarly = {
      no: notifEarlyNo ? "active" : "",
      yes: notifEarlyYes ? "active" : ""
    }
    const effect = isVisible ? "fadeInUp" : "";
    const proses = bank.length < 1 || noRek.length < 1 ? "disabled" : "";
    const disabled = name.length < 1 || mobileNumber.length < 1 ? "disabled" : "";
    const recommend = ["bnr_recommend01.png", "bnr_recommend02.png", "bnr_recommend03.png"];
    const offering = ["offer1.png", "offer2.png", "offer3.png", "offer1.png", "offer2.png", "offer3.png"]
    const nextScreen = getInstallationFlag() === 'true' ? '#' : '/installation/schedule';
    const titleSchedule = getInstallationFlag() === 'true' ? 'New Installation' : 'Service Dijadwalkan';
    const descriptionSchedule = getInstallationFlag() === 'true' ? 'You are to scheduled to meet our technician tomorrow' : 'Teknisi kami akan mengunjungi Anda pada 23 Agustus 2019.';

    return (
      <div className="home-product bg-gray-50 py-main pb-main pt-2">
        <div className="container">
          <div className={"row" + (userData ? '' : ' hidden')}>
            <div className="col-md-6 col-12 mb-3">
              <div className="card card-packages card-home-package animated fadeInUp delayp5">
                <div className="card-header">
                  <div className="card-header-content">
                    <h3>Unlimited connectivity with IndiHome. Get Yours now</h3>
                    <p>Get unlimited internet from as low as <span className="text-green">Rp320.000/mo</span></p>
                  </div>
                  <img alt="img" src={images.common["ic_shop_internet.png"]} className="w-75px" />
                </div>
                <div className="card-body">
                  <Link to="/shop/internet/package" className="btn btn-primary btn-block">Browse Package</Link>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-12 mb-3">
              <div className="card card-packages card-home-package animated fadeInUp delayp5">
                <div className="card-header">
                  <div className="card-header-content">
                    <h3>Installation will be finished soon!</h3>
                    <p>Explore available features to get the most from our services</p>
                  </div>
                  <img alt="img" src={images.common["ic_shop_internet.png"]} className="w-75px" />
                </div>
                <div className="card-body">
                  <button className="btn btn-primary btn-block">Explore Features</button>
                </div>
              </div>
            </div>
          </div>
          <div className="list-group hidden">
            <Link
              to="#"
              className="list-group-item w-arrow light"
              onClick={() => this.modal("reschedule")}
              style={{ background: "#000000" }}
            >
              <img alt="img" src={images.common["grfx_warning.png"]} className="w-50px" />
              <div className="list-group-item-content">
                <h5 className="title">Saat ini daerah Anda sedang mengalami gangguan massal</h5>
                <span className="desc">
                  Hal ini sedang dalam pengangan teknisi kami. {" "}
                </span>
              </div>
            </Link>
          </div>
          <div className="list-group">

          {this.props.load ?
          <>
            {/* <Link
              to="#"
              className="list-group-item w-arrow light animated fadeInUp delayp6"
              style={{ backgroundImage: "linear-gradient(to right, #7a0046 , #d12300, #ff7e00)" }}>
              <img alt="img" src={images.common["grfx_grfx_action_boost.png"]} className="w-50px" />
              <div className="list-group-item-content">
                <h5 className="title">Your current speed has been boosted to 50 Mbps!</h5>
                <span className="desc">
                  Valid until 20 July 2019 (3 days). {" "}
                </span>
              </div>
            </Link> */}
            <Link
              to="#"
              className="list-group-item w-arrow light hidden"
              onClick={() => this.modal("reschedule")}
              style={{ background: "#000000" }}
            >
              <img alt="img" src={images.common["grfx_warning.png"]} className="w-50px" />
              <div className="list-group-item-content">
                <h5 className="title">Saat ini daerah Anda sedang mengalami gangguan massal</h5>
                <span className="desc">
                  Hal ini sedang dalam pengangan teknisi kami. {" "}
                </span>
              </div>
            </Link>
            <Link
              to="#"
              className="list-group-item w-arrow light hidden"
              onClick={() => this.modal("reschedule")}
              style={{ background: "#000000" }}
            >
              <img alt="img" src={images.common["grfx_warning.png"]} className="w-50px" />
              <div className="list-group-item-content">
                <h5 className="title">Maaf, saat ini layanan Anda sedang terisolir</h5>
                <span className="desc">
                  Agar dapat digunakan kembali, silahkan melakukan pembayaran {" "}
                </span>
              </div>
            </Link>
            <Link
              to="#"
              className="list-group-item w-arrow light hidden"
              onClick={() => this.modal("reschedule")}
              style={{ background: "#000000" }}
            >
              <img alt="img" src={images.common["grfx_warning.png"]} className="w-50px" />
              <div className="list-group-item-content">
                <h5 className="title">Anda belum melakukan pembayaran tagihan</h5>
                <span className="desc">
                  Agar layanan dapat digunakan kembali, silahkan melakukan pembayaran {" "}
                </span>
              </div>
            </Link>

          </>
            :
            <section className="card-skeleton">
              <figure className="card-image loading"></figure>
            </section>
            }
          </div>

          {userData && (<section className="animated fadeInUp delayp4">
            {
              cardsArray && cardsArray.length > 0
            ?
            <div className="subheading">
              <p>{generalText.upcoming}</p>
            </div>
            :
            ((cardsArray && (<div className="subheading animated fadeInUp delayp6"> <Skeleton width={150} /> </div>)) || (null))
            }

            {
              cardsArray && cardsArray.length > 0
              ?
              (<Slider {...config.carousel2item}>
                {cardsArray}
              </Slider>)
              :
              ((cardsArray && (<Slider {...config.carousel3rd}>
                <div className={"carousel-item animated fadeInUp delayp1"}>
                        <section className="card-skeleton">
                          <figure className="card-image loading"></figure>
                        </section>
                      </div>
                </Slider>)) || (null))
            }
          </section>)}
          <section className="">
            {this.props.load ?
              <>
                <div className="subheading">
                  <p className="animated fadeInUp delayp6">{generalText.recommended}</p>
                </div>
                <Slider {...config.carousel3rd}>
                  {recommend.map((value, index) => {
                    let sumDelay = index + 1;
                    return (
                      <div key={index} className="carousel-item">
                        <CardContent
                          link="#"
                          img={value}
                          className={"subheading animated fadeInUp delayp" + sumDelay}
                          images={images}
                        />
                      </div>
                    )
                  })}
                </Slider>
              </>
              :
              <>
                <div className="subheading animated fadeInUp delayp6">
                  <Skeleton width={150} />
                </div>
                <Slider {...config.carousel3rd}>
                  {recommend.map((value, index) => {
                    let sumDelay = index + 1;
                    return (
                      <div className={"carousel-item animated fadeInUp delayp" + sumDelay} key={index}>
                        <section className="card-skeleton">
                          <figure className="card-image loading"></figure>
                        </section>
                      </div>
                    )
                  })}
                </Slider>
              </>
            }
          </section>
          <section>
            {this.props.load ?
              <>
                <div className="subheading">
                  <p className={"animated " + effect + " delayp1"}>{generalText.newOffer}</p>
                </div>
                <Slider {...config.carousel4th}>
                  {offering.map((value, index) => {
                    let sumDelay = index + 1;
                    return (
                      <div className="carousel-item" key={index}>
                        <CardContentStacked
                          link="/shop/internet/package/details"
                          className={"animated " + effect + " delayp" + sumDelay}
                          img={value}
                          mainText="Save 30% by subscribing to 2 minipacks for 1 year!"
                          images={images}
                        />
                      </div>
                    )
                  })}
                </Slider>
              </>
              :
              <>
                <div className="subheading">
                  <Skeleton width={150} />
                </div>
                <Slider {...config.carousel4th}>
                  {offering.map((value, index) => {
                    let sumDelay = index + 1;
                    return (
                      <div className="carousel-item" key={index}>
                        <section className={"card-skeleton animated fadeInUp delayp" + sumDelay}>
                          <figure className="card-image loading"></figure>
                        </section>
                      </div>
                    )
                  })}
                </Slider>
              </>
            }
          </section>
        </div>
        {/* Modal Refunds */}
        <Modal show={modalRefunds} onHide={() => this.modal('close')}>
          <div className="modal-body p-box">
            <Link to="#" className="close animated fadeInUp" onClick={() => this.modal('close')}>
              <span><i className="far fa-times text-primary"></i></span>
            </Link>
            <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
              <h3>Pengembalian Dana</h3>
            </div>
            <div className="subheading animated fadeInUp delayp2">
              <p>Keterangan</p>
            </div>
            <div className="box box-sm mb-5 animated fadeInUp delayp3">
              <div className="list-group list-group-xs list-group-transparent mb-0">
                <div className="list-group-item pl-0 pr-0">
                  <div className="list-group-item-content">
                    <span className="caption">Order ID</span>
                    <h5 className="title">#12345</h5>
                    <span className="desc text-primary font-weight-bold">
                      Lihat detail pesanan
                      </span>
                  </div>
                </div>
                <div className="list-group-item pl-0 pr-0">
                  <div className="list-group-item-content">
                    <span className="caption">Alasan Pengembalian</span>
                    <span className="desc">
                      Pemasangan tidak dapat dilakukan pada tempat tinggal/kantor yang ditemukan karena berada di luar jaringan
                        </span>
                  </div>
                </div>
              </div>
            </div>
            <Form className="'animated fadeInUp delayp4">
              <Form.Group>
                <Form.Label>Bank Penerima</Form.Label>
                <Form.Control as="select"
                  className="custom-select"
                  id="bank"
                  onChange={this.handleChange}
                >
                  <option value="">Pilih Bank</option>
                  <option value="1">BCA</option>
                  <option value="2">BNI</option>
                  <option value="3">Bank Mandiri</option>
                  <option value="4">CIMB Niaga</option>
                  <option value="5">OCBP NISP</option>
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>No. Rekening</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="No. Rekening"
                  id="noRek"
                  value={noRek}
                  onChange={this.handleChange}
                />
                <small className="help-block">Pengembalian dana akan dikirim ke nomor rekening ini. Pastikan nomor rekening Anda sudah benar.</small>
              </Form.Group>
              <div className="btn-placeholder animated fadeInUp delayp5">
                <button className={"btn btn-primary btn-block " + proses}>Proses</button>
              </div>
            </Form>
          </div>
        </Modal>
        {/* Modal Refunds Detail*/}
        <Modal show={modalRefundsDetail} onHide={() => this.modal('close')}>
          <div className="modal-body p-box">
            <Link to="#" className="close animated fadeInUp" onClick={() => this.modal('close')}>
              <span><i className="far fa-times text-primary"></i></span>
            </Link>
            <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
              <h3>Pengembalian Dana</h3>
            </div>
            <div className="subheading animated fadeInUp delayp2">
              <p>Detil Pengembalian Dana</p>
            </div>
            <div className="box box-sm box-w-text mb-4 animated fadeInUp delayp3">
              <div className="form-group">
                <label>Status</label>
                {/* <h3>Dalam Proses</h3> */}
                <h3 className="active">Selesai diroses</h3>
              </div>
              <div className="form-row">
                <div className="col-6">
                  <div className="form-group">
                    <label>Bank Penerima</label>
                    <p>BCA</p>
                  </div>
                </div>
                <div className="col-6">
                  <div className="form-group">
                    <label>No Rekening</label>
                    <p>21239123</p>
                  </div>
                </div>
              </div>

            </div>
            <div className="subheading">
              <p>Keterangan</p>
            </div>
            <div className="box box-sm mb-5 animated fadeInUp delayp3">
              <div className="list-group list-group-xs list-group-transparent mb-0">
                <div className="list-group-item pl-0 pr-0">
                  <div className="list-group-item-content">
                    <span className="caption">Order ID</span>
                    <h5 className="title">#12345</h5>
                    <span className="desc text-primary font-weight-bold">
                      Lihat detail pesanan
                      </span>
                  </div>
                </div>
                <div className="list-group-item pl-0 pr-0">
                  <div className="list-group-item-content">
                    <span className="caption">Alasan Pengembalian</span>
                    <span className="desc">
                      Pemasangan tidak dapat dilakukan pada tempat tinggal/kantor yang ditemukan karena berada di luar jaringan
                        </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="btn-placeholder mt-0">
              <button className="btn btn-primary btn-block">Sudah Diterima</button>
            </div>
            <div className="text-center">
              <p className="help-block mb-0">Perlu Bantuan?</p>
              <Link to="/help" className="btn btn-link">Hubungi Kami</Link>
            </div>
          </div>
        </Modal>
        {/* Modal Alert */}
        <Modal show={modalAlert} onHide={() => this.modal("alert")}>
          <div className="modal-body p-box-popup pb-0">
            <div className="box box-main mb-0">
              <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                <img className="header-img" src={images.common['ic_header_failed.png']} alt="Icon Success" />
              </header>
              <section className="">
                <h1>Sorry you can't reschedule anymore</h1>
                <p>You have exceeded reschedule installation limit. Please contact our customer service for further information</p>
              </section>
              <div className="btn-placeholder bottom">
                <Link to="#" className="btn btn-primary btn-block mb-3" onClick={() => this.modal("close")}>Close</Link>
                <Link to="/installation/reschedule/success" className="btn btn-transparent btn-block">Contact Us</Link>
              </div>
            </div>
          </div>
        </Modal>
        {/* Modal Reschedule */}
        <Modal show={modalReschedule} onHide={() => this.modal("reschedule")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <header className="header-light white w-l-icon icon-center">
                <img className="header-img" src={images.common['ic_header_register_new.png']} alt="Icon Success" />
              </header>
              <section className="">
                <h1>Confirm installation appointment</h1>
                <p>Our technician will visit you on</p>
                <div className="text-box mb-0">
                  <h5 className="title">Tuesday, 23 July 2019</h5>
                  <p className="desc">Estimated arrival time 07:00</p>
                </div>
                <div className="text-center">
                  <Link to="#" className="btn btn-link" onClick={() => this.modal("addContact")}>Add Secondary Contact</Link>
                </div>
              </section>
              <div className="btn-placeholder inline bottom">
                <Link to="#" className="btn btn-transparent btn-block" onClick={() => this.modal("setReschedule")}>Reschedule</Link>
                <Link to="#" className="btn btn-primary btn-block" onClick={() => this.modal("reschedule")}>Confirm</Link>
              </div>
            </div>
          </div>
        </Modal>
        {/* Modal Contact */}
        <Modal show={modalContact} onHide={() => this.modal("addContact")} centered={true}>
          <div className="modal-body p-box pb-0">
            <div className="heading pt-sm-down-4">
              <h3>Add a second contact person</h3>
              <p>Our technician will contact this person if you're not available</p>
            </div>
            <section className="mb-4">
              <Form>
                <Form.Group>
                  <Form.Label>Full Name</Form.Label>
                  <Form.Control
                    type="text"
                    id="name"
                    placeholder="Contact's full name"
                    value={name}
                    onChange={this.handleChange}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Mobile Number</Form.Label>
                  <Form.Control
                    type="number"
                    id="mobileNumber"
                    placeholder="Enter mobile number"
                    value={mobileNumber}
                    onChange={this.handleChange}
                  />
                </Form.Group>
                <div className="btn-placeholder inline">
                  <Link to="#" className="btn btn-transparent btn-block" onClick={() => this.modal("addContact")}>Cancel</Link>
                  <Link to="#" className={"btn btn-primary btn-block " + disabled} onClick={() => this.modal("confirmAddContact")}>Save</Link>
                </div>
              </Form>
            </section>
          </div>
        </Modal>
        {/* Modal Set Reschedule */}
        <Modal show={modalSetReschedule} onHide={() => this.modal("setReschedule")} centered={true}>
          <div className="modal-body p-box pb-0">
            <div className="heading pt-sm-down-4">
              <h3>Reschedule Installation</h3>
              <p>Select a new time and date. You only have 2 chance to reschedule your installation</p>
            </div>
            <section className="mb-4">
              <div className="subheading mb-3">
                <p>Set Schedule</p>
              </div>
              <Slider {...config.carouselDate}>
                <div className="carousel-item-sm" onClick={() => this.setSchedule(0)}>
                  <div className="badges badge-date active selector">
                    <small>Tue</small>
                    <h3>23</h3>
                    <span>Jul</span>
                  </div>
                </div>
                <div className="carousel-item-sm" onClick={() => this.setSchedule(1)}>
                  <div className="badges badge-date selector">
                    <small>Wed</small>
                    <h3>24</h3>
                    <span>Jul</span>
                  </div>
                </div>
                <div className="carousel-item-sm" onClick={() => this.setSchedule(2)}>
                  <div className="badges badge-date selector">
                    <small>Thu</small>
                    <h3>25</h3>
                    <span>Jul</span>
                  </div>
                </div>
                <div className="carousel-item-sm" onClick={() => this.setSchedule(3)}>
                  <div className="badges badge-date selector">
                    <small>Fri</small>
                    <h3>26</h3>
                    <span>Jul</span>
                  </div>
                </div>
                <div className="carousel-item-sm" onClick={() => this.setSchedule(4)}>
                  <div className="badges badge-date selector">
                    <small>Thu</small>
                    <h3>1</h3>
                    <span>Aug</span>
                  </div>
                </div>
                <div className="carousel-item-sm" onClick={() => this.setSchedule(5)}>
                  <div className="badges badge-date selector">
                    <small>Fri</small>
                    <h3>2</h3>
                    <span>Aug</span>
                  </div>
                </div>
                <div className="carousel-item-sm" onClick={() => this.setSchedule(6)}>
                  <div className="badges badge-date selector">
                    <small>Sat</small>
                    <h3>3</h3>
                    <span>Aug</span>
                  </div>
                </div>
              </Slider>
              <div className="subheading mb-3">
                <p>Estimated Arrival</p>
              </div>
              <div className="list-group">
                <div className="row row-1 mb-0">
                  <div className="col-6">
                    <div className="list-group-item list-check" onClick={() => this.estimatedArrival("0")}>
                      <div className="list-group-item-content">
                        <h5 className="title">Morning</h5>
                        <span className="desc">Technician will come around 08:00-12:00</span>
                        <div className="custom-control custom-radio d-none">
                          <input type="radio" className="custom-control-input" id="morning" name="radio-stacked" required />
                          <label className="custom-control-label" htmlFor="customControlValidation2">morning</label>
                        </div>
                        {/* <label className="input-check-card">
                                          <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                      </label> */}
                      </div>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="list-group-item list-check" onClick={() => this.estimatedArrival("1")}>
                      <div className="list-group-item-content">
                        <h5 className="title">Afternoon</h5>
                        <span className="desc">Technician will come around 13:00-17:00</span>
                        <div className="custom-control custom-radio d-none">
                          <input type="radio" className="custom-control-input" id="afternoon" name="radio-stacked" required />
                          <label className="custom-control-label" htmlFor="customControlValidation3">afternoon</label>
                        </div>
                        {/* <label className="input-check-card">
                                          <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                      </label> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <p className="help-block">Time range is the Estimated is the estimated arrival time. You will receive a notification when the technician is heading to your location</p>
              <div className="btn-placeholder inline">
                <Link to="#" className="btn btn-transparent btn-block" onClick={() => this.modal("setReschedule")}>Cancel</Link>
                <Link to="#" className="btn btn-primary btn-block" onClick={() => this.modal("confirmReschedule")}>Reschedule</Link>
              </div>
            </section>
          </div>
        </Modal>
        {/* Modal Confirm Reschedule */}
        <Modal show={modalConfirmReschedule} onHide={() => this.modal("confirmReschedule")} centered={true}>
          <div className="modal-body p-box pb-0">
            <div className="heading text-center pt-sm-down-4">
              <h3>Confirm Reschedule</h3>
              <p>Please confirm that you want to reschedule installation to</p>
            </div>
            <section className="text-center mb-4">
              <div className="text-box mb-3">
                <h5 className="title">Tuesday, 23 July 2019</h5>
                <p className="desc">Estimated arrival time 07:00</p>
              </div>
              <p>You only have 2 chances to reschedule your installation</p>
              <div className="btn-placeholder inline">
                <Link to="#" className="btn btn-transparent btn-block" onClick={() => this.modal("confirmReschedule")}>Cancel</Link>
                <Link to="/installation/reschedule/success" className="btn btn-primary btn-block">Confirm</Link>
              </div>
            </section>
          </div>
        </Modal>
        {/* Modal Reschedule */}

        {/* Modal Eearly Visit */}
        <Modal show={modalEarlyVisit} onHide={() => this.modal("earlyVisitNo")}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <header className="header-light white w-l-icon icon-center">
                <img className="header-img" src={images.common['ic_header_register_new.png']} alt="Icon Success" />
              </header>
              <section className="">
                <h1>Are you available for an earlier installation?</h1>
                <p>Our technician can come on</p>
                <div className="text-box mb-0">
                  <h5 className="title">Tuesday, 23 July 2019</h5>
                  <p className="desc">Estimated arrival time 07:00</p>
                </div>
              </section>
              <div className="btn-placeholder inline bottom">
                <Link to="#" className="btn btn-transparent btn-block" onClick={() => this.modal("earlyVisitNo")}>No</Link>
                <Link to="#" className="btn btn-primary btn-block" onClick={() => this.modal("confirmEarlyvisit")}>Yes</Link>
              </div>
            </div>
          </div>
        </Modal>
        {/* Modal Eearly Visit Confirm */}
        <Modal show={modalConfirmEarlyVisit} onHide={() => this.modal("confirmEarlyvisit")}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <div className="heading text-center pt-sm-down-4">
                <h3>Confirm Reschedule</h3>
                <p>Please confirm that you want to reschedule installation to</p>
              </div>
              <section className="">
                <h1>Are you available for an earlier installation?</h1>
                <p>Our technician can come on</p>
                <div className="text-box mb-0">
                  <h5 className="title">Tuesday, 23 July 2019</h5>
                  <p className="desc">Estimated arrival time 07:00</p>
                </div>
              </section>
              <div className="btn-placeholder inline bottom">
                <Link to="#" className="btn btn-transparent btn-block" onClick={() => this.modal("confirmEarlyvisit")}>cancel</Link>
                <Link to="#" className="btn btn-primary btn-block" onClick={() => this.modal("earlyVisitYes")}>Confirm</Link>
              </div>
            </div>
          </div>
        </Modal>
        <div className={"card card-notif " + notifView}>
          <p className="notif-text">Siti Hasana (08123456789) has been added as your second contact person</p>
        </div>
        <div className={"card card-notif " + notifViewEarly.no}>
          <p className="notif-text">Your technician will come as scheduled on Tuesday, 23 July 2019 between 12:00-17:00</p>
        </div>
        <div className={"card card-notif " + notifViewEarly.yes}>
          <p className="notif-text">Your installation schedule has been updated to Tuesday, 23 July 2019 at 07:00</p>
        </div>
      </div>
    );
  }
};

const SubscribeWatchSection = ({ images, load, isVisible, generalText }) => {
  const config = {
    carousel2nd: {
      className: "primary-carousel view-carousel",
      dots: false,
      infinite: false,
      arrows: false,
      speed: 500,
      slidesToShow: 2,
      slidesToScroll: 2,
      swipe: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: false,
            swipe: false,
            dots: false
          }
        },
        {
          breakpoint: 736,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: false,
            dots: false,
            swipe: true
          }
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 2.2,
            slidesToScroll: 2.2,
            infinite: false,
            swipe: true
          }
        }
      ]
    }
  };
  const movieBanner = ["poster1.png", "poster2.png", "poster3.png", "poster4.png", "poster5.png"];
  const effect = isVisible ? "fadeInUp" : "";
  return (
    <div className="home-subscribe-watch bg-white py-main animated fadeInUp delayp7">
      <div className="container">
        <div className="row">
          {load ?
            <div className="col-md-6 content-center-left">
              <div className="subheading">
                <p className={"animated " + effect + " delayp1"}>{generalText.movieSubcribe.movieSubcribeCaption}</p>
              </div>
              <div className={"d-none-mobile animated " + effect + " delayp2"}>
                <ul
                  className="icon-list mb-3"
                >
                  <li className="icon-list-item">
                    <img
                      alt=""
                      src={images.common["logo_iflix.jpg"]}
                      className="img-fluid w-75px"
                    />
                  </li>
                  <li className="icon-list-item">
                    <img
                      alt=""
                      src={images.common["logo_hooq.jpg"]}
                      className="img-fluid w-100px"
                    />
                  </li>
                  <li className="icon-list-item">
                    <img
                      alt=""
                      src={images.common["logo_catchplay.jpg"]}
                      className="img-fluid w-125px"
                    />
                  </li>
                </ul>
                <div className="heading">
                  <h3 className={"animated " + effect + " delayp3"}>
                  {generalText.movieSubcribe.movieSubcribeTitle}
                </h3>
                  <p className="">
                  {generalText.movieSubcribe.movieSubcribeDesc}
                  </p>
                </div>
                <div
                  className={"btn-placeholder animated " + effect + " delayp4"}
                >
                  <Link to="/shop/tv/apps" className="btn btn-primary">Subscribe</Link>
                </div>
              </div>
            </div>
            :
            <div className="col-md-6 content-center-left">
              <Skeleton width={150} />
              <br />
              <Skeleton height={50} />
              <br />
              <Skeleton width={250} />
              <br />
              <Skeleton width={250} />
              <br />
              <Skeleton width={150} />
            </div>
          }

          <div className="col-md-6">
            {load ?
              <Slider {...config.carousel2nd}>
                {movieBanner.map((value, index) => {
                  let sumDelay = index + 1;
                  return (
                    <div className="carousel-item" key={index}>
                      <CardContent
                        link="#"
                        img={value}
                        images={images}
                        className={"animated " + effect + " delayp" + sumDelay}
                      />
                    </div>
                  )
                })}
              </Slider>
              :
              <Slider {...config.carousel2nd}>
                {movieBanner.map((value, index) => {
                  let sumDelay = index + 1;
                  return (
                    <div className="carousel-item" key={index}>
                      <section className={"card-skeleton animated fadeInUp delayp" + sumDelay}>
                        <figure className="card-image loading"></figure>
                      </section>
                    </div>
                  )
                })}
              </Slider>
            }
          </div>
          <div className="col-md-12">
            <div className="btn-placeholder d-block-mobile mb-3">
              <button className="btn btn-primary btn-block">Subscribe</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
