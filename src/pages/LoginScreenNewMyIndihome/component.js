import React, { useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';

const LoginScreenNewMyIndihome = () => {
    const { trigger, navCase, pageCase, brand, sample, common } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    const images = { brand, sample, common };
    return (
      <TextContext.Consumer>{(context)=>{
        const { loginText } = context;
        return (
          <section>
              <div className="container container-xs">
              <div className="box box-main animated fadeInUp">
                  <header className="header-light white icon-center animated fadeInUp delayp1">
                  <div className="header-actions">
                      <Link to="/register/otp" className="btn btn-link"><i className="fal fa-times" /></Link>
                  </div>
                  <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
                  </header>
                  <section className="animated fadeInUp delayp2">
                    <h1>{loginText.loginScreenNewMyIn.title}</h1>
                    <p>{loginText.loginScreenNewMyIn.subtitle}</p>
                  </section>
                  <div className="btn-placeholder bottom animated fadeInUp delayp3">
                  <Link to="/login:" className="btn btn-primary btn-block">{loginText.loginScreenNewMyIn.btnStart}</Link>
                  </div>
              </div>
              </div>
          </section>
        )
      }}
      </TextContext.Consumer>
    )
}

export default LoginScreenNewMyIndihome;