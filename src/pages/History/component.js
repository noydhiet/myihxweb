import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Modal, Form } from 'react-bootstrap';
import Slider from "react-slick";

class History extends Component {
  state = {
    modalFilter: false
  }

  triggerPageHistory = (mode) => {
    switch(mode) {
      case "modalFilter":
        this.setState({
          modalFilter: !this.state.modalFilter
        });
        break;

      case "close":
        this.setState({
          modalFilter: false
        });
        break;

      default:
        return null
    }
  }
  render() {
    const { billActive, purchaseActive, activityActive } = this.props;
    const config = {
      carouselTab: {
        className: "tab-list",
        dots: false,
        infinite: false,
        arrows: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
          {
            breakpoint: 991,
            settings: {
              dots: false,
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 575,
            settings: {
              dots: false,
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 324,
            settings: {
              dots: false,
              slidesToShow: 3,
              slidesToScroll: 3
            }
          }
        ]
      }
    };
    const headerConfig = billActive === "active" ? 
          <div className="header-actions show-sm" style={{display: "flex important"}}>
            <Link to="/shop/" className="btn btn-link">
              <i className="fa fa-arrow-left"></i>
            </Link>
            <Link to="#" className="btn btn-link" onClick={()=>this.triggerPageHistory("modalFilter")}>
              <i className="far fa-filter"></i>
            </Link>
          </div>
          :
          <div className="header-actions d-block d-md-none" style={{display: "flex important"}}>
            <Link to="/shop/" className="btn btn-link">
              <i className="fa fa-arrow-left"></i>
            </Link>
          </div>
    return (
      <div>
        <div className="cover cover-sm cover-bg-white-sm bg-red-gradient">
          {headerConfig}
          <div className="container">
            <nav aria-label="breadcrumb" className="d-none d-md-block">
              <ol className="breadcrumb animated fadeInUp">
                <li className="breadcrumb-item">
                  <Link to="/home">
                    <i className="fa fa-arrow-left mr-1"></i>Home
                  </Link>
                </li>
              </ol>
            </nav>
            <div className="cover-content">
              <h1 className="cover-title">History</h1>
              {/* <p className="cover-text animated fadeInUp delayp2">Mulai berlangganan paket internet, TV dan telepon mulai dari <strong> Rp150.000 </strong> per bulan <sup>*</sup></p> */}
            </div>
          </div>
        </div>
        <section className="shop-tab-list d-md-none">
          <div className="container">
            <Slider {...config.carouselTab}>
              <div className={"tab-item " + billActive}>
                <Link to="/history/bill">Bill</Link>
              </div>
              <div className={"tab-item " + purchaseActive}>
                <Link to="/history/purchase">Purchase</Link>
              </div>
              <div className={"tab-item " + activityActive}>
                <Link to="/history/activity">Activity</Link>
              </div>
            </Slider>
          </div>
        </section>

        {/* Start - Modal Filter History */}
        <Modal show={this.state.modalFilter} onHide={()=>this.triggerPageHistory("close")}>
          <div className="modal-body p-box">
            <Link to="#" className="close animated fadeInUp" onClick={()=>this.triggerPageHistory("close")}>
                <span><i className="far fa-times text-primary"></i></span>
            </Link>
            <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
                <h3>Filter History</h3>
            </div>
            <section className="animated fadeInUp delayp2">
              <Form>
                <Form.Group>
                  <Form.Label>From</Form.Label>
                  <Form.Control as="select" 
                    className="custom-select"
                    name="coverageDistrict"
                  >
                    <option>January 2019</option>
                    <option>Month 2019</option>
                    <option>Month 2019</option>
                    <option>Month 2019</option>
                    <option>Month 2019</option>
                    <option>Month 2019</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Label>To</Form.Label>
                  <Form.Control as="select" 
                    className="custom-select"
                    name="coverageDistrict"
                  >
                    <option>Select Month</option>
                    <option>Month 2019</option>
                    <option>Month 2019</option>
                    <option>Month 2019</option>
                    <option>Month 2019</option>
                    <option>Month 2019</option>
                  </Form.Control>
                </Form.Group>
                <div className="btn-placeholder">
                  <button className="btn btn-primary btn-block">Continue</button>
                </div>
              </Form>
            </section>
          </div>
        </Modal>
        {/* End - Modal Filter History */}
      </div>
    );
  }
}

export default History;
