import React, { useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { PageContext } from './../../context/PageContext';
const ByNeedsFinish = () => {
  const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
  const images = { brand, common, sample } ;
  useEffect(()=>{
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  return (
    <section>
      <div className="container">
        <div className="box box-invisible animated fadeInUp">
          <header>
            {/* <div className="header-actions d-block d-md-none">
                <Link
                  to="/shop/internet/package-finder/needs/3"
                  className="btn btn-link"
                >
                  <i className="fa fa-arrow-left"></i>
                </Link>
              </div> */}
            <div className="heading animated fadeInUp delayp1 text-center pt-sm-down-4">
              <h1>Our Recommendations?</h1>
              <p>We think these packages will be great for you!</p>
            </div>
          </header>
          <section className="section-header-card py-main">
            <div className="row">
              <div className="col-lg-4">
                <Link
                  to="/shop/internet/package/details"
                  className="card card-packages card-packages-lg animated fadeInUp delayp2"
                >
                  <div className="card-header">
                    <div className="badges-list">
                      <div className="badges badge-red left">
                        <h3>10</h3>
                        <span>Mbps</span>
                      </div>
                      <div className="badges badge-green middle">
                        <h3>92</h3>
                        <span>Channels</span>
                      </div>
                      <div className="badges badge-blue right">
                        <h3>100</h3>
                        <span>Minutes</span>
                      </div>
                    </div>
                    <ul className="list-unstyled">
                      <li>Internet unlimited</li>
                      <li>203 USee TV channels</li>
                      <li>1000 minutes call</li>
                    </ul>
                  </div>
                  <div className="card-body w-btn">
                    <div className="card-body-top">
                      <div className="subheading">
                        <p>Triple Play Value</p>
                      </div>
                      <h3>
                        Rp180.000<small> / bln</small>
                      </h3>
                      <div className="content">
                        <p>
                          Perfect for everyday internet users with light daily
                          use
                          </p>
                      </div>
                    </div>
                    <div className="card-body-bottom">
                      <div className="subheading">
                        <p>Bonus Subscriptions</p>
                      </div>
                      <div className="content">
                        <p>
                          Get bonus subscriptions from various apps and games!
                          </p>
                      </div>
                      <img
                        src={images.common["logo_add-ons.jpg"]}
                        className="img-fluid"
                        alt="Logo Add-Ons"
                      />
                    </div>
                    <span className="btn btn-primary">Details</span>
                  </div>
                </Link>
              </div>
              <div className="col-lg-4">
                <Link
                  to="/shop/internet/package/details"
                  className="card card-packages card-packages-lg animated fadeInUp delayp3"
                >
                  <div className="card-header">
                    <div className="badges-list">
                      <div className="badges badge-red left">
                        <h3>20</h3>
                        <span>Mbps</span>
                      </div>
                      <div className="badges badge-green middle">
                        <h3>92</h3>
                        <span>Channels</span>
                      </div>
                      <div className="badges badge-blue right">
                        <h3>100</h3>
                        <span>Minutes</span>
                      </div>
                    </div>
                    <ul className="list-unstyled">
                      <li>Internet unlimited</li>
                      <li>203 USee TV channels</li>
                      <li>1000 minutes call</li>
                    </ul>
                  </div>
                  <div className="card-body w-btn">
                    <div className="card-body-top">
                      <div className="subheading">
                        <p>IndiHome Gamer</p>
                      </div>
                      <h3>
                        Rp180.000<small> / bln</small>
                      </h3>
                      <div className="content">
                        <p>
                          Paket dengan berbagai keuntungan di game favorit
                          Anda!
                          </p>
                      </div>
                    </div>
                    <div className="card-body-bottom">
                      <div className="subheading">
                        <p>Bonus Subscriptions</p>
                      </div>
                      <div className="content">
                        <p>
                          Get bonus subscriptions from various apps and games!
                          </p>
                      </div>
                      <img
                        src={images.common["logo_games.jpg"]}
                        className="img-fluid"
                        alt="Logo Games"
                      />
                    </div>
                    <span className="btn btn-primary">Details</span>
                  </div>
                </Link>
              </div>
              <div className="col-lg-4">
                <Link
                  to="/shop/internet/package/details"
                  className="card card-packages card-packages-lg animated fadeInUp delayp4"
                >
                  <div className="card-header">
                    <div className="badges-list">
                      <div className="badges badge-red left">
                        <h3>50</h3>
                        <span>Mbps</span>
                      </div>
                      <div className="badges badge-blue right">
                        <h3>201</h3>
                        <span>Channels</span>
                      </div>
                      <div className="badges badge-green middle">
                        <h3>1000</h3>
                        <span>Minutes</span>
                      </div>
                    </div>
                    <ul className="list-unstyled">
                      <li>Internet unlimited</li>
                      <li>203 USee TV channels</li>
                      <li>1000 minutes call</li>
                    </ul>
                  </div>
                  <div className="card-body w-btn">
                    <div className="card-body-top">
                      <div className="subheading">
                        <p>IndiHome Premium</p>
                      </div>
                      <h3>
                        Rp1.225.000<small> / bln</small>
                      </h3>
                      <div className="content">
                        <p>
                          Perfect for everyday internet users with light daily
                          use
                          </p>
                      </div>
                    </div>
                    <div className="card-body-bottom">
                      <div className="subheading">
                        <p>Bonus Subscriptions</p>
                      </div>
                      <div className="content">
                        <p>
                          Get bonus subscriptions from various apps and games!
                          </p>
                      </div>
                      <img
                        src={images.common["logo_add-ons.jpg"]}
                        className="img-fluid"
                        alt="Logo Add-Ons"
                      />
                    </div>
                    <span className="btn btn-primary">Details</span>
                  </div>
                </Link>
              </div>
            </div>
          </section>
          <section className="text-center animated fadeInUp delayp5 pt-3">
            <p className="mb-0">Belum menemukan yang Anda cari?</p>
            <Link
              to="/shop/internet/package/all"
              className="btn btn-link btn-lg"
            >
              Lihat Semua Paket <i className="far fa-angle-right ml-1"></i>
            </Link>
          </section>
        </div>
      </div>
    </section>
  )
}

export default ByNeedsFinish;
