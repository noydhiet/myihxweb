import React from 'react';
import renderer from 'react-test-renderer';
import LoginEmailPassword from '../index';

jest.mock('../../../services', () => ({
  login: ({ email, password }) => new Promise((resolve, reject) => {
    if (email === 'test@telkom.co.id' && password === 'test123') resolve('success');
    else reject('failed');
  }),
}));
jest.mock('react-router-dom');

const instance = props => renderer.create(<LoginEmailPassword images={{ common: {} }} {...props} />).getInstance();

describe('LoginEmailPassword', () => {
  test('render', () => {
    const result = instance().render();
    expect(result.type).toBe(React.Fragment);
  });

  test('handleChange', () => {
    const wrapper = instance();
    const e = { target: { id: 'name', value: 'test' } };
    wrapper.handleChange(e);
    expect(wrapper.state.name).toBe('test');
  });

  test('trigger', () => {
    const wrapper = instance();

    wrapper.trigger();
    expect(wrapper.state.icon).toBe(true);

    wrapper.trigger();
    expect(wrapper.state.icon).toBe(false);
  });

  test('submit', () => {
    const wrapper = instance();
    const e = { preventDefault: jest.fn() };
    wrapper.submit(e);
    expect(e.preventDefault).toHaveBeenCalled();
  });
});
