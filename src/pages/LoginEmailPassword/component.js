import React from 'react';
import { Form, Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ContainerBase from '../../include/ContainerBase';
import { login } from '../../services';
import { setExpireTime, setToken, setUserData } from '../../utils/storage';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';

export default class LoginEmailPassword extends React.Component {
  static contextType = PageContext;
  state = {
    icon: false,
    password: '',
    email: localStorage.getItem('email'),
    alert: '',
    modal: false,
    isSubmitting: false
  }

  componentDidMount() {
    this.context.trigger(1);
    this.context.pageCase("w-bg");
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  trigger = (type) => () => {
    this.setState({
      [type]: !this.state[type]
    })
  }

  submit = (e) => {
    e.preventDefault();

    const { email, password } = this.state;
    this.setState({isSubmitting: true}, () => {
      login({ email, password })
      .then(({ data }) => {
        if (data.ok) {
          const userData = JSON.parse(atob(data.data.token.split('.')[1]));
          userData.userName = data.data.userName;

          setToken(data.data.token);
          setUserData(userData);
          setExpireTime(userData.exp);
          window.location.href = `/${this.pathSuffix()}`
        } else if (data.status === 404) {
          this.trigger('modal')();
        } else {
          this.setState({ alert: 'invalid' });
        }
        this.setState({isSubmitting: false})
      })
      .catch(() => { this.setState({ alert: 'invalid', isSubmitting: false   }); });
    });
    
  }

  pathSuffix = () => {
    const { match: { params } } = this.props;

    if (!params.pathSuffix) return '';
    return params.pathSuffix.replace(/--/g, '/').substring(1);
  }

  render() {
    const { icon, modal, password, alert, isSubmitting } = this.state
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    const config = {
      disabledBtn: !isSubmitting && password ? '' : 'disabled',
      iconPassword: icon ? "fas fa-eye" : "fas fa-eye-slash",
      typePassword: icon ? "text" : "password",
      alert: alert ? 'block' : 'none',
      justifyIcon: alert ? '35%' : '18%'
    }

    return (
      <TextContext.Consumer>{(context)=> {
        const { loginText } = context;
        const containerProps = {
          image: { alt: 'Icon Login Email', src: images.common['ic_header_password.png'], },
          title: loginText.loginEmailPass.title,
        };
        const alertMsg = this.state.alert === "invalid" && loginText.loginEmailPass.alert;
        const Loading = () => {
          return (
            <span><i className="fa fa-circle-notch fa-spin"></i></span>
          )
        }
        return (
          <>
            <ContainerBase {...containerProps}>
              <Form onSubmit={this.submit}>
                <Form.Group className="form-custom w-preppend">
                  <Form.Label> {loginText.loginEmailPass.subtitle} </Form.Label>
                  <Form.Control
                    autoFocus
                    type={config.typePassword}
                    name="password"
                    id="password"
                    value={password}
                    onChange={this.handleChange}
                    placeholder={loginText.loginEmailPass.placeholder}
                  />
                  <div className="invalid-feedback" style={{ display: config.alert }}>{alertMsg}</div>
                  <span onClick={this.trigger('icon')} className="preppend-icon" style={{ bottom: config.justifyIcon }}><i className={config.iconPassword}></i> </span>
                </Form.Group>
                <div className="btn-placeholder-forgot">
                  <Link to="/login/forgot-password" className="btn btn-link animated fadeInUp delayp3">{loginText.loginEmailPass.linkForgetPass}</Link>
                </div>
                <div className="btn-placeholder">
                  <Link to="#" type="submit" className={"btn btn-primary btn-block animated fadeInUp delayp4 " + config.disabledBtn}   onClick={this.submit}>{isSubmitting ? <Loading /> : loginText.loginEmailPass.btnNext}</Link>
                </div>
              </Form>
            </ContainerBase>
            {/* MODAL  */}
            <Modal show={modal} onHide={this.trigger('modal')} className="modal-centered" centered={true}>
              <div className="box box-main mb-0">
                <section>
                  <h1>Account not yet registered</h1>
                  <p>Looks like this account is not yet registered! Do you want to register instead?</p>
                </section>
                <div className="btn-placeholder inline bottom">
                  <button className="btn btn-block w-border" onClick={this.trigger('modal')}> try again </button>
                  <Link to="/register/account-details" className="btn btn-primary btn-block"> register </Link>
                </div>
              </div>
            </Modal>
            {/* MODAL  */}
          </>
        )
      }}</TextContext.Consumer>
    )
  }
}

LoginEmailPassword.defaultProps = {
  pageCase: () => { },
  trigger: () => { },
};
