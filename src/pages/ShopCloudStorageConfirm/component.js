import React, { useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { PageContext } from './../../context/PageContext';
const ShopCloudStorageConfirm = (props) => {
  const { trigger, navCase, pageCase, loggedIn } = useContext(PageContext);
  useEffect(() => {
    loggedIn();
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [loggedIn, trigger, navCase, pageCase]);
  const { match: { params } } = props;
  const elements = params.pathSuffix.split('--', 2);
  const dataAmount = elements[0].replace(':','');;
  const price = elements[1];
  return (
    <section>
      <div className="container container-sm">
        <div className="box animated fadeInUp">
          <header>
            <div className="header-actions">
              <Link to="/shop/more/cloud-storage" className="btn btn-link">
                <i className="fa fa-arrow-left"></i>
              </Link>
            </div>
          </header>
          <section className="section-header-text">
            <div className="heading">
              <h1>Confirm Subscription</h1>
              <p>Confirm subscription to continue</p>
            </div>
          </section>
          <section className="py-main-sm pb-0">
            <div className="subheading">
              <p>Product</p>
            </div>
            <Link
              to="#"
              className="card card-packages w-arrow animated fadeInUp delayp2"
            >
              <div className="card-header">
                <div className="badges-list">
                  <div className="badges badge-gray left">
                    <h3>{dataAmount}</h3>
                    <span>GB</span>
                  </div>
                </div>
                <div className="heading mb-0">
                  <h6 className="title">{dataAmount}GB Cloud Subscription</h6>
                  <p className="mb-0">Cloud Storage monthly subscription.</p>
                </div>
              </div>
              <div className="card-body w-btn">
                <h3>
                  Rp{price}<small> /month</small>
                </h3>
                <p className="help-block mb-0">Billing starts next month</p>
              </div>
            </Link>
          </section>
          <section className="py-main-sm pb-0">
            <div className="subheading">
              <p>Total</p>
            </div>
            <div className="form-group">
              <h4>Rp{price}</h4>
              <small className="help-block">Amount will be charged to your next bill. Includes 15% tax.</small>
            </div>
            <div className="btn-placeholder bottom">
              <div className="row">
                <div className="col-6">
                  <Link to="/shop/more/cloud-storage" className="btn btn-transparent btn-block"> cancel </Link>
                </div>
                <div className="col-6">
                  <Link to="/shop/more/cloud-storage/success" className="btn btn-primary btn-block"> <span> confirm </span> </Link>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
  )
}

export default ShopCloudStorageConfirm;
