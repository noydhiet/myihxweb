import React, { useState } from 'react';
import { Modal, Form } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const ModalView = () => {
  const [checkboxes, setCheckboxes] = useState([]);
  const checkBox = idx => {
    if (checkboxes.includes(idx)) setCheckboxes(checkboxes.filter(i => i !== idx));
    else setCheckboxes([...checkboxes, idx]);
  }

  //const checkboxesLabel = ['Issue does not get resolved on first contact', 'Long wait for handling interferrences', 'Technician are not on time', 'Agents/technicians are not friendly'];
  const checkboxesLabel = ['Issue is resolved on first contact', 'Short wait for handling interferrences', 'Technician are arrive on time', 'Agents/technicians are friendly'];
    return (
        <>
        {/* Start - Modal Download Bill History */}
            <Modal show={true} >
              <div className="modal-body p-box">
                <Link to="#" className="close animated fadeInUp" >
                  <span><i className="far fa-times text-primary"></i></span>
                </Link>
                <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
                  <h3>Please select the reason below</h3>
                </div>
                <section className="animated fadeInUp delayp2 mb-4">
                  <div className="list-group">
                    {checkboxesLabel.map((i, idx) => (
                      <div className="list-group-item w-checkbox" key={idx} onClick={() => checkBox(idx)}>
                        <div className="form-group checkbox">
                          <div className="custom-control custom-checkbox">
                            <input checked={checkboxes.includes(idx)} onChange={() => checkBox(idx)} type="checkbox" className="custom-control-input check" />
                            <label className="custom-control-label"></label>
                          </div>
                        </div>
                        <div className="list-group-item-content" style={{paddingLeft: "1rem"}}>
                          <span className="desc">{i}</span>
                        </div>
                      </div>
                    ))}
                  </div>
                  <Form.Group>
                      <Form.Control
                      placeholder="Other Feedback"
                      />
                  </Form.Group>
                </section>
                <div className="row">
                  <div className="col-12">
                    <button className={"btn btn-primary btn-block" + (checkboxes.length ? '' : ' disabled')} disabled={!checkboxes.length}>Submit</button>
                  </div>
                </div>
              </div>
            </Modal>
            {/* End - Modal Download Bill History */}
        </>
    )
}

export default ModalView;