import React from 'react';
import renderer from 'react-test-renderer';
import PersonalDataReviewKTP from '../index';

jest.mock('react-router-dom');

const instance = props => renderer.create(<PersonalDataReviewKTP images={{ common: {} }} {...props} />).getInstance();

describe('PersonalDataReviewKTP', () => {
  test('render', () => {
    const result = instance().render();
    expect(result.type).toBe('section');
  });

  test('handleChange', () => {
    const wrapper = instance();
    const e = { target: { id: 'name', value: 'test' } };
    wrapper.handleChange(e);
    expect(wrapper.state.name).toBe('test');
  });
});
