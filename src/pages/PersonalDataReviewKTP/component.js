import React, { useState, useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { Form } from "react-bootstrap";
import { PageContext } from './../../context/PageContext';
import { getToken } from '../../utils/storage';

const ReviewKtp = () => {
  const { trigger, navCase, pageCase, imgKtp } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  const [inputs, setInputs] = useState({
    ktpNumber: '',
    motherName: ''
  });
  if (!getToken()) {
    window.location.href = '/login';
    return
  }
  const handleInputChange = (e) => {
    e.persist();
    setInputs(inputs => ({ ...inputs, [e.target.name]: e.target.value }));
  }
  const btnDisabled = inputs.ktpNumber && inputs.motherName ? '' : ' disabled';

  return (
    <section>
      <div className="container container-sm">
        <div className="box box-main animated fadeInUp">
          <header className="header-light white icon-center mb-3 animated fadeInUp delayp1">
            <div className="header-actions">
              <Link to="/personal-data" className="btn btn-link"><i className="fa fa-arrow-left"></i> </Link>
            </div>
            <div className="heading animated fadeInUp delayp1">
              <h1>Review KTP Photo</h1>
              <p>Please make sure your KTP picture is clear and shop complete information</p>
            </div>
            <img className="header-img fluid" src={imgKtp} alt="Icon Success" />
          </header>
          <section className="animated fadeInUp delayp2">
            <Form>
              <Form.Group>
                <Form.Label> Ktp Number </Form.Label>
                <Form.Control
                  type="text"
                  name="ktpNumber"
                  value={inputs.ktpNumber}
                  placeholder="Enter KTP number"
                  onChange={handleInputChange}
                />
              </Form.Group>
            </Form>
            <Form>
              <Form.Group>
                <Form.Label> Mother's Maiden Name </Form.Label>
                <Form.Control
                  type="text"
                  name="motherName"
                  value={inputs.motherName}
                  placeholder="Enter mother's maiden name"
                  onChange={handleInputChange}
                />
                <Form.Text className="text-muted">
                  This Information is needed to make sure uploaded file is yours
                </Form.Text>
              </Form.Group>
            </Form>
          </section>
          <div className="btn-placeholder inline animated fadeInUp delayp3">
            <Link to="/personal-data" className="btn btn-transparent btn-block"> Reupload </Link>
            <Link to="/personal-data" className={"btn btn-primary btn-block" + btnDisabled}> Confirm  </Link>
          </div>
        </div>
      </div>
    </section>
  )
}

export default ReviewKtp

