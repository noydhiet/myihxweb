import React, { Component } from "react";
import { Link } from "react-router-dom";
import ShopInternet from "./../ShopInternet";
import Slider from "react-slick";
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import { getToken } from '../../utils/storage';
import TrackVisibility from "react-on-screen";

class ShopInternetSpeedOnDemand extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    const { common, brand, sample, modal } = this.context;
    const images = { common, brand, sample };
    return (
      <TextContext.Consumer>{(context)=>{
        const { shopText } = context;
        return (
          <div>
            <ShopInternet extraSpeedActive="active" />
            <SectionWithBanner images={images} shopText={shopText}/>
            <TrackVisibility> 
              <SectionPackages images={images} modal={modal} />
            </TrackVisibility>
          </div>
        )
      }}

      </TextContext.Consumer>
    );
  }
}

export default ShopInternetSpeedOnDemand;

const SectionWithBanner = ({ images, shopText }) => {
  return (
    <div className="py-main pb-0 bg-cover-section speed-on-demand content-center">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-8 content-center-left">
            <div className="heading mb-2">
              <h3 className="animated fadeInUp delayp5">{shopText.shopInternetExtraSpeedBanner.title}</h3>
              <p className="animated fadeInUp delayp6">
              {shopText.shopInternetExtraSpeedBanner.desc}
              </p>
            </div>
            <div className="btn-placeholder mt-2 animated fadeInUp delayp7">
              <div className="row d-none d-md-block">
                <div className="col-lg-5">
                  <Link
                    to="/help"
                    className="btn btn-primary"
                  >
                    {shopText.shopInternetExtraSpeedBanner.btn}
                  </Link>
                </div>
              </div>
              <Link
                to="/shop/internet/speed-on-demand"
                className="btn btn-link light d-md-none"
              >
                {shopText.shopInternetExtraSpeedBanner.btn}
              </Link>
            </div>
          </div>
          <div className="col-6 d-none d-md-block content-center">
            <img
              src={images.common["banner_bnr_internet_sod.png"]}
              className="img-fluid animated fadeInUp delayp7 rounded"
              alt="Speed Boost"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

class SectionPackages extends Component {
  render() {
    const urlConfirm = getToken() ? '/shop/internet/speed-on-demand/confirm' : '#';
    const cardPack = new Array(4);
    const carousel = {
      className: "primary-carousel overflow-inherit mb-3",
      dots: false,
      infinite: false,
      arrows: false,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2.2,
            slidesToScroll: 2.2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1.5,
            slidesToScroll: 1.5
          }
        }
      ]
    };
    return (
      <div>
        <div className="py-main bg-gray-50">
          <div className="container">
            {/* <div className="heading text-md-center d-none d-md-block">
            <h2 className="animated fadeInUp delayp1">1 Day Boost</h2>
          </div> */}
            <div className="subheading">
              <p className="animated fadeInUp delayp1">1 Day Boost</p>
            </div>
            <Slider {...carousel}>
              {
                [...cardPack].map((i, idx) => 
                  (
                    <div className="carousel-item">
                    <Link
                      onClick={() => { if(!getToken()) this.props.modal('modalSubscribe') } } 
                      to={urlConfirm}
                      className="card card-packages card-packages-sm animated fadeInUp delayp3"
                    >
                      <div className="card-header">
                        <div className="badges-list">
                          <div className="badges badge-red">
                            <h3>20</h3>
                            <span>Mbps</span>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <h3>
                          Rp40.000<small> / bln</small>
                        </h3>
                        <p>3 x 24 hours</p>
                        <span className="btn btn-primary">Buy</span>
                      </div>
                    </Link>
                  </div>
    
                  ) 
                )
              }
           
            </Slider>
          </div>
        </div>
        <div className="py-main pt-0 bg-gray-50">
          <div className="container">
            {/* <div className="heading text-md-center d-none d-md-block">
            <h2 className="animated fadeInUp delayp1">3 Days Boost</h2>
          </div> */}
            <div className="subheading">
              <p className="animated fadeInUp delayp1">3 Days Boost</p>
            </div>
            <Slider {...carousel}>
              {
                [...cardPack].map((i, idx) => (
                  <div className="carousel-item">
                    <Link
                      onClick={() => { if(!getToken()) this.props.modal('modalSubscribe') } } 
                      to={urlConfirm}
                      className="card card-packages card-packages-sm animated fadeInUp delayp4"
                    >
                      <div className="card-header">
                        <div className="badges-list">
                          <div className="badges badge-red">
                            <h3>20</h3>
                            <span>Mbps</span>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <h3>
                          Rp40.000<small> / bln</small>
                        </h3>
                        <p>3 x 24 hours</p>
                        <span className="btn btn-primary">Buy</span>
                      </div>
                    </Link>
                  </div>
                ))
              }
            </Slider>
          </div>
        </div>
        <div className="py-main pt-0 bg-gray-50">
          <div className="container">
            {/* <div className="heading text-md-center d-none d-md-block">
            <h2 className="animated fadeInUp delayp1">7 Days Boost</h2>
          </div> */}
            <div className="subheading">
              <p className="animated fadeInUp delayp1">7 Days Boost</p>
            </div>
            <Slider {...carousel}>
              {
                [...cardPack].map((i, idx) => (
                  <div className="carousel-item">
                    <Link
                      onClick={() => { if(!getToken()) this.props.modal('modalSubscribe') } } 
                      to={urlConfirm}
                      className="card card-packages card-packages-sm animated fadeInUp delayp4"
                    >
                      <div className="card-header">
                        <div className="badges-list">
                          <div className="badges badge-red">
                            <h3>20</h3>
                            <span>Mbps</span>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <h3>
                          Rp40.000<small> / bln</small>
                        </h3>
                        <p>3 x 24 hours</p>
                        <span className="btn btn-primary">Buy</span>
                      </div>
                    </Link>
                  </div>
                ))
              }
            </Slider>
          </div>
        </div>
      </div>
    );
  }
}

