import React, { Component } from 'react';
import Inbox from '../Inbox';
import { PageContext } from '../../context/PageContext';
import { Link } from 'react-router-dom'

class InboxPersonal extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }

  render() {
    const { common, brand, sample } = this.context;
    const images = { common, brand, sample };

    return (
      <div>
        <Inbox personalPage="active" />
        <SectionContent images={images} />
        {/* <SectionContent1 images={images}/>
        <SectionContent2 images={images}/>
        <SectionContent3 images={images}/> */}
      </div>
    )
  }
}

export default InboxPersonal;

const SectionContent = ({ images }) => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-4 d-none d-md-block">
          <div className="box box-in-cover py-3">
            <div className="list-group list-group-activity mb-0">
              <Link to="/inbox/all" className="list-group-item w-arrow">
                <div className="list-group-item-content">
                  <p className="title mb-0">All</p>
                </div>
              </Link>
              <Link to="/inbox/personal" className="list-group-item w-arrow">
                <div className="list-group-item-content mb-0">
                  <p className="title mb-0 active">Personal</p>
                </div>
              </Link>
              <Link to="/inbox/promo" className="list-group-item w-arrow">
                <div className="list-group-item-content mb-0">
                  <p className="title mb-0">Promo</p>
                </div>
              </Link>
            </div>
          </div>
        </div>
        <div className="col-md-8 col-12">
          <div className="box box-in-cover mb-3 mt-sm-down-4 min-h-auto animated fadeInUp">
            <div className="subheading">
              <p>Active Packages</p>
            </div>
            <Link to="/promo-detail" className="mb-4">
              <div className="list-group list-group-transparent mb-0">
                <div className="list-group-item mb-0">
                  <img alt="" src={images.common["avatar_av_notif_promo.png"]} className="w-50px" />
                  <div className="list-group-item-content">
                    <small className="help-block">21 June 2019, 10:00</small>
                    <p className="mb-0">Most Popular for Family of 4:Unlimited 50Mbps Internet & USee TV</p>
                  </div>
                </div>
              </div>
              <img alt="" src={images.common["img_placeholder_rectangle.jpg"]} className="img-fluid" />
            </Link>
            <Link to="/promo-detail" className="mb-4">
              <div className="list-group list-group-transparent mb-0">
                <div className="list-group-item mb-0">
                  <img alt="" src={images.common["avatar_av_notif_promo.png"]} className="w-50px" />
                  <div className="list-group-item-content">
                    <small className="help-block">21 June 2019, 10:00</small>
                    <p className="mb-0">Most Popular for Family of 4:Unlimited 50Mbps Internet & USee TV</p>
                  </div>
                </div>
              </div>
              <img alt="" src={images.common["img_placeholder_rectangle.jpg"]} className="img-fluid" />
            </Link>
            <Link to="/promo-detail" className="mb-4">
              <div className="list-group list-group-transparent mb-0">
                <div className="list-group-item mb-0">
                  <img alt="" src={images.common["avatar_av_notif_promo.png"]} className="w-50px" />
                  <div className="list-group-item-content">
                    <small className="help-block">21 June 2019, 10:00</small>
                    <p className="mb-0">Most Popular for Family of 4:Unlimited 50Mbps Internet & USee TV</p>
                  </div>
                </div>
              </div>
              <img alt="" src={images.common["img_placeholder_rectangle.jpg"]} className="img-fluid" />
            </Link>
          </div>

          {/* Start - Blank State (Hidden) */}
          <div className="box min-h-auto animated fadeInUp hidden">
            <div className="row">
              <div className="col-md-6 col-12 mx-auto">
                <div className="blank-state">
                  <img alt="" src={images.common["ic_header_failed.png"]} />
                  <div className="heading">
                    <p className="dark">
                      You don't have any active subscription. <br /> Buy your
                      first package to start enjoying IndiHome!
                      </p>
                  </div>
                  <div className="btn-placeholder">
                    <Link
                      to="/shop/internet/package"
                      className="btn btn-primary"
                    >
                      Browse Package
                      </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* End - Blank State */}
        </div>
      </div>
    </div>
  );
};

// const SectionContent1 = ({images}) => {
//     return (
//         <section className="py-main animated fadeInUp delayp4">
//             <div className="container">
//                 <div className="row row-4">
//                     <div className="col-md-6 col-12 content-center">
//                         <div className="list-group list-group-transparent">
//                             <div className="list-group-item">
//                                 <img src={images.common["avatar_av_notif_promo.png"]} className="w-50px"/>
//                                 <div className="list-group-item-content">
//                                     <small className="help-block">21 June 2019, 10:00</small>
//                                     <p className="mb-0">Most Popular for Family of 4:Unlimited 50Mbps Internet & USee TV</p>
//                                 </div>
//                             </div>
//                         </div>
//                     </div>
//                     <div className="col-md-6 col-12">
//                         <img src={images.common["img_placeholder_rectangle.jpg"]} className="img-fluid" />
//                     </div>
//                 </div>
//             </div>
//         </section>
//     )
// }

// const SectionContent2 = ({images}) => {
//     return (
//         <section className="py-main bg-white animated fadeInUp delayp5">
//             <div className="container">
//                 <div className="row row-4">
//                     <div className="col-md-6 col-12 content-center">
//                         <div className="list-group list-group-transparent">
//                             <div className="list-group-item">
//                                 <img src={images.common["avatar_av_notif_promo.png"]} className="w-50px"/>
//                                 <div className="list-group-item-content">
//                                     <small className="help-block">21 June 2019, 10:00</small>
//                                     <p className="mb-0">Watch Gray's Anatomy Season 16, now available on Hooq!</p>
//                                 </div>
//                             </div>
//                         </div>
//                     </div>
//                     <div className="col-md-6 col-12">
//                         <img src={images.common["img_placeholder_rectangle.jpg"]} className="img-fluid" />
//                     </div>
//                 </div>
//             </div>
//         </section>
//     )
// }

// const SectionContent3 = ({images}) => {
//     return (
//         <section className="py-main animated fadeInUp delayp6">
//             <div className="container">
//                 <div className="row row-4">
//                     <div className="col-md-6 col-12 content-center">
//                         <div className="list-group list-group-transparent">
//                             <div className="list-group-item">
//                                 <img src={images.common["avatar_av_notif_promo.png"]} className="w-50px"/>
//                                 <div className="list-group-item-content">
//                                     <small className="help-block">21 June 2019, 10:00</small>
//                                     <p className="mb-0">Most Popular for Family of 4:Unlimited 50Mbps Internet & USee TV</p>
//                                 </div>
//                             </div>
//                         </div>
//                     </div>
//                     <div className="col-md-6 col-12">
//                         <img src={images.common["img_placeholder_rectangle.jpg"]} className="img-fluid" />
//                     </div>
//                 </div>
//             </div>
//         </section>
//     )
// }
