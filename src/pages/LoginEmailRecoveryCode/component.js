import React from 'react';
import ContainerBase from '../../include/ContainerBase';
import { Link } from 'react-router-dom'
import { PageContext } from '../../context/PageContext';
import { verifyPasswordRecoveryCode, forgetPassword } from '../../services'
import { Modal, Form } from 'react-bootstrap';
import querystring from 'querystring';

const TIME_IN_SECONDS = 179

export default class LoginEmailRecoveryCode extends React.Component {
  static contextType = PageContext;
  state = {
    alert: '',
    token: '',
    icon: false,
    modalServiceUnavailable: false,
    isButtonLoading: false,
    timer: TIME_IN_SECONDS,
  }

  tick = () => {
    this.setState( prevState => { return { timer: prevState.timer - 1 } })
  }

  stopTimer = () => {
    clearInterval(this.myInterval);
  }

  resendRecoveryCode = e => {
    e.preventDefault();
    const { openNotif } = this.context;
    this.setState({
      timer: TIME_IN_SECONDS,
    });
    this.myInterval = setInterval(this.tick, 1000);
    const { channel, value } = querystring.parse(window.location.search.replace('?', ''))
    forgetPassword(channel, value)
      .then(({ data }) => {
        if (data.status === 200 || data.status === 201) {
          openNotif('forgetPassword');
        } else {
          console.log(data)
        }
      })
      .catch(({ response }) => { console.log(response)  });
  }

  componentWillUnmount() {
    this.stopTimer();
  }

  componentDidMount() {
    this.context.trigger(1);
    this.context.pageCase("w-bg");
    this.myInterval = setInterval(this.tick, 1000);
  }

  handleChange = ({ target }) => {
    const { id, value } = target;

    this.setState({
      [id]: value,
      alert: value ? '' : 'Please enter the recovery code.'
    });
  }

  submitVerificationCode = () => {
    const { channel, value } = querystring.parse(window.location.search.replace('?', ''))
    const payload = {
      channel: channel,
      value: value,
      recoveryCode: this.state.token
    }
    console.log('submitVerificationCode', payload)
    this.setState({isButtonLoading: true, alert: ''}, () => {
      verifyPasswordRecoveryCode(payload).then( ({ data }) => {
        console.log('data', data)
        if(data.status === 200 || data.status === 201) {
          this.props.history.push({pathname: '/login/email/new-password', state: {token: this.state.token}});
        } else if(data.status === 401) {
          this.setState({alert: 'Password reset token is invalid or has expired.', isButtonLoading: false})
        } else {
          this.setState({modalServiceUnavailable: true, isButtonLoading: false})
        }
      }).catch(error => {
        this.setState({modalServiceUnavailable: true, isButtonLoading: false})
        console.error(error);
      })
    })
  }

  trigger = type => this.setState({ [type]: !this.state[type] })

  render() {
    const { alert, token, icon, isButtonLoading, timer } = this.state;
    const { brand, common, sample, notifForgetPassword, closeNotif } = this.context;
    const images = { brand, common, sample };
    const displayTimer = timer === 0 ? false : true;
    const displayResend = timer === 0 ? true : false;
    timer === 0 && this.stopTimer();
    const minutes = `${Math.floor(timer / 60)}`.padStart(2, '0');
    const seconds = `${timer % 60}`.padStart(2, '0');
    const config = {
      alert: alert ? 'block' : 'none',
      justifyIcon: alert ? '36%' : '16%',
      colorIconPassword: icon ? "#ee3124" : "#4d4d4d",
      disabledBtn: isButtonLoading || !token ? 'disabled' : '',
      iconPassword: icon ? "fas fa-eye" : "fas fa-eye-slash",
      typePassword: icon ? "text" : "password",
      displayTimer: displayTimer ? 'block' : 'none',
      displayResend: displayResend ? 'block' : 'none'
    };
    const containerProps = {
      image: { alt: 'Icon Enter Recovery Code', src: images.common['ic_header_verification.png'], },
      title: 'Enter Recovery Code',
    };
    (notifForgetPassword && closeNotif('forgetPassword'))
    console.log('config.alert', config.alert)
    const Loading = () => {
      return (
        <span><i className="fa fa-circle-notch fa-spin"></i></span>
      )
    }
    return (
      <>
      <ContainerBase {...containerProps}>
        <Form>
          <Form.Group className="form-custom">
            <Form.Label> Recovery Code </Form.Label>
            <Form.Control
              autoFocus
              type={config.typePassword}
              name="token"
              id="token"
              onChange={this.handleChange}
              value={token}
              placeholder="Enter Your Recovery Code / Token"
            />
            <div className="invalid-feedback" style={{ display: config.alert }}>{alert}</div>
            <span onClick={() => this.trigger('icon')} className="preppend-icon" style={{bottom: config.justifyIcon, color: config.colorIconPassword}}><i className={config.iconPassword}></i></span>
          </Form.Group>
          <div className="btn-placeholder">
            <Link to="#" onClick={e => {e.preventDefault(); this.submitVerificationCode()}} className={"btn btn-primary btn-block animated fadeInUp delayp4 " + config.disabledBtn} disabled={config.disabledBtn}>{isButtonLoading ? <Loading /> :<span>Next</span>}</Link>
          </div>
          {/* TODO uncommnent the line below to show the running timer */}
          {/* <div className="countdown-timer text-center animated fadeInUp delayp3">
            <div className="btn btn-link btn-resend-timer disabled" style={{ display: config.displayTimer }}>RESEND CODE IN <span id="timer"> {minutes}:{seconds} </span></div>
            <div className="btn-placeholder btn-resend">
              <button className="btn btn-link" style={{ display: config.displayResend, margin: 'auto' }} onClick={e => this.resendRecoveryCode(e)}>RESEND CODE</button>
            </div>
          </div> */}
        </Form>
      </ContainerBase>
      {/* Modal Session Time Out */}
      <Modal show={this.state.modalServiceUnavailable} onHide={() => this.setState({modalServiceUnavailable: false})} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <section>
                <h1>Service Error</h1>
                <p>A service error has occured while processing your request</p>
              </section>
              <div className="btn-placeholder inline bottom">
                <Link to="#" className="btn btn-primary btn-block" onClick={e => {e.preventDefault(); this.setState({modalServiceUnavailable: false});}}>Ok</Link>
              </div>
            </div>
          </div>
        </Modal>
      </>
    )
  }
}
