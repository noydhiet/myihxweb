import React, { Component, useState } from "react";
import { Link } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import { CardContent } from './../../include/CardPackage'
import Slider from "react-slick";
import ShopTv from './../ShopTv';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import TrackVisibility from "react-on-screen";

class ShopTvMinipack extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    return (
      <TextContext.Consumer>{(context)=>{
        const { shopText, generalText } = context;
        return (
          <div>
            <ShopTv minipackPage="active" />
            <SectionWithBanner images={images} shopText={shopText}/>
            <TrackVisibility once offset={200}> 
              <PopularProductSection images={images} generalText={generalText}/>
            </TrackVisibility>
          </div>
        )
      }}

      </TextContext.Consumer>
    )
  }
}
export default ShopTvMinipack;

const SectionWithBanner = ({ images, shopText }) => {
  return (
    <div className="py-main pb-0 bg-cover-section minipacks content-center">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-8 content-center-left">
            <div className="heading mb-2">
              <h3 className="animated fadeInUp delayp5">{shopText.shopTvMinipackBanner.title}</h3>
              <p className="animated fadeInUp delayp6">
              {shopText.shopTvMinipackBanner.desc}
                </p>
            </div>
            <div className="btn-placeholder mt-2 animated fadeInUp delayp7">
              <div className="row d-none d-md-block">
                <div className="col-lg-5">
                  <Link
                    to="/help"
                    className="btn btn-primary"
                  >
                    {shopText.shopTvMinipackBanner.title}
                    </Link>
                </div>
              </div>
              <Link
                to="/shop/internet/speed-on-demand"
                className="btn btn-link light d-md-none"
              >
                {shopText.shopTvMinipackBanner.btn}
                </Link>
            </div>
          </div>
          <div className="col-6 d-none d-md-block content-center">
            <img
              src={images.common["banner_bnr_tv_minipack.png"]}
              className="img-fluid animated fadeInUp delayp7 rounded"
              alt="Speed Boost"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

const PopularProductSection = ({ images, generalText }) => {
  const [cond, setCond] = useState({
    modalFilter: false,
  });
  const [filter, setFilter] = useState([]);
  const [resFilter, setResFilter] = useState([]);
  const coverStyle = {
    background: `url(${images.common["bnr_blue_2nd.png"]}) no-repeat center`,
    backgroundSize: "cover",
    border: "none"
  };
  const checkBox = (e) => {
    if (filter.includes(e)) {
      setFilter(filter.filter(i => i !== e));
    } else {
      setFilter( [...filter, e] )
    }
  }
  const triggerPage = (mode) => {
    switch (mode) {
      case "openFilter":
        setCond({ modalFilter: true, });
        setFilter(resFilter);
        break;
      case "close":
        setCond({ modalFilter: false });
        setFilter(resFilter);
        break;
      case 'apply':
        setCond({ modalFilter: false, });
        setResFilter(filter);
        break;

      default:
        return null;
    }
  }
  const removeFilter = (value) => {
    const idx = filterText.indexOf(value);
    setResFilter(resFilter.filter(i => i !== idx));
  }
  const config = {
    carousel4th: {
      className: "primary-carousel overflow-inherit",
      dots: false,
      infinite: false,
      arrows: false,
      speed: 500,
      slidesToShow: 6,
      slidesToScroll: 6,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 6,
            slidesToScroll: 6
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4
          }
        }
      ]
    }
  };
  const filterText = ["Movies", "TV Series", "Japanese", "Korean", "Chinese", "Sports", "Entertainment", "Lifestyle", "News", "Kids", "Knowledge", "Local News"]
  const listChannel = [
    {
      link: "#",
      image: { alt: 'Channel Img', src: "channel1.png", },
    },
    {
      link: "#",
      image: { alt: 'Channel Img', src: "channel2.png", },
    },
    {
      link: "#",
      image: { alt: 'Channel Img', src: "channel3.png", },
    },
    {
      link: "#",
      image: { alt: 'Channel Img', src: "channel4.png", },
    },
    {
      link: "#",
      image: { alt: 'Channel Img', src: "channel5.png", },
    },
    {
      link: "#",
      image: { alt: 'Channel Img', src: "channel6.png", },
    },
  ]
  return (
    <div className="py-main bg-gray-50">
      <div className="container">
        <div className="subheading w-links">
          <p>Minipacks</p>
          <Link to="#" className="link font-weight-bold" onClick={() => triggerPage("openFilter")}>
            <i className="far fa-filter"></i>
          </Link>
        </div>
        {/* <div className="row">
                {filterResult.map(function(value, index){
                  return (
                    <div className="col-md-2 col-4" key={index}>
                      <div className="list-group">
                        <div className="list-group-item pd-sm pr-3">
                          <div className="list-group-item-content">
                            <p className="desc">{value}</p>
                          </div>
                          <i className="far fa-times text-primary"></i>
                        </div>
                      </div>
                    </div>
                  )
                })}
            </div> */}
        <ul className="filter-list">
          {filterText.filter((_, idx) => resFilter.includes(idx)).map(function (value, index) {
            return (
              <li key={index} className="filter-list-item">
                <div className="list-group">
                  <div className="list-group-item">
                    <div className="list-group-item-content">
                      <p className="desc">{value}</p>
                    </div>
                    <i className="far fa-times text-primary" onClick={() => removeFilter(value)}></i>
                  </div>
                </div>
              </li>
            )
          })}
        </ul>
        <div className="heading text-md-center d-none d-md-block">
          <h2 className="animated fadeInUp delayp8">{generalText.popularProduct}</h2>
        </div>
        <div className="subheading d-block d-md-none">
          <p className="animated fadeInUp delayp8">{generalText.popularProduct}</p>
        </div>
        <section className="animated fadeInUp delayp9">
          <div className="row">
            <div className="col-lg-4">
              <div
                className="card card-packages card-packages-lg w-list-channel animated fadeInUp delayp2"
              >
                <div className="card-header">
                  <div className="badges-list">
                    <div className="badges badge-blue left">
                      <h3>8</h3>
                      <span>Channels</span>
                    </div>
                    <div className="badges badge-green middle">
                      <h3>92</h3>
                      <span>Channels</span>
                    </div>
                    <div className="badges badge-red right">
                      <h3>100</h3>
                      <span>Mbps</span>
                    </div>
                  </div>
                  <div className="header-text">
                    <h3>Dinasty 1</h3>
                    <p>Chinese channels including Kaku TV, Panda TV, ICN TV, Kungfu TV and more!</p>
                  </div>
                </div>

                <div className="card-body-channel">
                  <Slider {...config.carousel4th}>
                    {listChannel.map(function (value, index) {
                      return (
                        <div className="carousel-item" key={index}>
                          <CardContent
                            link={value.link}
                            img={value.image.src}
                            className="subheading"
                            images={images}
                          />
                        </div>
                      )
                    })}
                  </Slider>
                </div>

                <div className="card-body w-btn">
                  <div className="card-body-top">
                    <div className="subheading">
                      <p>Triple Play Value</p>
                    </div>
                    <h3>
                      Rp169.000<small> / bln</small>
                    </h3>
                    <div className="content">
                      <p>
                        Perfect for everyday internet users with light daily
                        use
                          </p>
                    </div>
                  </div>
                  <div className="card-body-bottom">
                    <div className="subheading">
                      <p>Bonus Subscriptions</p>
                    </div>
                    <div className="content">
                      <p>
                        Get bonus subscriptions from various apps and games!
                          </p>
                    </div>

                    <img
                      src={images.common["logo_add-ons.jpg"]}
                      className="img-fluid"
                      alt="Logo Add-Ons"
                    />
                  </div>
                  <Link to="/shop/tv/minipack/details" className="btn btn-primary">Details</Link>
                </div>
              </div>
            </div>


            <div className="col-lg-4">
              <div
                className="card card-packages card-packages-lg w-list-channel animated fadeInUp delayp3"
              >
                <div className="card-header">
                  <div className="badges-list">
                    <div className="badges badge-blue left">
                      <h3>17</h3>
                      <span>Channels</span>
                    </div>
                    <div className="badges badge-green middle">
                      <h3>100</h3>
                      <span>Minutes</span>
                    </div>
                    <div className="badges badge-red right">
                      <h3>100</h3>
                      <span>Mbps</span>
                    </div>
                  </div>
                  <div className="header-text">
                    <h3>Extra HD</h3>
                    <p>Enjoy hight definition channels from arround the world!</p>
                  </div>
                </div>
                <div className="card-body-channel">
                  <Slider {...config.carousel4th}>
                    {listChannel.map(function (value, index) {
                      return (
                        <div className="carousel-item" key={index}>
                          <CardContent
                            link={value.link}
                            img={value.image.src}
                            className="subheading"
                            images={images}
                          />
                        </div>
                      )
                    })}
                  </Slider>
                </div>
                <div className="card-body w-btn">
                  <div className="card-body-top">
                    <div className="subheading">
                      <p>IndiHome Gamer</p>
                    </div>
                    <h3>
                      Rp535.000<small> / bln</small>
                    </h3>
                    <div className="content">
                      <p>
                        Paket dengan berbagai keuntungan di game favorit
                        Anda!
                          </p>
                    </div>
                  </div>
                  <div className="card-body-bottom">
                    <div className="subheading">
                      <p>Bonus Subscriptions</p>
                    </div>
                    <div className="content">
                      <p>
                        Get bonus subscriptions from various apps and games!
                          </p>
                    </div>
                    <img
                      src={images.common["logo_games.jpg"]}
                      className="img-fluid"
                      alt="Logo Games"
                    />
                  </div>
                  <Link to="/shop/tv/minipack/details" className="btn btn-primary">Details</Link>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div
                className="card card-packages card-packages-lg w-list-channel animated fadeInUp delayp3"
              >
                <div className="card-header">
                  <div className="badges-list">
                    <div className="badges badge-blue left">
                      <h3>32</h3>
                      <span>Channels</span>
                    </div>
                    <div className="badges badge-green middle">
                      <h3>100</h3>
                      <span>Minutes</span>
                    </div>
                    <div className="badges badge-red right">
                      <h3>100</h3>
                      <span>Mbps</span>
                    </div>
                  </div>
                  <div className="header-text">
                    <h3>Extra HD</h3>
                    <p>Enjoy hight definition channels from arround the world!</p>
                  </div>
                </div>
                <div className="card-body-channel">
                  <Slider {...config.carousel4th}>
                    {listChannel.map(function (value, index) {
                      return (
                        <div className="carousel-item" key={index}>
                          <CardContent
                            link={value.link}
                            img={value.image.src}
                            className="subheading"
                            images={images}
                          />
                        </div>
                      )
                    })}
                  </Slider>
                </div>
                <div className="card-body w-btn">
                  <div className="card-body-top">
                    <div className="subheading">
                      <p>IndiHome Gamer</p>
                    </div>
                    <h3>
                      Rp535.000<small> / bln</small>
                    </h3>
                    <div className="content">
                      <p>
                        Paket dengan berbagai keuntungan di game favorit
                        Anda!
                          </p>
                    </div>
                  </div>
                  <div className="card-body-bottom">
                    <div className="subheading">
                      <p>Bonus Subscriptions</p>
                    </div>
                    <div className="content">
                      <p>
                        Get bonus subscriptions from various apps and games!
                          </p>
                    </div>
                    <img
                      src={images.common["logo_games.jpg"]}
                      className="img-fluid"
                      alt="Logo Games"
                    />
                  </div>
                  <Link to="/shop/tv/minipack/details" className="btn btn-primary">Details</Link>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="text-center animated fadeInUp delayp16 pt-3 pb-4">
          <p className="mb-0">Belum menemukan yang Anda cari?</p>
          <Link to="/shop/internet/package/all" className="btn btn-link btn-lg">Lihat Semua Paket <i className="far fa-angle-right ml-1"></i></Link>
        </section>
        <div className="row">
          <div className="col-lg-8 mx-auto">
            <div className="list-group list-group-banner animated fadeInUp delayp17">
              <Link
                to="/shop/internet/package-finder"
                className="list-group-item light w-arrow w-bg-cover"
                style={coverStyle}
              >
                <img alt="grfx_find" src={images.common["grfx_find.png"]} className="w-75px" />
                <div className="list-group-item-content">
                  <h5 className="title w-md-up-75 mb-0">Perlu bantuan memilih paket internet? <br className="d-none d-sm-block" />Coba Package Finder Kami</h5>
                </div>
                {/* <button className="btn btn-light d-none d-md-block">Coba Sekarang</button> */}
              </Link>
            </div>
          </div>
        </div>
      </div>
      {/* Modal - Unlimited Internet Quota */}
      <Modal show={cond.modalFilter} onHide={() => triggerPage('close')}>
        <div className="modal-body p-box">
          <Link className="close animated fadeInUp" to="#" onClick={() => triggerPage('close')}>
            <span><i className="far fa-times text-primary"></i></span>
          </Link>
          <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
            <h3>Filter by content</h3>
          </div>
          <div className="row">
            {filterText.map(function (value, index) {
              const isActive = filter.includes(index);
              return (
                <div className="col-4" key={index} onClick={() => checkBox(index)}>
                  <div className="list-group">
                    <div className={"list-group-item list-check-filter" + (isActive ? ' active' : '')}>
                      <div className="list-group-item-content">
                        <p className="desc">{value}</p>
                      </div>
                      <label className="input-check-card">
                        <input type="checkbox" className="check" checked={isActive} />
                        <span></span>
                      </label>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
          <div className="btn-placeholder mt-0">
            <Link to="#" className="btn btn-primary btn-block" onClick={() => triggerPage('apply')}>Apply Filter</Link>
          </div>
        </div>
      </Modal>
    </div>
  )
}
