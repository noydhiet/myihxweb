import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const ShopInternetUpgradeSpeedConfirm = () => {
  const { trigger, navCase, pageCase, loggedIn } = useContext(PageContext);
  useEffect(() => {
    loggedIn();
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [loggedIn, trigger, navCase, pageCase]);
  return (
    <section>
      <div className="container container-sm">
        <div className="box animated fadeInUp">
          <header>
            <div className="header-actions">
              <Link to="/shop/internet/upgrade-speed" className="btn btn-link">
                <i className="fa fa-arrow-left"></i>
              </Link>
            </div>
          </header>
          <section className="section-header-text">
            <div className="heading">
              <h1>Confirm order</h1>
              <p>Confirm your selection and order</p>
            </div>
          </section>
          <section className="py-main-sm pb-0">
            <div className="subheading">
              <p>Product</p>
            </div>
            <Link
              to="#"
              className="card card-packages w-arrow animated fadeInUp delayp2"
            >
              <div className="card-header">
                <div className="badges-list">
                  <div className="badges badge-red left">
                    <h3>50</h3>
                    <span>Mbps</span>
                  </div>
                </div>
                <div className="heading mb-0">
                  <h6 className="title mb-1">Paket BUMN</h6>
                  <p className="mb-0">Upgrade speed to 50Mbps. Your TV and voice package will not be affected</p>
                </div>
              </div>
              <div className="card-body w-btn">
                <h3>
                  Rp700.000<small>/month</small>
                </h3>
                <p className="help-block mb-0">The pricing will replace your internet package fee</p>
              </div>
            </Link>
          </section>
          <section className="py-main-sm pb-0">
            <div className="form-group">
              <label>Total</label>
              <h4>Rp700.000</h4>
              <small className="help-block">Amount will be charged to your next bill. Includes 15% tax</small>
            </div>
            <div className="btn-placeholder bottom">
              <div className="row">
                <div className="col-6">
                  <Link to="/shop/internet/upgrade-speed" className="btn btn-transparent btn-block">Cancel</Link>
                </div>
                <div className="col-6">
                  <Link to="/shop/internet/upgrade-speed/success" className="btn btn-primary btn-block"> <span>Confirm</span> </Link>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>

  )
}

export default ShopInternetUpgradeSpeedConfirm;