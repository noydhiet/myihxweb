import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Slider from 'react-slick'

class Promo extends Component {
  render() {
    const {promoDetail, tnc} = this.props;
    const config = {
      carouselTab: {
          className: "tab-list",
          dots: false,
          infinite: false,
          arrows: false,
          speed: 500,
          slidesToShow: 2,
          slidesToScroll: 2,
          responsive: [
              {
                breakpoint: 991,
                settings: {
                  dots: false,
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              },
              {
                breakpoint: 575,
                settings: {
                  dots: false,
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              },
              {
                breakpoint: 324,
                settings: {
                  dots: false,
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              }
          ]
      }
    }
    return (
      <div>
        {/* <div className="cover cover-sm cover-bg-white-sm bg-red-gradient">
          <div className="header-actions d-block d-md-none">
            <Link
              to="/shop/"
              className="btn btn-link"
            >
              <i className="fa fa-arrow-left"></i>
            </Link>
          </div>
          <div className="container">
            <nav aria-label="breadcrumb" className="d-none d-md-block">
              <ol className="breadcrumb animated fadeInUp">
                <li className="breadcrumb-item"><Link to="/home"><i className="fa fa-arrow-left mr-1"></i>Home</Link></li>
              </ol>
            </nav>
            <div className="cover-content">
              <h1 className="cover-title animated fadeInUp delayp1">Promo</h1>
            </div>
          </div>
        </div> */}
         <div className="cover cover-responsive bg-wave red">
          <div className="cover-responsive-img">
            {/* <div className="cover-responsive-overlay"></div> */}
          </div>
          <div className="container">
            <div className="cover-content w-pd-sm">
              <h3 className="cover-title animated fadeInUp delayp1">
                Wujudkan Semarak Kebahagiaan
              </h3>
              <p className="cover-text animated fadeInUp delayp2">
                30% off Minipack IndiKids dan IndiSport
              </p>
            </div>
          </div>
        </div> 
        <section className="shop-tab-list animated fadeInUp delayp3">
            <div className="container">
            <Slider {...config.carouselTab}>
                <div className={"tab-item " + promoDetail}> 
                    <Link to="/promo-detail">Promo Detail</Link> 
                </div>
                <div className={"tab-item " + tnc}> 
                    <Link to="/promo-tnc">Terms & Condition</Link> 
                </div>
            </Slider>
            </div>
        </section>
      </div>
    )
  }
}


export default Promo

    


