import React from 'react';
import renderer from 'react-test-renderer';
import InstallationDetailSuccess from '../';
import PageContextProvider from '../../../context/PageContext';

jest.mock('react-router-dom');
jest.mock('../../../utils/requireContext');

const wrapper = props => renderer.create(
  <PageContextProvider>
    <InstallationDetailSuccess {...props} />
  </PageContextProvider>);

describe('InstallationDetailSuccess', () => {
  test('render', () => {
    const result = wrapper().toJSON();
    expect(result.type).toBe('section');
  });
});
