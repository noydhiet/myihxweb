import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const InstallationDetailSuccess = () => {
  const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  const images = { brand, common, sample };
  return (
    <section>
      <div className="container container-sm">
        <div className="box box-main animated fadeInUp">
          <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
            <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
          </header>
          <section className="animated fadeInUp delayp2">
            <h1>Installation is done!</h1>
            <p>Your indiHome is ready! Now you can enjoy fast internet at home</p>
          </section>
          <div className="btn-placeholder bottom animated fadeInUp delayp3">
            <Link to="/help" className="btn btn-primary btn-block">Learn More About IndiHome</Link>
            <Link to="/home" className="btn btn-transparent btn-block">Back To Home</Link>
          </div>
        </div>
      </div>
    </section>
  )
}

export default InstallationDetailSuccess