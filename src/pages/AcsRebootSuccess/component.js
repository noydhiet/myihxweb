import React, { useEffect, useContext } from 'react';
import { PageContext } from './../../context/PageContext';

const AcsRebootSuccess = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = { brand, common, sample } ;
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
            <div className="container container-sm">
                <div className="box box-main animated fadeInUp">
                    <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                    <img className="header-img" src={images.common['grfx_acs.png']} alt="Icon Success" />
                    </header>
                    <section className="animated fadeInUp delayp2">
                        <h1>Reboot Modem</h1>
                        <p>If you’re having issues with your connection, a modem reboot may help. You will be disconnected from all services for a few minutes.</p>
                        <p>If you’re having issues with your connection, a modem reboot may help. You will be disconnected from all services for a few minutes.</p>
                    </section>
                    <div className="btn-placeholder bottom animated fadeInUp delayp3">
                        <button className="btn btn-primary btn-block hidden">Reboot Modem</button>
                        <button className="btn btn-primary btn-block disabled">Rebooting...Please Waiting</button>
                    </div>
                </div>
            </div>
        </section> 
    )
}

export default AcsRebootSuccess;