import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Modal, Form } from "react-bootstrap";
import Slider from "react-slick";
import { PageContext } from './../../context/PageContext';
import { technicianAvailability, scheduleTechnician } from '../../services'
import moment from 'moment';
import { getToken, getUserProfile } from '../../utils/storage';

class InstallationSchedule extends Component {
  static contextType = PageContext;
  constructor(props) {
    super(props);
    this.state = {
      modalContact: false,
      modalSessionRoutes: false,
      status: false,
      name: '',
      mobileNumber: '',
      time: 10 * 60,
      dateSlots: undefined,
      cardMorningAfternoon: [{ show: false, text: '', timeSlot: '' }, { show: false, text: '', timeSlot: '' }],
      morningCheck: false,
      afternoonCheck: false,
      selectedDate: undefined,
      selectedDateIndex: undefined,
      selectedCard: undefined,
      modelForbiddenInstalltion: false,
      isButtonLoading: false,
      modalServiceUnavailable: false
    };
    if(!getToken()) {
      props.history.push('/');
    }
  }

  tick = () => {
    const { time } = this.state;
    if (time === 0) {
      this.stopTimer();
      this.setState({ modalSessionRoutes: true });
    }
    else {
      this.setState({ time: time - 1 });
    }
  }

  stopTimer = () => {
    clearInterval(this.intervalHandle);
  }

  componentDidMount() {
    this.context.loggedIn();
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("w-bg");
    //Countdown
    this.intervalHandle = setInterval(this.tick, 1000);
    technicianAvailability()
    .then((data) => {
      if(data.statusText === 'OK' && data.data.data.availableTimes) {
          var tempDate = undefined;
          const groupedDateSlots = data.data.data.availableTimes.length === 0 ? null : data.data.data.availableTimes.reduce((groupedData, { availability, bookingId, crewId, dateTime, information, timeSlot }) => {
            const formattedDate = moment(dateTime, 'DD-MM-YYYY HH:mm').format('DD-MM-YYYY')
            if(!tempDate) tempDate = formattedDate
            if (!groupedData[formattedDate]) groupedData[formattedDate] = [];
            groupedData[formattedDate].push({ availability, bookingId, crewId, information, timeSlot });
            return groupedData;
          }, {});
          if(groupedDateSlots) {
            this.setState({
              dateSlots: groupedDateSlots
            }, () => this.setSchedule(0, groupedDateSlots, tempDate));
          }
          
      } else {
        this.setState({
          dateSlots: null
        });
      }
    }).catch((error) => {
      this.setState({
        dateSlots: null
      });
      console.error(error);
    })
  }
  scheduleTechnician = (e) => {
    e.preventDefault();
    const { dateSlots, selectedDate, selectedCard, isButtonLoading } = this.state;
    const { history } = this.props;
    
    let bookingId = undefined;
    let time = undefined;
    for(const [index, item] of dateSlots[selectedDate].entries()) {
      if(item.availability === 1) {
        if((selectedCard === "0" && item.timeSlot.slotId === "Morning") || (selectedCard === "1" && item.timeSlot.slotId === "Afternoon")) {
          bookingId = item.bookingId;
          time = item.timeSlot.slot;
          break;
        }
      }
    }
    if(bookingId && !isButtonLoading) {
      this.stopTimer()
      this.setState({isButtonLoading: true});
      scheduleTechnician(bookingId).then((data) => {
        if(data.data.status === 200 || data.data.status == 201) {
          history.push('/installation/schedule/success:' + selectedDate + '--' + time)
        } else {
          this.intervalHandle = setInterval(this.tick, 1000);
          this.setState({
            modalServiceUnavailable: true
          })
        }
        this.setState({
          isButtonLoading: false
        })
      }).catch((error) => {
        console.error(error)
        this.intervalHandle = setInterval(this.tick, 1000);
        if(error.response.status === 403) {
          this.setState({modelForbiddenInstalltion: true});
        }
        this.setState({
          isButtonLoading: false
        })
      });
    }
  }
  setSchedule = (mode, localDateSlots, formattedDate) => {
    const cardIndexSelect = [ false, false ];
    const { cardMorningAfternoon } = this.state;
    for(const [index, item] of localDateSlots[formattedDate].entries()) {
      const indexToUpdate = item.timeSlot.slotId === 'Morning' ? 0 : 1;
      if(item.availability === 1) {
        cardIndexSelect[indexToUpdate] = true;
          cardMorningAfternoon[indexToUpdate].show = true;
          cardMorningAfternoon[indexToUpdate].text =  item.timeSlot.slotId;
          cardMorningAfternoon[indexToUpdate].timeSlot = item.timeSlot.slot;
      } else {
          cardMorningAfternoon[indexToUpdate].show = false;
          cardMorningAfternoon[indexToUpdate].text =  item.timeSlot.slotId;
          cardMorningAfternoon[indexToUpdate].timeSlot = 'Unavailable';
      }
    }
    this.setState({
      selectedDate: formattedDate, cardMorningAfternoon: cardMorningAfternoon, selectedDateIndex: mode
    });
    if(cardIndexSelect[0]) {
      this.estimatedArrival("0");
    } else if(cardIndexSelect[1]) {
      this.estimatedArrival("1");
    } else {
      this.estimatedArrival(undefined);
    }
  };

  estimatedArrival = mode => {
    switch (mode) {
      case "0":
        this.setState({
          selectedCard: "0", morningCheck: true, afternoonCheck: false
        });
        break;

      case "1":
        this.setState({
          selectedCard: "1", morningCheck: false, afternoonCheck: true
        });
        break;
        
      default:
        this.setState({
          selectedCard: undefined, morningCheck: false, afternoonCheck: false
        });
        return null;
    }
  };

  modal = (mode) => {
    switch (mode) {
      case "modalContact":
        this.setState({ modalContact: !this.state.modalContact });
        break;

      case "retrySession":
        this.intervalHandle = setInterval(this.tick, 1000);
        this.setState({
          modalSessionRoutes: !this.state.modalSessionRoutes,
          time: 10 * 60
        });
        break;

      case "save":
        this.setState({
          modalContact: !this.state.modalContact,
          status: !this.state.false,
          name: "",
          mobileNumber: ""
        });
        break;

      case "close":
        this.setState({
          modalContact: false,
          modalSessionRoutes: false,
          modelForbiddenInstalltion: false,
          modalServiceUnavailable: false
        });
        break;

      default:
        return null
    }
  };
  //   save = () => {
  //     this.setState({
  //       modalContact: !this.state.modalContact,
  //       status: !this.state.false,
  //       name: "",
  //       mobileNumber: ""
  //     });
  //   };
  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  render() {
    const { modalContact, modalSessionRoutes, status, name, mobileNumber, time, dateSlots, selectedDateIndex, selectedCard, modelForbiddenInstalltion, isButtonLoading, cardMorningAfternoon, morningCheck, afternoonCheck, modalServiceUnavailable } = this.state;
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    const recommend = ["bnr_recommend01.png", "bnr_recommend02.png", "bnr_recommend03.png"];
    const userProfile = getUserProfile();
    const accounts = userProfile && userProfile.accounts.length > 0 ? userProfile.accounts[0] : null;
    const location = accounts && Object.keys(accounts.Location).length > 0 ? accounts.Location : null;
    const carousel = {
      className: "primary-carousel overflow-inherit mb-4",
      dots: false,
      infinite: false,
      arrows: false,
      speed: 500,
      slidesToShow: 5.3,
      slidesToScroll: 5.3,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 7,
            slidesToScroll: 7
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 5.5,
            slidesToScroll: 5.5
          }
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 4.5,
            slidesToScroll: 4.5
          }
        },
        {
          breakpoint: 320,
          settings: {
            slidesToShow: 3.5,
            slidesToScroll: 3.5
          }
        }
      ]
    };
    const coverStyle = {
      background: `url(${images.common["bnr_blue_2nd.png"]}) no-repeat center`,
      backgroundSize: "cover",
      border: "none"
    };
    const disabled =
      name.length < 1 || mobileNumber.length < 1 ? "disabled" : "";
    const viewContact = status ? (
      <div className="list-group">
        <Link
          to="/shop/internet/package-finder"
          className="list-group-item transparent w-arrow"
        >
          <img alt="" src={images.common["ic_location_pin.png"]} className="wx-25px" />
          <div className="list-group-item-content">
            <h5 className="title w-md-up-75 mb-0">Siti Hasanah</h5>
            <span className="desc">08123456789</span>
            <p className="text-primary mb-0">Edit Contact</p>
          </div>
        </Link>
      </div>
    ) : (

        <div className="list-group list-group-banner">
          <Link
            to="#"
            className="list-group-item light w-arrow bg-cover"
            style={coverStyle}
            onClick={() => this.modal("modalContact")}
          >
            <img alt="" src={images.common["ic_call.png"]} className="w-50px" />
            <div className="list-group-item-content">
              <h5 className="title w-md-up-75 mb-0">Add Secondary Contact</h5>
              <p className='desc'>Not home during the installation? <br /> Add a second contact person</p>
            </div>
          </Link>
        </div>
      );
      const carousalCards = !dateSlots 
      ?
      <div key={0}
      className="carousel-item-sm">
      <div className={`badges badge-date active selector`}>
        <small>No slots available</small>
        <h3></h3>
        <span></span>
      </div>
  </div>
  :
  Object.keys(dateSlots).map((formattedDate, index) => {
    const date = moment(formattedDate, 'DD-MM-YYYY');
     return(
        <div key={index}
            className="carousel-item-sm"
            onClick={() => this.setSchedule(index, dateSlots, formattedDate)} >
            <div className={`badges badge-date ${index === selectedDateIndex ? 'active' : ''} selector`}>
              <small>{date.format('ddd')}</small>
              <h3>{date.format('DD')}</h3>
              <span>{date.format('MMM')}</span>
            </div>
        </div>
     )
  })
      const Loading = ({ images }) => {
        return (
          <span><i className="fa fa-circle-notch fa-spin"></i></span>
        )
      }
      
    return (
      <section>
        <div className="container container-sm">
          <div className="box position-relative animated fadeInUp">
            <div className="countdown-timer block">
              <p>
                <i className="fal fa-clock"></i> {`${Math.floor(time / 60)}`.padStart(2, '0')}:{`${time % 60}`.padStart(2, '0')}
              </p>
            </div>
            <header className="">
              <div className="header-actions">
                <Link to="/home" className="btn btn-link">
                  <i className="fa fa-arrow-left" />
                </Link>
              </div>
              <div className="heading animated fadeInUp delayp1">
                <h1>Schedule Installation</h1>
                <p>
                  Finish your schedule before the timer runs out to book a
                  technician.
                </p>
              </div>
            </header>
            <section className="section-header-card py-main-sm animated fadeInUp delayp2">
              <div className="subheading mb-2">
                <p>Package</p>
              </div>
              <Link
                to="/shop/internet/package/details"
                className="card card-packages w-arrow animated fadeInUp delayp2"
              >
                <div className="card-header">
                  <div className="badges-list">
                    <div className="badges badge-red left">
                      <h3>10</h3>
                      <span>Mbps</span>
                    </div>
                  </div>
                  <ul className="list-unstyled">
                    <li>Internet unlimited</li>
                    <li>203 USee TV channels</li>
                    <li>1000 minutes call</li>
                  </ul>
                </div>
                <div className="card-body w-btn">
                  <h3>
                    Rp180.000<small> / bln</small>
                  </h3>
                  <small className="help-block">One time payment</small>
                  <span className="btn btn-primary">Details</span>
                </div>
              </Link>
              <div className="form-group">
                <label>Address</label>
                <p>
                  {location ? `${location.street}, ${location.district}, ${location.city}, ${location.province}, ${location.postalCode}` : ''}
                </p>
              </div>
            </section>
            <section className="section-fluid bg-gray-50 animated fadeInUp delayp4">
              <div className="subheading mb-3">
                <p>Set Schedule</p>
              </div>
              {
                dateSlots === undefined
              ? 
              <>
                <Slider {...carousel}>
                  {recommend.map((value, index)=>{
                    let sumDelay = index+1;
                    return (
                      <div className={"carousel-item animated fadeInUp delayp" + sumDelay} key={index}>
                        <section className="card-skeleton">
                          <figure className="card-image loading"></figure>
                        </section>
                      </div>
                    )
                  })}
                </Slider>
                </>
              :
               <Slider {...carousel}>
               {carousalCards}
               </Slider>
              }
              
              <div className="subheading mb-3">
                <p>Estimated Arrival</p>
              </div>
              <div className="list-group">
                <div className="row row-1 mb-0">
                  <div className="col-6">
                    <div
                      className={`list-group-item list-check ${morningCheck ? 'active' : ''} ${cardMorningAfternoon[0].show ? '' : 'disabled'}`}
                      onClick={() => {if(cardMorningAfternoon[0].show) this.estimatedArrival("0")}}
                    >
                      <div className="list-group-item-content">
                          <h5 className="title">{cardMorningAfternoon[0].show ? cardMorningAfternoon[0].text : 'Morning'}</h5>
                        <span className="desc">
                          {cardMorningAfternoon[0].show ? `Technician will come around ${cardMorningAfternoon[0].timeSlot}`: 'Unavailable'}
                        </span>
                        <div className="custom-control custom-radio d-none">
                          <input
                            type="radio"
                            className="custom-control-input"
                            id="morning"
                            name="radio-stacked"
                            required
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="customControlValidation2"
                          >
                            morning
                          </label>
                        </div>
                        {/* <label className="input-check-card">
                                        <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                    </label> */}
                      </div>
                    </div>
                  </div>
                  
                  <div className="col-6">
                    <div
                      className={`list-group-item list-check ${afternoonCheck ? 'active' : ''} ${cardMorningAfternoon[1].show ? '' : 'disabled'}`}
                      onClick={() => {if(cardMorningAfternoon[1].show) this.estimatedArrival("1")}}
                    >
                      <div className="list-group-item-content">
                        <h5 className="title">{cardMorningAfternoon[1].show ? cardMorningAfternoon[1].text : 'Afternoon'}</h5>
                        <span className="desc">
                        {cardMorningAfternoon[1].show ? `Technician will come around ${cardMorningAfternoon[1].timeSlot}`: 'Unavailable'}
                        </span>
                        <div className="custom-control custom-radio d-none">
                          <input
                            type="radio"
                            className="custom-control-input"
                            id="afternoon"
                            name="radio-stacked"
                            required
                          />
                          <label
                            className="custom-control-label"
                            htmlFor="customControlValidation3"
                          >
                            afternoon
                          </label>
                        </div>
                        {/* <label className="input-check-card">
                                        <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                    </label> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <p className="help-block">
                Time range is the Estimated is the estimated arrival time. You
                will receive a notification when the technician is heading to
                your location
              </p>
            </section>
            <section className="py-main-sm pb-0">
              <div className="subheading">
                <p>Secondary Contact</p>
              </div>
              {viewContact}
              <div className="subheading">
                <p>Good To Know</p>
              </div>
              <ul className="list-primary">
                <li>
                  {" "}
                  <span>
                    {" "}
                    Installation will take a least 3 hours. Installation may
                    take longer if there are complications. Make sure that you
                    or a contact person is available during that time.{" "}
                  </span>{" "}
                </li>
                <li>
                  {" "}
                  <span>
                    {" "}
                    There may be additional charges once the installation
                    begins. These charges will be confirmed to you beforehand{" "}
                  </span>
                </li>
                <li>
                  {" "}
                  <span>
                    {" "}
                    If the installation is not successful, we will reschedule or
                    issue a refund{" "}
                  </span>
                </li>
              </ul>
              <div className="btn-placeholder">
                <div className="row row-2">
                  <div className="col-6">
                    <Link to="/home" className="btn btn-transparent btn-block">
                      Cancel
                    </Link>
                  </div>
                  <div className="col-6">
                    <Link to="#"
                      className={`btn btn-primary btn-block ${!selectedCard || isButtonLoading ? 'disabled' : ''}`}
                      onClick={(e) => this.scheduleTechnician(e)}>
                      {isButtonLoading ? <Loading images={images}/> : "Next"}
                    </Link>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
        {/* Modal */}
        <Modal show={modalContact} onHide={() => this.modal('close')} centered={true}>
          <div className="modal-body p-box pb-0">
            <div className="heading pt-sm-down-4">
              <h3>Add a second contact person</h3>
              <p>
                Our technician will contact this person if you're not available
              </p>
            </div>
            <section className="mb-4">
              <Form>
                <Form.Group>
                  <Form.Label>Full Name</Form.Label>
                  <Form.Control
                    type="text"
                    id="name"
                    placeholder="Contact's full name"
                    value={name}
                    onChange={this.handleChange}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Mobile Number</Form.Label>
                  <Form.Control
                    type="text"
                    id="mobileNumber"
                    placeholder="Enter mobile number"
                    value={mobileNumber}
                    onChange={this.handleChange}
                  />
                </Form.Group>
                <div className="btn-placeholder">
                  <div className="row row-2">
                    <div className="col-6">
                      <Link
                        to="#"
                        className="btn btn-transparent btn-block"
                        onClick={() => this.modal("close")}
                      >
                        Cancel
                      </Link>
                    </div>
                    <div className="col-6">
                      <Link
                        to="#"
                        className={"btn btn-primary btn-block " + disabled}
                        onClick={() => this.modal("save")}
                      >
                        Save
                      </Link>
                    </div>
                  </div>
                </div>
              </Form>
            </section>
          </div>
        </Modal>
        {/* Modal Session Time Out */}
        <Modal show={modalSessionRoutes} onHide={() => this.modal("retrySession")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <section>
                <h1>Session Time Out</h1>
                <p>Sorry your session is expired. Retry and try to finish your schedule before the timer expires.</p>
              </section>
              <div className="btn-placeholder inline bottom">
                <Link to="#" className="btn btn-primary btn-block" onClick={() => this.modal("retrySession")}>Retry</Link>
              </div>
            </div>
          </div>
        </Modal>
        {/* Modal Session Time Out */}
        <Modal show={modalServiceUnavailable} onHide={() => this.modal("close")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <section>
                <h1>Service Error</h1>
                <p>A service error has occured while processing your request</p>
              </section>
              <div className="btn-placeholder inline bottom">
                <Link to="#" className="btn btn-primary btn-block" onClick={e => {e.preventDefault(); this.modal("close");}}>Ok</Link>
              </div>
            </div>
          </div>
        </Modal>
        {/* Model for booking not allowed */}
        <Modal show={modelForbiddenInstalltion} onHide={() => this.modal("close")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <section>
                <h1>Sorry you have a pending installation</h1>
                <p>At the moment you can only have one installation in progress at a time</p>
              </section>
              <Link to="#" className="btn btn-primary btn-block mb-3" onClick={() => this.modal("close")}>Close</Link>
              <Link to={{pathname: '/home', state: {redirect: 'installation_detail'}}} className="btn btn-transparent btn-block">See my pending installation</Link>
            </div>
          </div>
        </Modal>
      </section>
    );
  }
}

export default InstallationSchedule;
