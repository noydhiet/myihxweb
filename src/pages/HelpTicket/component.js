import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { Form, Modal } from 'react-bootstrap';
import { PageContext } from './../../context/PageContext';
import { reportInstallationIssue, userProfile } from '../../services'
import { setUserProfile } from '../../utils/storage';

const HelpTicket = props => {
  const { trigger, navCase, pageCase, common, brand, sample } = useContext(PageContext);
  const images = { common, brand, sample };
  const [ inputs, setInputs ] = useState({ message: "", issueIndex: "" });
  const [ isSubmitting, setIsSubmitting ] = useState(false);
  const [ modelForbiddenRaiseTicket, setModelForbiddenRaiseTicket ] = useState(false);
  const [ messageForbidden, setMessageForbidden ] = useState('');
  const [modalServiceUnavailable, setModalServiceUnavailable] = useState(false);
  const handleOnChange = (e) => (setInputs({ ...inputs, [e.target.id]: e.target.value }));
  const submitHelpTicket = (e) => {
    e.preventDefault();
    if(!isSubmitting) {
      setIsSubmitting(true)
      userProfile()
      .then( ({data}) => {
        console.log('profileResponse---', data)
        if(data.status === 200 || data.status === 201) {
          setUserProfile(data.data);
          if(data.data.accounts.length > 0) {
            const { location: { state } } = props;
            const payload = {issueId: state.issuesArray[inputs.issueIndex].issueId, categoryId: state.categoryId, message: inputs.message}
            const queryParam = `selectedIndiHomeNum=${data.data.accounts[0].indiHomeNum}`
            console.log('queryParam', payload, queryParam)
            reportInstallationIssue(payload, queryParam)
            .then((response) => {
              console.log('reportInstallationIssue', response)
              if(response.data.ok) {
                  if(response.data.status === 200 || response.data.status === 201){
                  const { history } = props;
                  history.push('/help');
                } else if(response.data.status === 4005) {
                  const { history } = props;
                  history.push({pathname: '/installation/fisik-ticket', state: { data: data, payload: payload }})
                } else if(response.data.status === 4001) {
                  setMessageForbidden('admin')
                  setModelForbiddenRaiseTicket(true)
                } else if(response.data.status === 4002) {
                  setMessageForbidden('logik')
                  setModelForbiddenRaiseTicket(true)
                } else if(response.data.status === 4003) {
                  setMessageForbidden('fisik')
                  setModelForbiddenRaiseTicket(true)
                } else if(response.data.status === 4004) {
  
                } else {
                  setModalServiceUnavailable(true)
                }
                pageCase("submitHelpTicket")
              }
              setIsSubmitting(false);
            }).catch((error) => {
              console.error(error);
              setIsSubmitting(false);
            });
          } else {
            console.log('Attention!! No Indihome number attached to the account')
            setIsSubmitting(false);
          }
        }
      })
      .catch( error => {console.error(error)});
    }
  } 
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
    
    const { location: { state } } = props;
    if(state) {
      setInputs({ ...inputs, ['issueIndex']: state.issueIndex })
    }
  }, [trigger, navCase, pageCase]);
  
  const disabled = !(inputs.issueIndex && inputs.message) || isSubmitting ? 'disabled' : '';
  
  const { history, location: { state } } = props;
  if(!state) {
    history.push('/help');
    return (null);
  }
  const Loading = ({ images }) => {
    return (
      <span><i className="fa fa-circle-notch fa-spin"></i></span>
    )
  }
  const modal = action => {
    switch(action) {
      case 'close':
          setModelForbiddenRaiseTicket(false)
        break;
    }
  }
  console.log('statestate123', state)
  return (
    <section>
      <div className="container container-sm">
        <div className="box position-relative animated fadeInUp">
          <header className="">
            <div className="header-actions">
            {/* /help/internet/detail/1 */}
              <Link to="#" onClick={e => { e.preventDefault(); history.goBack();}} className="btn btn-link">
                <i className="fa fa-arrow-left" />
              </Link>
            </div>
          </header>
          <section className="section-header-text">
            <div className="heading">
              <h1>Report an Issue</h1>
              <p>We're sorry to hear that you're having difficulties. Report your issue here and our team will get in contact with you to fix it.</p>
            </div>
          </section>
          <Form>
            <section className="animated fadeInUp delayp2">
              <Form.Group>
                <Form.Label>Issue</Form.Label>
                <Form.Control as="select"
                  className="custom-select"
                  id="issueIndex"
                  value={inputs.issueIndex}
                  onChange={handleOnChange}
                >
                  <option value="">Choose One</option>
                  {
                    (state && state.issuesArray) &&
                    state.issuesArray.map( (item, index) => <option key={index} value={index}>{item.issue}</option> )
                  }
                   
                </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>Message/Comment</Form.Label>
                <Form.Control
                  as="textarea"
                  placeholder="Enter message"
                  id="message"
                  value={inputs.message}
                  onChange={handleOnChange}
                  rows="4"
                />
              </Form.Group>
            </section>
            <div className="btn-placeholder bottom">
                <Link to="#" className={`btn btn-primary btn-block ` + disabled} onClick={(e) => submitHelpTicket(e)} >{isSubmitting ? <Loading images={images}/> : 'Report Issue'}</Link>
            </div>
          </Form>
        </div>
      </div>
      {/* Model for ticket raise not allowed */}
      <Modal show={modelForbiddenRaiseTicket} onHide={() => modal("close")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                <img className="header-img" src={images.common['grfx_warning.png']} alt="Icon Failed" />
              </header>
              <section>
                <h1>{`Sorry you still have a pending report`}</h1>
                <p>{`At the moment you can only have one ${messageForbidden} report at a time.`}</p>
              </section>
              <Link to="#" className="btn btn-primary btn-block mb-3" onClick={(e) => {e.preventDefault(); modal("close");} }>Close</Link>
                <Link to={{pathname: '/help', state: {redirect: 'fisik_detail'}}} className="btn btn-transparent btn-block">{`See my pending report`}</Link>
            </div>
          </div>
        </Modal>
        {/* Modal Session Time Out */}
        <Modal show={modalServiceUnavailable} onHide={() => setModalServiceUnavailable(false)} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <section>
                <h1>Service Error</h1>
                <p>A service error has occured while processing your request</p>
              </section>
              <div className="btn-placeholder inline bottom">
                <Link to="#" className="btn btn-primary btn-block" onClick={e => {e.preventDefault(); setModalServiceUnavailable(false)}}>Ok</Link>
              </div>
            </div>
          </div>
        </Modal>
    </section>
  )
}

export default HelpTicket;
