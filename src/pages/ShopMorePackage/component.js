import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';

class ShopMorePackage extends Component {
    render() {
        const { indiCloudPage, cloudStoragePage, indiStudyPage } = this.props;
        const config = {
            carouselTab: {
                className: "tab-list",
                dots: false,
                infinite: false,
                arrows: false,
                speed: 500,
                slidesToShow: 3,
                slidesToScroll: 3,
                responsive: [
                    {
                      breakpoint: 991,
                      settings: {
                        dots: false,
                        slidesToShow: 3,
                        slidesToScroll: 3
                      }
                    },
                    {
                      breakpoint: 575,
                      settings: {
                        dots: false,
                        slidesToShow: 3,
                        slidesToScroll: 3
                      }
                    },
                    {
                      breakpoint: 324,
                      settings: {
                        dots: false,
                        slidesToShow: 2.55,
                        slidesToScroll: 2.55
                      }
                    }
                ]
            }
          }
        return (
        <div>
            <div className="cover cover-sm cover-bg-white-sm  bg-red-gradient">
                <div className="header-actions d-block d-md-none">
                    <Link
                    to="/shop/"
                    className="btn btn-link"
                    >
                    <i className="fa fa-arrow-left"></i>
                    </Link>
                </div>
                <div className="container">
                    <nav aria-label="breadcrumb" className="d-none d-md-block">
                    <ol className="breadcrumb animated fadeInUp">
                        <li className="breadcrumb-item"><Link to="/shop"><i className="fa fa-arrow-left mr-1"></i> Shop</Link></li>
                    </ol>
                    </nav>
                    <div className="cover-content">
                    <h1 className="cover-title animated fadeInUp delayp1">More</h1>
                    {/* <p className="cover-text animated fadeInUp delayp2">Mulai berlangganan paket internet, TV dan telepon mulai dari <strong> Rp150.000 </strong> per bulan <sup>*</sup></p> */}
                    </div>
                </div>
            </div>
            <section className="shop-tab-list animated fadeInUp delayp3">
                <div className="container">
                <Slider {...config.carouselTab}>
                    <div className={"tab-item " + indiCloudPage}> 
                        <Link to="/shop/more/indihome-cloud">IndiHome Cloud</Link> 
                    </div>
                    <div className={"tab-item " + cloudStoragePage}> 
                        <Link to="/shop/more/cloud-storage">Cloud Storage</Link>
                    </div>
                    <div className={"tab-item " + indiStudyPage}> 
                        <Link to="/shop/more/indihome-study">IndiHome Study</Link> 
                    </div>
                </Slider>
                </div>
            </section>
        </div>
    )
  }
}

export default ShopMorePackage;