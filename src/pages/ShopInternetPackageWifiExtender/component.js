import React, { Component } from "react";
import { Link } from "react-router-dom";
import ShopInternet from "./../ShopInternet";
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import TrackVisibility from "react-on-screen";

class ShopInternetPackageWifiExtender extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    const { common, brand, sample } = this.context;
    const images = { common, brand, sample };
    return (
      <TextContext.Consumer>{(context)=>{
        const { shopText } = context;
        return (
          <section>
            <ShopInternet wifiExtenderActive="active" />
            <SectionWithBanner images={images} shopText={shopText}/>
            <TrackVisibility once offset={100}> 
              <SectionText images={images} shopText={shopText}/>
            </TrackVisibility>
            <TrackVisibility once offset={400}> 
              <SectionFeatures images={images} />
            </TrackVisibility>
          </section>
        )
      }}

      </TextContext.Consumer>
    );
  }
}

export default ShopInternetPackageWifiExtender;

const SectionWithBanner = ({ images, shopText }) => {
  return (
    <div className="py-main bg-cover-section wifi-extender content-center">
      <div className="container">
        <div className="row row-4">
          <div className="col-md-6 col-8 content-center-left">
            <div className="heading mb-2">
              <h3 className="animated fadeInUp delayp5">
              {shopText.shopInternetWifiExtenderBanner.title}
              </h3>
              <p className="mb-0 animated fadeInUp delayp6">
              {shopText.shopInternetWifiExtenderBanner.desc}
              </p>
            </div>
          </div>
          <div className="col-6 d-none d-md-block">
            <img
              src={images.common["banner_bnr_internet_wifiextender.png"]}
              className="img-fluid rounded animated fadeInUp delayp7"
              alt="Indonesia map"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

const SectionText = ({ isVisible, shopText }) => {
  const effect = isVisible ? "fadeInUp" : "";
  return (
    <div className="py-main bg-white">
      <div className="container">
        <div className="subheading">
          <p className={"animated " + effect + " delayp1"}>About this product</p>
        </div>
        <div className={"heading w-md-up-50 animated " + effect + " delayp2"}>
          <p>
              {shopText.shopInternetWifiExtenderBanner.about}
          </p>
        </div>
      </div>
    </div>
  );
};

const SectionFeatures = ({ images, isVisible }) => {
  const features = [
    {
      img: "grfx_grfx_line_benefit_1.png",
      title: "Unlimited high speed internet",
      desc: "Unlimited high speed internet"
    },
    {
      img: "grfx_grfx_line_benefit_1.png",
      title: "Automatic seamless connection",
      desc: "Connect autimatically when your device is in range of a hotspot"
    },
    {
      img: "grfx_grfx_line_benefit_1.png",
      title: "Connect up to 5 devices",
      desc: "Use the same username for up to 5 devices"
    },
  ]
  const effect = isVisible ? "fadeInUp" : "";
  return (
    <div className="py-main-sm pt-0 bg-white">
      <div className="container">
        <div className="row row-4">
          <div className={"col-md-6 col-12 mb-3 animated " + effect + " delayp1"}>
            <div className="subheading">
              <p>Features</p>
            </div>
            <div className="list-group mb-0">
              {features.map((value, index)=>{
                return (
                  <Link
                    to="/shop/internet/package-finder/budget/1"
                    className="list-group-item transparent"
                    key={index}
                  >
                    <img
                      src={images.common[`${value.img}`]}
                      className="img-fluid w-75px"
                      alt="Budget Icon"
                    />
                    <div className="list-group-item-content">
                      <h5 className="title">{value.title}</h5>
                      <span className="desc">
                      {value.desc}
                      </span>
                    </div>
                  </Link>
                )
              })}
            </div>
          </div>
          <div className={"col-md-6 col-12 animated " + effect + " delayp2"}>
            <div className="subheading">
              <p>How To Activate</p>
            </div>
            <div className="milestone">
              <div className="milestone-item active">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Datang ke Plasa Telkom terdekat</h5>
                  <span className="desc">Lorem ipsum dolor sit ahmet</span>
                </div>
              </div>
              <div className="milestone-item active">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Daftar dengan nomor IndiHome Anda</h5>
                  <span className="desc">Lorem ipsum dolor sit ahmet</span>
                </div>
              </div>
              <div className="milestone-item active last-child">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">
                    Teknisi kami akan datang untuk memasang wifi extender Anda
                  </h5>
                  <span className="desc">Lorem ipsum dolor sit ahmet</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
