import React, { Component } from "react";
import { Link } from "react-router-dom";
import { PageContext } from './../../context/PageContext';
import { CardContent } from './../../include/CardPackage'
import Slider from "react-slick";

class ShopTvAppsDetails extends Component {
    static contextType = PageContext;
    componentDidMount() {
        this.context.trigger(2);
        this.context.navCase("w-md-up-block");
        this.context.pageCase("w-bg");
    }
    render() {
        const { common, brand, sample } = this.context;
        const images = { common, brand, sample };
        const config = {
            carousel: {
              className: "primary-carousel overflow-inherit",
              dots: false,
              infinite: false,
              arrows: false,
              speed: 500,
              slidesToShow: 4,
              slidesToScroll: 4,
              responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: false,
                    swipe: false,
                    dots: false
                  }
                },
                {
                  breakpoint: 736,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: false,
                    dots: false,
                  }
                },
                {
                  breakpoint: 575,
                  settings: {
                    slidesToShow: 3.2,
                    slidesToScroll: 3.2,
                    infinite: false,
                  }
                }
              ]
            }
        };
        return (
            <section>
              <div className="container container-sm">
                <div className="box animated fadeInUp">
                  <header>
                    <div className="header-actions">
                      <Link to="/shop/internet/speed-on-demand" className="btn btn-link">
                        <i className="fa fa-arrow-left"></i>
                      </Link>
                    </div>
                  </header>
                  <section className="section-header-text pt-4">
                    <div className="heading hidden">
                      <h1>HOOQ</h1>
                    </div>
                    <div className="heading hidden">
                      <h1>iFlix</h1>
                    </div>
                    <div className="heading">
                      <h1>Catchplay</h1>
                    </div>
                  </section>
                  <section className="py-main-sm pb-0">
                    <div className="subheading hidden">
                      <p>Watch All These on HOOQ</p>
                    </div>
                    <div className="subheading hidden">
                      <p>Watch All These on iFlix</p>
                    </div>
                    <div className="subheading">
                      <p>Watch All These on Catchplay</p>
                    </div>
                    <Slider {...config.carousel}>
                      <div className="carousel-item">
                        <CardContent
                          link="#"
                          img="poster1.png"
                          images={images}
                        />
                      </div>
                      <div className="carousel-item">
                        <CardContent
                          link="#"
                          img="poster2.png"
                          images={images}
                        />
                      </div>
                      <div className="carousel-item">
                        <CardContent
                          link="#"
                          img="poster3.png"
                          images={images}
                        />
                      </div>
                      <div className="carousel-item">
                        <CardContent
                          link="#"
                          img="poster4.png"
                          images={images}
                        />
                      </div>
                      <div className="carousel-item">
                        <CardContent
                          link="#"
                          img="poster5.png"
                          images={images}
                        />
                      </div>
                    </Slider>
                  </section>
                  <section className="py-main-sm pb-0">
                    <div className="subheading">
                        <p>Promo</p>
                    </div>
                    {/* <p>Berlangganan HOOQ Rp36.000/bulan dan dapatkan penawaran menarik:</p>
                    <ul>
                        <li>Untuk pelanggan baru Hooq, bayar 1 bulan nikmati bebas nonton selama 3 bulan</li>
                        <li>Bonus 1 tiket sewa film Hollywood terbaru di Hooq setiap bulan</li>
                    </ul> */}
                    {/* <p>Gratis untuk semua pelanggan IndiHome, for a limited time!</p> */}
                    <div className="subheading">
                        <p>About this Service</p>
                    </div>
                    {/* <p>Selamat datang di Hooq, pintu gerbang dunia hiburan tanpa batas! Temukan berbagai tayangan petualangan seru, horor, dan komedi non-stop hanya dengan satu klik.</p>
                    <p>Nonton lebih dari 10.000 film dari serial loka hingga Hollywood blockbuster favorit ad free - Nonton film sepuasnya tanpat jeda iklan</p>
                    <p>Nonton Hooq lebih nyaman melalui TV Anda menggunakan Hybrid Boox USeeTV. Selain itu juga bisa dinikmati melalui laptop, desktop, tablet dan smartphone</p>
                    <p>Tersedia fitur Pay TV yaitu Live Streaming TV Channel Langganan bisa dibatalkan kapan saja.</p> */}

                    {/* <p>Dengan iFlix, Anda bisa menonton tayangan favorit mulai dari serial TV hollywood, film-film terbaik dari seluruh dunia; acara TV lokal; sitkom; kartun, sampai drama korea, semuanya bisa Anda nikmati di iflix kapan saja, dan di mana saja!</p>
                    <p>Untuk menonton tayangan iflix, Anda dapat mengunduh konten-kontennya terlebih dahulu hingga ke lima perangkat dan menontonnya secara offline. Tidak hanya itu saja, tontonan iflix dapat disaksikan di dua perangkat secara bersamaan. Layanan Add-on iflix dapat digunakan di: televisi (via Hybrid Box IndiHome), PC/laptop, atau tablet/smartphone.</p> */}

                    <p>Catchplay adalah cara terbaik untuk menonton dan menemukan film kesukaan Anda, Catchplay dapat membuat Anda merasakan serunya memiliki bioskop pribadi di rumah setiap hari!</p>
                    <p>Dengan koleksi film yang di-update setiap minggunya Catchplay menawarkan berbagai pilihan film dan serial TV terbaik dari Hollywood , Asia, dan karya-karya terbaik Indonesia untuk di tonton kapan dan di mana saja. Membayangkan saja sudah seru kan?</p>
                    <p>Habiskan waktu terbaik Anda dengan menonton semua film dan serial TV favorit di Catchplay</p>

                    <div className="subheading">
                        <p>Watch On</p>
                    </div>
                    <div className="row mb-3">
                        <div className="col-4 content-center">
                            <div>
                              <img alt="icon" src={images.common["icon_ic_selection_device_tv_b.png"]} className="img-fluid w-100px" />
                              <p className="text-center">TV</p>
                            </div>
                        </div>
                        <div className="col-4 content-center">
                            <div>
                              <img alt="icon" src={images.common["icon_ic_selection_device_computer_b.png"]} className="img-fluid w-100px" />
                              <p className="text-center">Komputer</p>
                            </div>
                        </div>
                        <div className="col-4 content-center">
                            <div>
                              <img alt="icon" src={images.common["icon_ic_selection_device_mobile_b.png"]} className="img-fluid w-100px" />
                              <p className="text-center">Handphone & Tablet</p>
                            </div>
                        </div>
                    </div>
                    <div className="list-group mb-0">
                        <Link to="/shop/internet/package-finder/budget/1" className="list-group-item mb-0">
                            <div className="list-group-item-content">
                                <h5 className="title">Rp36.000</h5>
                                <span className="desc">View Pricing Detail</span>
                            </div>
                            <div className="btn-placeholder mt-0">
                                <Link to="/shop/tv/hybrid-box/confirm" className="btn btn-primary">Subscribe</Link>
                            </div>
                        </Link>
                    </div>
                    {/* <div className="btn-placeholder">
                      <button className="btn btn-primary btn-block">Subscribe</button>
                    </div> */}
                  </section>
                </div>
              </div>
            </section>
        )
    }
}
 

export default ShopTvAppsDetails;
