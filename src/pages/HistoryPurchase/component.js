import React, { Component } from "react";
import { Link } from "react-router-dom";
import History from "./../History";
import { NavMobile } from "./../../include/NavMobile";
import { PageContext } from './../../context/PageContext';

class HistoryPurchase extends Component {
    static contextType = PageContext;
    componentDidMount() {
        this.context.trigger(2);
        this.context.navCase("w-md-up-block");
        this.context.pageCase("l-bg");
        this.context.footerCase("w-nav-bottom");
    }
    componentWillMount() {
        this.context.footerCase("");
    }
  render() {
    const { common, brand, sample, condition } = this.context;
    const images = { common, brand, sample };
    return (
      <div>
        <NavMobile condition={condition} historyPage="active" />
        <History purchaseActive="active" />
        <SectionContent images={images} />
      </div>
    );
  }
}

export default HistoryPurchase;

const SectionContent = ({ images }) => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-4 d-none d-md-block">
          <div className="box box-in-cover py-3">
            <div className="list-group list-group-activity mb-0">
              <Link to="/history/bill" className="list-group-item w-arrow">
                <div className="list-group-item-content">
                  <p className="title mb-0">Bill</p>
                </div>
              </Link>
              <Link to="/history/purchase" className="list-group-item w-arrow">
                <div className="list-group-item-content mb-0">
                  <p className="title mb-0 active">Purchase</p>
                </div>
              </Link>
              <Link to="/history/activity" className="list-group-item w-arrow">
                <div className="list-group-item-content mb-0">
                  <p className="title mb-0">Activity</p>
                </div>
              </Link>
            </div>
          </div>
        </div>
        <div className="col-md-8 col-12">
          <div className="box box-in-cover mb-3 mt-sm-down-4 min-h-auto animated fadeInUp">
            <div className="subheading">
              <p>Active Packages</p>
            </div>
            <div className="list-group list-group-activity mb-0">
              <div className="list-group-item w-arrow">
                <div className="list-group-item-content mb-0">
                  <p className="title">10 Mbps IndiGamer TriplePlay</p>
                  <small className="help-block">
                    10Mbps + 92 channels + 100 minutes
                  </small>
                  <Link
                    to="/history/purchase/details"
                    className="btn btn-link px-0 py-0"
                  >
                    Lihat Detail
                  </Link>
                </div>
              </div>
              <div className="list-group-item w-arrow">
                <div className="list-group-item-content mb-0">
                  <p className="title">30 Mbps Extra Speed</p>
                  <small className="help-block">
                    3 days package * 1-3 July 2019
                  </small>
                </div>
              </div>
              <div className="list-group-item w-arrow">
                <div className="list-group-item-content mb-0">
                  <p className="title">Minipack IndiKids Lite</p>
                  <small className="help-block">3 channels</small>
                  <Link
                    to="/history/purchase/details"
                    className="btn btn-link px-0 py-0"
                  >
                    Lihat Detail
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="box animated fadeInUp delayp5 min-h-auto mb-3">
            <div className="subheading">
              <p>Recently Ended</p>
            </div>
            <div className="list-group list-group-activity mb-0">
              <p className="help-block mb-0">May 2019</p>
              <div className="list-group-item w-arrow">
                <div className="list-group-item-content mb-0">
                  <p className="title">30 Mbps Extra Speed</p>
                  <small className="help-block">
                    3 days package * 1-3 May 2019
                  </small>
                </div>
              </div>
              <div className="list-group-item w-arrow">
                <div className="list-group-item-content mb-0">
                  <p className="title">Minipack IndiKids Lite</p>
                  <small className="help-block">3 channels</small>
                  <Link
                    to="/history/purchase/details/"
                    className="btn btn-link px-0 py-0"
                  >
                    Lihat Detail
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="box min-h-auto animated fadeInUp delayp6">
            <div className="subheading">
              <p>Recently Ended</p>
            </div>
            <div className="list-group list-group-activity mb-0">
              <p className="help-block mb-0">April 2019</p>
              <div className="list-group-item w-arrow">
                <div className="list-group-item-content mb-0">
                  <p className="title">30 Mbps Extra Speed</p>
                  <small className="help-block">
                    3 days package * 1-3 May 2019
                  </small>
                </div>
              </div>
              <div className="list-group-item w-arrow">
                <div className="list-group-item-content mb-0">
                  <p className="title">HOOQ</p>
                  <small className="help-block">1 month subscription</small>
                  <Link
                    to="/history/purchase/details"
                    className="btn btn-link px-0 py-0"
                  >
                    Lihat Detail
                  </Link>
                </div>
              </div>
            </div>
          </div>

          {/* Start - Blank State (Hidden) */}
          <div className="box min-h-auto animated fadeInUp hidden">
            <div className="row">
              <div className="col-md-6 col-12 mx-auto">
                <div className="blank-state">
                  <img alt="" src={images.common["ic_header_failed.png"]} />
                  <div className="heading">
                    <p className="dark">
                      You don't have any active subscription. <br /> Buy your
                      first package to start enjoying IndiHome!
                    </p>
                  </div>
                  <div className="btn-placeholder">
                    <Link
                      to="/shop/internet/package"
                      className="btn btn-primary"
                    >
                      Browse Package
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* End - Blank State */}
        </div>
      </div>
    </div>
  );
};
