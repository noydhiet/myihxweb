import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const FaqInfo = () => {
    const { trigger, navCase, pageCase, modal } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
            <section>
                <div className="container container-sm">
                    <div className="box position-relative animated fadeInUp">
                        <header className="">
                            <div className="header-actions">
                                <Link to="/help/internet/detail/1" className="btn btn-link">
                                <i className="fa fa-arrow-left" />
                                </Link>
                            </div>
                        </header>
                        <section className="section-header-text">
                            <div className="heading">
                                <h1>Internet is connected but the speed is slow</h1>
                            </div>
                        </section>
                        <section className="py-main-sm  pb-0 animated fadeInUp delayp2">
                            <div className="heading">
                                <h2>Checking Connected Devices</h2>
                                <p>Check the other devices connected to the network. If too many devices are connected or on bandwidth heavy activities like video streaming or downloading large files, your internet speed can be affected.</p>
                                <p>Check the other devices connected to the network. If too many devices are connected or on bandwidth heavy activities like video streaming or downloading large files, your internet speed can be affected.</p>
                            </div>
                            <div className="heading">
                                <h2>Checking Connected Devices</h2>
                                <p>Check the other devices connected to the network. If too many devices are connected or on bandwidth heavy activities like video streaming or downloading large files, your internet speed can be affected.</p>
                                <p>Check the other devices connected to the network. If too many devices are connected or on bandwidth heavy activities like video streaming or downloading large files, your internet speed can be affected.</p>
                            </div>
                        </section>
                    </div>
                  </div>
            </section>
  )
}

export default FaqInfo;
