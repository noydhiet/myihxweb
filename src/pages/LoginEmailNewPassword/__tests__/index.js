import React from 'react';
import renderer from 'react-test-renderer';
import LoginEmailNewPassword from '../index';

jest.mock('../../../services', () => ({
  resetPassword: ({ password, resetPasswordToken }) => new Promise((resolve, reject) => {
    if (password === 'test123' && resetPasswordToken === '123') resolve('success');
    else reject('failed');
  }),
}));
jest.mock('react-router-dom');

const instance = props => renderer.create(<LoginEmailNewPassword images={{ common: {} }} {...props} />).getInstance();

describe('LoginEmailNewPassword', () => {
  test('render', () => {
    const result = instance().render();
    expect(result.type).toBe(React.Fragment);
  });

  test('handleChange', () => {
    const wrapper = instance();
    const e = { target: { id: 'name', value: 'test' } };
    wrapper.handleChange(e);
    expect(wrapper.state.name).toBe('test');
  });

  test('trigger', () => {
    const wrapper = instance();

    wrapper.trigger();
    expect(wrapper.state.icon).toBe(true);

    wrapper.trigger();
    expect(wrapper.state.icon).toBe(false);
  });

  test('submit', () => {
    const match = { params: { token: '123' } };
    const wrapper = instance({ match });
    const e = { preventDefault: jest.fn() };
    wrapper.submit(e);
    expect(e.preventDefault).toHaveBeenCalled();
  });
});
