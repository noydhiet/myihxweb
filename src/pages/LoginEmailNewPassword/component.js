import React from 'react';
import { Form } from 'react-bootstrap';
import { CardNotif } from '../../include/CardPackage';
import ContainerBase from '../../include/ContainerBase';
import { resetPassword } from '../../services';
import { PageContext } from './../../context/PageContext';
import { Link } from 'react-router-dom';

export default class LoginEmailNewPassword extends React.Component {
  static contextType = PageContext;
  state = {
    icon: false,
    password: '',
    alert: '',
    notif: '',
    isButtonLoading: false
  }

  componentDidMount() {
    this.context.trigger(1);
    this.context.pageCase("w-bg");
    const { history, location: { state }} = this.props;
    if(!state) {
      history.push('/login/forgot-password')
    }
  }

  handleChange = (e) => {
    const { id, value } = e.target;
    const passwordCheck = v => !(/[A-Za-z]+/.test(v) && /[0-9]+/.test(v) && value.length >= 6);
    const resAlert = passwordCheck(value) ? 'Min. 6 characters with a mix of letters & numbers' : ''; 

    this.setState({
      [id]: value,
      alert: resAlert,
    })
  }

  trigger = () => {
    this.setState({
      icon: !this.state.icon
    })
  }

  submit = (e) => {
    e.preventDefault();
    const { history, location: { state } } = this.props;
    const { password } = this.state;
    const { openNotif } = this.context;
    const resetPasswordToken = state.token;
    this.setState({
      isButtonLoading: true
    }, () => {
      resetPassword({ password, resetPasswordToken })
      .then(({ data }) => {
        if (data.ok) {
          this.setState({ notif: 'Password has been reset successfully. You can now login.' });
          openNotif('recoveryPassword');
          history.push('/login:');
        } else {
          this.setState({ alert: 'Password reset token is invalid or has expired.', isButtonLoading: false });
        }
      })
      .catch(() => { this.setState({ alert: 'Password reset token is invalid or has expired.', isButtonLoading: false }); });
    });
    
  }

  render() {
    const { icon, notif, password, alert, isButtonLoading } = this.state;
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    const config = {
      iconPassword: icon ? "fas fa-eye" : "fas fa-eye-slash",
      colorIconPassword: icon ? "#ee3124" : "#4d4d4d",
      typePassword: icon ? "text" : "password",
      disabledBtn: !isButtonLoading && password && !alert ? '' : 'disabled',
      justifyIcon: alert ? '36%' : '16%',
      alert: alert ? 'block' : 'none',
      notifActive: notif ? 'active' : '',
    };
    const containerProps = {
      image: { alt: 'Icon Login Email', src: images.common['ic_header_password.png'], },
      subTitle: 'Create a new password to protect your account',
      title: 'New Password',
    };
    const Loading = () => {
      return (
        <span><i className="fa fa-circle-notch fa-spin"></i></span>
      )
    }
    return (
      <>
        <ContainerBase {...containerProps}>
          <Form>
            <Form.Group className="form-custom w-preppend">
              <Form.Label> Password </Form.Label>
              <Form.Control
                autoFocus
                type={config.typePassword}
                name="password"
                id="password"
                value={password}
                onChange={this.handleChange}
                placeholder="New Password"
              />
              {/* <small className="help-block">Min. 6 characters with a mix of letters and numbers</small> */}
              <div className="invalid-feedback" style={{ display: config.alert }}>{alert}</div>
              <span onClick={this.trigger} className="preppend-icon" style={{bottom: config.justifyIcon, color: config.colorIconPassword}}><i className={config.iconPassword}></i> </span>
            </Form.Group>
            <div className="btn-placeholder">
            <Link to="#" type="submit" className={"btn btn-primary btn-block animated fadeInUp delayp4 " + config.disabledBtn} onClick={this.submit}><span> {isButtonLoading ? <Loading /> : 'create new password'} </span></Link>
            </div>
          </Form>
        </ContainerBase>
        <CardNotif active={config.notifActive} textTop={notif} />
      </>
    )
  }
}

LoginEmailNewPassword.defaultProps = {
  pageCase: () => {},
  trigger: () => {},
};
