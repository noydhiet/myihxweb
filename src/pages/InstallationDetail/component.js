import React, { useEffect, useState, useContext } from 'react'
import { Link } from 'react-router-dom'
import { PageContext } from './../../context/PageContext';
import moment from 'moment';
import { Modal, Form } from 'react-bootstrap'
import Slider from "react-slick";
import { technicianAvailability, reScheduleTechnician, technicianAvailabilityFeasibility } from '../../services';
import { getUserProfile } from '../../utils/storage';

const InstallationDetail = (props) => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const [installationDate, setInstallationDate] = useState({});
    const [modalReschedule, setModalReschedule] = useState(false);
    const [modalNotReschedule, setModalNotReschedule] = useState({ reason: '', message: '', show: false });
    const [attemptExhausted, setAttemptExhausted] = useState(false);
    const [modalServiceUnavailable, setModalServiceUnavailable] = useState(false);
    const [isButtonLoading, setButtonLoading] = useState(false);
    const [checkingFeasibility, setCheckingFeasibility] = useState(false);
    const [showConfirmationReschedule, setShowConfirmationReschedule] = useState(false);
    const [dateSlots, setDateSlots] = useState(undefined);
    const [selectedDate, setSelectedDate] = useState(undefined);
    const [selectedDateIndex, setSelectedDateIndex] = useState(undefined);
    const [selectedCard, setSelectedCard] = useState(undefined);
    const [modalContact, setModalContact] = useState(false);
    const [ inputs, setInputs ] = useState({ name: '', mobileNumber: '' });
    const [cardMorningAfternoon, setCardMorningAfternoon] = useState([{ show: false, text: '', timeSlot: '' }, { show: false, text: '', timeSlot: '' }]);
    const [morningCheck, setMorningCheck] = useState(false);
    const [afternoonCheck, setAfternoonCheck] = useState(false);

    const setSchedule = (mode, localDateSlots, formattedDate) => {
      setSelectedDate(formattedDate.toString());
      setSelectedDateIndex(mode)
      const cardIndexSelect = [ false, false ];
      for(const [index, item] of localDateSlots[formattedDate.toString()].entries()) {
        const indexToUpdate = item.timeSlot.slotId === 'Afternoon' ? 1 : 0;
        if(item.availability === 1) {
          cardIndexSelect[indexToUpdate] = true;
          setCardMorningAfternoon(oldArray => ({...oldArray, [indexToUpdate]: {'show': true, 'text': item.timeSlot.slotId, 'timeSlot': item.timeSlot.slot}}));
        } else {
          setCardMorningAfternoon(oldArray => ({...oldArray, [indexToUpdate]: {'show': false, 'text': item.timeSlot.slotId, 'timeSlot': 'Unavailable'}}));
        }
      }
      if(cardIndexSelect[0]) {
        estimatedArrival("0");
      } else if(cardIndexSelect[1]) {
        estimatedArrival("1");
      } else {
        estimatedArrival(undefined);
      }
    };
    const handleChange = e => {
      setInputs({ ...inputs, [e.target.id]: e.target.value })
    };
    const estimatedArrival = mode => {
        switch (mode) {
          case "0":
            setSelectedCard("0");
            setMorningCheck(true);
            setAfternoonCheck(false);
            break;
          case "1":
            setSelectedCard("1");
            setMorningCheck(false);
            setAfternoonCheck(true);
            break;
          default:
            setSelectedCard(undefined)
            setMorningCheck(false);
            setAfternoonCheck(false);
            return null;
        }
    };
    const modal = (mode) => {
      switch (mode) {

        case "modalContact":
            setModalContact(true);
            break;

        case "close":
            setModalContact(false);
            
            setAttemptExhausted(false)
            setModalNotReschedule({...modalNotReschedule, ['show']: false})
          break;
        case "closeReschedule":
          setModalReschedule(false)
        break;

        default:
          return null
      }
    };
    const reScheduleTechnicianClick = (e) => {
      e.preventDefault();
      setModalReschedule(true); 
      setShowConfirmationReschedule(false);
      const { history } = props;
      let bookingId = undefined;
      let time = undefined;
      for(const [index, item] of dateSlots[selectedDate].entries()) {
          if(item.availability === 1) {
            if((selectedCard === "0" && item.timeSlot.slotId === "Morning") || (selectedCard === "1" && item.timeSlot.slotId === "Afternoon")) {
              bookingId = item.bookingId;
              time = item.timeSlot.slot;
              break;
            }
        }
      } 
      if(bookingId && !isButtonLoading) {
        setButtonLoading(true);
        reScheduleTechnician(bookingId).then((data) => {
          if(data.data.ok) {
            history.push('/installation/reschedule/success/' + selectedDate + '--' + time)
          }
          setButtonLoading(false);
        }).catch((error) => {
          if(error.response && error.response.status === 400) {
            setModalReschedule(false)
            setAttemptExhausted(true)
          }
          setButtonLoading(false);
          console.error(error)
        });
      } else {
        setButtonLoading(false);
        console.error('Error', bookingId)
      }
    }
    const fetchSlots = e => {
      e.preventDefault();
      if(!checkingFeasibility) {
        setCardMorningAfternoon(oldArray => ({...oldArray, [0]: {'show': false, 'text': '', 'timeSlot': ''}}));
        setCardMorningAfternoon(oldArray => ({...oldArray, [1]: {'show': false, 'text': '', 'timeSlot': ''}}));
        estimatedArrival(undefined);
        setDateSlots(undefined)
        setCheckingFeasibility(true)
        technicianAvailabilityFeasibility()
        .then( response => {
          console.log('technicianAvailabilityFeasibility', response)
          setCheckingFeasibility(false)
          if(response.data.status === 200 || response.data.status === 201) {
            if(response.data.data.rescheduleAvailable) {
              setModalReschedule(true);
              technicianAvailability().then((data) => {
                console.log('technicianAvailability==', data)
                if((data.data.status === 200 || data.data.status === 201) && data.data.data.availableTimes) {
                    var tempDate = undefined;
                    const groupedDateSlots = data.data.data.availableTimes.length === 0 ? null : data.data.data.availableTimes.reduce((groupedData, { availability, bookingId, crewId, dateTime, information, timeSlot }) => {
                      const formattedDate = moment(dateTime, 'DD-MM-YYYY HH:mm').format('DD-MM-YYYY')
                      if(!tempDate) tempDate = formattedDate
                      if (!groupedData[formattedDate]) groupedData[formattedDate] = [];
                      groupedData[formattedDate].push({ availability, bookingId, crewId, information, timeSlot });
                      return groupedData;
                    }, {});
                    if(groupedDateSlots) {
                      setDateSlots(groupedDateSlots);
                      setSchedule(0, groupedDateSlots, tempDate);
                    } else {
                      setModalServiceUnavailable(true)
                      setModalReschedule(false);
                    }
                } else {
                  setModalReschedule(false);
                  setModalServiceUnavailable(true)
                }
              }).catch((error) => {
                setModalServiceUnavailable(true)
                setModalReschedule(false);
                console.error(error);
              });
            } else {
              setModalNotReschedule({ ...modalNotReschedule, ['reason']: response.data.data.reason, ['message']: response.data.data.message, ['show']: true })
            }
          } else {
            setModalServiceUnavailable(true)
          }
        }).catch( error => {
          setCheckingFeasibility(false)
          setModalServiceUnavailable(true)
          console.error(error)
        });
      }
    }
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
        if(props.location.state) {
          const {appointments} = props.location.state;
          if(appointments.timeSlot && Object.keys(appointments.timeSlot).length > 0) {
            setInstallationDate(appointments.timeSlot);
          }
          else {
            const { history } = props;
            history.push('/')
          }
        } else {
          const { history } = props;
          history.push('/')
        }
    }, [trigger, navCase, pageCase]);

    const date =  installationDate.date ? moment(`${installationDate.date} ${installationDate.time}`, 'DD-MM-YYYY HH:mm') : undefined;
    const images = { brand, common, sample } ;
    const coverStyle = {
        background: `url(${images.common["bnr_blue_2nd.png"]}) no-repeat center`,
        border: "none"
    };
    const Loading = ({ images }) => {
      return (
        <span><i className="fa fa-circle-notch fa-spin"></i></span>
      )
    }
    const  carouselDate = {
      className: "primary-carousel overflow-inherit mb-4",
      dots: false,
      infinite: false,
      arrows: false,
      speed: 500,
      slidesToShow: 5.3,
      slidesToScroll: 5.3,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 5.5,
            slidesToScroll: 5.5
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 3.5,
            slidesToScroll: 3.5
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 4.5,
            slidesToScroll: 4.5
          }
        },
        {
          breakpoint: 320,
          settings: {
            slidesToShow: 3.5,
            slidesToScroll: 3.5
          }
        }
      ]
    };
    const carousel = {
      className: "primary-carousel overflow-inherit mb-4",
      dots: false,
      infinite: false,
      arrows: false,
      speed: 500,
      slidesToShow: 5.3,
      slidesToScroll: 5.3,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 7,
            slidesToScroll: 7
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 5.5,
            slidesToScroll: 5.5
          }
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 4.5,
            slidesToScroll: 4.5
          }
        },
        {
          breakpoint: 320,
          settings: {
            slidesToShow: 3.5,
            slidesToScroll: 3.5
          }
        }
      ]
    };
    const appointments = props.location.state ? props.location.state.appointments : null;
    const technician = appointments ? appointments.technician : null;
    const technician_status = appointments && appointments.currentStatus ? appointments.currentStatus.title : ''
    const userProfile = getUserProfile();
    const accounts = userProfile && userProfile.accounts.length > 0 ? userProfile.accounts[0] : null;
    const location = accounts && Object.keys(accounts.Location).length > 0 ? accounts.Location : null;
    const recommend = ["bnr_recommend01.png", "bnr_recommend02.png", "bnr_recommend03.png"];
      const disabled = inputs.name && inputs.mobileNumber ? '' : 'disabled';

      const carousalCards = !dateSlots 
      ?
      <div key={0}
            className="carousel-item-sm">
            <div className={`badges badge-date active selector`}>
              <small>No slots available</small>
              <h3></h3>
              <span></span>
            </div>
          </div>
          :
          Object.keys(dateSlots).map((formattedDate, index) => {
            const date = moment(formattedDate, 'DD-MM-YYYY');
            return(<div key={index}
              className="carousel-item-sm"
              onClick={e => { e.preventDefault(); setSchedule(index, dateSlots, formattedDate)}} >
              <div className={`badges badge-date ${selectedDateIndex === index ? 'active' : ''} selector`}>
                <small>{date.format('ddd')}</small>
                <h3>{date.format('DD')}</h3>
                <span>{date.format('MMM')}</span>
              </div>
            </div>)
          })
    return (
        <section>
            <div className="container container-sm">
                <div className="box position-relative animated fadeInUp">
                    <header className="">
                        <div className="header-actions">
                            <Link to="/home" className="btn btn-link"><i className="fa fa-arrow-left" /></Link>
                        </div>
                        <div className="heading animated fadeInUp delayp1">
                            <h1>Appointment Detail</h1>
                        </div>
                    </header>
                    <section className="section-header-card py-main-sm animated fadeInUp delayp2">
                        <div className="subheading">
                            <p>Your Appointment</p>
                        </div>
                        <div className="list-group">
                            <div className="list-group-item unclickable">
                                <div className="badges badge-date dark">
                                <small>{ date ? date.format('ddd') : '' }</small>
                                <h3>{date ? date.format('DD') : ''}</h3>
                                <span>{date ? date.format('MMM') : ''}</span>
                                </div>
                                <div className="list-group-item-content">
                                <h5 className="title">New installation</h5>
                                <span className="desc mb-2">
                                    Estimated arrival time {installationDate.slot ? installationDate.slot : ''}
                                </span>
                  <Link to="#" className="font-weight-bold" onClick={e => fetchSlots(e)}>Reschedule</Link>
                </div>
              </div>
            </div>
            <div className="subheading">
              <p>Package</p>
            </div>
            <Link
              to="/shop/internet/package/details"
              className="card card-packages w-arrow animated fadeInUp delayp2"
            >
              <div className="card-header">
                <div className="badges-list">
                  <div className="badges badge-red left">
                    <h3>10</h3>
                    <span>Mbps</span>
                  </div>
                </div>
                <ul className="list-unstyled">
                  <li>Internet unlimited</li>
                  <li>203 USee TV channels</li>
                  <li>1000 minutes call</li>
                </ul>
              </div>
              <div className="card-body w-btn">
                <h3>
                  Rp180.000<small> / bln</small>
                </h3>
                <small className="help-block">One time payment</small>
                <span className="btn btn-primary">Details</span>
              </div>
            </Link>
            <div className="form-group">
              <label>Address</label>
              <p>{location ? `${location.street}, ${location.district}, ${location.city}, ${location.province}, ${location.postalCode}` : ''}</p>
            </div>
            {
              appointments && appointments.orderNumber
              ?
              (
                <div className="subheading">
                  <p>Your Technician</p>
                </div>
              )
              :
              (
                null
              )
            }
            {
              appointments && appointments.orderNumber
              ?
              (
                <div className="list-group">
                  <div
                    to="/shop/internet/package-finder"
                    className="list-group-item transparent"
                  >
                    <img
                      alt=""
                      src={images.common["sampleImg.jpg"]}
                      className="w-50px rounded-circle"
                    />
                    <div className="list-group-item-content">
                      <h5 className="title w-md-up-75 mb-0">{technician && technician.displayname ? technician.displayname : 'Technician is yet to be assigned'}</h5>
                      <p className="desc">{technician_status}</p>
                    </div>
                    <div>
                      {
                        !technician || technician.email === ""
                        ?
                        <i className="fas fa-comment-alt-dots text-primary pr-3"></i>
                        :
                        <a href={`mailto:${technician && technician.email}`}>
                          <i className="fas fa-comment-alt-dots text-primary pr-3"></i>
                        </a>
                      } 
                      {
                        !technician || technician.phone === ""
                        ?
                        <i className="fas fa-phone text-primary"></i> 
                        :
                        <a href={`tel:${technician && technician.phone}`}>
                          <i className="fas fa-phone text-primary"></i>
                        </a>
                      }
                    
                    </div>
                  </div>
                </div>
              )
              :
              (
                null
              )
            }
            <div className="subheading">
              <p>Secondary Contact</p>
            </div>
            <div className="list-group list-group-banner">
              <Link
                to="#"
                className="list-group-item light w-arrow bg-cover"
                style={coverStyle}
                onClick={() => modal("modalContact")}>
                <img alt="" src={images.common["ic_call.png"]} className="w-50px" />
                <div className="list-group-item-content">
                  <h5 className="title w-md-up-75 mb-0">Add Secondary Contact</h5>
                  <p className='desc'>Not home during the installation? <br /> Add a second contact person</p>
                </div>
              </Link>
            </div>
            <div className="subheading">
              <p>Good To Know</p>
            </div>
            <ul className="list-primary">
              <li> <span> Installation will take a least 3 hours. Installation may take longer if there are complications. Make sure that you or a contact person is available during that time. </span> </li>
              <li> <span> There may be additional charges once the installation begins. These charges will be confirmed to you beforehand </span></li>
              <li> <span> If the installation is not successful, we will reschedule or issue a refund </span></li>
            </ul>
          </section>
        </div>
      </div>
      <Modal show={checkingFeasibility} onHide={() => {setCheckingFeasibility(false)}} centered={true}>
        <div className="btn btn-white btn-block">
          <Loading images={images}/>
        </div>
      </Modal>
       {/* Modal Set Reschedule */}
       <Modal show={modalReschedule} onHide={() => modal("closeReschedule")} centered={true}>
          <div className="modal-body p-box pb-0">
            <div className="heading pt-sm-down-4">
              <h3>Reschedule Installation</h3>
              <p>Select a new time and date. You only have 2 chance to reschedule your installation</p>
            </div>
            <section className="mb-4">
              <div className="subheading mb-3">
                <p>Set Schedule</p>
              </div>
             {
            dateSlots === undefined
              ? 
              <>
                <Slider {...carousel}>
                  {recommend.map((value, index)=>{
                    let sumDelay = index+1;
                    return (
                      <div className={"carousel-item animated fadeInUp delayp" + sumDelay} key={index}>
                        <section className="card-skeleton">
                          <figure className="card-image loading"></figure>
                        </section>
                      </div>
                    )
                  })}
                </Slider>
                </>
              :
              <Slider {...carouselDate}>
                {carousalCards}
              </Slider>
              }
              <div className="subheading mb-3">
                <p>Estimated Arrival</p>
              </div>
              <div className="list-group">
                <div className="row row-1 mb-0">
                  <div className="col-6">
                    <div id="morningContainer" className={`list-group-item list-check ${morningCheck ? 'active' : ''} ${cardMorningAfternoon[0].show ? '' : 'disabled'}`} onClick={() => { if(cardMorningAfternoon[0].show) estimatedArrival("0") }}>
                      <div className="list-group-item-content">
                        <h5 className="title">{cardMorningAfternoon[0].show ? cardMorningAfternoon[0].text : 'Morning'}</h5>
                        <span className="desc">{cardMorningAfternoon[0].show ? `Technician will come around ${cardMorningAfternoon[0].timeSlot}`: 'Unavailable'}</span>
                        <div className="custom-control custom-radio d-none">
                          <input type="radio" className="custom-control-input" id="morning" name="radio-stacked" required />
                          <label className="custom-control-label" htmlFor="customControlValidation2">morning</label>
                        </div>
                        {/* <label className="input-check-card">
                                          <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                      </label> */}
                      </div>
                    </div>
                  </div>
                  <div className="col-6">
                    <div id="afternoonContainer" className={`list-group-item list-check ${afternoonCheck ? 'active' : ''} ${cardMorningAfternoon[1].show ? '' : 'disabled'}`} onClick={() => { if(cardMorningAfternoon[1].show) estimatedArrival("1") }}>
                      <div className="list-group-item-content">
                        <h5 className="title">{cardMorningAfternoon[1].show ? cardMorningAfternoon[1].text : 'Afternoon'}</h5>
                        <span className="desc"> {cardMorningAfternoon[1].show ? `Technician will come around ${cardMorningAfternoon[1].timeSlot}`: 'Unavailable'}</span>
                        <div className="custom-control custom-radio d-none">
                          <input type="radio" className="custom-control-input" id="afternoon" name="radio-stacked" required />
                          <label className="custom-control-label" htmlFor="customControlValidation3">afternoon</label>
                        </div>
                        {/* <label className="input-check-card">
                                          <input type="radio" className="custom-control-input" id="customControlValidation2" name="radio-stacked" required />
                                      </label> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <p className="help-block">Time range is the Estimated is the estimated arrival time. You will receive a notification when the technician is heading to your location</p>
              <div className="btn-placeholder inline">
                <Link to="#" className="btn btn-transparent btn-block" onClick={() => modal("closeReschedule")}>Cancel</Link>
                <Link to="#" className={`btn btn-primary btn-block ${!selectedCard || isButtonLoading ? 'disabled': ''}`} onClick={(e) => { e.preventDefault(); if(!isButtonLoading) {setModalReschedule(false); setShowConfirmationReschedule(true);} } } >{isButtonLoading ? <Loading images={images}/> : "Reschedule"}</Link>
              </div>
            </section>
            </div>
        </Modal>
        {/* Reschedule confirmation dialog */}
        <Modal show={showConfirmationReschedule} centered={true} onHide={() => this.trigger('modal')} className="modal-centered">
        <div className="box box-main mb-0">
          <section>
            <h1>Confirm Reschedule</h1>
            <p>Are you sure to reschedule the installation?</p>
          </section>
          <section>
              <div className="row">
                <div className="col-6">
                    <button className="btn btn-primary btn-block" onClick={ e => reScheduleTechnicianClick(e)}>Yes</button>
                  </div>
                <div className="col-6">
                  <button className="btn btn-transparent btn-block" onClick={e => { e.preventDefault(); setModalReschedule(true); setShowConfirmationReschedule(false);}}>No</button>
                </div>
              </div>
            </section>
        </div>
      </Modal>
      {/* Model for not reschedulable */}
      <Modal show={modalNotReschedule.show} onHide={() => modal("close")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                <img className="header-img" src={images.common['grfx_grfx_status_failed.png']} alt="Icon Failed" />
              </header>
              <section>
                <h1>{modalNotReschedule.reason}</h1>
                <p>{modalNotReschedule.message}</p>
              </section>
              <Link to="#" className="btn btn-primary btn-block mb-3" onClick={() => modal("close")}>Close</Link>
                <a href="tel:121" className="btn btn-transparent btn-block" onClick={() => modal("close")}>Contact Us</a>
            </div>
          </div>
        </Modal>
        {/* Model for booking exhausted */}
        <Modal show={attemptExhausted} onHide={() => modal("close")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                <img className="header-img" src={images.common['grfx_grfx_status_failed.png']} alt="Icon Failed" />
              </header>
              <section>
                <h1>Sorry you can't reschedule anymore</h1>
                <p>You have exceeded reschedule installation limit. Please contact our customer service for further information</p>
              </section>
              <Link to="#" className="btn btn-primary btn-block mb-3" onClick={() => modal("close")}>Close</Link>
                <a href="tel:121" className="btn btn-transparent btn-block" onClick={() => modal("close")}>Contact Us</a>
            </div>
          </div>
        </Modal>
        {/* Modal Session Time Out */}
        <Modal show={modalServiceUnavailable} onHide={() => setModalServiceUnavailable(false)} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0">
              <section>
                <h1>Service Error</h1>
                <p>A service error has occured while processing your request</p>
              </section>
              <div className="btn-placeholder inline bottom">
                <Link to="#" className="btn btn-primary btn-block" onClick={e => {e.preventDefault(); setModalServiceUnavailable(false);}}>Ok</Link>
              </div>
            </div>
          </div>
        </Modal>
        {/* Modal */}
        <Modal show={modalContact} onHide={() => modal('close')} centered={true}>
          <div className="modal-body p-box pb-0">
            <div className="heading pt-sm-down-4">
              <h3>Add a second contact person</h3>
              <p>
                Our technician will contact this person if you're not available
              </p>
            </div>
            <section className="mb-4">
              <Form>
                <Form.Group>
                  <Form.Label>Full Name</Form.Label>
                  <Form.Control
                    type="text"
                    id="name"
                    placeholder="Contact's full name"
                    value={inputs.name}
                    onChange={handleChange}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Mobile Number</Form.Label>
                  <Form.Control
                    type="text"
                    id="mobileNumber"
                    placeholder="Enter mobile number"
                    value={inputs.mobileNumber}
                    onChange={handleChange}
                  />
                </Form.Group>
                <div className="btn-placeholder">
                  <div className="row row-2">
                    <div className="col-6">
                      <Link
                        to="#"
                        className="btn btn-transparent btn-block"
                        onClick={() => modal("close")}
                      >
                        Cancel
                      </Link>
                    </div>
                    <div className="col-6">
                      <Link
                        to="#"
                        className={"btn btn-primary btn-block " + disabled}
                        onClick={() => modal("close")}
                      >
                        Save
                      </Link>
                    </div>
                  </div>
                </div>
              </Form>
            </section>
          </div>
        </Modal>
    </section>
  )
}

export default InstallationDetail