import React, { Component } from 'react';
import { Link } from "react-router-dom";
import ShopInternet from '../ShopInternet';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import { getToken } from '../../utils/storage';
import TrackVisibility from "react-on-screen";

class ShopInternetUpgradeToFiber extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    const { brand, common, sample, modal } = this.context;
    const images = { brand, common, sample };
    const navigateTo = '/shop/internet/upgrade-to-fiber/confirm';

    return (
      <TextContext.Consumer>{(context)=>{
        const { shopText } = context;
        return (
          <section>
            <ShopInternet upgradeToFiberActive="active" shopPrice="Rp100.000" shopPriceFixed="Rp100.000" btnText="Order" linkTo={navigateTo} />
            <SectionWithBanner images={images} shopText={shopText}/>
            <TrackVisibility once offset={100}>
              <SectionText shopText={shopText} />
            </TrackVisibility>
            <TrackVisibility once offset={350}>
              <SectionFeatures images={images} modal={modal} />
            </TrackVisibility>
          </section>
        )
      }}

      </TextContext.Consumer>
    );
  }
}

export default ShopInternetUpgradeToFiber;

const SectionWithBanner = ({ images, shopText }) => {
  return (
    <div className="py-main bg-cover-section upgrade-to-fiber content-center">
      <div className="container">
        <div className="row row-4">
          <div className="col-md-6 col-8 content-center-left">
            <div className="heading mb-2">
              <h3 className="animated fadeInUp delayp5">{shopText.shopInternetUpgradeFiberBanner.title}</h3>
              <p className="mb-0 animated fadeInUp delayp6">{shopText.shopInternetUpgradeFiberBanner.desc}</p>
              <div className="btn-placeholder mt-0 mb-2 animated fadeInUp delayp7">
                <Link to="/shop/internet/upgrade-to-fiber/confirm" className="btn btn-link pl-0">{shopText.shopInternetUpgradeFiberBanner.btn}</Link>
              </div>
            </div>
          </div>
          <div className="col-6 d-none d-md-block">
            <div className="content-center" style={{ height: "100%" }}>
              <img src={images.common["banner_bnr_internet_fiber.png"]} className="img-fluid rounded animated fadeInUp delayp7" alt="Indonesia map" />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const SectionText = ({isVisible, shopText}) => {
  const effect = isVisible ? "fadeInUp" : "";
  return (
    <div className="py-main bg-white">
      <div className="container">
        <p className={"mb-0 w-md-up-50 animated " + effect + " delayp1"}>
        {shopText.shopInternetUpgradeFiberBanner.about}
        </p>
      </div>
    </div>
  );
};

const SectionFeatures = ({ images, modal, isVisible }) => {
  const effect = isVisible ? "fadeInUp" : "";
  return (
    <div className="py-main-sm bg-white">
      <div className="container">
        <div className="row row-4">
          <div className={"col-md-6 col-12 mb-3 animated " + effect + " delayp1"}>
            <div className="subheading">
              <p>Features</p>
            </div>
            <div className="list-group mb-0">
              <Link
                to="#"
                className="list-group-item transparent"
              >
                <img
                  src={images.common["grfx_grfx_line_fiber_1.png"]}
                  className="img-fluid w-75px"
                  alt="Budget Icon"
                />
                <div className="list-group-item-content">
                  <h5 className="title">Unlimited high speed internet</h5>
                  <span className="desc">
                    Get up to 100Mbps high speed internet
                    </span>
                </div>
              </Link>
              <Link
                to="#"
                className="list-group-item transparent"
              >
                <img
                  src={images.common["grfx_grfx_line_fiber_1.png"]}
                  className="img-fluid w-75px"
                  alt="Budget Icon"
                />
                <div className="list-group-item-content">
                  <h5 className="title">Automatic seamless connection</h5>
                  <span className="desc">
                    Connect autimatically when your device is in range of a
                    hotspot
                    </span>
                </div>
              </Link>
              <Link
                to="#"
                className="list-group-item transparent"
              >
                <img
                  src={images.common["grfx_grfx_line_fiber_1.png"]}
                  className="img-fluid w-75px"
                  alt="Budget Icon"
                />
                <div className="list-group-item-content">
                  <h5 className="title">Connect up to 5 devices</h5>
                  <span className="desc">
                    Use the same username for up to 5 devices
                    </span>
                </div>
              </Link>
            </div>
          </div>
          <div className={"col-md-6 col-12 animated " + effect + " delayp2"}>
            <div className="subheading">
              <p>How To Activate</p>
            </div>
            <div className="milestone">
              <div className="milestone-item active">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">Activate from myIndiHome app</h5>
                  <span className="desc">
                    click the button below to activate wifi.id. You will get a
                    username and password
                    </span>
                </div>
              </div>
              <div className="milestone-item active">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">
                    Add a device by adding ther MAC Address on myIndiHome
                    </h5>
                  <Link to="#" className="">
                    How to find my MAC Address
                    </Link>
                </div>
              </div>
              <div className="milestone-item active last-child">
                <div className="milestone-item-img">
                  <div className="dot"></div>
                </div>
                <div className="milestone-item-content">
                  <h5 className="title">
                    Login to wifi.id seamless wifi hotspot with your username
                    and password
                    </h5>
                  <span className="desc">
                    Connect to a wifi.id hotspot and login to start using
                    wifi.id!
                    </span>
                </div>
              </div>
            </div>
            <div className="btn-placeholder">
              {getToken() ?
                (<Link to="/shop/internet/upgrade-to-fiber/confirm" className="btn btn-primary block-sm">Activate</Link>) :
                (<button className="btn btn-primary block-sm" onClick={() => modal('modalSubscribe')}>Activate</button>)}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
