import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { Form, Modal } from "react-bootstrap";
import { PageContext } from '../../context/PageContext';
import { newPassword } from '../../services';
import { getUserData } from '../../utils/storage';

const ProfileChangePassword = props => {
  const { history } = props;
  const { trigger, navCase, pageCase } = useContext(PageContext);
  const [modal, setModal] = useState(false);
  const [inputs, setInputs] = useState({
    alert: {
      password: true,
      newPassword: true,
    },
    icon: {
      password: false,
      newPassword: false,
    },
    touched: {
      password: false,
      newPassword: false,
    },
    password: '',
    newPassword: '',
  });

  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);

  const handleInputChange = e => {
    e.persist();
    const { name, value } = e.target;

    setInputs(inputs => ({
      ...inputs,
      [name]: value,
      alert: { ...inputs.alert, [name]: !(/[A-Za-z]+/.test(value) && /[0-9]+/.test(value) && value.length >= 6) },
      touched: { ...inputs.touched, [name]: true } }));
  };

  const handleSubmit = e => {
    e.preventDefault();

    newPassword({ email: getUserData().email, password: inputs.password, newPassword: inputs.newPassword })
      .then(({ data }) => {
        data.ok ? history.push('/profile') : setModal(true);
      })
      .catch(() => { setModal(true); });
  }

  const _renderInput = (label, id) => (
    <Form.Group className="form-custom w-preppend">
      <Form.Label>{label}</Form.Label>
      <Form.Control
        className={inputs.alert[id] && inputs.touched[id] ? 'is-invalid' : ''}
        type={inputs.icon[id] ? 'text' : 'password'}
        name={id}
        value={inputs[id]}
        onChange={handleInputChange}
        placeholder="Enter Your Password"
      />
      <div className="invalid-feedback">Min. 6 characters with a mix of letters &amp; numbers</div>
      <span onClick={() => setInputs({ ...inputs, icon: { ...inputs.icon, [id]: !inputs.icon[id] } })} className="preppend-icon" style={{ bottom: inputs.alert[id] && inputs.touched[id] ? '33%' : '15%' }}>
        <i className={inputs.icon[id] ? 'fas fa-eye' : 'fas fa-eye-slash'} />
      </span>
    </Form.Group>
  );

  const config = {
    disabled: inputs.alert.password || inputs.alert.newPassword,
  };

  return (
    <section>
      <div className="container container-xs">
        <div className="box animated fadeInUp">
          <header>
            <div className="header-actions">
              <Link to="/profile" className="btn btn-link">
                <i className="fa fa-arrow-left"></i>
              </Link>
            </div>
          </header>
          <section className="section-header-text">
            <div className="heading">
              <h1>Change Password</h1>
              <p>Enter your current password to change password</p>
            </div>
          </section>
          <section className="py-main-sm pb-0">
            <Form>
              {_renderInput('Current Password', 'password')}
              <div className="btn-placeholder-forgot">
                <Link to="/login/email/forgot-password" className="btn btn-link animated fadeInUp delayp3">Forgot Password</Link>
              </div>
              {_renderInput('New Password', 'newPassword')}
            </Form>
          </section>
          <div className="btn-placeholder bottom">
            <button className={`btn btn-primary btn-block ${config.disabled}`} disabled={config.disabled} type="submit" value="submit" onClick={handleSubmit}>Save Changes</button>
          </div>
        </div>
      </div>
      <Modal show={modal} onHide={() => setModal(false)} className="modal-centered">
        <div className="box box-main mb-0">
          <section>
            <h1>Failed to change password</h1>
            <p>You might have entered wrong password</p>
          </section>
          <button className="btn btn-block w-border" onClick={() => setModal(false)}> try again </button>
        </div>
      </Modal>
    </section>
  );
};

export default ProfileChangePassword;
