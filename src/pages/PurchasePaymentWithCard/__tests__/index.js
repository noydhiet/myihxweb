import React from 'react';
import renderer from 'react-test-renderer';
import PurchasePaymentWithCard from '../index';
import PageContextProvider from '../../../context/PageContext';

jest.mock('react-router-dom');
jest.mock('react-day-picker/DayPickerInput');
jest.mock('../../../utils/requireContext');

const wrapper = props => renderer.create(
  <PageContextProvider>
    <PurchasePaymentWithCard {...props} />
  </PageContextProvider>);

describe('PurchasePaymentWithCard', () => {
  test('render', () => {
    const result = wrapper().toJSON();
    expect(result.type).toBe('section');
  });
});
