import React, { useState, useEffect, useRef, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Form, Col, Overlay, Tooltip } from 'react-bootstrap';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { PageContext } from './../../context/PageContext';

const PurchasePaymentWithCard = (props) => {
  const { trigger, navCase, pageCase, brand, common, sample, modal } = useContext(PageContext);
  const [inputs, setInputs] = useState({
    cardholder: "",
    cardnumber: "",
    cvvcode: ""
  });
  const [tooltips, setShow] = useState(false);
  const target = useRef(null);
  const handleInputChange = (e) => {
    e.persist();
    setInputs(inputs => ({ ...inputs, [e.target.name]: e.target.value }));
  }
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  const handleSubmit = (e) => {
    e.preventDefault();
    modal("rating")
    props.history.push("/purchase/success");
  }
  const configForm = {
    showCcProvider: inputs.cardnumber.length >= 4 ? "block" : "none",
    disabledBtn: inputs.cardholder.length > 0 &&
      inputs.cardnumber.length > 0 &&
      inputs.cvvcode.length > 0 ? "" : "disabled"
  }
  const images = { brand, common, sample };
  return (
    <section>
      <div className="container container-sm">
        <div className="box show-out-box animated fadeInUp">
          <header>
            <div className="header-actions">
              <Link to="/purchase/payment-method" className="btn btn-link">
                <i className="fa fa-arrow-left"></i>
              </Link>
            </div>
          </header>
          <section className="section-header-text">
            <div className="heading">
              <h1>Pay With Card</h1>
              <p>Securely pay with your Visa or Mastercard debit/credit card.</p>
            </div>
          </section>
          <section>
            <div className="subheading">
              Total Due
                            </div>
            <Form onSubmit={handleSubmit}>
              <Form.Group>
                <Form.Label>Cardholder</Form.Label>
                <Form.Control
                  type="text"
                  name="cardholder"
                  placeholder="Enter Cardholder's name"
                  onChange={handleInputChange} value={inputs.cardholder}
                  required
                />
              </Form.Group>
              <Form.Group className="form-custom w-icon-right">
                <Form.Label>Card Number</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Enter 16 digit card number"
                  name="cardnumber"
                  onChange={handleInputChange} value={inputs.cardnumber}
                  required
                />
                <span className="preppend-icon"><img alt="icon" src={images.common["icon_ic_card_visa.png"]} className="img-fluid w-50px" style={{ display: configForm.showCcProvider }} /></span>
              </Form.Group>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Label>Expiration Date</Form.Label>
                  <DayPickerInput style={{ display: "block" }} component={props => <input {...props} className="form-control" placeholder="MM/YY" />} />
                </Form.Group>
                <Form.Group as={Col} className="form-custom w-icon-right">
                  <Form.Label>Cvv Code</Form.Label>
                  <Form.Control
                    type="text"
                    name="cvvcode"
                    placeholder="CVV Code"
                    onChange={handleInputChange} value={inputs.cvvcode}
                  />
                  <span className="preppend-icon" ref={target} onClick={() => setShow(!tooltips)}><img alt="icon" src={images.common["ic_help_red.png"]} className="img-fluid w-20px" /></span>
                </Form.Group>
              </Form.Row>
              <Form.Group className="mb-0">
                <div className="btn-placeholder bottom">
                  <button className={"btn btn-primary btn-block " + configForm.disabledBtn} type="submit" value="submit">Pay</button>
                </div>
              </Form.Group>
              <Overlay target={target.current} show={tooltips} placement="bottom">
                {props => (
                  <Tooltip id="overlay-example" {...props}>
                    CVV card is 3-digits security code, usually located on the back of your card
                                    </Tooltip>
                )}
              </Overlay>
            </Form>
          </section>
        </div>
      </div>
    </section>
  )
}

export default PurchasePaymentWithCard;
