import React, { useEffect, useContext, useState } from 'react';
import { Modal } from 'react-bootstrap';
import { PageContext } from '../../context/PageContext';

const AcsReboot= () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const [cond, setCond] = useState({
        rebootStatus: false
    })
    const triggerPage = (mode) => {
        switch(mode) {
            case "reboot-success":
                setCond({
                    rebootStatus: !cond.rebootStatus
                });
                break;

            case "close":
                setCond({
                    rebootStatus: false
                });
                break;

            default:
                return null
        }
    }
    const images = { brand, common, sample } ;
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
            <div className="container container-sm">
                <div className="box box-main animated fadeInUp">
                    <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                    <img className="header-img" src={images.common['grfx_acs.png']} alt="Icon Success" />
                    </header>
                    <section className="animated fadeInUp delayp2">
                        <h1>Reboot Modem</h1>
                        <p>If you’re having issues with your connection, a modem reboot may help. You will be disconnected from all services for a few minutes.</p>
                        <p>If you’re having issues with your connection, a modem reboot may help. You will be disconnected from all services for a few minutes.</p>
                    </section>
                    <div className="btn-placeholder bottom animated fadeInUp delayp3">
                        <button className="btn btn-primary btn-block hidden">Reboot Modem</button>
                        <button className="btn btn-primary btn-block disabled">Rebooting...Please Waiting</button>
                    </div>
                </div>
            </div>
            <Modal show={cond.rebootStatus} onHide={() => triggerPage('close')}>
                <div className="modal-body p-box">
                    <div className="text-center mb-3">
                        <img src={images.common["ic_header_check.png"]} className="img-fluid w-200px" alt="icon"/>
                    </div>
                    <div className="heading text-center animated fadeInUp delayp1 pt-sm-down-4">
                        <h3>Reboot Complete</h3>
                        <p>Did that solve your problem?</p>
                    </div>
                    <div className="btn-placeholder">
                        <div className="row">
                            <div className="col-md-6">
                                <button className="btn btn-transparent btn-block" onClick={() => triggerPage('close')}>No</button>
                            </div>
                            <div className="col-md-6">
                                <button className="btn btn-primary btn-block" onClick={() => triggerPage('close')}>Yes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
        </section> 
    )
}

export default AcsReboot;