import React, { useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { PageContext } from './../../context/PageContext';

const PersonalData = ({ history }) => {
  const { trigger, navCase, pageCase, brand, common, sample, imgKtp, setPersonalData, imgSignature, checkToken } = useContext(PageContext);     
  const images = { brand, common, sample };
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  const upload = ({ target }) => {
    const { files, name } = target;
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      const contextName = name === 'e-ktp' ? 'imgKtp' : 'imgSignature';
      setPersonalData(contextName, reader.result);
      localStorage.setItem(contextName, reader.result);
      history.push(`/personal-data/review-${name}`);
    });
    reader.readAsDataURL(files[0]);
  }
  // const btnDisabled = (imgKtp && imgSignature) ? '' : ' disabled';
  const btnDisabled = imgSignature ? '' : ' disabled';
  (!checkToken() && history.push('/login:personal-data'));

  return (
    <section>
      <div className="container container-sm">
        <div className="box animated fadeInUp">
          <header className="header-light white w-icon">
            <div className="header-actions">
              <Link to="/home" className="btn btn-link"><i className="fa fa-arrow-left"></i> </Link>
              <Link to="/home" className="btn btn-link"><i className="fa fa-question-circle"></i> </Link>
            </div>
            <img className="header-img" src={images.common["graphic_ktp.png"]} alt="Icon Success" />
            <div className="heading animated fadeInUp delayp1">
              <small className="text-uppercase help-block">Step 1 0f 2</small>
              <h1>Upload ID</h1>
              <p>Please verify your identity to confirm & secure your account. This will only take a few minutes.</p>
              <div className="list-group list-group-dark">
                <div className="list-group-item">
                  <img src={images.common["icon_ic_selection_time.png"]} alt="timer-logo" className="w-50px img-fluid" />
                  <div className="list-group-item-content">
                    <p className="text-uppercase help-block mb-0">Complete By</p>
                    <h4 className="text-primary mb-0">23:59:59</h4>
                    <p className="desc">to avoid order cancellation</p>
                  </div>
                </div>
              </div>
            </div>
          </header>
          <section className="animated fadeInUp delayp2">
            <div className="list-group w-btn-upload">
              <label className="list-group-item w-arrow">
                <img src={images.common[imgKtp ? 'icon_ic_done.png' : 'ic_id_ktp.png']} className="img-fluid w-50px" alt="People Icon" />
                <div className="list-group-item-content">
                  <h5 className="title">e-KTP</h5>
                </div>
                <span className="btn-upload">{imgKtp ? 'change' : 'upload'}</span>
                <input name="e-ktp" type="file" onChange={upload} accept="image/*" className="form-file-upload" />
              </label>
              <label className="list-group-item w-arrow">
                <img src={images.common[imgSignature ? 'icon_ic_done.png' : 'ic_id_ktp.png']} className="img-fluid w-50px" alt="People Icon" />
                <div className="list-group-item-content">
                  <h5 className="title">KTP</h5>
                </div>
                <span className="btn-upload">upload</span>
                <input name="signature" type="file" onChange={upload} accept="image/*" className="form-file-upload" />
              </label>
              <label className="list-group-item w-arrow">
                <img src={images.common["ic_id_passport.png"]} className="img-fluid w-50px" alt="People Icon" />
                <div className="list-group-item-content">
                  <h5 className="title">Passport</h5>
                </div>
                <span className="btn-upload">upload</span>
                <input name="signature" type="file" onChange={upload} accept="image/*" className="form-file-upload" />
              </label>
              <label className="list-group-item w-arrow">
                <img src={images.common["ic_id_drivers.png"]} className="img-fluid w-50px" alt="People Icon" />
                <div className="list-group-item-content">
                  <h5 className="title">Driver's License</h5>
                </div>
                <span className="btn-upload">{imgSignature ? 'change' : 'upload'}</span>
                <input name="signature" type="file" onChange={upload} accept="image/*" className="form-file-upload" />
              </label>
            </div>
          </section>
          <div className="btn-placeholder bottom">
            <Link to="/purchase/confirm-order" className={"btn btn-primary btn-block" + (btnDisabled || '')}>Next</Link>
            <Link to="/purchase/confirm-order" className="btn btn-block">Do This Later</Link>
          </div>
        </div>
      </div>
    </section>
  )
}

PersonalData.defaultProps = {
  history: () => { },
};

export default PersonalData;
