import React from 'react';
import renderer from 'react-test-renderer';
import PersonalData from '../';
import PageContextProvider from '../../../context/PageContext';

jest.mock('react-router-dom');
jest.mock('../../../utils/requireContext');

const wrapper = props => renderer.create(
  <PageContextProvider>
    <PersonalData {...props} />
  </PageContextProvider>);

describe('PersonalData', () => {
  test('render', () => {
    const result = wrapper().toJSON();
    expect(result.type).toBe('section');
  });
});
