import React, { useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { PageContext } from './../../context/PageContext';
import moment from 'moment';

const FisikTicketReOpenSuccess = (props) => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
        window.onpopstate = onBackButtonEvent;
    }, [trigger, navCase, pageCase]);
    const { match } = props;
    const onBackButtonEvent = (e) => {
        e.preventDefault();
        props.history.replace('/help');
    }
    const date_time = match.params.date_time;
    const elements = date_time.split('--', 2);
    const selectedDate = moment(elements[0].replace(':',''), 'DD-MM-YYYY').format('dddd, DD MMMM YYYY');
    const time = elements[1];
    const images = { brand, common, sample } ;
    return (
      <section>
        <div className="container container-sm">
            <div className="box box-main animated fadeInUp">
                <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
                </header>
                <section className="animated fadeInUp delayp2">
                    <h1>Reschedule Success!</h1>
                    <p>Your new installation schedule is</p>
                    <div className="text-box">
                        <h5 className="title">{selectedDate}</h5>
                        <p className="desc">Estimated arrival time {time}</p>
                    </div>
                </section>
                <div className="btn-placeholder bottom animated fadeInUp delayp3">
                    <Link to="/home" className="btn btn-primary btn-block">Back To Home</Link>
                </div>
            </div>
        </div>
      </section>
  )
}

export default FisikTicketReOpenSuccess