import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from '../../context/PageContext';

const ShopInternetCloudStorageSuccess = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = { brand, common, sample } ;
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase])
    return (
        <section>
            <div className="container container-sm">
                <div className="box box-main animated fadeInUp">
                    <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                    <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
                    </header>
                    <section className="animated fadeInUp delayp2">
                        <h1>50GB IndiHome Cloud Storage is Active</h1>
                        <p>Subscription will active within 1x24 hours. You can access IndiHome Cloud from the app or website withh MyIndiHome e-mail & password.</p>
                    </section>
                    <div className="btn-placeholder bottom animated fadeInUp delayp3">
                        <div className='text-center'>
                            <p className="help-block">Need any help? <span className="text-primary font-weight-bold">Contact Us</span></p>
                        </div>
                        <Link to="/home" className="btn btn-primary btn-block">Back To Home</Link>
                    </div>
                </div>
            </div>
       </section>
  )
}

export default ShopInternetCloudStorageSuccess;