import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import { PageContext } from './../../context/PageContext';

const ShopInternetPackageWifiIdSeamlessSuccess = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = { brand, common, sample } ;
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg"); 
     }, [trigger, navCase, pageCase]);
     const [ cond, setCond ] = useState({
         modal: false
     });
     const triggerPage = (mode) => {
         switch(mode) {
             case "openModal":
                 setCond({
                     modal: !cond.modal
                 });
                 break;

           

            default:
                return null
   
         }
     }
     return (
         <section>
             <div className="container container-sm">
                 <div className="box box-main animated fadeInUp">
                     <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                     <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
                     </header>
                     <section className="animated fadeInUp delayp2">
                         <h1>wifi.id has been activated!</h1>
                         <p>Register your first device to start using wifi.id. Make sure you're connected to wifi.id hotspot</p>
                     </section>
                     <div className="btn-placeholder bottom animated fadeInUp delayp3">
                         <Link to="/shop/internet/wifi-id-seamless/register-device" className="btn btn-primary btn-block">Register your first device</Link>
                     </div>
                 </div>
             </div>
             <Modal show={cond.modal} onHide={()=>this.triggerPage('close')}>
                <div className="modal-body p-box">
                    <div className="heading text-center animated fadeInUp delayp1 pt-sm-down-4">
                        <h3>Exit without regitering your first device?</h3>
                        <p>You won't be able to use wifi.id until you register a device. Are you sure you want to do this later?</p>
                    </div>
                    <div className="btn-placeholder">
                        <div className="row">
                            <div className="col-6">
                                <Link to="/home" className="btn btn-transparent btn-block">Yes</Link>
                            </div>
                            <div className="col-6">
                                <Link to="#" className="btn btn-primary btn-block" onClick={triggerPage("close")}>No</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
         </section>
     )
}

export default ShopInternetPackageWifiIdSeamlessSuccess;