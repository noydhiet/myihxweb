import React, { Component } from 'react';
import { CardNotif } from '../../include/CardPackage';
import ContainerBase from '../../include/ContainerBase';
import OtpInput from '../../include/OtpInput';
import { register2fo, userActivation, sendOtp, signUp } from '../../services';
import { checkOtpIsBlocked, setOtpBlockedTime, setBlockedNumber, getBlockedNumber, OTP_CHANNEL } from '../../utils/storage';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import { Modal } from 'react-bootstrap';

const TIME_IN_SECONDS = 179

export default class RegisterOtp extends Component {
static contextType = PageContext;
  state = {
    registrationData: JSON.parse(localStorage.getItem('registrationData')),
    alert: getBlockedNumber() === JSON.parse(localStorage.getItem('registrationData')).mobile && checkOtpIsBlocked(),
    attempt: getBlockedNumber() === JSON.parse(localStorage.getItem('registrationData')).mobile && checkOtpIsBlocked() ? 0 : 3,
    channel: localStorage.getItem(OTP_CHANNEL),
    notif: '',
    timer: TIME_IN_SECONDS,
    showProgressbar: true
  }

  componentDidMount() {
    const { history, location: { state }} = this.props;
    this.context.trigger(1);
    this.context.pageCase("w-bg");
    if(state) {
      const { channel, email, mobile } = state;
      if(mobile !== getBlockedNumber() || (mobile === getBlockedNumber() && !checkOtpIsBlocked())) {
        sendOtp(channel, mobile, email)
        .then(({ data }) => {
          if (data.status === 200 || data.status === 201) {
            this.myInterval = setInterval(() => {
              this.setState( prevState => { return { timer: prevState.timer - 1 } })
            }, 1000);
            this.setState({showProgressbar: false});
          } else if (data.status === 402) {
            this.trigger('returnBlocked');
            this.setState({showProgressbar: false});
          } else {
            this.setState({ showProgressbar: false, notif: data.message, attempt: 0 });
            setTimeout( () => {
              this.setState({notif: ''});
            }, 5000)
          }
        })
        .catch(({ message }) => {
          this.setState({ timer: 0, showProgressbar: false, notif: message })
          setTimeout( () => {
            this.setState({notif: ''});
          }, 3000)
        });
      } else {
        this.setState({showProgressbar: false});
      }
    } else {
      history.push('/');
    }
  }

  componentWillUnmount() {
    clearInterval(this.myInterval);
  }

  verifyOTP(otp) {
    const { channel, registrationData } = this.state;
    const { mobile, email } = registrationData;
    register2fo(channel, mobile, email, otp)
      .then(({ data }) => { 
        if (data.ok) this.signUp()
        else if (data.status === 402) this.trigger('returnBlocked');
        else this.trigger('returnFailed'); })
      .catch(() => { this.trigger('returnFailed'); });
  }

  resendOtp() {
    const { channel, registrationData } = this.state;
    const { email, mobile } = registrationData;
    sendOtp(channel, mobile, email)
      .then(({ data }) => {
        if (!data.ok) {
          this.setState({ notif: data.message, showProgressbar: false, attempt: 0 }); 
          setTimeout( () => {
            this.setState({notif: ''});
          }, 5000)
        }
        else {
          this.setState({showProgressbar: false})
          this.myInterval = setInterval(() => {
            this.setState( prevState => { return {timer: prevState.timer - 1} })
          }, 1000);
        }
      }).catch(({ message }) => { 
        this.setState({ notif: message, showProgressbar: false, timer: 0 }); 
        setTimeout( () => {
          this.setState({notif: ''});
        }, 5000)
      });
  }

  signUp() {
    const { registrationData } = this.state;

    signUp({ ...registrationData, userRole: 'generalUser', status:'status' })
      .then(({ data }) => { data.ok ? this.activateUser(data.data.tokenUserActive) : this.trigger('registerFailed'); })
      .catch(() => { this.trigger('registerFailed'); })
  }

  activateUser(userActiveToken) {
    const { registrationData } = this.state;
    const { email } = registrationData;

    userActivation({ email, userActiveToken })
      .then(({ data }) => { this.trigger(data.ok ? 'returnSuccess' : 'activateFailed') })
      .catch(() => { this.trigger('activateFailed'); });
  }


  trigger = (mode) => {
    const { history } = this.props;

    switch (mode) {
      case 'activateFailed':
        this.setState({ notif: 'Failed to activate account' });
        break;

      case 'registerFailed':
        this.setState({ notif: 'Failed to register account' });
        break;

      case 'returnBlocked':
        const { registrationData } = this.state;
        const { mobile } = registrationData;
        this.setState({ alert: true, attempt: 0 });
        setOtpBlockedTime();
        setBlockedNumber(mobile)
        break;

      case 'returnFailed':
        this.setState({ alert: true, attempt: this.state.attempt - 1 });
        break;

      case 'returnSuccess':
        history.push('/register');
        break;

      case 'stopTimer':
        clearInterval(this.myInterval);
        break;

      case 'startTimer':
        this.setState({ timer: TIME_IN_SECONDS, showProgressbar: true });
        this.resendOtp();
        
        break;

      default:
        break;
    }
  }

  render() {
    const { alert, attempt, registrationData, notif, timer, channel } = this.state;
    const { mobile } = registrationData;
    let displayTimer = true
    let displayResend = false
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    //Timer
    if (timer === 0) {
      this.trigger('stopTimer');
      displayTimer = false
      displayResend = true
    }
    const Loading = () => {
      return (
        <span><i className="fa fa-circle-notch fa-spin"></i></span>
      )
    }
    const mobile4Digits = mobile.slice(-4).padStart(mobile.length,"•");
    const config = {
      displayTimer: displayTimer && attempt > 0 ? 'block' : 'none',
      displayResend: displayResend && attempt > 0 ? 'block' : 'none',
      displayInvalid: {
        display: alert ? 'block' : 'none'
      },
      notifActive: notif ? 'active' : '',
      min: `${Math.floor(timer / 60)}`.padStart(2, '0'),
      sec: `${timer % 60}`.padStart(2, '0'),
    };
    return (
      <>
      <TextContext.Consumer>{(context)=>{
        const { registerText: { otp } } = context;
        const { title, subtitleSMS, subtitleWhatsApp, btnResendTimer, btnResendCode, alertWrong, alertExceeded } = otp;
        const containerProps = {
          image: { alt: 'Icon OTP', src: images.common['ic_header_verification.png'], },
          subtitle: (('sms' === channel ? subtitleSMS : subtitleWhatsApp) + mobile4Digits),
          title,
        };
        const alert = {
          message: attempt > 0 ? alertWrong : alertExceeded,
        }
        return (
          <>
            <ContainerBase {...containerProps}>
              <form className="needs-validation" id="form">
                <div className="otp-code-container animated fadeInUp delayp2">
                  <OtpInput disabled={displayResend || attempt <= 0} names={['otp1', 'otp2', 'otp3', 'otp4']} onSubmit={this.verifyOTP.bind(this)} />
                  <div className="invalid-feedback text-center" style={config.displayInvalid}>{alert.message}</div>
                </div>
              </form>
              <div className="countdown-timer text-center animated fadeInUp delayp3">
                <div className="btn btn-link btn-resend-timer disabled" style={{ display: config.displayTimer }}>{btnResendTimer} <span id="timer"> {config.min}:{config.sec} </span></div>
                <div className="btn-placeholder btn-resend">
                  <button className="btn btn-link" style={{ display: config.displayResend, margin: 'auto' }} onClick={() => this.trigger("startTimer")}>{btnResendCode}</button>
                </div>
              </div>
            </ContainerBase>
            <CardNotif active={config.notifActive} textTop={notif} />
          </>
        )
      }}</TextContext.Consumer>
      <Modal show={this.state.showProgressbar} onHide={ () => {}} centered={true}>
        <div className="btn btn-white btn-block">
          <Loading />
        </div>
      </Modal>
      </>
    )
  }
}

RegisterOtp.defaultProps = {
  pageCase: () => {},
  trigger: () => {},
};
