import React from 'react';
import renderer from 'react-test-renderer';
import RegisterOtp from '../index';

jest.mock('../../../services', () => ({
  register2fo: (number, otp) => new Promise((resolve, reject) => {
    if (number === '081234567890' && otp === '1234') resolve('success');
    else reject('failed');
  }),
  sendOtp: (number) => new Promise((resolve, reject) => {
    if (number === '081234567890') resolve('success');
    else reject('failed');
  }),
}));
jest.mock('react-router-dom');

const instance = props => renderer.create(<RegisterOtp images={{ common: {} }} {...props} />).getInstance();

describe('RegisterOtp', () => {
  test('render', () => {
    const wrapper = instance();
    expect(wrapper.render().type).toBe(React.Fragment);
    
    wrapper.state.timer = 0;
    wrapper.trigger = jest.fn();
    wrapper.render();
    expect(wrapper.trigger).toHaveBeenCalledWith('stopTimer');
  });

  test('trigger', () => {
    const history = { push: jest.fn() };
    const wrapper = instance({ history });

    wrapper.trigger('returnFailed');
    expect(wrapper.state.alert).toBe(true);

    wrapper.trigger('returnSuccess');
    expect(history.push).toHaveBeenCalledWith('/register/success');
  });
});
