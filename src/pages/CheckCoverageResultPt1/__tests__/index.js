import React from 'react';
import renderer from 'react-test-renderer';
import CheckCoverageResultPt1 from '../index';
import PageContextProvider from '../../../context/PageContext';

jest.mock('react-router-dom');
jest.mock('../../../utils/requireContext');

const wrapper = props => renderer.create(
  <PageContextProvider>
    <CheckCoverageResultPt1 {...props} />
  </PageContextProvider>);

describe('CheckCoverageResultPt1', () => {
  test('render', () => {
    const result = wrapper().toJSON();
    expect(result.type).toBe('section');
  });
});
