import React, { useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { PageContext } from './../../context/PageContext';

const PT1 = () => {
  const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
  useEffect(()=>{
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  const images = { brand, common, sample } ;
  return (
    <section>
      <div className="container container-sm">
        <div className="box box-main animated fadeInUp">
          <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
            <div className="header-actions">
              <Link to="/check-coverage/map" className="btn btn-link">
                <i className="fa fa-arrow-left"></i>
              </Link>
            </div>
            <img
              className="header-img"
              src={images.common["ic_header_check.png"]}
              alt="Icon Success"
            />
          </header>
          <section className="animated fadeInUp delayp2">
            <h1>IndiHome service is available in your area!</h1>
            <p>
              Congratulation! Your location is within IndiHome are coverage.
              Browse packages and activate it in a few easy steps!
              </p>
          </section>
          <div className="btn-placeholder bottom animated fadeInUp delayp3">
            <Link
              to="/shop/internet/package"
              className="btn btn-primary btn-block"
            >
              Browse Package
              </Link>
            <Link to="/shop/internet/package" className="btn btn-block">
              find the best package for me
              </Link>
          </div>
        </div>
      </div>
    </section>
  )
}

PT1.defaultProps = {
  navCase: () => { },
  pageCase: () => { },
  trigger: () => { },
};

export default PT1;
