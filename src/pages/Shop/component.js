import React, { Component } from "react";
import { Link } from "react-router-dom";
import { NavMobile } from './../../include/NavMobile'
import { CardContent, CardContentStacked } from './../../include/CardPackage'
import Slider from "react-slick";
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import TrackVisibility from "react-on-screen";
import Skeleton from 'react-loading-skeleton';

class Shop extends Component {
  static contextType = PageContext;
  state = {
    load: false
  }
  componentDidMount() {
    this.context.trigger(2);
    // this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
    this.context.footerCase("w-nav-bottom");
    setTimeout(()=>{
      this.setState({
        load: true
      })
    }, 3000)
  }
  componentWillUnmount() {
    this.context.footerCase("");
  }
  render() {
    const { common, brand, sample, condition } = this.context;
    const images = { common, brand, sample };
    const shopMenu = [
      {
        link: "/shop/internet/package",
        className: "card card-shop red animated fadeInUp delayp4",
        img: "ic_shop_internet_white.png",
        alt: "Internet"
      },
      {
        link: "/shop/tv/minipack",
        className: "card card-shop blue animated fadeInUp delayp5",
        img: "ic_shop_internet_white.png",
        alt: "TV"
      },
      {
        link: "/shop/call",
        className: "card card-shop green animated fadeInUp delayp5",
        img: "ic_shop_call_white.png",
        alt: "Call"
      },
      {
        link: "/shop/more/indihome-cloud",
        className: "card card-shop black animated fadeInUp delayp5",
        img: "ic_shop_more_white.png",
        alt: "More"
      },
    ]
    return (
      <TextContext.Consumer>{(context)=>{
        const { shopText, generalText } = context;
        return (
          <div>

          <NavMobile condition={condition} shopPage="active" />
          <div className="cover cover-responsive bg-wave red">
            <div className="cover-responsive-img landing-pages-cover">
              {/* <div className="cover-responsive-overlay"></div> */}
            </div>
            <div className="container">
              <div className="cover-content">
                <h3 className="cover-title animated fadeInUp delayp1">
                  {shopText.coverShopTitle}
              </h3>
                <p className="cover-text animated fadeInUp delayp2">
                  {shopText.coverShopDesc}
              </p>
                <div className="btn-placeholder animated fadeInUp delayp3">
                  <Link to="#" className="btn btn-light">
                    Upgrade Package
                </Link>
                </div>
              </div>
            </div>
          </div>
  
          <section className="py-main bg-white">
            <div className="container">
              <div className="row row-1 row-md-up-2">
                {this.state.load ? 
                <>
                  {shopMenu.map((value, index)=>{
                    return (
                      <div className="col-3" key={index}>
                        <Link
                          to={value.link}
                          className={value.className}
                        >
                          <img
                            src={images.common[`${value.img}`]}
                            className="img-fluid"
                            alt={value.alt}
                          />
                          <h5>{value.alt}</h5>
                          <i className="far fa-angle-right"></i>
                        </Link>
                      </div>
                    )
                  })}
                </>
                  :
                <>
                  {shopMenu.map((value, index)=>{
                    return (
                      <div className="col-3" key={index}>
                        <section className="card-skeleton animated fadeInUp delayp5">
                          <figure className="card-image loading"></figure>
                        </section>
                      </div>
                    )
                  })}
                  </>
                }
              </div>
            </div>
          </section>
          <TrackVisibility once offset={550}>
            <SectionProduct images={images} load={this.state.load} generalText={generalText}/>
          </TrackVisibility>
          <TrackVisibility once offset={400}>
            <SubscribeWatchSection images={images} load={this.state.load} generalText={generalText}/>
          </TrackVisibility>
          <TrackVisibility once offset={400}>
            <SectionPackageFinder images={images} load={this.state.load} generalText={generalText}/>
          </TrackVisibility>
        </div>
        )
      }}

      </TextContext.Consumer>
    );
  }
}

export default Shop;

class SectionProduct extends Component {
  render() {
    const { images, load, isVisible, generalText } = this.props;
    const config = {
      carousel4th: {
        className: "primary-carousel view-carousel w-text",
        dots: false,
        infinite: false,
        arrows: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1.5,
              slidesToScroll: 1.5
            },
          }
        ]
      },
      carousel3rd: {
        className: "primary-carousel view-carousel mb-3",
        dots: false,
        infinite: false,
        arrows: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 3
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1.1,
              slidesToScroll: 1.1
            }
          }
        ]
      }
    };
    const newOffering = [
      {
        link: "/shop/internet/package/details",
        img: "offer1.png",
        mainText: "Save 30% by subscribing to 2 minipacks for 1 year!"
      },
      {
        link: "/shop/internet/package/details",
        img: "offer2.png",
        mainText: "Save 30% by subscribing to 2 minipacks for 1 year!"
      },
      {
        link: "/shop/internet/package/details",
        img: "offer3.png",
        mainText: "Save 30% by subscribing to 2 minipacks for 1 year!"
      },
      {
        link: "/shop/internet/package/details",
        img: "offer1.png",
        mainText: "Save 30% by subscribing to 2 minipacks for 1 year!"
      },
      {
        link: "/shop/internet/package/details",
        img: "offer2.png",
        mainText: "Save 30% by subscribing to 2 minipacks for 1 year!"
      },
      {
        link: "/shop/internet/package/details",
        img: "offer3.png",
        mainText: "Save 30% by subscribing to 2 minipacks for 1 year!"
      }
    ];
    const effect = isVisible ? "fadeInUp" : "";
    const recommend = ["bnr_recommend01.png", "bnr_recommend02.png", "bnr_recommend03.png"];
    return (
      <div className="shop-product bg-gray-50 py-main">
        <div className="container">
          <section className="animated fadeInUp delayp5">
            {load ? 
            <>
              <div className="subheading">
                <p className={"animated " + effect + " delayp1"}>{generalText.recommended}</p>
              </div>
              <Slider {...config.carousel3rd}>
                {recommend.map((value, index)=>{
                  let sumDelay = index+1;
                  return (
                    <div className="carousel-item" key={index}>
                      <CardContent
                        link="#"
                        img={value}
                        className={"subheading animated " + effect + " delayp" + sumDelay}
                        images={images}
                      />
                    </div>
                  )
                })}
              </Slider>
            </>
              :
            <>
              <Skeleton width={150}/>
              <Slider {...config.carousel3rd}>
                {recommend.map((value, index)=>{
                  return (
                    <div className="carousel-item" key={index}>
                      <section className="card-skeleton animated fadeInUp delayp5">
                        <figure className="card-image loading"></figure>
                      </section>
                    </div>
                  )
                })}
              </Slider>
            </>
            }
          </section>
          <section className="animated fadeInUp delayp6">
            {load ? 
              <>
              <div className="subheading">
                <p className={"animated " + effect + " delayp1"}>{generalText.newOffer}</p>
              </div>
              <Slider {...config.carousel4th}>
                {newOffering.map((value, index)=>{
                  let sumDelay = index+1;
                  return (
                    <div className="carousel-item" key={index}>
                      <CardContentStacked
                        link={value.link}
                        className={"animated " + effect + " delayp" + sumDelay}
                        img={value.img}
                        mainText={value.mainText}
                        images={images}
                      />
                    </div>
                  )
                })}
              </Slider>
              </>
              :
              <>
              <Skeleton width={150} />
              <Slider {...config.carousel4th}>
                {newOffering.map((value, index)=>{
                  let sumDelay = index+1;
                  return (
                    <div className="carousel-item" key={index}>
                      <section className={"card-skeleton animated fadeInUp delayp" + sumDelay}>
                        <figure className="card-image loading"></figure>
                      </section>
                    </div>
                  )
                })}
              </Slider>
              </>
            }
          </section>
        </div>
      </div>
    )
  }
}

const SubscribeWatchSection = ({ load, images, isVisible, generalText }) => {
  const config = {
    carousel2nd: {
      className: "primary-carousel view-carousel",
      dots: false,
      infinite: false,
      arrows: false,
      speed: 500,
      slidesToShow: 2,
      slidesToScroll: 2,
      swipe: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: false,
            swipe: false,
            dots: false
          }
        },
        {
          breakpoint: 736,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: false,
            dots: false,
            swipe: true
          }
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 2.2,
            slidesToScroll: 2.2,
            infinite: false,
            swipe: true
          }
        }
      ]
    }
  };
  const dataCarousel = ["poster1.png", "poster2.png", "poster3.png", "poster4.png", "poster5.png"];
  const effect = isVisible ? "fadeInUp" : "";
  return (
    <div className="home-subscribe-watch bg-white py-main">
      <div className="container">
        <div className={"subheading animated " + effect + " delayp1"}>
          <p>{generalText.movieSubcribe.movieSubcribeCaption}</p>
        </div>
        <div className="row">
          <div className="col-md-6 content-center-left">
            <div className="d-none-mobile">
              <ul
                className={"icon-list mb-3 animated " + effect + " delayp2"}
              >
                <li className="icon-list-item">
                  <img
                    alt=""
                    src={images.common["logo_iflix.jpg"]}
                    className="img-fluid w-75px"
                  />
                </li>
                <li className="icon-list-item">
                  <img
                    alt=""
                    src={images.common["logo_hooq.jpg"]}
                    className="img-fluid w-100px"
                  />
                </li>
                <li className="icon-list-item">
                  <img
                    alt=""
                    src={images.common["logo_catchplay.jpg"]}
                    className="img-fluid w-125px"
                  />
                </li>
              </ul>
              <div className={"heading animated " + effect + " delayp3"}>
                <h3 className="">
                {generalText.movieSubcribe.movieSubcribeTitle}
                </h3>
                <p className="">
                {generalText.movieSubcribe.movieSubcribeDesc}
                </p>
              </div>
              <div
                className={"btn-placeholder animated " + effect + " delayp4"}
              >
                <button className="btn btn-primary">{generalText.movieSubcribe.movieSubcribeBtn}</button>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            {load ? 
                <Slider {...config.carousel2nd}>
                  {dataCarousel.map((value, index)=>{
                    let sumDelay = index+1;
                    return (
                      <div className="carousel-item" key={index}>
                        <CardContent
                          link="#"
                          img={value}
                          images={images}
                          className={"animated " + effect + " delayp" + sumDelay}
                        />
                      </div>
                    )
                  })}
                </Slider>
                :
                <Slider {...config.carousel2nd}>
                  {dataCarousel.map((value, index)=>{
                    return (
                      <div className="carousel-item" key={index}>
                         <section className="card-skeleton animated fadeInUp delayp5">
                          <figure className="card-image loading"></figure>
                        </section>
                      </div>
                    )
                  })}
                </Slider>
            }
            
          </div>
          <div className="col-md-12">
            <div className="btn-placeholder d-block-mobile mb-3">
              <button className="btn btn-primary btn-block">{generalText.movieSubcribe.movieSubcribeBtn}</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const SectionPackageFinder = ({ images, isVisible }) => {
  const coverStyle = {
    background: `url(${images.common["bnr_blue_2nd.png"]}) no-repeat center`,
    backgroundSize: "cover",
    border: "none"
  };
  const effect = isVisible ? "fadeInUp" : "";
  return (
    <section className="bg-white py-main-sm animated fadeInUp delayp8">
      <div className="col-lg-8 mx-auto py-main-sm">
        <div className={"list-group list-group-banner animated " + effect + " delayp1"}>
          <Link
            to="/shop/internet/package-finder"
            className="list-group-item light w-arrow w-bg-cover"
            style={coverStyle}
          >
            <img alt="" src={images.common["grfx_find.png"]} className="w-75px" />
            <div className="list-group-item-content">
              <h5 className="title w-md-up-75 mb-0">Perlu bantuan memilih paket internet? <br className="d-none d-sm-block" />Coba Package Finder Kami</h5>
            </div>
            {/* <button className="btn btn-light d-none d-md-block">Coba Sekarang</button> */}
          </Link>
        </div>
      </div>
    </section>
  )
}
