import React, { useEffect, useContext } from 'react'
import ContainerBase from '../../include/ContainerBase';
import { PageContext } from './../../context/PageContext';

const LoginMethodGetVerificationCode = () => {
    const { trigger, navCase, pageCase, brand, sample, common } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    const images = { brand, sample, common };
    const containerProps = {
        image: { alt: 'Icon Verification', src: images.common['ic_header_verification.png'], },
        title: 'Choose Verification Method',
      };
    return (
    <ContainerBase {...containerProps}>
      <div className="list-group">
        <button className="list-group-item w-arrow animated fadeInUp delayp2">
          <img src={images.common['ic_whatsapp.png']} className="img-fluid w-25px" alt="Icon WA" />
          <span>By <strong>Whatsapp</strong> to *******678</span>
        </button>
        <button className="list-group-item w-arrow animated fadeInUp delayp2">
          <img src={images.common['ic_form_sms.png']} className="img-fluid w-25px" alt="Icon SMS" />
          <span>By <strong>SMS</strong> to ********678 </span>
        </button>
        <button className="list-group-item w-arrow animated fadeInUp delayp2">
          <img src={images.common['icon_ic_email.png']} className="img-fluid w-25px" alt="Icon SMS" />
          <span>By <strong>Email</strong> to ***abc@gmail.com </span>
        </button>
      </div>
    </ContainerBase>
    )
}

export default LoginMethodGetVerificationCode;