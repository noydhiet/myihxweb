import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const ShopInternetUpgradeSpeedSuccess = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const images = { brand, common, sample } ;
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
            <div className="container container-sm">
                <div className="box box-main animated fadeInUp">
                    <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                    <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
                    </header>
                    <section className="animated fadeInUp delayp2">
                        <h1>Your speed has been upgraded to 50Mbps!</h1>
                        <p>Enjoy your faster internet!Upgrade may take 1 x 24 hours to take effect</p>
                    </section>
                    <div className="btn-placeholder bottom animated fadeInUp delayp3">
                        <Link to="/home" className="btn btn-primary btn-block">Back To Home</Link>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default ShopInternetUpgradeSpeedSuccess;