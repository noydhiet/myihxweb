import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';
import { Form } from 'react-bootstrap';
import { getToken } from '../../utils/storage';

const ProfileChangeEmail = () => {
  const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
  const [cond, setCond] = useState({
    email: ""
  });
  const images = { brand, common, sample };
  const handleInputChange = (e) => {
    e.persist();
    setCond(inputs => ({ ...inputs, [e.target.name]: e.target.value }));
  }
  useEffect(() => {
    trigger(2);
    navCase("w-md-up-block");
    pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  if (!getToken()) {
    window.location.href = '/login';
    return
  }

  const disabled = cond.email.length < 1 ? "disabled" : "";
  return (
    <div className="container container-xs">
      <div className="box animated fadeInUp">
        <header className="header-light white w-icon">
          <div className="header-actions">
            <Link to="/profile" className="btn btn-link"><i className="fa fa-arrow-left"></i></Link>
            <Link to="/" className="btn btn-link"><i className="fa fa-question-circle"></i></Link>
          </div>
          <img className="header-img" src={images.common["ic_header_email.png"]} alt="img" />
          <div className="heading animated fadeInUp delayp1">
            <h1>Change Email</h1>
          </div>
        </header>

        <Form>
          <section className="animated fadeInUp delayp2">
            <Form.Group>
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                type="email"
                name="email"
                value={cond.email}
                onChange={handleInputChange}
              />
            </Form.Group>
          </section>

          <div className="btn-placeholder bottom">
            {/* <button className={"btn btn-primary btn-block " + disabled} type="submit" value="submit">Login</button> */}
            <Link to="/purchase/link-aja/otp" className={"btn btn-primary btn-block " + disabled}>Next</Link>
          </div>
        </Form>
      </div>
    </div>
  )
}

export default ProfileChangeEmail;
