import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';
const ShopInternetIndihomeStudyConfirm = () => {
    const { trigger, navCase, pageCase } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
            <div className="container container-sm">
                <div className="box animated fadeInUp">
                    <header>
                    <div className="header-actions">
                        <Link to="/shop/internet/indihome-study" className="btn btn-link">
                        <i className="fa fa-arrow-left"></i>
                        </Link>
                    </div>
                    </header>
                    <section className="section-header-text">
                        <div className="heading">
                            <h1>Confirm order</h1>
                            <p>Please make sure your package details are right before confirming</p>
                        </div>
                    </section>
                    <section className="py-main-sm pb-0">
                        <div className="subheading">
                            <p>Product</p>
                        </div>
                        <Link
                            to="#"
                            className="card card-packages w-arrow animated fadeInUp delayp2"
                            >
                            <div className="card-header">
                                <div className="badges-list">
                                    <div className="badges badge-red left">
                                    <h3>100</h3>
                                    <span>Mbps</span>
                                    </div>
                                </div>
                                <div>
                                    <h4>IndiHome Study</h4>
                                    <p>Package of e-book, explanation video, and try out questions</p>
                                </div>
                            {/* <ul className="list-unstyled">
                                <li>IndiHome Cloud Storage</li>
                            </ul> */}
              </div>
              <div className="card-body w-btn">
                <h3>
                  Rp85.000<small>/month</small>
                </h3>
              </div>
            </Link>
          </section>
          <section className="py-main-sm pb-0">
            <div className="subheading">
              <p>Fee Detail</p>
            </div>
            <div className="form-group">
              <label>Total</label>
              <h4>Rp85.000</h4>
              <small className="help-block">Include 10% tax. This amount will be billed after installation, and abonemen fee will be billed every month later</small>
            </div>
            <div className="btn-placeholder bottom">
              <div className="row">
                <div className="col-6">
                  <Link to="/shop/internet/indihome-study" className="btn btn-transparent btn-block">Cancel</Link>
                </div>
                <div className="col-6">
                  <Link to="/shop/internet/indihome-study/success" className="btn btn-primary btn-block"> <span>Confirm</span> </Link>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
  )
}

export default ShopInternetIndihomeStudyConfirm;