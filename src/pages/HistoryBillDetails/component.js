import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import { PageContext } from './../../context/PageContext';

const HistoryBillDetails = () => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    const [ cond, setCond ] = useState({
        modalDetail: false
    });
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    const triggerPage = () => {
        setCond({
            modalDetail: !cond.modalDetail
        })
    }
    const paymentDetail = [
        {
            title: "100MB Internet Package",
            price: "Rp1.890.000"
        },
        {
            title: "30MB Extra speed quota",
            price: "Rp150.000"
        },
        {
            title: "Minipack Dinasty",
            price: "Rp100.000"
        },
        {
            title: "10% Tax",
            price: "Rp120.000"
        },
        {
            title: "Admin Fee",
            price: "Rp1.000"
        }
    ]
    const images = { brand, common, sample } ;
    return (
        <section>
            <div className="container container-sm">
                <div className="box box-main text-left animated fadeInUp">
                    <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                        <img className="header-img" src={images.common['ic_header_success.png']} alt="Icon Success" />
                    </header>
                    <section className="mb-2 animated fadeInUp delayp2">
                        <h1>Transaction Successful</h1>
                        <p className="small-text">Monday, 10th June 2019</p>
                        <p className="small-text">Transaction ID 12341234</p>
                    </section>
                    <section className="py-main-sm pb-0">
                        <div className="subheading">
                            <p>Paymeny Details</p>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-12">
                                <div className="form-group">
                                    <label className="help-block">Billing Period</label>
                                    <p>July 2019</p>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="form-group">
                                    <label className="help-block">Payment Method</label>
                                    <p>LinkAja</p>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="py-main-sm pb-0">
                        <div className="subheading mb-0">
                            <p>Billing Details</p>
                        </div>
                        <div className="list-group list-group-activity mb-0">
                            <div className="list-group-item w-arrow">
                                <div className="list-group-item-content mb-0">
                                <p className="title">10 Mbps IndiGamer TriplePlay</p>
                                <small className="help-block">
                                    10Mbps + 92 channels + 100 minutes
                                </small>
                                <Link
                                    to="#"
                                    className="btn btn-link px-0 py-0"
                                    onClick={triggerPage}
                                >
                                    Lihat Detail
                                </Link>
                                </div>
                            </div>
                            <div className="list-group-item w-arrow">
                                <div className="list-group-item-content mb-0">
                                <p className="title">30 Mbps Extra Speed</p>
                                <small className="help-block">
                                    3 days package * 1-3 July 2019
                                </small>
                                </div>
                            </div>
                            <div className="list-group-item w-arrow">
                                <div className="list-group-item-content mb-0">
                                <p className="title">Minipack IndiKids Lite</p>
                                <small className="help-block">3 channels</small>
                                <Link
                                    to="#"
                                    className="btn btn-link px-0 py-0"
                                    onClick={triggerPage}
                                >
                                    Lihat Detail
                                </Link>
                                </div>
                            </div>
                        </div>
                        <div className="list-group list-group-transparent mb-0">
                            <div className="list-group-item p-0">
                                <div className="list-group-item-content">
                                    <div className="subheading">
                                        <p>Total Paid</p>
                                    </div>
                                    <h2>Rp2.650.000</h2>
                                    <small className="help-block">Includes 10% tax</small>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            {/* Modal - Unlimited Internet Quota */}
        <Modal show={cond.modalDetail} onHide={triggerPage}>
            <div className="modal-body p-box">
                <Link to="#" className="close animated fadeInUp" onClick={triggerPage}>
                <span><i className="far fa-times text-primary"></i></span>
                </Link>
                <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
                <h3>Payment Details</h3>
                </div>
                <div className="list-group list-group-activity mb-0 animated fadeInUp delayp2 mb-0">
                    {paymentDetail.map(function(value, index){
                        return (
                            <Link to="/history/bill/details" className="list-group-item pr-0" key={index}>
                                <div className="list-group-item-content mb-0">
                                <div className="list-group-item-content-w-status">
                                    <p className="title">{value.title}</p>
                                    <p className="text-status">{value.price}</p>
                                </div>
                                </div>
                            </Link>
                        )
                    })}
                </div>
                <hr />
                <div className="list-group list-group-activity mb-0 animated fadeInUp delayp2 mb-0">
                    <Link to="/history/bill/details" className="list-group-item pr-0">
                        <div className="list-group-item-content mb-0">
                        <div className="list-group-item-content-w-status">
                            <p className="title">Total Pembayaran</p>
                            <p className="text-status">Rp2.060.000</p>
                        </div>
                        </div>
                    </Link>
                </div>
                <div className="btn-placeholder">
                    <button className="btn btn-primary btn-block">Ok</button>
                </div>
            </div>
        </Modal>
        </section>
    )
}

export default HistoryBillDetails