import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import { CardContent } from './../../include/CardPackage'
import { Player } from 'video-react';
import Slider from "react-slick";
import ShopTv from './../ShopTv';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import { getToken } from '../../utils/storage';
import TrackVisibility from "react-on-screen";

class ShopTvApps extends Component {
  static contextType = PageContext;
  state = {
    modalVideo: false
  }
  componentDidMount() {
      this.context.trigger(2);
      this.context.navCase("w-md-up-block");
      this.context.pageCase("l-bg");
      this.context.footerCase("w-nav-bottom");
  }
  componentWillUnmount() {
      this.context.footerCase("");
  }
  triggerPage = (mode) => {
      switch(mode) {
        case "openVideo":
          this.setState({
            modalVideo: !this.state.modalVideo
          });
        break;

      case "close":
        this.setState({
          modalVideo: false
        });
        break;

      default:
        return null;
    }
  }
  render() {
    const { modalVideo } = this.state;
    const { brand, common, sample, modal } = this.context;
    const images = { brand, common, sample };
    return (
      <TextContext.Consumer>{(context)=>{
        const { shopText, generalText } = context;
        return (

          <div>
          <ShopTv appsPage="active" />
          <SectionWithBanner images={images} shopText={shopText}/>
          <TrackVisibility once offset={200}> 
            <SectionHooq images={images} triggerPage={this.triggerPage} modal={modal} generalText={generalText}/>
          </TrackVisibility>
          <SectionIflix images={images} triggerPage={this.triggerPage} modal={modal}  generalText={generalText}/>
          <SectionCatchplay images={images} triggerPage={this.triggerPage} modal={modal}  generalText={generalText}/>
          <Modal show={modalVideo} onHide={() => this.triggerPage('close')} className="modal-video">
            <div className="modal-body p-box">
              <Link className="close animated fadeInUp" to="#" onClick={() => this.triggerPage('close')}>
                <span><i className="far fa-times text-primary"></i></span>
              </Link>
              <Player
                playsInline
                poster="/assets/poster.png"
                src="https://media.w3.org/2010/05/sintel/trailer_hd.mp4"
              />
              <div className="text-placeholder">
                <h3>Watch Avengers: EndGame and more on HOOQ</h3>
                <p>Subsribe now to watch awesome international & local box office movies!</p>
              </div>
              <div className="btn-placeholder">
                <button className="btn btn-primary btn-block">Subscribe Now</button>
                <button className="btn btn-block text-white">More Details</button>
              </div>
            </div>
          </Modal>
        </div>
        )
      }}

      </TextContext.Consumer>
    )
  }
}
export default ShopTvApps;

const SectionWithBanner = ({ images, isVisible, shopText }) => {
  return (
    <div className="py-main pb-0 bg-cover-section apps content-center">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-8 content-center-left">
            <div className="heading mb-2">
              <h3 className="animated fadeInUp delayp5">{shopText.shopTvApps.title}</h3>
              <p className="animated fadeInUp delayp6">
              {shopText.shopTvApps.desc}
                </p>
            </div>
          </div>
          <div className="col-6 d-none d-md-block content-center">
            <img
              src={images.common["banner_bnr_tv_apps.png"]}
              className="img-fluid animated fadeInUp delayp7 rounded"
              alt="Speed Boost"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

class SectionHooq extends Component {
  render() {
    const { images, modal, generalText } = this.props;
    const config = {
      carousel2nd: {
        className: "primary-carousel view-carousel",
        dots: false,
        infinite: false,
        arrows: false,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2,
        swipe: false,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              infinite: false,
              swipe: false,
              dots: false
            }
          },
          {
            breakpoint: 736,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              infinite: false,
              dots: false,
              swipe: true
            }
          },
          {
            breakpoint: 575,
            settings: {
              slidesToShow: 3.2,
              slidesToScroll: 3.2,
              infinite: false,
              swipe: true
            }
          }
        ]
      }
    };
    const nextScreen = !getToken() ? '#' : '/shop/tv/apps/hooq/details';
    return (
      <div className="home-subscribe-watch py-main-lg animated fadeInUp delayp7">
        <div className="container">
          <div className="row">
            <div className="col-md-6 content-center-left">
              <div className="heading heading-w-link">
                <img
                  alt=""
                  src={images.common["logo_hooq.jpg"]}
                  className="img-fluid mb-3 w-100px"
                />
                <h3 className="d-md-none">Rp36.000<small>/month</small></h3>
              </div>
              <div className="heading">
                <h3 className="">
                  {generalText.hooq.title}
                        </h3>
                <p className="d-none d-md-block">{generalText.hooq.desc}</p>
              </div>
              <div className="btn-placeholder d-none d-md-block mb-3">
                <Link to={ nextScreen } className="btn btn-primary" onClick={() => { if(!getToken()) modal("modalSubscribe") } }>Subscribe</Link>
              </div>
            </div>
            <div className="col-md-6">
              <Slider {...config.carousel2nd}>
                <div className="carousel-item" onClick={()=>this.props.triggerPage("openVideo")}>
                  <CardContent
                    link="#"
                    img="poster1.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item" onClick={()=>this.props.triggerPage("openVideo")}>
                  <CardContent
                    link="#"
                    img="poster2.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item" onClick={()=>this.props.triggerPage("openVideo")}>
                  <CardContent
                    link="#"
                    img="poster3.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item" onClick={()=>this.props.triggerPage("openVideo")}>
                  <CardContent
                    link="#"
                    img="poster4.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item" onClick={()=>this.props.triggerPage("openVideo")}>
                  <CardContent
                    link="#"
                    img="poster5.png"
                    images={images}
                  />
                </div>
              </Slider>
            </div>
            <div className="col-md-12">
              <div className="btn-placeholder d-block-mobile mb-3">
                <Link to={ nextScreen } onClick={() =>  { if(!getToken()) { modal("modalSubscribe") } }} className="btn btn-primary btn-block">Subscribe</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class SectionIflix extends Component {
  render() {
    const { images, modal, generalText } = this.props;
    const config = {
      carousel2nd: {
        className: "primary-carousel view-carousel",
        dots: false,
        infinite: false,
        arrows: false,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2,
        swipe: false,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              infinite: false,
              swipe: false,
              dots: false
            }
          },
          {
            breakpoint: 736,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              infinite: false,
              dots: false,
              swipe: true
            }
          },
          {
            breakpoint: 575,
            settings: {
              slidesToShow: 3.2,
              slidesToScroll: 3.2,
              infinite: false,
              swipe: true
            }
          }
        ]
      }
    };
    const nextScreen = !getToken() ? '#' : '/shop/tv/apps/iflix/details';
    return (
      <div className="home-subscribe-watch bg-white py-main-lg animated fadeInUp delayp7">
        <div className="container">
          <div className="row">
            <div className="col-md-6 content-center-left">
              <div className="heading heading-w-link">
                <img
                  alt=""
                  src={images.common["logo_iflix.jpg"]}
                  className="img-fluid w-100px"
                />
                <h3 className="d-md-none">Free</h3>
              </div>
              <div className="heading">
                <h3 className="">
                 {generalText.iflix.title}
                        </h3>
              </div>
              <div className="btn-placeholder d-none d-md-block mb-3">
                <Link to={ nextScreen } onClick={() => { if(!getToken()) { modal("modalSubscribe") } }} className="btn btn-primary">Subscribe</Link>
              </div>
            </div>
            <div className="col-md-6">
              <Slider {...config.carousel2nd}>
                <div className="carousel-item" onClick={()=>this.props.triggerPage("openVideo")}>
                  <CardContent
                    link="#"
                    img="poster1.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item" onClick={()=>this.props.triggerPage("openVideo")}>
                  <CardContent
                    link="#"
                    img="poster2.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item" onClick={()=>this.props.triggerPage("openVideo")}>
                  <CardContent
                    link="#"
                    img="poster3.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item" onClick={()=>this.props.triggerPage("openVideo")}>
                  <CardContent
                    link="#"
                    img="poster4.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item" onClick={()=>this.props.triggerPage("openVideo")}>
                  <CardContent
                    link="#"
                    img="poster5.png"
                    images={images}
                  />
                </div>
              </Slider>
            </div>
            <div className="col-md-12">
              <div className="btn-placeholder d-block-mobile mb-3">
                <Link to={ nextScreen } onClick={() => { if(!getToken()) { modal("modalSubscribe") } }} className="btn btn-primary btn-block">Subscribe</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class SectionCatchplay extends Component {
  render() {
    const { images, modal, generalText } = this.props;
    const config = {
      carousel2nd: {
        className: "primary-carousel  view-carousel",
        dots: false,
        infinite: false,
        arrows: false,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2,
        swipe: false,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              infinite: false,
              swipe: false,
              dots: false
            }
          },
          {
            breakpoint: 736,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              infinite: false,
              dots: false,
              swipe: true
            }
          },
          {
            breakpoint: 575,
            settings: {
              slidesToShow: 3.2,
              slidesToScroll: 3.2,
              infinite: false,
              swipe: true
            }
          }
        ]
      }
    };
    const nextScreen = !getToken() ? '#' : '/shop/tv/apps/catchplay/details';
    return (
      <div className="home-subscribe-watch py-main-lg animated fadeInUp delayp7">
        <div className="container">
          <div className="row">
            <div className="col-md-6 content-center-left">
              <div className="heading heading-w-link">
                <img
                  alt=""
                  src={images.common["logo_catchplay.jpg"]}
                  className="img-fluid w-100px"
                />
                <h3 className="d-md-none">{generalText.catchplay.desc}</h3>
              </div>
              <div className="heading">
                <h3 className="">
                  {generalText.catchplay.title}
                        </h3>
                <p className="d-none d-md-block">{generalText.catchplay.desc}</p>
              </div>
              <div className="btn-placeholder d-none d-md-block mb-3">
                <Link to={ nextScreen } onClick={() => { if(!getToken()) { modal("modalSubscribe") } }} className="btn btn-primary">Subscribe</Link>
              </div>
            </div>
            <div className="col-md-6">
              <Slider {...config.carousel2nd}>
                <div className="carousel-item" onClick={()=>this.props.triggerPage("openVideo")}>
                  <CardContent
                    link="#"
                    img="poster1.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item" onClick={()=>this.props.triggerPage("openVideo")}>
                  <CardContent
                    link="#"
                    img="poster2.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item" onClick={()=>this.props.triggerPage("openVideo")}>
                  <CardContent
                    link="#"
                    img="poster3.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item" onClick={()=>this.props.triggerPage("openVideo")}>
                  <CardContent
                    link="#"
                    img="poster4.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item" onClick={()=>this.props.triggerPage("openVideo")}>
                  <CardContent
                    link="#"
                    img="poster5.png"
                    images={images}
                  />
                </div>
              </Slider>
            </div>
            <div className="col-md-12">
              <div className="btn-placeholder d-block-mobile mb-3">
                <Link to={ nextScreen } onClick={() => { if(!getToken()) { modal("modalSubscribe") } }} className="btn btn-primary btn-block">Subscribe</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}