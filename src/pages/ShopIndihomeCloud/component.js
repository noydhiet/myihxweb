import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { NavMobile } from './../../include/NavMobile'
import Slider from "react-slick";
import ShopMorePackage from './../ShopMorePackage';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';

class ShopIndihomeCloud extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
    this.context.footerCase("w-nav-bottom");
  }
  componentWillUnmount() {
    this.context.footerCase("");
  }
  render() {
    const { common, brand, sample, condition, modal, checkToken } = this.context;
    const images = { common, brand, sample };
    return (
      <TextContext.Consumer>{(context)=>{
        const { shopText } = context;
        return (
          <div>
            <NavMobile condition={condition} shopPage="active" />
            <ShopMorePackage indiCloudPage="active" />
            <SectionWithBanner images={images} shopText={shopText}/>
            <SectionText  images={images} shopText={shopText}/>
          </div>

        )
      }}

      </TextContext.Consumer>
    )
  }
}

export default ShopIndihomeCloud;

const SectionWithBanner = ({ images, shopText }) => {
  return (
    <div className="py-main bg-cover-section indihome-cloud content-center">
      <div className="container">
        <div className="row row-4">
          <div className="col-md-6 col-8 content-center-left">
            <div className="heading mb-2">
              <h3 className="animated fadeInUp delayp5">{shopText.shopMoreIndihomeCloudBanner.title}</h3>
              <p className="mb-0 animated fadeInUp delayp6">{shopText.shopMoreIndihomeCloudBanner.desc}</p>
            </div>
            <div className="btn-placeholder mt-2 animated fadeInUp delayp7">
              <div className="row d-none d-md-block">
                <div className="col-lg-5">
                  <Link
                    to="/help"
                    className="btn btn-primary"
                  >
                    {shopText.shopMoreIndihomeCloudBanner.btn}
                                </Link>
                </div>
              </div>
              <Link
                to="/shop/more/indihome-cloud"
                className="btn btn-link light d-md-none"
              >
                {shopText.shopMoreIndihomeCloudBanner.btn}
                            </Link>
            </div>
          </div>
          <div className="col-6 d-none d-md-block">
            <div className="content-center" style={{ height: "100%" }}>
              <img src={images.common["banner_bnr_more_cloud.png"]} className="img-fluid rounded animated fadeInUp delayp7" alt="Indonesia map" />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const SectionText = ({ shopText }) => {;
  return (
    <div className="py-main bg-white">
      <div className="container">
        <div className="subheading">
          <p className="animated fadeInUp delayp1">About this product</p>
        </div>
        <div className="heading w-md-up-50 animated fadeInUp delayp2">
          <p>
              {shopText.shopMoreIndihomeCloudBanner.about}
          </p>
        </div>
      </div>
    </div>
  );
};

