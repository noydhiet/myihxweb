import React, { Component, useState } from "react";
import { Link } from "react-router-dom";
import { Modal } from 'react-bootstrap';
import { PageContext } from './../../context/PageContext';
import TrackVisibility from 'react-on-screen';

class ShopInternetPackageCategoryDetails extends Component {
    static contextType = PageContext;
    componentDidMount() {
        this.context.trigger(2);
        this.context.navCase("w-md-up-block");
        this.context.pageCase("l-bg");
    }
    render() { 
        const { common, brand, sample } = this.context;
        const images = { common, brand, sample };
        return ( 
            <>
                <div className="cover cover-responsive bg-wave red">
                    <div className="cover-responsive-img">
                    {/* <div className="cover-responsive-overlay"></div> */}
                    </div>
                    <div className="container">
                        <div className="cover-content">
                            <h3 className="cover-title animated fadeInUp delayp1">
                            IndiHome Gamer
                            </h3>
                            <p className="cover-text animated fadeInUp delayp2">
                            Rasakan sensai bermain game dengan jaringan internet. Super cepat dengan indiHome Paket Corner
                            </p>
                        </div>
                    </div>
                </div>
                <SectionBenefits images={images}/>
                <TrackVisibility once offset={500}>
                  <SectionPackages images={images}/>
                </TrackVisibility>
                <TrackVisibility once offset={400}>
                  <SectionTnc />
                </TrackVisibility>
            </>
         );
    }
}
 
export default ShopInternetPackageCategoryDetails;

const SectionBenefits = ({images}) => {
    const [cond, setCond] = useState({
        modalBenefits: false
    });
    const triggerPage = (mode) => {
        switch(mode) {
            case "openModalBenefits":
                setCond({
                    modalBenefits: !cond.modalBenefits
                });
                break;

            case "close":
                setCond({
                    modalBenefits: false
                });
                break;

            default:
                return null
        }
    }
    return (
        <section className="py-main-sm">
            <div className="container">
                <div className="subheading animated fadeInUp delayp3">
                    <p>Special Benefits</p>
                </div>
                <div className="list-group mb-0 animated fadeInUp delayp4" onClick={()=>triggerPage("openModalBenefits")}>
                    <Link to="#" className="list-group-item w-arrow">
                        <img src={images.common["graphic_grfx_package_benefit_copy_4.png"]} className="img-fluid w-100px" alt="Budget Icon" />
                        <div className="list-group-item-content">
                            <h5 className="title">Exclusive items, in game currency, double EXP and more!</h5>
                        </div>
                    </Link>
                </div>
            </div>
            <Modal show={cond.modalBenefits} onHide={()=>triggerPage('close')}>
                <div className="modal-body p-box">
                    <Link to="#" className="close animated fadeInUp delayp1" onClick={()=>triggerPage('close')}>
                    <span><i className="far fa-times text-primary"></i></span>
                    </Link>
                    <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
                        <h3>Benefit IndiHome Paket Gamer</h3>
                    </div>
                    <div className="list-group list-group-transparent animated fadeInUp delayp2">
                        <Link to="#" className="list-group-item">
                            <img src={images.common["graphic_grfx_package_benefit_copy_4.png"]} className="img-fluid w-100px" alt="Budget Icon" />
                            <div className="list-group-item-content">
                                <h5 className="title">PUBG Lite</h5>
                                <ul>
                                    <li>Free itemweapon 130 days</li>
                                    <li>Free itemweapon 130 days</li>
                                    <li>Free itemweapon 130 days</li>
                                </ul>
                            </div>
                        </Link>
                        <Link to="#" className="list-group-item">
                            <img src={images.common["graphic_grfx_package_benefit_copy_4.png"]} className="img-fluid w-100px" alt="Budget Icon" />
                            <div className="list-group-item-content">
                                <h5 className="title">Garena Free Fire</h5>
                                <ul>
                                    <li>Free itemweapon 130 days</li>
                                    <li>Free itemweapon 130 days</li>
                                    <li>Free itemweapon 130 days</li>
                                </ul>
                            </div>
                        </Link>
                        <Link to="#" className="list-group-item">
                            <img src={images.common["graphic_grfx_package_benefit_copy_4.png"]} className="img-fluid w-100px" alt="Budget Icon" />
                            <div className="list-group-item-content">
                                <h5 className="title">Arena of Valor</h5>
                                <ul>
                                    <li>Free itemweapon 130 days</li>
                                    <li>Free itemweapon 130 days</li>
                                    <li>Free itemweapon 130 days</li>
                                </ul>
                            </div>
                        </Link>
                        <Link to="#" className="list-group-item">
                            <img src={images.common["graphic_grfx_package_benefit_copy_4.png"]} className="img-fluid w-100px" alt="Budget Icon" />
                            <div className="list-group-item-content">
                                <h5 className="title">Point Blank</h5>
                                <ul>
                                    <li>Free itemweapon 130 days</li>
                                    <li>Free itemweapon 130 days</li>
                                    <li>Free itemweapon 130 days</li>
                                </ul>
                            </div>
                        </Link>
                    </div>
                    <div className="btn-placeholder animated fadeInUp delayp3">
                        <Link to="/faq" className="btn btn-primary btn-block">How To Redeem</Link>
                    </div>
                </div>
            </Modal>
        </section>
    )
}

const SectionPackages = ({images, isVisible}) => {
    const effect = isVisible ? "fadeInUp" : "";
    return (
        <section className="py-main-sm  animated fadeInUp delayp4">
            <div className="container">
                <div className={"subheading animated " + effect + " delayp1"}>
                    <p>Packages</p>
                </div>
                <div className="row">
                <div className="col-lg-4">
                  <Link
                    to="#"
                    className={"card card-packages card-packages-lg animated " + effect + " delayp2"}
                  >
                    <div className="card-header">
                      <div className="badges-list">
                        <div className="badges badge-red left">
                          <h3>10</h3>
                          <span>Mbps</span>
                        </div>
                        <div className="badges badge-green middle">
                          <h3>92</h3>
                          <span>Channels</span>
                        </div>
                        <div className="badges badge-blue right">
                          <h3>100</h3>
                          <span>Minutes</span>
                        </div>
                      </div>
                      <div className="header-text">
                          <h3>Paket BUMN</h3>
                          <p>Internet unlimited, 92 USee TV channels, 100 minutes voice call</p>
                      </div>
                    </div>
                    <div className="card-body w-btn">
                      <div className="card-body-top">
                        <div className="subheading">
                          <p>Triple Play Value</p>
                        </div>
                        <h3>
                          Rp480.000<small> / bln</small>
                        </h3>
                        <div className="content">
                          <p>
                            Perfect for everyday internet users with light daily
                            use
                          </p>
                        </div>
                      </div>
                      <div className="card-body-bottom">
                        <div className="subheading">
                          <p>Bonus Subscriptions</p>
                        </div>
                        <div className="content">
                          <p>
                            Get bonus subscriptions from various apps and games!
                          </p>
                        </div>
                        <img
                          src={images.common["logo_add-ons.jpg"]}
                          className="img-fluid"
                          alt="Logo Add-Ons"
                        />
                      </div>
                      <span className="btn btn-primary">Subscribe</span>
                    </div>
                  </Link>
                </div>
                <div className="col-lg-4">
                  <Link
                    to="#s"
                    className={"card card-packages card-packages-lg animated " + effect + " delayp3"}
                  >
                    <div className="card-header">
                      <div className="badges-list">
                        <div className="badges badge-red left">
                          <h3>20</h3>
                          <span>Mbps</span>
                        </div>
                        <div className="badges badge-green middle">
                          <h3>92</h3>
                          <span>Channels</span>
                        </div>
                        <div className="badges badge-blue right">
                          <h3>100</h3>
                          <span>Minutes</span>
                        </div>
                      </div>
                      <div className="header-text">
                          <h3>Semarak Kebahagiaan</h3>
                          <p>Internet unlimited, 92 USee TV channels, 100 minutes voice call</p>
                      </div>
                    </div>
                    <div className="card-body w-btn">
                      <div className="card-body-top">
                        <div className="subheading">
                          <p>IndiHome Gamer</p>
                        </div>
                        <h3>
                          Rp535.000<small> / bln</small>
                        </h3>
                        <div className="content">
                          <p>
                            Paket dengan berbagai keuntungan di game favorit
                            Anda!
                          </p>
                        </div>
                      </div>
                      <div className="card-body-bottom">
                        <div className="subheading">
                          <p>Bonus Subscriptions</p>
                        </div>
                        <div className="content">
                          <p>
                            Get bonus subscriptions from various apps and games!
                          </p>
                        </div>
                        <img
                          src={images.common["logo_games.jpg"]}
                          className="img-fluid"
                          alt="Logo Games"
                        />
                      </div>
                      <span className="btn btn-primary">Subscribe</span>
                    </div>
                  </Link>
                </div>
                <div className="col-lg-4">
                  <Link
                    to="/shop/internet/package/details"
                    className={"card card-packages card-packages-lg animated " + effect + " delayp4"}
                  >
                    <div className="card-header">
                      <div className="badges-list">
                        <div className="badges badge-red left">
                          <h3>50</h3>
                          <span>Mbps</span>
                        </div>
                        <div className="badges badge-blue right">
                          <h3>201</h3>
                          <span>Channels</span>
                        </div>
                        <div className="badges badge-green middle">
                          <h3>1000</h3>
                          <span>Minutes</span>
                        </div>
                      </div>
                      <div className="header-text">
                          <h3>Premium</h3>
                          <p>Internet unlimited, 92 USee TV channels, 100 minutes voice call</p>
                      </div>
                    </div>
                    <div className="card-body w-btn">
                      <div className="card-body-top">
                        <div className="subheading">
                          <p>IndiHome Premium</p>
                        </div>
                        <h3>
                          Rp535.000<small> / bln</small>
                        </h3>
                        <div className="content">
                          <p>
                            Perfect for everyday internet users with light daily
                            use
                          </p>
                        </div>
                      </div>
                      <div className="card-body-bottom">
                        <div className="subheading">
                          <p>Bonus Subscriptions</p>
                        </div>
                        <div className="content">
                          <p>
                            Get bonus subscriptions from various apps and games!
                          </p>
                        </div>
                        <img
                          src={images.common["logo_add-ons.jpg"]}
                          className="img-fluid"
                          alt="Logo Add-Ons"
                        />
                      </div>
                      <span className="btn btn-primary">Subscribe</span>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
        </section>
    )
}

const SectionTnc = ({isVisible}) => {
    const effect = isVisible ? "fadeInUp" : "";
    return (
        <section className="py-main-sm animated fadeInUp delayp5">
            <div className="container">
                <div className={"subheading animated " + effect + " delayp1"}>
                    <p>Terms & Confitions </p>
                </div>
                <h4 className={"animated " + effect + " delayp2"}>1. Lorem ipsum</h4>
                <p className={"animated " + effect + " delayp3"}>
                    he standard Lorem Ipsum passage, used since the 1500s "Lorem
                    ipsum dolor sit amet, consectetur adipiscing elit, sed do
                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                    enim ad minim veniam, quis nostrud exercitation ullamco laboris
                    nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                    in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                    nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                    sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <h4 className={"animated " + effect + " delayp4"}>2. Lorem ipsum</h4>
                <p className={"animated " + effect + " delayp5"}>
                    he standard Lorem Ipsum passage, used since the 1500s "Lorem
                    ipsum dolor sit amet, consectetur adipiscing elit, sed do
                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                    enim ad minim veniam, quis nostrud exercitation ullamco laboris
                    nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                    in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                    nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                    sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <h4 className={"animated " + effect + " delayp6"}>3. Lorem ipsum</h4>
                <p className={"animated " + effect + " delayp7"}>
                    he standard Lorem Ipsum passage, used since the 1500s "Lorem
                    ipsum dolor sit amet, consectetur adipiscing elit, sed do
                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                    enim ad minim veniam, quis nostrud exercitation ullamco laboris
                    nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                    in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                    nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                    sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <div className={"btn-placeholder animated " + effect + " delayp8"}>
                    <button className="btn btn-primary block-sm">
                        Check Coverage
                    </button>
                </div>
            </div>
        </section>
    )
}