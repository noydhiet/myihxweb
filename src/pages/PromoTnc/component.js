import React, { Component } from 'react';
import Promo from '../Promo';
import { PageContext } from '../../context/PageContext';

class PromoTnc extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    return (
      <div>
        <Promo tnc="active"/>
        <SectionPopular />
      </div>
    )
  }
}

export default PromoTnc;


const SectionPopular = () => {
    return (
        <section className="py-main">
            <div className="container">
                <div className="row">
                    <div className="col-md-10 col-12 mx-auto animated fadeInUp delayp4">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <div className="btn-placeholder">
                            <button className="btn btn-primary d-none d-md-block">Get Offer</button>
                            <button className="btn btn-primary btn-block d-md-none">Get Offer</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
