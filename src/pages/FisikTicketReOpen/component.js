import React, { useEffect, useContext } from 'react';
import { Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const FisikTicketReOpen = (props) => {
    const { trigger, navCase, pageCase, brand, common, sample } = useContext(PageContext);
    useEffect(() => {
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    const images = { brand, common, sample };
    const { match } = props;
    const ticketId = match.params.ticketId;
    console.log('ticketId', ticketId)
    return (
        <section>
        <div className="container container-sm">
            <div className="box box-main animated fadeInUp">
            <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                <img className="header-img" src={images.common['grfx_grfx_sad.png']} alt="Icon Success" />
            </header>
            <section className="animated fadeInUp delayp2">
                <h1>We're sorry to hear that</h1>
                <p>We're sorry to hear that your issue hasn't been solved. it will help if you give a detailed description of the issue you are facing, and our team will help you futher.</p>
            </section>
            <Form>
                <Form.Control
                    as="textarea"
                    rows="3"
                    placeholder="Enter comment or message (optional)"
                />
            </Form>
            <div className="btn-placeholder bottom animated fadeInUp delayp3">
                <Link to={{pathname: '/installation/fisik-ticket', state: {ticketId: ticketId}}} className="btn btn-primary btn-block">Reopen Issue</Link>
            </div>
            </div>
        </div>
        </section>
    )
}

export default FisikTicketReOpen;