import React, { Component } from 'react';
import InboxHeader from '../InboxHeader';
import { PageContext } from '../../context/PageContext';
import { Link } from 'react-router-dom'
import { getInbox, setInboxMsgRead } from '../../services';
import { getUserData } from '../../utils/storage';
import moment from 'moment';

class Inbox extends Component {
  static contextType = PageContext;
  constructor(props) {
    super(props)
    this.state = {
      userData: getUserData(),
      listInbox: [],
      dataInbox: [],
      activeLabel: '',
    }
    this.markAsRead = this.markAsRead.bind(this);
  }
  
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
    this.getUserInbox();
  }
  componentDidUpdate(prevProps) {
    const { match: { params: { type } } } = this.props;
    
    (type !== prevProps.match.params.type) && this.getInboxType();
  }

  getInboxType = () => {
    const { match: { params: { type } } } = this.props;
    const { dataInbox } = this.state;

    switch (type) {
      case 'all':
        this.setState({ listInbox: dataInbox.promoMessages, activeLabel: type })
        break;
      case 'personal':
        this.setState({ listInbox: dataInbox.personalMessages, activeLabel: type })
        break;
      case 'promo':
        this.setState({ listInbox: dataInbox.promoMessages, activeLabel: type })
        break;
    
      default:
        break;
    }
  }

  getUserInbox = () => {
    const { match: { params: { type }}, history} = this.props;
    const { userData } = this.state;
    const { checkToken } = this.context;

    if(!checkToken()) {
      history.push('/login:inbox--all'); 
      return;
    }
    switch (type) {
      case 'all':
        getInbox(userData.userId)
          .then(({data}) => (data.ok && 
          this.setState({ 
            dataInbox: data.data,
            listInbox: data.data.promoMessages,
            activeLabel: type,
          })))
          .catch(err => console.log(err))
        break;
      case 'personal':
        getInbox(userData.userId)
          .then(({data}) => (data.ok && 
          this.setState({ 
            dataInbox: data.data,
            listInbox: data.data.personalMessages,
            activeLabel: type,
          })))
          .catch(err => console.log(err))
        break;
      case 'promo':
        getInbox(userData.userId)
          .then(({data}) => (data.ok && 
          this.setState({ 
            dataInbox: data.data,
            listInbox: data.data.promoMessages,
            activeLabel: type,
          })))
          .catch(err => console.log(err))
        break;
    
      default:
        break;
    }
  }

  markAsRead(index) {
    const { listInbox } = this.state;
    const body = { "checked": true };
    const messageId = listInbox[index].messageId;
    console.log('messageId', messageId)
    setInboxMsgRead(messageId, body).then( response => {
      console.log('setInboxMsgRead', response)
      if(response.data.ok) {
        listInbox[index].isChecked = true;
        this.setState(listInbox);
      }
    }).catch( error => console.error(error));
  }

  render() {
    const { common, brand, sample } = this.context;
    const { listInbox, activeLabel } = this.state;
    const images = { common, brand, sample };

    console.log('this.state.dataInbox', listInbox)
    
    return (
      <div>
        <InboxHeader allPage="active"/>
        <SectionContent listInbox={listInbox} activeLabel={activeLabel} images={images} markAsRead={this.markAsRead}/>
      </div>
    )
  }
}

export default Inbox;

const SectionContent = ({ images, listInbox, activeLabel, markAsRead }) => {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-4 d-none d-md-block">
            <div className="box box-in-cover py-3">
              <div className="list-group list-group-activity mb-0">
                <Link to="/inbox/all" className="list-group-item w-arrow">
                  <div className="list-group-item-content">
                    <p className={"title mb-0 " + (activeLabel==='all' ? "active" : "")}>All</p>
                  </div>
                </Link>
                <Link to="/inbox/personal" className="list-group-item w-arrow">
                  <div className="list-group-item-content mb-0">
                    <p className={"title mb-0 " + (activeLabel==='personal' ? "active" : "")}>Personal</p>
                  </div>
                </Link>
                <Link to="/inbox/promo" className="list-group-item w-arrow">
                  <div className="list-group-item-content mb-0">
                    <p className={"title mb-0 " + (activeLabel==='promo' ? "active" : "")}>Promo</p>
                  </div>
                </Link>
              </div>
              <img alt="" src={images.common["img_placeholder_rectangle.jpg"]} className="img-fluid" />
            </div>
          </div>
          <div className="col-md-8 col-12">
            { listInbox && listInbox.length ? 
            (<div className="box box-in-cover mb-3 mt-sm-down-4 min-h-auto animated fadeInUp">
                <div className="subheading">
                  <p>Active Packages</p>
                </div>
                  {listInbox.map((inbox, index) => (
                    <div key={index} className={`${inbox.isChecked ? 'mb-4' : 'list-group list-group-very-light-pink mb-4'}`} onClick={ e => {e.preventDefault(); !inbox.isChecked && markAsRead(index);} }>
                      <div className="list-group list-group-transparent mb-0">
                        <div className="list-group-item mb-0">
                          <img alt="img" 
                            src={images.common[`avatar_av_notif_${inbox.messageType ==='personal'?'personal':'promo'}.png`]} 
                            className="w-50px"/>
                          <div className="list-group-item-content">
                            <small className="help-block">{moment(inbox.timestamp).format('D MMMM YYYY, HH:mm')}</small>
                            <p className="mb-0">{inbox.header}</p>
                          </div>
                        </div>
                      </div>
                      <img alt="img" src={inbox.imageUrl && (inbox.imageUrl.startsWith("http") || inbox.imageUrl.startsWith("https")) ? inbox.imageUrl : images.common["img_placeholder_rectangle.jpg"]} className="img-fluid" />
                    </div>
                  ))}
              </div>) : 
            (<div className="box min-h-auto animated fadeInUp">
                <div className="row">
                  <div className="col-md-6 col-12 mx-auto">
                    <div className="blank-state">
                      <img alt="img" src={images.common["ic_header_failed.png"]} />
                      <div className="heading">
                        <p className="dark">
                          You don't have any active subscription. <br /> Buy your
                          first package to start enjoying IndiHome!
                        </p>
                    </div>
                    <div className="btn-placeholder">
                      <Link
                        to="/shop/internet/package"
                        className="btn btn-primary"
                      >
                        Browse Package
                        </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>) 
            }
          </div>
        </div>
      </div>
  );
};

