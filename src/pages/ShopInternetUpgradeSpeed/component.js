import React, { Component } from 'react';
import { Link } from "react-router-dom";
import ShopInternet from '../ShopInternet';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import TrackVisibility from "react-on-screen";

class ShopInternetUpgradeSpeed extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    return (
      <TextContext.Consumer>{(context)=>{
        const { shopText } = context;
        return (
          <section>
            <ShopInternet upgradeSpeedActive="active" />
            <SectionWithBanner images={images} shopText={shopText}/>
            <TrackVisibility once offset={200}> 
              <SectionPackages />
            </TrackVisibility>
          </section>
        )
      }}

      </TextContext.Consumer>
    )
  }
}

const SectionWithBanner = ({ images, shopText }) => {
  return (
    <div className="py-main bg-cover-section upgrade-speed content-center">
      <div className="container">
        <div className="row row-4">
          <div className="col-md-6 col-8 content-center-left">
            <div className="heading mb-2">
              <h3 className="animated fadeInUp delayp5">
              {shopText.shopInternetUpgradeSpeedBanner.title}
              </h3>
              <p className="mb-0 animated fadeInUp delayp6">
              {shopText.shopInternetUpgradeSpeedBanner.desc}
              </p>
            </div>
            <div className="btn-placeholder mt-2 animated fadeInUp delayp7">
              <div className="row d-none d-md-block">
                <div className="col-lg-5">
                  <Link to="/shop/internet/upgrade-speed" className="btn btn-primary">{shopText.shopInternetUpgradeSpeedBanner.btn}</Link>
                </div>
              </div>
              <Link to="/shop/internet/upgrade-speed" className="btn btn-link light d-md-none">{shopText.shopInternetUpgradeSpeedBanner.btn}</Link>
            </div>
          </div>
          <div className="col-6 d-none d-md-block">
            <img src={images.common["banner_bnr_internet_speed.png"]} className="img-fluid rounded animated fadeInUp delayp7" alt="Indonesia map" />
          </div>
        </div>
      </div>
    </div>
  )
}

const SectionPackages = ({ isVisible }) => {
  const packages = [
    {
      badges: {
        color: "badge-red",
        title: "20",
        desc: "Mbps"
      },
      title: "Rp40.000",
      desc: "3 x 24 hours"
    },
    {
      badges: {
        color: "badge-red",
        title: "20",
        desc: "Mbps"
      },
      title: "Rp40.000",
      desc: "3 x 24 hours"
    },
    {
      badges: {
        color: "badge-red",
        title: "20",
        desc: "Mbps"
      },
      title: "Rp40.000",
      desc: "3 x 24 hours"
    },
    {
      badges: {
        color: "badge-red",
        title: "20",
        desc: "Mbps"
      },
      title: "Rp40.000",
      desc: "3 x 24 hours"
    },

  ]
  const effect = isVisible ? "fadeInUp" : "";
  return (
    <div className="py-main-sm">
      <div className="container">
        <div className="row">
          {packages.map((value, index)=>{
            let sumDelay = index+1;
            return (
              <div className="col-md-3 col-6" key={index}>
                <Link
                  to="/shop/internet/upgrade-speed/confirm"
                  className={"card card-packages card-packages-sm animated " + effect + " delayp" + sumDelay}
                >
                  <div className="card-header">
                    <div className="badges-list">
                      <div className={"badges " + value.badges.color}>
                        <h3>{value.badges.title}</h3>
                        <span>{value.badges.desc}</span>
                      </div>
                    </div>
                  </div>
                  <div className="card-body">
                    <h3>
                      {value.title}<small> / bln</small>
                    </h3>
                    <p>{value.desc}</p>
                    <span className="btn btn-primary">Buy</span>
                  </div>
                </Link>
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}

export default ShopInternetUpgradeSpeed
