import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const BillTicket1 = () => {
    const { trigger, navCase, pageCase } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
            <div className="container container-sm">
                <div className="box animated fadeInUp">
                    <header>
                    <div className="header-actions">
                        <Link to="/shop/internet/wifi-id-seamless" className="btn btn-link">
                        <i className="fa fa-arrow-left"></i>
                        </Link>
                    </div>
                    </header>
                    <section className="section-header-text">
                        <div className="heading">
                            <h1>Issue Report</h1>
                        </div>
                    </section>
                    <section className="py-main-sm pb-0">
                        <div className="subheading">
                            <p>Your Report</p>
                        </div>
                        <div className="list-group">
                            <div className="list-group-item">
                                <div className="list-group-item-content">
                                    <h3>#123456</h3>
                                    <p className="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="py-main-sm pb-0">
                        <div className="subheading">
                            <p>Report Process</p>
                        </div>
                        <div className="py-main-sm milestone">
                            <div className="milestone-item success">
                                <div className="milestone-item-img">
                                <div className="dot"></div>
                                </div>
                                <div className="milestone-item-content">
                                <h5 className="title">Report received</h5>
                                <span className="desc">We are trying to solve this year</span>
                                </div>
                            </div>
                            <div className="milestone-item">
                                <div className="milestone-item-img">
                                <div className="dot"></div>
                                </div>
                                <div className="milestone-item-content">
                                <h5 className="title">Issue is being addressed</h5>
                                <span className="desc">We have assigned a special team to resolve this issue</span>
                                </div>
                            </div>
                            <div className="milestone-item last-child">
                                <div className="milestone-item-img">
                                <div className="dot"></div>
                                </div>
                                <div className="milestone-item-content">
                                <h5 className="title">Issue solved </h5>
                                <span className="desc">Issue has been resolved!</span>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div className="btn-placeholder">
                        <Link to="/bill/ticket/done" className="btn btn-primary btn-block">Problem Solved</Link>
                    </div>
                </div>
              </div>
          </section>
  )
}

export default BillTicket1;
