import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';

class ShopCall extends Component {
    static contextType = PageContext;
    componentDidMount() {
        this.context.trigger(2);
        this.context.navCase("w-md-up-block");
        this.context.pageCase("l-bg");
    }
    render() { 
        const { brand, common, sample  } = this.context;
        const images = { brand, common, sample };
        return ( 
            <TextContext.Consumer>{(context)=>{
                const { shopText } = context;
                return (
                    <section>
                        <div className="cover cover-sm cover-bg-white-sm  bg-red-gradient">
                            <div className="header-actions d-block d-md-none">
                                <Link
                                to="/shop/"
                                className="btn btn-link"
                                >
                                <i className="fa fa-arrow-left"></i>
                                </Link>
                            </div>
                            <div className="container">
                                <nav aria-label="breadcrumb" className="d-none d-md-block">
                                <ol className="breadcrumb animated fadeInUp">
                                    <li className="breadcrumb-item"><Link to="/shop"><i className="fa fa-arrow-left mr-1"></i>{shopText.shopCallCover.title}</Link></li>
                                </ol>
                                </nav>
                                <div className="cover-content">
                                <h1 className="cover-title animated fadeInUp delayp1">{shopText.shopCallCover.title}</h1>
                                </div>
                            </div>}
                        </div>
                        <SectionWithBanner images={images} shopText={shopText}/>
                        <SectionText shopText={shopText}/>
                        <SectionFeatures images={images} />
                    </section>

                )
            }}

            </TextContext.Consumer>
         );
    }
}
 
export default ShopCall;

const SectionWithBanner = ({images, shopText}) => {
    return (
        <div className="py-main bg-cover-section call content-center">
            <div className="container">
                <div className="row row-4">
                    <div className="col-md-6 col-8 content-center-left">
                        <div className="heading mb-2">
                            <h3 className="animated fadeInUp delayp5">{shopText.shopCallBanner.title}</h3>
                            <p className="mb-0 animated fadeInUp delayp6">{shopText.shopCallBanner.desc}</p>
                        </div>
                    </div>
                    <div className="col-6 d-none d-md-block">
                        <div className="content-center" style={{height: "100%"}}>
                            <img src={images.common["banner_bnr_call_movin.png"]} className="img-fluid rounded animated fadeInUp delayp7" alt="Indonesia map"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
  
const SectionText = ({shopText}) => {
    return (
        <div className="py-main bg-white animated fadeInUp delayp7">
            <div className="container">
                <div className="subheading">
                    <p>About this service</p>
                </div>
                <p className="mb-0 w-md-up-50">
                {shopText.shopCallBanner.about}
                </p>
            </div>
        </div>
    );
};

  
const SectionFeatures = ({ images }) => {
    return (
        <div className="py-main-sm bg-white animated fadeInUp delayp8">
            <div className="container">
                <div className="row row-4">
                    <div className="col-md-6 col-12">
                        <div className="subheading">
                        <p>How To Activate</p>
                        </div>
                        <div className="milestone">
                        <div className="milestone-item active">
                            <div className="milestone-item-img">
                            <div className="dot"></div>
                            </div>
                            <div className="milestone-item-content">
                            <h5 className="title">Download Aplikasi Movin'</h5>
                            <span className="desc">
                                Aplikasi Movin' tersedia di Play Store dan App Store
                            </span>
                            </div>
                        </div>
                        <div className="milestone-item active">
                            <div className="milestone-item-img">
                            <div className="dot"></div>
                            </div>
                            <div className="milestone-item-content">
                            <h5 className="title">
                                Login dengan myIndiHome e-mail dan password
                            </h5>
                            <span className="desc">
                                Anda bisa menggunakan akun yang sama di Movin' dan myIndiHome
                            </span>
                            </div>
                        </div>
                        <div className="milestone-item active last-child">
                            <div className="milestone-item-img">
                            <div className="dot"></div>
                            </div>
                            <div className="milestone-item-content">
                            <h5 className="title">
                                Aktivasi Movin' di app Movin' dan segera nikmati layanannya!
                            </h5>
                            <span className="desc">
                                Lakukan aktivasi di app Movin' untuk menggunakan layanan ini.
                            </span>
                            </div>
                        </div>
                        </div>
                        <div className="btn-placeholder">
                        <Link
                            to="#"
                            className="btn btn-primary block-sm"
                        >
                            Open Movin'
                        </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
