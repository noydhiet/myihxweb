import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';
import { getToken } from '../../utils/storage';

class PointsPreview extends Component {
  constructor() {
    super();
    this.state = {
      modalFilter: false
    }
  }

  triggerPageHistory = (mode) => {
    switch (mode) {
      case "modalFilter":
        this.setState({
          modalFilter: !this.state.modalFilter
        });
        break;

      case "close":
        this.setState({
          modalFilter: false
        });
        break;

      default:
        return null
    }
  }
  render() {
    const { historyActive, expiryActive } = this.props;
    const config = {
      carouselTab: {
        className: "tab-list",
        dots: false,
        infinite: false,
        arrows: false,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [
          {
            breakpoint: 991,
            settings: {
              dots: false,
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 575,
            settings: {
              dots: false,
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 324,
            settings: {
              dots: false,
              slidesToShow: 2,
              slidesToScroll: 2
            }
          }
        ]
      }
    };
    const headerConfig = historyActive === "active" ?
      <div className="header-actions show-sm" style={{ display: "flex important" }}>
        <Link to="/shop/" className="btn btn-link">
          <i className="fa fa-arrow-left"></i>
        </Link>
        <Link to="#" className="btn btn-link" onClick={() => this.triggerPageHistory("modalFilter")}>
          <i className="far fa-filter"></i>
        </Link>
      </div>
      :
      <div className="header-actions d-block d-md-none" style={{ display: "flex important" }}>
        <Link to="/shop/" className="btn btn-link">
          <i className="fa fa-arrow-left"></i>
        </Link>
      </div>
    return (
      <div>
        <div className="cover cover-sm cover-bg-white-sm bg-red-gradient">
          {headerConfig}
          <div className="container">
            <nav aria-label="breadcrumb" className="d-none d-md-block">
              <ol className="breadcrumb animated fadeInUp">
                <li className="breadcrumb-item">
                  <Link to="/home">
                    <i className="fa fa-arrow-left mr-1"></i>Home
                  </Link>
                </li>
              </ol>
            </nav>
            <div className="cover-content">
              <p className="caption">Indihome Points</p>
              <h1 className="cover-title animated fadeInUp delayp1">1.234</h1>
              <p className="cover-text">20 pts will expire on 2 July 2020</p>
            </div>
          </div>
        </div>
        <section className="shop-tab-list d-md-none">
          <div className="container">
            <Slider {...config.carouselTab}>
              <div className={"tab-item " + historyActive}>
                <Link to="/points/history">Point History</Link>
              </div>
              <div className={"tab-item " + expiryActive}>
                <Link to="/points/expiry">Points Expiry</Link>
              </div>
            </Slider>
          </div>
        </section>
      </div>
    )
  }
}

export default PointsPreview;
