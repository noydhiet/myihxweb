import React from 'react';
import { Form } from 'react-bootstrap';
import ContainerBase from '../../include/ContainerBase';
import { forgetPassword } from '../../services';
import { PageContext } from './../../context/PageContext';
import { Link } from 'react-router-dom';

export default class LoginEmailForgotPassword extends React.Component {
  static contextType = PageContext;
  state = {
    alert: '',
    loginID: '',
    notif: '',
    isButtonLoading: false
  }

  componentDidMount(){
    this.context.trigger(1);
    this.context.pageCase("w-bg");
  }

  handleChange = ({ target }) => {
    const { id, value } = target;
    const reMail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const renumber = /^(\+62|62|0)+[0-9]+$/;
    const mobileCheck = v => {
      return /^((?:\+62|62)|0)[8]{1}[0-9]{6,12}$/.test(v)
      // let mobileNumber;
      //     if(v.startsWith('0')) {
      //       mobileNumber = v.substring(1);
      //     } else if(v.startsWith('62')) {
      //       mobileNumber = v.substring(2);
      //     } else if(v.startsWith('+62')) {
      //       mobileNumber = v.substring(3);
      //     } else {
      //       return false;
      //     }
      //     return !(/\s/g.test(mobileNumber) || !/^[0-9]+$/i.test(mobileNumber)) && ((mobileNumber.length < 14 && mobileNumber.length > 6 ))
    };
    const alert = reMail.test(value) || mobileCheck(value) ? '' : 'Please input a valid email/mobile number';

    this.setState({
      [id]: value,
      alert,
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({
      isButtonLoading: true
    }, () => {
      const { loginID } = this.state;
      const renumber = /^(\+62|62|0)+[0-9]+$/;
      const remail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      const mobileCheck = v => {
        return /^((?:\+62|62)|0)[8]{1}[0-9]{6,12}$/.test(v)
        // let mobileNumber;
        // if(v.startsWith('0')) {
        //   mobileNumber = v.substring(1);
        // } else if(v.startsWith('62')) {
        //   mobileNumber = v.substring(2);
        // } else if(v.startsWith('+62')) {
        //   mobileNumber = v.substring(3);
        // } else {
        //   return false;
        // }
        // return !(/\s/g.test(mobileNumber) || !/^[0-9]+$/i.test(mobileNumber)) && ((mobileNumber.length < 14 && mobileNumber.length > 6 ))
      };
      if (remail.test(loginID)) {
        this.handleSendRecoveryCode('email', loginID);
      } else if (mobileCheck(loginID)) {
        this.handleSendRecoveryCode('sms', loginID.replace(/^\+*62/, '0'));
      } else {
        this.setState({
          isButtonLoading: false
        });
        this.context.trigger();
      }
    });
    
  }

  handleSendRecoveryCode = (channel, value) => {
    const { history } = this.props;
    const { openNotif } = this.context;
    
    // **IN CASE WHATSAPP API FOR FORGOT PASSWORD IS AVAILABLE**
    // if (channel === 'mobile') {
    //   checkUserExist('mobile', value)
    //   .then(({ data }) => {
    //     if (data.status === 200) {
    //       localStorage.setItem('mobile', data.data.mobile)
    //       history.push('/login/mobile/verification:login--email--recovery-code');
    //     }
    //     else this.setState({ alert: 'This email / phone number is not registered yet' });
    //   })
    //   .catch(() => { this.setState({ alert: 'This email / phone number is not registered yet'}) })
    // } else {
    // put the forget password API
    // }
    forgetPassword(channel, value)
      .then(({ data }) => {
        if (data.status === 200 || data.status === 201) {
          openNotif('forgetPassword');
          history.push(`/login/email/recovery-code?channel=${channel}&value=${this.state.loginID}`);
        } else {
          this.setState({
            isButtonLoading: false,
            alert: `This ${channel === 'sms' ? 'mobile number' : 'email address'} is not yet registered`
          });
        }
      })
      .catch(() => { this.setState({ isButtonLoading: false, alert: 'This email / phone number is not registered yet' }); });
  }

  render() {
    const { alert, notif, loginID, isButtonLoading } = this.state;
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    const config = {
      alert: alert ? 'block' : 'none',
      notifActive: notif ? 'active' : '',
      disabledBtn: isButtonLoading || alert || !loginID ? 'disabled' : '',
    };
    const containerProps = {
      image: { alt: 'Icon Forget Password', src: images.common['ic_header_password.png'], },
      title: 'Forgot Password',
      subtitle: 'We got you covered! Receive password recovery code in your registered email / mobile number.'
    };

    console.log(isButtonLoading)
    const Loading = () => {
      return (
        <span><i className="fa fa-circle-notch fa-spin"></i></span>
      )
    }
    return (
      <ContainerBase {...containerProps}>
        <Form>
          <Form.Group className="form-custom">
            <Form.Label> Mobile Number or Email </Form.Label>
            <Form.Control
              autoFocus
              type="text"
              name="loginID"
              id="loginID"
              onChange={this.handleChange}
              value={loginID}
              placeholder="Enter Your Registered Email or Mobile Number"
            />
            <div className="invalid-feedback" style={{ display: config.alert }}>{alert}</div>
          </Form.Group>
          <div className="btn-placeholder">
            <Link to="#" className={"btn btn-primary btn-block animated fadeInUp delayp4 " + config.disabledBtn} onClick={this.handleSubmit}><span>{isButtonLoading ? <Loading /> : "Send Recovery Code"}</span></Link>
          </div>
        </Form>
      </ContainerBase>
    )
  }
}
