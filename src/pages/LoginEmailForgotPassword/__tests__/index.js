import React from 'react';
import renderer from 'react-test-renderer';
import LoginEmailForgotPassword from '../index';

jest.mock('../../../services', () => ({
  forgetPassword: ({ email }) => new Promise((resolve, reject) => {
    if (email === 'test@telkom.co.id') resolve('success');
    else reject('failed');
  }),
}));
jest.mock('react-router-dom');

const instance = props => renderer.create(<LoginEmailForgotPassword images={{ common: {} }} {...props} />).getInstance();

describe('LoginEmailForgotPassword', () => {
  test('render', () => {
    const result = instance().render();
    expect(result.type).toBe(React.Fragment);
  });

  test('handleChange', () => {
    const wrapper = instance();
    const e = { target: { id: 'name', value: 'test' } };
    wrapper.handleChange(e);
    expect(wrapper.state.name).toBe('test');
    expect(wrapper.state.alert).toBe('Please enter a valid email address');
    
    e.target.value = 'test@telkom.co.id';
    wrapper.handleChange(e);
    expect(wrapper.state.alert).toBe('');
  });

  test('submit', () => {
    const wrapper = instance();
    const e = { preventDefault: jest.fn() };
    wrapper.submit(e);
    expect(e.preventDefault).toHaveBeenCalled();
  });
});
