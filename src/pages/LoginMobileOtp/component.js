import React from 'react';
import PropTypes from 'prop-types';
import { CardNotif } from '../../include/CardPackage';
import ContainerBase from '../../include/ContainerBase';
import OtpInput from '../../include/OtpInput';
import { verifyOtp, sendOtp } from '../../services';
import { setExpireTime, setToken, setUserData, checkOtpIsBlocked, setOtpBlockedTime, getBlockedNumber, setBlockedNumber, OTP_CHANNEL } from '../../utils/storage';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';
import { Modal } from 'react-bootstrap';

const TIME_IN_SECONDS = 179

export default class LoginMobileOtp extends React.Component {
  static contextType = PageContext;
  state = {
    alert: getBlockedNumber() === localStorage.getItem('mobile') && checkOtpIsBlocked(),
    attempt: getBlockedNumber() === localStorage.getItem('mobile') && checkOtpIsBlocked() ? 0 : 3,
    mobile: localStorage.getItem('mobile') || '',
    email: localStorage.getItem('email') || '',
    otpChannel: localStorage.getItem(OTP_CHANNEL) || '',
    timer: TIME_IN_SECONDS,
    showProgressbar: true
  }

  componentDidMount() {
    const { history, location: { state }} = this.props;
    this.context.trigger(1);
    this.context.pageCase("w-bg");
    if(state) {
      const { channel, email, mobile } = state;
      if(mobile !== getBlockedNumber() || (mobile === getBlockedNumber() && !checkOtpIsBlocked())) {
        console.log('LoginMobileOtp outsire', channel, email, mobile)
        sendOtp(channel, mobile, email)
        .then(({ data }) => {
          console.log('LoginMobileOtp inside', data)
          if (data.status === 200 || data.status === 201) {
            this.myInterval = setInterval(() => {
              this.setState( prevState => { return { timer: prevState.timer - 1 } })
            }, 1000);
            this.setState({showProgressbar: false});
          } else if (data.status === 402) {
            this.trigger('returnBlocked');
            this.setState({showProgressbar: false});
          } else {
            this.setState({ showProgressbar: false, notif: data.message, attempt: 0 });
            setTimeout( () => {
              this.setState({notif: ''});
            }, 3000);
          }
        })
        .catch(({ message }) => {
          this.setState({ timer: 0, showProgressbar: false, notif: message })
          setTimeout( () => {
            this.setState({notif: ''});
          }, 5000);
        });
      } else {
        this.setState({showProgressbar: false});
      }
    } else {
      history.push('/');
    }
  }

  componentWillUnmount() {
    clearInterval(this.myInterval);
  }

  validateOTP(otp) {
    const { otpChannel, mobile, email } = this.state;
    console.log('validateOTP', otpChannel, mobile, email)
    verifyOtp(otpChannel, mobile, email, otp)
      .then(({ data }) => {
        console.log('LoginMobileOtpverifyOtp',data)
        if (data.ok) {
          const userData = JSON.parse(atob(data.data.token.split('.')[1]));
          userData.userName = data.data.userName;
          
          setToken(data.data.token);
          setUserData(userData);
          setExpireTime(userData.exp);
          this.trigger('returnSuccess');          
        } else if (data.status === 402) {
          this.trigger('returnBlocked');
        } else {
          this.trigger('returnFailed');
        }
      })
      .catch(() => { this.trigger('returnFailed'); });
  }

  resendOtp() {
    const { otpChannel, mobile, email } = this.state;

    sendOtp(otpChannel, mobile, email)
      .then(({ data }) => { 
        if (!data.ok) {
          this.setState({ notif: data.message, showProgressbar: false, attempt: 0 }); 
          setTimeout( () => {
            this.setState({notif: ''});
          }, 5000);
        }
        else {
          this.myInterval = setInterval(() => {
            this.setState( prevState => { return {timer: prevState.timer - 1} })
          }, 1000);
          this.setState({showProgressbar: false})
        }
      }).catch(({ message }) => { 
        this.setState({ notif: message, showProgressbar: false, timer: 0 }); 
        setTimeout( () => {
          this.setState({notif: ''});
        }, 5000);
      });
  }

  pathSuffix = () => {
    const { match: { params } } = this.props;
    
    if (!params.pathSuffix) return '';
    return params.pathSuffix.replace(/--/g, '/').substring(1);
  }

  trigger = (mode) => {
    const { history } = this.props;

    switch (mode) {
      case 'returnBlocked':
        this.setState({ alert: true, attempt: 0 });
        setOtpBlockedTime();
        setBlockedNumber(localStorage.getItem('mobile'))
        break;

      case 'returnFailed':
        this.setState({ alert: true, attempt: this.state.attempt - 1 });
        break;

      case 'returnSuccess':
        history.push(`/${this.pathSuffix()}`);
        break;

      case 'stopTimer':
        clearInterval(this.myInterval);
        break;

      case 'startTimer':
        this.setState({ timer: TIME_IN_SECONDS, showProgressbar: true });
        this.resendOtp();
        break;

      default:
        break;
    }
  }

  render() {
    const { alert, attempt, mobile, notif, timer, otpChannel } = this.state;
    let displayTimer = true
    let displayResend = false
    const { brand, common, sample } = this.context;
    const images = { brand, common, sample };
    //Timer
    if (timer === 0) {
      this.trigger('stopTimer');
      displayTimer = false
      displayResend = true
    }
    const Loading = () => {
      return (
        <span><i className="fa fa-circle-notch fa-spin"></i></span>
      )
    }
    const mobile4Digits = mobile.slice(-4).padStart(mobile.length, "•");
    const config = {
      displayTimer: displayTimer && attempt > 0 ? 'block' : 'none',
      displayResend: displayResend && attempt > 0 ? 'block' : 'none',
      displayInvalid: {
        display: alert ? 'block' : 'none'
      },
      colorAlert: alert ? '#ee3124' : '#000000',
      notifActive: notif ? 'active' : '',
      min: `${Math.floor(timer / 60)}`.padStart(2, '0'),
      sec: `${timer % 60}`.padStart(2, '0'),
    };
    console.log('timer', config)
    return (
      <>
      <TextContext.Consumer>{(context)=> {
        const { loginText } = context;
        const containerProps = {
          image: { alt: 'Icon OTP', src: images.common['ic_header_verification.png'], },
          subtitle: (otpChannel === 'sms' ? loginText.mobileOtp.subtitleSMS  : loginText.mobileOtp.subtitleWhatsApp) + mobile4Digits,
          title: loginText.mobileOtp.title,
        };
        const alert = {
          message: attempt > 0 ? loginText.mobileOtp.alertWrong : loginText.mobileOtp.alertExceeded
        }
        return (
          <>
            <ContainerBase {...containerProps}>
              <form className="needs-validation" id="form">
                <div className="otp-code-container animated fadeInUp delayp2">
                  <OtpInput disabled={displayResend || attempt <= 0} names={['otp1', 'otp2', 'otp3', 'otp4']} onSubmit={this.validateOTP.bind(this)} />
                  <div className="invalid-feedback text-center" style={config.displayInvalid}>{alert.message}</div>
                </div>
              </form>
              <div className="countdown-timer text-center animated fadeInUp delayp3">
                <div className="btn btn-link btn-resend-timer disabled" style={{ display: config.displayTimer }}>{loginText.mobileOtp.btnResendTimer} <span id="timer"> {config.min}:{config.sec} </span></div>
                <div className="btn-placeholder btn-resend">
                  <button className="btn btn-link" style={{ display: config.displayResend, margin: 'auto' }} onClick={() => this.trigger("startTimer")}>{loginText.mobileOtp.btnResendCode}</button>
                </div>
              </div>
            </ContainerBase>
            <CardNotif active={config.notifActive} textTop={notif} />
          </>
        )
      }}
      </TextContext.Consumer>
      <Modal show={this.state.showProgressbar} onHide={ () => {}} centered={true}>
        <div className="btn btn-white btn-block">
          <Loading />
        </div>
      </Modal>
      </>
    )
  }
}

LoginMobileOtp.defaultProps = {
  pageCase: () => {},
  trigger: () => {},
};

LoginMobileOtp.propTypes = {
  history: PropTypes.object,
  pageCase: PropTypes.func,
  trigger: PropTypes.func,
};
