import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { PageContext } from './../../context/PageContext';

const ShopTvAppsCatchplayConfirm = () => {
    const { trigger, navCase, pageCase } = useContext(PageContext);
    useEffect(()=>{
        trigger(2);
        navCase("w-md-up-block");
        pageCase("w-bg");
    }, [trigger, navCase, pageCase]);
    return (
        <section>
            <div className="container container-sm">
                <div className="box animated fadeInUp">
                    <header>
                    <div className="header-actions">
                        <Link to="/shop/tv/apps/catchplay/details" className="btn btn-link">
                        <i className="fa fa-arrow-left"></i>
                        </Link>
                    </div>
                    </header>
                    <section className="section-header-text">
                        <div className="heading">
                            <h1>Confirm Subscription</h1>
                            <p>Confirm subsciption to continue</p>
                        </div>
                    </section>
                    <section className="py-main-sm pb-0">
                        <div className="subheading">
                            <p>Product</p>
                        </div>
                        <Link
                            to="#"
                            className="card card-packages w-arrow animated fadeInUp delayp2"
                            >
                            <div className="card-header">
                            <div className="badges-list">
                                 <div className="badges badge-catchplay left">
                                </div>
                            </div>
                            <div className="header-text">
                                <h3>Subscribe to Catchplay</h3>
                                <p>Unlimited movies and shows, only on HOOQ</p>
                            </div>
                            </div>
                            <div className="card-body w-btn">
                                <h3>
                                    Rp1.890.000<small>/month</small>
                                </h3>
                            </div>
                        </Link>
                    </section>
                    <section className="py-main-sm pb-0">
                        <div className="form-group">
                            <label>Total</label>
                            <h4>Rp36.000</h4>
                            <small className="help-block">Include 10% tax. This mount will be billed every month starting from 30th July 2019</small>
                        </div>
                        <div className="btn-placeholder bottom">
                            <div className="row">
                            <div className="col-6">
                                <Link to="/shop/tv/apps" className="btn btn-transparent btn-block">Cancel</Link>
                            </div>
                                <div className="col-6">
                                    <Link to="/shop/tv/apps/catchplay/success" className="btn btn-primary btn-block"> <span>Confirm</span> </Link>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    )
}

export default ShopTvAppsCatchplayConfirm;