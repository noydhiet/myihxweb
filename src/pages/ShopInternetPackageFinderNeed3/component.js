import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Form } from 'react-bootstrap';
import { PageContext } from './../../context/PageContext';
const ByNeeds3 = ({ history }) => {
  const { trigger, navCase, pageCase } = useContext(PageContext);
  useEffect(()=>{
      trigger(2);
      navCase("w-md-up-block");
      pageCase("w-bg");
  }, [trigger, navCase, pageCase]);
  const checkBox = (e) => {
    const check = document.getElementsByClassName("check");
    const listCheck = document.getElementsByClassName("list-check");
    for(let i = 0; i < check.length; i++) {
      if(i === e) {
        if(check[i].checked) {
            check[i].checked = false;
            listCheck[i].classList.remove("active");
        } else {
          check[i].checked = true
          listCheck[i].classList.add("active");
        }
      }
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    history.push("/shop/internet/package-finder/needs/finish");
  }
  return (
    <section>
      <div className="container container-sm">
        <div className="box animated fadeInUp">
          <header>
            <div className="header-actions">
              <Link to="/shop/internet/package-finder/needs/2" className="btn btn-link">
                <i className="fa fa-arrow-left"></i>
              </Link>
              <ul className="btn wizard-finder">
                <li className="active"></li>
                <li className="active"></li>
                <li className="active"></li>
              </ul>
            </div>
            <div className="heading animated fadeInUp delayp1">
              <h1>What kind of TV shows do you like?</h1>
            </div>
          </header>
          <Form onSubmit={handleSubmit}>
            <section className="section-header-card header-card-sm animated fadeInUp delayp2">
              <div className="list-group">
                <div className="row row-1 mb-3">
                  <div className="col-6">
                    <div className="list-group-item list-check" onClick={() => checkBox(0)}>
                      <div className="list-group-item-content">
                        <h5 className="title">Entertainment</h5>
                        <span className="desc">Variety shows, fashion & celebrity news.</span>
                        <label className="input-check-card">
                          <input type="checkbox" className="check" />
                          <span></span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="list-group-item list-check" onClick={() => checkBox(1)}>
                      <div className="list-group-item-content">
                        <h5 className="title">Movies</h5>
                        <span className="desc">Western box office movies.</span>
                        <label className="input-check-card">
                          <input type="checkbox" className="check" />
                          <span></span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="list-group-item list-check" onClick={() => checkBox(2)}>
                      <div className="list-group-item-content">
                        <h5 className="title">TV Series</h5>
                        <span className="desc">Western TV Series.</span>
                        <label className="input-check-card">
                          <input type="checkbox" className="check" />
                          <span></span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="list-group-item list-check" onClick={() => checkBox(3)}>
                      <div className="list-group-item-content">
                        <h5 className="title">Kids</h5>
                        <span className="desc">Kids and family friendly movies & shows</span>
                        <label className="input-check-card">
                          <input type="checkbox" className="check" />
                          <span></span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="list-group-item list-check" onClick={() => checkBox(4)}>
                      <div className="list-group-item-content">
                        <h5 className="title">Asian</h5>
                        <span className="desc">Asian movies and international channels.</span>
                        <label className="input-check-card">
                          <input type="checkbox" className="check" />
                          <span></span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="list-group-item list-check" onClick={() => checkBox(5)}>
                      <div className="list-group-item-content">
                        <h5 className="title">Sports</h5>
                        <span className="desc">Soccer and other sports competitions</span>
                        <label className="input-check-card">
                          <input type="checkbox" className="check" />
                          <span></span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="list-group-item list-check" onClick={() => checkBox(6)}>
                      <div className="list-group-item-content">
                        <h5 className="title">Lifestyle</h5>
                        <span className="desc">Home, cooking and travelling shows.</span>
                        <label className="input-check-card">
                          <input type="checkbox" className="check" />
                          <span></span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="list-group-item list-check" onClick={() => checkBox(7)}>
                      <div className="list-group-item-content">
                        <h5 className="title">Science</h5>
                        <span className="desc">Documentations and educational shows.</span>
                        <label className="input-check-card">
                          <input type="checkbox" className="check" />
                          <span></span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="list-group-item list-check" onClick={() => checkBox(8)}>
                      <div className="list-group-item-content">
                        <h5 className="title">News</h5>
                        <span className="desc">International news & reports.</span>
                        <label className="input-check-card">
                          <input type="checkbox" className="check" />
                          <span></span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="list-group-item list-check" onClick={() => checkBox(9)}>
                      <div className="list-group-item-content">
                        <h5 className="title">Indonesian</h5>
                        <span className="desc">Local TV channels & shows.</span>
                        <label className="input-check-card">
                          <input type="checkbox" className="check" />
                          <span></span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section className="text-center">
              <p className="font-size-lg mb-0">Or</p>
              <button className="btn btn-link">I don't need any TV package</button>
            </section>
            <div className="btn-placeholder">
              <button className="btn btn-primary btn-block" type="submit" value="submit">Next</button>
            </div>
          </Form>
        </div>
      </div>
    </section>
  )
}

export default ByNeeds3
