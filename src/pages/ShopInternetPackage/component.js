import React, { Component, useState } from 'react';
import { Link } from 'react-router-dom';
import { Modal, Accordion } from 'react-bootstrap';
import {  CardContent } from './../../include/CardPackage'
import Slider from "react-slick";
import ShopInternet from './../ShopInternet';
import TrackVisibility from 'react-on-screen';
import { PageContext } from './../../context/PageContext';
import { TextContext } from './../../context/TextContext';

class PackageInternet extends Component {
  static contextType = PageContext;
  componentDidMount() {
    this.context.trigger(2);
    this.context.navCase("w-md-up-block");
    this.context.pageCase("l-bg");
  }
  render() {
    const { common, brand, sample } = this.context;
    const images = { common, brand, sample };
    return (
      <TextContext.Consumer>{(context)=>{
        const { shopText, generalText } = context;
        return (
          <div>
            <ShopInternet paketActive="active" />
            <TrackVisibility once>
              <AvailabilitySection images={images} generalText={generalText}/>
            </TrackVisibility>
            <TrackVisibility once offset={400}>
              <InternetPlanSection images={images} shopText={shopText}/>
            </TrackVisibility>
            <TrackVisibility once offset={800}>
              <PopularProductSection images={images} generalText={generalText}/>
            </TrackVisibility>
          </div>
        )
      }}

      </TextContext.Consumer>
    )
  }
}

export default PackageInternet

const AvailabilitySection = ({ images, generalText }) => {
  return (
      <section className="py-main">
          <div className="container">
              <div className="row">
                  <div className="col-md-6 order-md-last mb-3">
                      <img src={images.common["img_indonesia-map.png"]} className="img-fluid" alt="maps-img" />
                  </div>
                  <div className="col-md-6 order-md-first">
                      <div className="subheading animated animated fadeInUp delayp4"><p>{generalText.coverage.coverageCaption}</p></div>
                        {/* <h3 className="help-block text-uppercase animated fadeInUp delayp7">Cek KetersAediaan indihome di dareah anda</h3> */}
                        <div className="heading">
                          <h3 className="animated fadeInUp delayp5">{generalText.coverage.coverageText}</h3>
                        </div>
                        <div className="btn-placeholder animated fadeInUp delayp6">
                          <Link to="/check-coverage" className="btn btn-primary">{generalText.coverage.coverageBtn}</Link>
                          {/* <small className="help-block mt-2"> Misalnya: Nama kompleks, apartemen, atau gedung</small> */}
                        </div>
                  </div>
              </div>
          </div>
      </section>
  )
}

class InternetPlanSection extends Component {
  state = {
    uiq: false,
    tod: false,
    fldc: false,
    accordionMovie: false
  }
  triggerPage = (mode) => {
    switch (mode) {
      case "uiq":
        this.setState({
          uiq: !this.state.uiq,
          tod: false,
          fldc: false
        })
        break;

      case "tod":
        this.setState({
          uiq: false,
          tod: !this.state.tod,
          fldc: false
        })
        break;

      case "fldc":
        this.setState({
          uiq: false,
          tod: false,
          fldc: !this.state.fldc
        });
        break;

      case "accordionMovie":
        this.setState({
          accordionMovie: !this.state.accordionMovie
        })
        break;

      default:
        return null
    }
  }

  render() {
    const { uiq, tod, fldc, accordionMovie } = this.state;
    const { images, isVisible, shopText } = this.props;
    const config = {
      carousel3rd: {
        className: "primary-carousel overflow-inherit",
        dots: false,
        infinite: false,
        arrows: false,
        speed: 500,
        slidesToShow: 3.1,
        slidesToScroll: 3.1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3.5,
              slidesToScroll: 3.5,
              infinite: false,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 3.5,
              slidesToScroll: 3.5,
              infinite: false,
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 3.5,
              slidesToScroll: 3.5,
              infinite: false,
            }
          }
        ]
      },
      accordionActive: accordionMovie ? "active" : ""
    }
    const effect = isVisible ? "fadeInUp" : "";
    return (
      <div className="py-main bg-gray-50 bg-md-up-white shop-internet-features">
        <div className="container">
          <div className="heading d-none d-md-block">
              <h3 className={"animated " + effect + " delayp1"}>{shopText.shopInternetOffer.shopInternetOfferTitle}</h3>
              <p className={"w-md-up-75 animated " + effect + " delayp2"}>{shopText.shopInternetOffer.shopInternetOfferDesc}</p>
          </div>
          <div className="subheading d-block d-md-none">
            <p className="animated fadeInUp delayp4">Our internet packages features</p>
          </div>
          <div className="row row-1 row-md-up-2">
            <div className="col-4">
              <div className={"card card-feature red animated " + effect + " delayp4"} onClick={() => this.triggerPage("uiq")}>
                <h5>Unlimited Internet Quota <i className="fal fa-angle-right"></i> </h5>
                <p>Enjoy unlimited internet for all your internet needs!</p>
              </div>
            </div>
            <div className="col-4">
              <div className={"card card-feature blue animated " + effect + " delayp5"} onClick={() => this.triggerPage("tod")}>
                <h5>TV On Demand <i className="fal fa-angle-right"></i> </h5>
                <p>Watch TV shows on demand. Missed a show? No Problem!</p>
              </div>
            </div>
            <div className="col-4">
              <div className={"card card-feature green animated " + effect + " delayp6"} onClick={() => this.triggerPage("fldc")}>
                <h5>Free Quota & Bonuses <i className="fal fa-angle-right"></i></h5>
                <p>Get free quota for call & lots of extra bonuses</p>
              </div>
            </div>
          </div>
        </div>
        {/* Modal - Unlimited Internet Quota */}
        <Modal show={uiq} onHide={() => this.triggerPage('uiq')}>
          <div className="modal-body p-box">
            <Link to="#" className="close animated fadeInUp" onClick={() => this.triggerPage('uiq')}>
              <span><i className="far fa-times text-primary"></i></span>
            </Link>
            <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
              <h3>Unlimited Internet Quota</h3>
              <p>Lorem Ipsum dolor sit amet. consectur adipicsing elti. Lorem Ipsum dolor sit amet. consectur adipicsing elti.</p>
            </div>
            <div className="list-group animated fadeInUp delayp2 mb-0">
              <div className="list-group-item">
                <div className="badges badge-red">
                  <h3>10</h3><span>Mbps</span>
                </div>
                <div className="list-group-item-content">
                  <h5 className="title">Advantage 1</h5>
                </div>
              </div>
              <div className="list-group-item">
                <div className="badges badge-red">
                  <h3>10</h3><span>Mbps</span>
                </div>
                <div className="list-group-item-content">
                  <h5 className="title">Advantage 2</h5>
                </div>
              </div>
              <div className="list-group-item">
                <div className="badges badge-red">
                  <h3>10</h3><span>Mbps</span>
                </div>
                <div className="list-group-item-content">
                  <h5 className="title">Advantage 2</h5>
                </div>
              </div>
              <div className="list-group-item">
                <div className="badges badge-red">
                  <h3>10</h3><span>Mbps</span>
                </div>
                <div className="list-group-item-content">
                  <h5 className="title">Advantage 2</h5>
                </div>
              </div>
            </div>
          </div>
        </Modal>
        {/* Modal - TV On Demand */}
        <Modal show={tod} onHide={() => this.triggerPage('tod')}>
          <div className="modal-body p-box">
            <Link to="#" className="close animated fadeInUp" onClick={() => this.triggerPage('tod')}>
              <span><i className="far fa-times text-primary"></i></span>
            </Link>
            <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
              <h3>TV On Demand</h3>
              <p>Watch TV shows on demand. Missed a show? No problem!</p>
            </div>
            <section className="animated fadeInUp delayp2 mb-4">
              <Accordion defaultActiveKey="0">
                <Accordion.Toggle eventKey="0" style={{ width: "100%", background: "transparent", border: "none", paddingLeft: "0", paddingRight: "0", height: "40px" }} onClick={() => this.triggerPage("accordionMovie")}>
                  <div className="heading heading-w-link mb-0">
                    <p className="text-body font-size-lg"><strong>Movies & TV Series</strong></p>
                    <p className="text-primary"><i className={"fas fa-chevron-down arrow " + config.accordionActive}></i></p>
                  </div>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                  <div className="row">
                    <div className="col-12">
                      <p className="help-block">Follow popular series such as Game of Thrones, Grey's Anatomy. The Walking Dead, etc in these channels</p>
                    </div>
                    <div className="col-3 col-md-4">
                      <div className="card card-packages">
                        <img alt="" src={images.common["channel1.png"]} className="img-fluid" />
                      </div>
                    </div>
                    <div className="col-3 col-md-4">
                      <div className="card card-packages">
                        <img alt="" src={images.common["channel2.png"]} className="img-fluid" />
                      </div>
                    </div>
                    <div className="col-3 col-md-4">
                      <div className="card card-packages">
                        <img alt="" src={images.common["channel3.png"]} className="img-fluid" />
                      </div>
                    </div>
                    <div className="col-3 col-md-4">
                      <div className="card card-packages">
                        <img alt="" src={images.common["channel4.png"]} className="img-fluid" />
                      </div>
                    </div>
                    <div className="col-3 col-md-4">
                      <div className="card card-packages">
                        <img alt="" src={images.common["channel5.png"]} className="img-fluid" />
                      </div>
                    </div>
                    <div className="col-3 col-md-4">
                      <div className="card card-packages">
                        <img alt="" src={images.common["channel1.png"]} className="img-fluid" />
                      </div>
                    </div>
                    <div className="col-3 col-md-4">
                      <div className="card card-packages">
                        <img alt="" src={images.common["channel2.png"]} className="img-fluid" />
                      </div>
                    </div>
                    <div className="col-3 col-md-4 mb-3">
                      <div className="card card-packages">
                        <img alt="" src={images.common["channel3.png"]} className="img-fluid" />
                      </div>
                    </div>
                    <div className="col-3 col-md-4 mb-3">
                      <div className="card card-packages">
                        <img alt="" src={images.common["channel4.png"]} className="img-fluid" />
                      </div>
                    </div>
                  </div>
                </Accordion.Collapse>
              </Accordion>
            </section>
            <section className="animated fadeInUp delayp2">
              <div className="heading">
                <p className="text-body font-size-lg mb-1"><strong>Kids Program</strong></p>
                <p>Need entertaining, educating, and age appropriate channels for your children? We got you covered with multiple choices</p>
              </div>
              <img src={images.common['img_placeholder_rectangle.jpg']} className="img-fluid" alt="TV Channels" />
            </section>
          </div>
        </Modal>
        {/* Modal - Free Long Distance Call */}
        <Modal show={fldc} onHide={() => this.triggerPage('fldc')}>
          <div className="modal-body p-box">
            <Link to="#" className="close animated fadeInUp" onClick={() => this.triggerPage('fldc')}>
              <span><i className="far fa-times text-primary"></i></span>
            </Link>
            <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
              <h3>Free Apps Subscriptions</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
            <section className="py-main-sm animated fadeInUp delayp2">
              <h3>For Movie fans</h3>
              <p className="help-block">Watch all these movies/TV series in HOOQ, iFlix, or CatchPlay. Accessible from phone, tablet, desktop, or TV</p>
              <Slider {...config.carousel3rd}>
                <div className="carousel-item">
                  <CardContent
                    link="#"
                    img="poster1.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item">
                  <CardContent
                    link="#"
                    img="poster2.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item">
                  <CardContent
                    link="#"
                    img="poster3.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item">
                  <CardContent
                    link="#"
                    img="poster4.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item">
                  <CardContent
                    link="#"
                    img="poster5.png"
                    images={images}
                  />
                </div>
              </Slider>
            </section>
            <section className="py-main-sm animated fadeInUp delayp3">
              <h3>For Gamers</h3>
              <p className="help-block">Play Strategy, adventure, or various game genres from phone, tablet, desktop, or TV</p>
              <Slider {...config.carousel3rd}>
                <div className="carousel-item">
                  <CardContent
                    link="#"
                    img="offer1.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item">
                  <CardContent
                    link="#"
                    img="offer2.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item">
                  <CardContent
                    link="#"
                    img="offer3.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item">
                  <CardContent
                    link="#"
                    img="offer1.png"
                    images={images}
                  />
                </div>
                <div className="carousel-item">
                  <CardContent
                    link="#"
                    img="offer2.png"
                    images={images}
                  />
                </div>
              </Slider>
            </section>
            <section className="py-main-sm animated fadeInUp delayp4">
              <h3>Heavy Call Needs?</h3>
              <p className="help-block">Get Movin'Phone quota and call from anywhere using the app!</p>
            </section>
            <div className="btn-placeholder mt-0">
              <Link to="/shop/internet/package" className="btn btn-primary btn-block">Ok, I Got it</Link>
            </div>
          </div>
        </Modal>
      </div>
    )
  }
}

const PopularProductSection = ({ isVisible, images, generalText }) => {
  const [cond, setCond] = useState({
    subscribeModal: false
  })
  const triggerPage = (mode) => {
    switch (mode) {
      case "openSubscribe":
        setCond({
          subscribeModal: !cond.subscribeModal
        });
        break;

      case "close":
        setCond({
          subscribeModal: false
        });
        break;

      default:
        return null
    }
  }
  const coverStyle = {
    background: `url(${images.common["bnr_blue_2nd.png"]}) no-repeat center`,
    backgroundSize: "cover",
    border: "none"
  };
  const effect = isVisible ? "fadeInUp" : "";
  return (
    <div className="py-main bg-gray-50">
      <div className="container">
        <div className="heading text-md-center d-none d-md-block">
          <h2 className={"animated " + effect + " delayp1"}>{generalText.popularProduct}</h2>
        </div>
        <div className="subheading d-block d-md-none">
          <p className={"animated " + effect + " delayp1"}>{generalText.popularProduct}</p>
        </div>
        <section className="animated fadeInUp delayp9">
          <div className="row">
            <div className="col-lg-4">
              <Link
                to="#"
                className={"card card-packages card-packages-lg animated " + effect + " delayp2"}
                onClick={() => triggerPage("openSubscribe")}
              >
                <div className="card-header">
                  <div className="badges-list">
                    <div className="badges badge-red left">
                      <h3>10</h3>
                      <span>Mbps</span>
                    </div>
                    <div className="badges badge-green middle">
                      <h3>92</h3>
                      <span>Channels</span>
                    </div>
                    <div className="badges badge-blue right">
                      <h3>100</h3>
                      <span>Minutes</span>
                    </div>
                  </div>
                  <div className="header-text">
                    <h3>Paket BUMN</h3>
                    <p>Internet unlimited, 92 USee TV channels, 100 minutes voice call</p>
                  </div>
                </div>
                <div className="card-body w-btn">
                  <div className="card-body-top">
                    <div className="subheading">
                      <p>Triple Play Value</p>
                    </div>
                    <h3>
                      Rp480.000<small> / bln</small>
                    </h3>
                    <div className="content">
                      <p>
                        Perfect for everyday internet users with light daily
                        use
                          </p>
                    </div>
                  </div>
                  <div className="card-body-bottom">
                    <div className="subheading">
                      <p>Bonus Subscriptions</p>
                    </div>
                    <div className="content">
                      <p>
                        Get bonus subscriptions from various apps and games!
                          </p>
                    </div>
                    <img
                      src={images.common["logo_add-ons.jpg"]}
                      className="img-fluid"
                      alt="Logo Add-Ons"
                    />
                  </div>
                  <span className="btn btn-primary">Subscribe</span>
                </div>
              </Link>
            </div>
            <div className="col-lg-4">
              <Link
                to="#s"
                className={"card card-packages card-packages-lg animated " + effect + " delayp3"}
                onClick={() => triggerPage("openSubscribe")}
              >
                <div className="card-header">
                  <div className="badges-list">
                    <div className="badges badge-red left">
                      <h3>20</h3>
                      <span>Mbps</span>
                    </div>
                    <div className="badges badge-green middle">
                      <h3>92</h3>
                      <span>Channels</span>
                    </div>
                    <div className="badges badge-blue right">
                      <h3>100</h3>
                      <span>Minutes</span>
                    </div>
                  </div>
                  <div className="header-text">
                    <h3>Semarak Kebahagiaan</h3>
                    <p>Internet unlimited, 92 USee TV channels, 100 minutes voice call</p>
                  </div>
                </div>
                <div className="card-body w-btn">
                  <div className="card-body-top">
                    <div className="subheading">
                      <p>IndiHome Gamer</p>
                    </div>
                    <h3>
                      Rp535.000<small> / bln</small>
                    </h3>
                    <div className="content">
                      <p>
                        Paket dengan berbagai keuntungan di game favorit
                        Anda!
                          </p>
                    </div>
                  </div>
                  <div className="card-body-bottom">
                    <div className="subheading">
                      <p>Bonus Subscriptions</p>
                    </div>
                    <div className="content">
                      <p>
                        Get bonus subscriptions from various apps and games!
                          </p>
                    </div>
                    <img
                      src={images.common["logo_games.jpg"]}
                      className="img-fluid"
                      alt="Logo Games"
                    />
                  </div>
                  <span className="btn btn-primary">Subscribe</span>
                </div>
              </Link>
            </div>
            <div className="col-lg-4">
              <Link
                to="/shop/internet/package/details"
                className={"card card-packages card-packages-lg animated " + effect + " delayp4"}
                onClick={() => triggerPage("openSubscribe")}
              >
                <div className="card-header">
                  <div className="badges-list">
                    <div className="badges badge-red left">
                      <h3>50</h3>
                      <span>Mbps</span>
                    </div>
                    <div className="badges badge-blue right">
                      <h3>201</h3>
                      <span>Channels</span>
                    </div>
                    <div className="badges badge-green middle">
                      <h3>1000</h3>
                      <span>Minutes</span>
                    </div>
                  </div>
                  <div className="header-text">
                    <h3>Premium</h3>
                    <p>Internet unlimited, 92 USee TV channels, 100 minutes voice call</p>
                  </div>
                </div>
                <div className="card-body w-btn">
                  <div className="card-body-top">
                    <div className="subheading">
                      <p>IndiHome Premium</p>
                    </div>
                    <h3>
                      Rp535.000<small> / bln</small>
                    </h3>
                    <div className="content">
                      <p>
                        Perfect for everyday internet users with light daily
                        use
                          </p>
                    </div>
                  </div>
                  <div className="card-body-bottom">
                    <div className="subheading">
                      <p>Bonus Subscriptions</p>
                    </div>
                    <div className="content">
                      <p>
                        Get bonus subscriptions from various apps and games!
                          </p>
                    </div>
                    <img
                      src={images.common["logo_add-ons.jpg"]}
                      className="img-fluid"
                      alt="Logo Add-Ons"
                    />
                  </div>
                  <span className="btn btn-primary">Subscribe</span>
                </div>
              </Link>
            </div>
          </div>
        </section>
        <section className={"text-center pt-3 pb-4 animated " + effect + " delayp5"}>
          <p className="mb-0">Belum menemukan yang Anda cari?</p>
          <Link to="/shop/internet/package/all" className="btn btn-link btn-lg">Lihat Semua Paket <i className="far fa-angle-right ml-1"></i></Link>
        </section>
        <div className="row">
          <div className="col-lg-8 mx-auto">
            <div className={"list-group list-group-banner animated " + effect + " delayp6"}>
              <Link
                to="/shop/internet/package-finder"
                className="list-group-item light w-arrow w-bg-cover"
                style={coverStyle}
              >
                <img alt="" src={images.common["grfx_find.png"]} className="w-75px" />
                <div className="list-group-item-content">
                  <h5 className="title w-md-up-75 mb-0">Perlu bantuan memilih paket internet? <br className="d-none d-sm-block" />Coba Package Finder Kami</h5>
                </div>
                {/* <button className="btn btn-light d-none d-md-block">Coba Sekarang</button> */}
              </Link>
            </div>
          </div>
        </div>
      </div>
      {/* Modal - Unlimited Internet Quota */}
      <Modal show={cond.subscribeModal} onHide={() => triggerPage('close')}>
        <div className="modal-body p-box">
          <Link to="#" className="close animated fadeInUp" onClick={() => triggerPage('close')}>
            <span><i className="far fa-times text-primary"></i></span>
          </Link>
          <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
            <h3>Unlimited Internet Quota</h3>
            <p>Lorem Ipsum dolor sit amet. consectur adipicsing elti. Lorem Ipsum dolor sit amet. consectur adipicsing elti.</p>
          </div>
          <div className="list-group animated fadeInUp delayp2 mb-0">
            <div className="list-group-item">
              <div className="badges badge-red">
                <h3>10</h3><span>Mbps</span>
              </div>
              <div className="list-group-item-content">
                <h5 className="title">Advantage 1</h5>
              </div>
            </div>
            <div className="list-group-item">
              <div className="badges badge-red">
                <h3>10</h3><span>Mbps</span>
              </div>
              <div className="list-group-item-content">
                <h5 className="title">Advantage 2</h5>
              </div>
            </div>
            <div className="list-group-item">
              <div className="badges badge-red">
                <h3>10</h3><span>Mbps</span>
              </div>
              <div className="list-group-item-content">
                <h5 className="title">Advantage 2</h5>
              </div>
            </div>
            <div className="list-group-item">
              <div className="badges badge-red">
                <h3>10</h3><span>Mbps</span>
              </div>
              <div className="list-group-item-content">
                <h5 className="title">Advantage 2</h5>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  )
}

