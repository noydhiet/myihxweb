import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Modal, Form } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { PageContext } from './context/PageContext';

import NavTopLogoDark from './include/NavTopLogo'
import NavbarMain from './include/Navbar'
import { Footer } from './include/Footer'
import { CardNotif } from './include/CardPackage';
//General
import TermsAndConditions from './pages/TermsAndConditions';
import PrivacyPolicy from './pages/PrivacyPolicy'

//Device
import DeviceIndihomeSmart from './pages/DeviceIndihomeSmart'
import DeviceIndihomeSmartMonitoring from './pages/DeviceIndihomeSmartMonitoring'
import DeviceIndihomeInfraredMotionDetector from './pages/DeviceIndihomeInfraredMotionDetector'
import DeviceIndihomeSmartConfirm from './pages/DeviceIndihomeSmartConfirm'
import DeviceIndihomeSmartSuccess from './pages/DeviceIndihomeSmartSuccess'

//Register
import Register from './pages/Register';
import AccountDetails from './pages/RegisterAccountDetail'
import ActivationRegister from './pages/RegisterActivation'
import OtpRegister from './pages/RegisterOtp'
import SuccessRegister from './pages/RegisterSuccess'
import AccessRequest from './pages/RegisterRequest'

//Help
import Help from './pages/Help'
import HelpInternet from './pages/HelpInternet'
import HelpInternetDetail1 from './pages/HelpInternetDetail1'
import HelpInternetDetail2 from './pages/HelpInternetDetail2'
import HelpInternetDetail3 from './pages/HelpInternetDetail3'
import HelpInternetDetail4 from './pages/HelpInternetDetail4'
import HelpTelephone from './pages/HelpTelephone'
import HelpTicket from './pages/HelpTicket'
import HelpTv from './pages/HelpTv'
import AcsReboot from './pages/AcsReboot'
import AcsRebootSuccess from './pages/AcsRebootSuccess'
import FaqInfo from './pages/Faq-info'

//Login
import Login from './pages/Login';
import OtpLogin from './pages/LoginMobileOtp';
import VerificationLogin from './pages/LoginMobileVerification';
import PasswordLogin from './pages/LoginEmailPassword';
import ForgotPassword from './pages/LoginEmailForgotPassword'
import NewPassword from './pages/LoginEmailNewPassword'
import RecoveryCode from './pages/LoginEmailRecoveryCode';
import LoginScreenNewMyIndihome from './pages/LoginScreenNewMyIndihome'
import LoginMethodGetVerificationCode from './pages/LoginMethodGetVerificationCode'
import LoginEnterVerificationCode from './pages/LoginEnterVerificationCode'
import LoginNewPassword from './pages/LoginNewPassword'

//Home
import Home from './pages/Home';

//Inbox
import Inbox from './pages/Inbox'

//Promo
import PromoDetail from './pages/PromoDetail'
import PromoTnc from './pages/PromoTnc'

//Bill
import BillTicket1 from './pages/BillTicket1'
import BillTicket2 from './pages/BillTicket2'
import BillDetails from './pages/BillDetails';
import BillSuccess from './pages/BillSuccess';

//Check-coverage
import CheckCoverage from './pages/CheckCoverage'
import CheckCoverageMap from './pages/CheckCoverageMap'
import CVAvailable from './pages/CheckCoverageAvailable'
import PT1 from './pages/CheckCoverageResultPt1'
import PT2 from './pages/CheckCoverageResultPt2'
import PT3 from './pages/CheckCoverageResultPt3'

//Package Finder
import PackageFinder from './pages/ShopInternetPackageFinder'
import ByBudget1 from './pages/ShopInternetPackageFinderBudget1'
import ByBudgetFinish from './pages/ShopInternetPackageFinderBudgetFinish'
import ByNeeds1 from './pages/ShopInternetPackageFinderNeed1'
import ByNeeds2 from './pages/ShopInternetPackageFinderNeed2'
import ByNeeds3 from './pages/ShopInternetPackageFinderNeed3'
import ByNeedsFinish from './pages/ShopInternetPackageFinderFinish'

//Points
import Points from './pages/Points'
import PointsPreview from './pages/PointsPreview'
import PointsHistory from './pages/PointsHistory'
import PointsHistoryPreview from './pages/PointsHistoryPreview'
import PointExpiry from './pages/PointExpiry'
import PointExpiryPreview from './pages/PointExpiryPreview'

//Landing Pages 
import LandingPages from './pages/LandingPages'

//Refunds
import RefundComplete from './pages/RefundComplete'

//Shop 
import Shop from './pages/Shop'

//Shop - Popular Add On
import ShopPopularAddOn from './pages/ShopPopularAddOn'

// Shop - Call
import ShopCall from './pages/ShopCall'

// Shop - Internet - Package
import ShopInternet from './pages/ShopInternet'
import PackageInternet from './pages/ShopInternetPackage'
import PackageInternetAll from './pages/ShopInternetPackageAll'
import ShopInternetPackageCategory from './pages/ShopInternetPackageCategory'
import ShopInternetPackageCategoryDetails from './pages/ShopInternetPackageCategoryDetails'
import PackageInternetDetail from './pages/ShopInternetPackageDetail'

// Shop - Internet - Speed On Demand
import ShopInternetSpeedOnDemand from './pages/ShopInternetSpeedOnDemand'
import ShopInternetSpeedOnDemandSuccess from './pages/ShopInternetSpeedOnDemandSuccess'
import ShopInternetSpeedOnDemandConfirm from './pages/ShopInternetSpeedOnDemandConfirm'
import ServiceNotAvailable from './pages/ShopInternetServiceNotAvailable'

//Shop - Internet - Wifi Id Seamless
import ShopInternetPackageWifiIdSeamless from './pages/ShopInternetPackageWifiIdSeamless'
import ShopInternetPackageWifiIdSeamlessConfirm from './pages/ShopInternetPackageWifiIdSeamlessConfirm'
import ShopInternetPackageWifiIdSeamlessSuccess from './pages/ShopInternetPackageWifiIdSeamlessSuccess'
import ShopInternetPackageWifiIdSeamlessRegisterDevice from './pages/ShopInternetPackageWifiIdSeamlessRegisterDevice'
import ShopInternetPackageWifiIdSeamlessConnect from './pages/ShopInternetPackageWifiIdSeamlessConnect'

//Shop - Internet - Wifi Extender
import ShopInternetPackageWifiExtender from './pages/ShopInternetPackageWifiExtender'
import ShopInternetPackageWifiExtenderConfirm from './pages/ShopInternetPackageWifiExtenderConfirm'
import ShopInternetPackageWifiExtenderSuccess from './pages/ShopInternetPackageWifiExtenderSuccess'

//Shop - Internet - Upgrade Speed
import ShopInternetUpgradeSpeed from './pages/ShopInternetUpgradeSpeed'
import ShopInternetUpgradeSpeedConfirm from './pages/ShopInternetUpgradeSpeedConfirm'
import ShopInternetUpgradeSpeedSuccess from './pages/ShopInternetUpgradeSpeedSuccess'

//Shop - Internet - Upgrade to Fiber
import ShopInternetUpgradeToFiber from './pages/ShopInternetUpgradeToFiber'
import ShopInternetUpgradeToFiberConfirm from './pages/ShopInternetUpgradeToFiberConfirm'
import ShopInternetUpgradeToFiberSuccess from './pages/ShopInternetUpgradeToFiberSuccess'

//Shop - Internet - Cloud Storage
import ShopInternetCloudStorage from './pages/ShopInternetCloudStorage'
import ShopInternetCloudStorageConfirm from './pages/ShopInternetCloudStorageConfirm'
import ShopInternetCloudStorageSuccess from './pages/ShopInternetCloudStorageSuccess'

//Shop - Internet - Indihome Study
import ShopInternetIndihomeStudy from './pages/ShopInternetIndihomeStudy'
import ShopInternetIndihomeStudyConfirm from './pages/ShopInternetIndihomeStudyConfirm'
import ShopInternetIndihomeStudySuccess from './pages/ShopInternetIndihomeStudySuccess'

//Shop - Tv - Minipack
import ShopTvMinipack from './pages/ShopTvMinipack'
import ShopTvMinipackDetail from './pages/ShopTvMinipackDetail'
import ShopTvMinipackConfirm from './pages/ShopTvMinipackConfirm'
import ShopTvMinipackSuccess from './pages/ShopTvMinipackSuccess'

//Shop - Tv - Apps
import ShopTvApps from './pages/ShopTvApps'
import ShopTvAppsHooqDetails from './pages/ShopTvAppsHooqDetails';
import ShopTvAppsHooqConfirm from './pages/ShopTvAppsHooqConfirm';
import ShopTvAppsHooqSuccess from './pages/ShopTvAppsHooqSuccess';
import ShopTvAppsIflixDetails from './pages/ShopTvAppsIflixDetails';
import ShopTvAppsIflixRedirect from './pages/ShopTvAppsIflixRedirect'
import ShopTvAppsIflixSuccess from './pages/ShopTvAppsIflixSuccess'
import ShopTvAppsCatchplayDetails from './pages/ShopTvAppsCatchplayDetails';
import ShopTvAppsCatchplayConfirm from './pages/ShopTvAppsCatchplayConfirm';
import ShopTvAppsCatchplaySuccess from './pages/ShopTvAppsCatchplaySuccess';

import ShopTvAppsEdukids from './pages/ShopTvAppsEdukids'
import ShopTvAppsEdukidsConfirm from './pages/ShopTvAppsEdukidsConfirm'
import ShopTvAppsEdukidsSuccess from './pages/ShopTvAppsEdukidsSuccess'


//Shop - HybridBox
import ShopTvHybridBox from './pages/ShopTvHybridBox'
import ShopTvHybridBoxConfirm from './pages/ShopTvHybridBoxConfirm'
import ShopTvHybridBoxSuccess from './pages/ShopTvHybridBoxSuccess'

//Shop - Tv - Storage
import ShopTvStorage from './pages/ShopTvStorage'
import ShopTvStorageConfirm from './pages/ShopTvStorageConfirm'
import ShopTvStorageSuccess from './pages/ShopTvStorageSuccess'

//Shop - More
import ShopIndihomeCloud from './pages/ShopIndihomeCloud';
import ShopIndihomeCloudConfirm from './pages/ShopIndihomeCloudConfirm'
import ShopIndihomeCloudSuccess from './pages/ShopIndihomeCloudSuccess'
import ShopCloudStorage from './pages/ShopCloudStorage'
import ShopCloudStorageConfirm from './pages/ShopCloudStorageConfirm'
import ShopCloudStorageSuccess from './pages/ShopCloudStorageSuccess'
import ShopIndihomeStudy from './pages/ShopIndihomeStudy'

//Shop - Storage
import ShopStorageTv from './pages/ShopStorageTv'
import ShopStorageTvConfirm from './pages/ShopStorageTvConfirm'
import ShopStorageTvSuccess from './pages/ShopStorageTvSuccess'
import ShopStorageCloud from './pages/ShopStorageCloud'
import ShopStorageCloudConfirm from './pages/ShopStorageCloudConfirm'
import ShopStorageCloudSuccess from './pages/ShopStorageCloudSuccess'

//Usage
import UsageSummary from './pages/UsageSummary'

//Installation 
import InstallationSchedule from './pages/InstallationSchedule'
import InstallationSchedulePreview from './pages/InstallationSchedulePreview'
import InstallationScheduleSuccess from './pages/InstallationScheduleSuccess'
// import InstallationConfirm from './pages/InstallationConfirm'
// import InstallationConfirmEarlyVisit from './pages/InstallationConfirmEarlyVisit'
// import InstallationReschedule from './pages/InstallationReschedule'
import InstallationRescheduleSuccess from './pages/InstallationRescheduleSuccess'
import InstallationDetail from './pages/InstallationDetail'
import InstallationDetailWaitingTicket from './pages/InstallationDetailWaitingTicket'
import InstallationDetailWaiting from './pages/InstallationDetailWaiting'
import InstallationDetailActive from './pages/InstallationDetailActive'
import InstallationDetailSuccess from './pages/InstallationDetailSuccess'
import InstallationDetailFailure from './pages/InstallationDetailFailure'
import FisikTicket from './pages/FisikTicket'
import FisikTicketScheduled from './pages/FisikTicketScheduled'
import FisikTicketStatus from './pages/FisikTicketStatus'
import FisikTicketStatusAssigned from './pages/FisikTicketStatusAssigned'
import FisikTicketStatusClosed from './pages/FisikTicketStatusClosed'
import FisikTicketDone from './pages/FisikTicketDone'
import FisikTicketReOpen from './pages/FisikTicketReOpen'
import FisikTicketHistory from './pages/FisikTicketHistory'
import FisikTicketIssueReport from './pages/FisikTicketIssueReport'
import FisikTicketReOpenSuccess from './pages/FisikTicketReOpenSuccess'
//Personal Data
import PersonalData from './pages/PersonalData'
import ReviewSignature from './pages/PersonalDataReviewSignature'
import ReviewKtp from './pages/PersonalDataReviewKTP'
import PersonalDataReviewEKTP from './pages/PersonalDataReviewEKTP'
import PersonalDataLivenessTest from './pages/PersonalDataLivenessTest'
import PersonalDataLivenessTestBox from './pages/PersonalDataLivenessTestBox'
import PersonalDataVerificationProgress from './pages/PersonalDataVerificationProgress'

//Purchase
import PurchasePaymentMethod from './pages/PurchasePaymentMethod'
import PurchasePaymentMethodPreview from './pages/PurchasePaymentMethodPreview'
import PurchasePaymentWithCard from './pages/PurchasePaymentWithCard'
import PurchasePaymentWithLinkAja from './pages/PurchasePaymentWithLinkAja'
import PurchasePaymentWithLinkAjaPreview from './pages/PurchasePaymentWithLinkAjaPreview'
import PurchaseConnectWithLinkAja from './pages/PurchaseConnectWithLinkAja'
import PurchaseConnectWithLinkAjaPreview from './pages/PurchaseConnectWithLinkAjaPreview'
import PurchaseLinkAjaOtp from './pages/PurchaseLinkAjaOtp'
import PurchaseLinkAjaOtpPreview from './pages/PurchaseLinkAjaOtpPreview'
import PurchasePaymentWithTransfer from './pages/PurchasePaymentWithTransfer'
import PurchasePaymentWithTransferConfirm from './pages/PurchasePaymentWithTransferConfirm'
import PurchaseConfirmOrder from './pages/PurchaseConfirmOrder'
import PaymentPurchaseOrderPreview from './pages/PurchaseConfirmOrderPreview'
import PurchaseSuccess from './pages/PurchaseSuccess'

//Profile
import Profile from './pages/Profile'
import ProfileEdit from './pages/ProfileEdit'
import ProfileChangePassword from './pages/ProfileChangePassword'
import ProfileCompletionTask from './pages/ProfileCompletionTask'
import ProfileChangeNumber from './pages/ProfileChangeNumber'
import ProfileChangeNumberMethod from './pages/ProfileChangeNumberMethod'
import ProfileChangeNumberVerify from './pages/ProfileChangeNumberVerify'
import ProfileChangeEmail from './pages/ProfileChangeEmail'
import ProfileChangeEmailVerify from './pages/ProfileChangeEmailVerify'
import ProfileVerifiedAccount from './pages/ProfileVerifiedAccount'

//Rewards
import RewardsBrowse from './pages/RewardsBrowse'
import RewardsVoucher from './pages/RewardsVoucher'

//Svm2
import Svm2Method from './pages/Svm2-method'
import Svm2Otp from './pages/Svm2-Otp'

//History
import HistoryBill from './pages/HistoryBill'
import HistoryBillDetails from './pages/HistoryBillDetails'
import HistoryPurchase from './pages/HistoryPurchase'
import HistoryPurchaseDetails from './pages/HistoryPurchaseDetails'
import HistoryActivity from './pages/HistoryActivity'
import HistoryActivityProblemReport1 from './pages/HistoryActivityProblemReport1'
import HistoryActivityInstallationReport from './pages/HistoryActivityInstallationReport'
import HistoryTicket from './pages/HistoryTicket'
import HistoryTicket1 from './pages/HistoryTicket1'

//Wallet
import WalletDetails from './pages/WalletDetails'
import WalletDetailsPreview from './pages/WalletDetailsPreview'
// import FisikTicketScheduled from './pages/FisikTicketScheduled/component';
// import PointExpiry from './pages/PointExpiry/component';

import { withRouter } from 'react-router'

import { clearStorages, setSessionTimer, getDeviceId, setDeviceId, getFcmToken, setFcmToken, getToken, getBearerHooq, setBearerHooq } from './utils/storage';
import { saveDeviceToken } from './services'
import { messaging, analytics } from "./init-fcm";
class Routes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOffline: false
    }
    this.unlisten = this.props.history.listen((location, action) => {
      console.log('analytics', analytics)
      if(analytics) {
        analytics.logEvent(`${action} -> ${location.pathname}`);
        console.log('You changed the page to: => ', action + ' -> ', location.pathname)
      }
    });
  }
  static contextType = PageContext;

  componentDidMount() {   
    const { modal } = this.context; 
    setSessionTimer(() => modal('sessionTimeOut'));
    this.notificationSetup();
    this.handleConnectionChange();
    window.addEventListener('online', this.handleConnectionChange);
    window.addEventListener('offline', this.handleConnectionChange);
  }

  notificationSetup() {
    if(messaging) {
      messaging.requestPermission()
      .then(async function() {
        const token = await messaging.getToken();
        console.log("token", token);
        console.log("getFcmToken", getFcmToken());
        console.log("getDeviceId", getDeviceId());
        console.log("getToken", getToken());
        if(token !== getFcmToken() || (getToken() && getToken() !== 'null' && (!getBearerHooq() || getBearerHooq() ==='null'))) {
          setFcmToken(token)
          if(getToken() && getToken() !== 'null') {
            setBearerHooq(true);
          }
          const getDeviceId1 = !getDeviceId() || getDeviceId() === 'null' ? '' : getDeviceId();
          const payload = {
            "deviceId": getDeviceId1,
            "deviceType": "WEB",
            "fcmToken": token
          }
          console.log("payload", payload);
          saveDeviceToken(payload).then(response => {
            console.log('saveDeviceToken', response.data)
            if(response.status === 200 && response.data.message === 'success') {
              setDeviceId(response.data.data.deviceId)
            }
          }).catch( error => {
            console.error(error);
          });
        }
      })
      .catch(function(err) {
        console.log("Unable to get permission to notify.", err);
      });
      // navigator.serviceWorker.addEventListener("message", (event) => {
      //   console.log('message', event);
      // });
    }
  }

  componentWillUnmount() {
    this.unlisten();
    window.removeEventListener('online', this.handleConnectionChange);
    window.removeEventListener('offline', this.handleConnectionChange);
  }

  handleConnectionChange = () => {
    const condition = navigator.onLine ? 'online' : 'offline';
    if (condition === 'online') {
      const webPing = setInterval(
        () => {
          fetch('//google.com', {
            mode: 'no-cors',
            })
          .then(() => {
            this.setState({ isOffline: false }, () => {
              return clearInterval(webPing)
            });
          }).catch(() => this.setState({ isOffline: true }) )
        }, 2000);
      return;
    }
    return this.setState({ isOffline: true });
  }

  render() {
    const { modalSubscribe, modalSessionTimeOut, condition, navCasePage, modalAdditionalFee, notifAdditionalFee, notifRating, notifForgetPassword, notifRecoveryPassword, modal, modalSetNewSchedule, modalRating, modalSelectReason,  modalRatingDone, modalSwitchToApp, value, icon, position, display, footerCasePage, brand, common, sample } = this.context;
    const images = { common, sample, brand };
    const viewNav = condition === 1 ? <NavTopLogoDark images={images} condition={condition} /> : <NavbarMain images={images} condition={condition} navCasePage={navCasePage} />
    const viewNotifAdditionalFee = notifAdditionalFee ? "active" : ""
    const viewNotifRatingDone = notifRating ? "active" : "";
    const viewNotifForgetPassword = notifForgetPassword ? "active" : "";
    const viewNotifRecoveryPassword = notifRecoveryPassword ? "active" : "";
    const pathSuffix = window.location.pathname.replace(/\//g, '--').substring(2);
    const config = {
      position: position,
      display: display,
      activeFormRating: value > 3 ? " active" : ""
    }
    if (notifAdditionalFee === true) {
      this.context.closeNotif("additionalFee")
    }
    if (notifRating) {
      this.context.closeNotif("ratingDone");
    }
    if (this.context.bgCase === "w-bg") {
      this.context.addBg();
    } else {
      this.context.removeBg();
    }
    return (
      <div>
        {viewNav}
        <Switch>
          <Route path="/myindihome" component={LandingPages} exact />
          <Route path="/shop/indihome-smart" component={DeviceIndihomeSmart} exact />
          <Route path="/shop/indihome-smart/smart-monitoring" component={DeviceIndihomeSmartMonitoring} exact />
          <Route path="/shop/indihome-smart/infrared-motion-detector" component={DeviceIndihomeInfraredMotionDetector} exact />
          <Route path="/shop/indihome-smart/confirm" component={DeviceIndihomeSmartConfirm} exact />
          <Route path="/shop/indihome-smart/success" component={DeviceIndihomeSmartSuccess} exact />
          <Route path="/profile" component={Profile} exact />
          <Route path="/profile/edit" component={ProfileEdit} exact />
          <Route path="/profile/change-password" component={ProfileChangePassword} exact />
          <Route path="/profile/change-phone-number" component={ProfileChangeNumber} exact />
          <Route path="/profile/change-phone-number/method" component={ProfileChangeNumberMethod} exact />
          <Route path="/profile/change-phone-number/verify" component={ProfileChangeNumberVerify} exact />
          <Route path="/profile/change-email" component={ProfileChangeEmail} exact />
          <Route path="/profile/change-email/verify" component={ProfileChangeEmailVerify} exact />
          <Route path="/profile/completion-task" component={ProfileCompletionTask} exact />
          <Route path="/profile/verified-account" component={ProfileVerifiedAccount} exact />
          <Route path="/bill/details" component={BillDetails} exact />
          <Route path="/bill/ticket/waiting" component={BillTicket1} exact />
          <Route path="/bill/ticket/done" component={BillTicket2} exact />
          <Route path="/bill/success" component={BillSuccess} exact />
          <Route path="/history/bill" component={HistoryBill} exact />
          <Route path="/history/bill/details" component={HistoryBillDetails} exact />
          <Route path="/history/purchase" component={HistoryPurchase} exact />
          <Route path="/history/purchase/details" component={HistoryPurchaseDetails} exact />
          <Route path="/history/activity" component={HistoryActivity} exact />
          <Route path="/history/activity/problem-report" component={HistoryActivityProblemReport1} exact />
          <Route path="/history/activity/installation-report" component={HistoryActivityInstallationReport} exact />
          <Route path="/history/ticket" component={HistoryTicket} exact />
          <Route path="/history/ticket-1" component={HistoryTicket1} exact />
          <Route path="/help" component={Help} exact />
          <Route path="/help/internet" component={HelpInternet} exact />
          <Route path="/help/internet/detail/1" component={HelpInternetDetail1} exact />
          <Route path="/help/internet/detail/2" component={HelpInternetDetail2} exact />
          <Route path="/help/internet/detail/3" component={HelpInternetDetail3} exact />
          <Route path="/help/internet/detail/4" component={HelpInternetDetail4} exact />
          <Route path="/help/internet/detail/ticket" component={HelpTicket} exact />
          <Route path="/help/internet/reboot-modem" component={AcsReboot} exact />
          <Route path="/help/internet/reboot-success" component={AcsRebootSuccess} exact />
          <Route path="/help/telephone" component={HelpTelephone} exact />
          <Route path="/help/faq-info" component={FaqInfo} exact />
          <Route path="/help/tv" component={HelpTv} exact />
          <Route path="/inbox/:type" component={Inbox} exact />
          <Route path="/promo-detail" component={PromoDetail} exact />
          <Route path="/promo-tnc" component={PromoTnc} exact />
          <Route path="/personal-data" component={PersonalData} exact />
          <Route path="/personal-data/review-signature" component={ReviewSignature} exact />
          <Route path="/personal-data/review-ktp" component={ReviewKtp} exact />
          <Route path="/personal-data/review-e-ktp" component={PersonalDataReviewEKTP} exact />
          <Route path="/personal-data/liveness-test" component={PersonalDataLivenessTest} exact />
          <Route path="/personal-data/liveness-test/result" component={PersonalDataLivenessTestBox} exact />
          <Route path="/personal-data/verification-progress" component={PersonalDataVerificationProgress} exact />
          <Route path="/purchase/payment-method" component={PurchasePaymentMethod} exact />
          <Route path="/purchase/payment-method-preview" component={PurchasePaymentMethodPreview} exact />
          <Route path="/purchase/payment-method/payment-with-card" component={PurchasePaymentWithCard} exact />
          <Route path="/purchase/payment-method/payment-with-linkaja" component={PurchasePaymentWithLinkAja} exact />
          <Route path="/purchase/payment-method/payment-with-linkaja-preview" component={PurchasePaymentWithLinkAjaPreview} exact />
          <Route path="/purchase/connect-link-aja" component={PurchaseConnectWithLinkAja} exact />
          <Route path="/purchase/connect-link-aja-preview" component={PurchaseConnectWithLinkAjaPreview} exact />
          <Route path="/purchase/link-aja/otp" component={PurchaseLinkAjaOtp} exact />
          <Route path="/purchase/link-aja/otp-preview" component={PurchaseLinkAjaOtpPreview} exact />
          <Route path="/purchase/payment-method/payment-with-transfer" component={PurchasePaymentWithTransfer} exact />
          <Route path="/purchase/payment-method/payment-with-transfer/confirm" component={PurchasePaymentWithTransferConfirm} exact />
          <Route path="/purchase/confirm-order" component={PurchaseConfirmOrder} exact />
          <Route path="/purchase/confirm-order-preview" component={PaymentPurchaseOrderPreview} exact />
          <Route path="/purchase/success" component={PurchaseSuccess} exact />
          <Route path="/installation/schedule" component={InstallationSchedule} exact />
          <Route path="/installation/schedule-preview" component={InstallationSchedulePreview} exact />
          <Route path="/installation/schedule/success:pathSuffix" component={InstallationScheduleSuccess} exact />
          <Route path="/installation/reschedule/success/:date_time" component={InstallationRescheduleSuccess} exact />
          <Route path="/installation/detail" component={InstallationDetail} exact />
          <Route path="/installation/detail-active" component={InstallationDetailActive} exact />
          <Route path="/installation/detail-failure" component={InstallationDetailFailure} exact />
          <Route path="/installation/detail-success" component={InstallationDetailSuccess} exact />
          <Route path="/installation/detail-waiting" component={InstallationDetailWaiting} exact />
          <Route path="/installation/detail-waiting/ticket/:bookingId" component={InstallationDetailWaitingTicket} exact />
          <Route path="/installation/fisik-ticket" component={FisikTicket} exact />
          <Route path="/installation/fisik-ticket/scheduled:pathSuffix" component={FisikTicketScheduled} exact />
          <Route path="/installation/fisik-ticket/status" component={FisikTicketStatus} exact />
          <Route path="/installation/fisik-ticket/status-assigned" component={FisikTicketStatusAssigned} exact />
          <Route path="/installation/fisik-ticket/status-closed" component={FisikTicketStatusClosed} exact />
          <Route path="/installation/fisik-ticket/done" component={FisikTicketDone} exact />
          <Route path="/installation/fisik-ticket/re-open/:ticketId" component={FisikTicketReOpen} exact />
          <Route path="/installation/fisik-ticket/re-open/success/:date_time" component={FisikTicketReOpenSuccess} exact />
          <Route path="/installation/fisik-ticket/history" component={FisikTicketHistory} exact />
          <Route path="/installation/fisik-ticket/issue-report" component={FisikTicketIssueReport} exact />
          <Route path="/check-coverage" component={CheckCoverage} exact />
          <Route path="/check-coverage/map" component={CheckCoverageMap} exact />
          <Route path="/check-coverage/available" component={CVAvailable} exact />
          <Route path="/check-coverage/result-pt1" component={PT1} exact />
          <Route path="/check-coverage/result-pt2" component={PT2} exact />
          <Route path="/check-coverage/result-pt3" component={PT3} exact />
          <Route path="/points" component={Points} exact />
          <Route path="/points-preview" component={PointsPreview} exact />
          <Route path="/points/history" component={PointsHistory} exact />
          <Route path="/points/history-preview" component={PointsHistoryPreview} exact />
          <Route path="/points/expiry" component={PointExpiry} exact />
          <Route path="/points/expiry-preview" component={PointExpiryPreview} exact />
          <Route path="/rewards/browse" component={RewardsBrowse} exact />
          <Route path="/rewards/vouchers" component={RewardsVoucher} exact />
          <Route path="/refunds/complete" component={RefundComplete} exact />
          <Route path="/shop" component={Shop} exact />
          <Route path="/shop/popular-addon" component={ShopPopularAddOn} exact />
          <Route path="/shop/call" component={ShopCall} exact />
          <Route path="/shop/internet" component={ShopInternet} exact />
          <Route path="/shop/internet/package-finder" component={PackageFinder} exact />
          <Route path="/shop/internet/package-finder/budget/1" component={ByBudget1} exact />
          <Route path="/shop/internet/package-finder/budget/finish" component={ByBudgetFinish} exact />
          <Route path="/shop/internet/package-finder/needs/1" component={ByNeeds1} exact />
          <Route path="/shop/internet/package-finder/needs/2" component={ByNeeds2} exact />
          <Route path="/shop/internet/package-finder/needs/3" component={ByNeeds3} exact />
          <Route path="/shop/internet/package-finder/needs/finish" component={ByNeedsFinish} exact />
          <Route path="/shop/internet/package" component={PackageInternet} exact />
          <Route path="/shop/internet/package/details" component={PackageInternetDetail} exact />
          <Route path="/shop/internet/package/all" component={PackageInternetAll} exact />
          <Route path="/shop/internet/package/category" component={ShopInternetPackageCategory} exact />
          <Route path="/shop/internet/package/category/details" component={ShopInternetPackageCategoryDetails} exact />
          <Route path="/shop/internet/speed-on-demand" component={ShopInternetSpeedOnDemand} exact />
          <Route path="/shop/internet/speed-on-demand/success" component={ShopInternetSpeedOnDemandSuccess} exact />
          <Route path="/shop/internet/speed-on-demand/confirm" component={ShopInternetSpeedOnDemandConfirm} exact />
          <Route path="/shop/internet/service-not-available" component={ServiceNotAvailable} exact />
          <Route path="/shop/internet/wifi-id-seamless" component={ShopInternetPackageWifiIdSeamless} exact />
          <Route path="/shop/internet/wifi-id-seamless/confirm" component={ShopInternetPackageWifiIdSeamlessConfirm} exact />
          <Route path="/shop/internet/wifi-id-seamless/success" component={ShopInternetPackageWifiIdSeamlessSuccess} exact />
          <Route path="/shop/internet/wifi-id-seamless/register-device" component={ShopInternetPackageWifiIdSeamlessRegisterDevice} exact />
          <Route path="/shop/internet/wifi-id-seamless/connect-wifi-id" component={ShopInternetPackageWifiIdSeamlessConnect} exact />
          <Route path="/shop/internet/wifi-extender" component={ShopInternetPackageWifiExtender} exact />
          <Route path="/shop/internet/wifi-extender/confirm" component={ShopInternetPackageWifiExtenderConfirm} exact />
          <Route path="/shop/internet/wifi-extender/success" component={ShopInternetPackageWifiExtenderSuccess} exact />
          <Route path="/shop/internet/upgrade-speed" component={ShopInternetUpgradeSpeed} exact />
          <Route path="/shop/internet/upgrade-speed/confirm" component={ShopInternetUpgradeSpeedConfirm} exact />
          <Route path="/shop/internet/upgrade-speed/success" component={ShopInternetUpgradeSpeedSuccess} exact />
          <Route path="/shop/internet/upgrade-to-fiber" component={ShopInternetUpgradeToFiber} exact />
          <Route path="/shop/internet/upgrade-to-fiber/confirm" component={ShopInternetUpgradeToFiberConfirm} exact />
          <Route path="/shop/internet/upgrade-to-fiber/success" component={ShopInternetUpgradeToFiberSuccess} exact />
          <Route path="/shop/internet/cloud-storage" component={ShopInternetCloudStorage} exact />
          <Route path="/shop/internet/cloud-storage/confirm" component={ShopInternetCloudStorageConfirm} exact />
          <Route path="/shop/internet/cloud-storage/success" component={ShopInternetCloudStorageSuccess} exact />
          <Route path="/shop/internet/indihome-study" component={ShopInternetIndihomeStudy} exact />
          <Route path="/shop/internet/indihome-study/confirm" component={ShopInternetIndihomeStudyConfirm} exact />
          <Route path="/shop/internet/indihome-study/success" component={ShopInternetIndihomeStudySuccess} exact />
          <Route path="/shop/storage/tv-storage" component={ShopStorageTv} exact />
          <Route path="/shop/storage/tv-storage/confirm" component={ShopStorageTvConfirm} exact />
          <Route path="/shop/storage/tv-storage/success" component={ShopStorageTvSuccess} exact />
          <Route path="/shop/storage/cloud" component={ShopStorageCloud} exact />
          <Route path="/shop/storage/cloud/confirm" component={ShopStorageCloudConfirm} exact />
          <Route path="/shop/storage/cloud/success" component={ShopStorageCloudSuccess} exact />
          <Route path="/shop/tv/minipack" component={ShopTvMinipack} exact />
          <Route path="/shop/tv/minipack/details" component={ShopTvMinipackDetail} exact />
          <Route path="/shop/tv/minipack/confirm" component={ShopTvMinipackConfirm} exact />
          <Route path="/shop/tv/minipack/success" component={ShopTvMinipackSuccess} exact />
          <Route path="/shop/tv/apps" component={ShopTvApps} exact />
          <Route path="/shop/tv/apps/iflix/details" component={ShopTvAppsIflixDetails} exact />
          <Route path="/shop/tv/apps/iflix/redirect" component={ShopTvAppsIflixRedirect} exact />
          <Route path="/shop/tv/apps/iflix/success" component={ShopTvAppsIflixSuccess} exact />
          <Route path="/shop/tv/apps/hooq/details" component={ShopTvAppsHooqDetails} exact />
          <Route path="/shop/tv/apps/hooq/confirm" component={ShopTvAppsHooqConfirm} exact />
          <Route path="/shop/tv/apps/hooq/success" component={ShopTvAppsHooqSuccess} exact />
          <Route path="/shop/tv/apps/catchplay/details" component={ShopTvAppsCatchplayDetails} exact />
          <Route path="/shop/tv/apps/catchplay/confirm" component={ShopTvAppsCatchplayConfirm} exact />
          <Route path="/shop/tv/apps/catchplay/success" component={ShopTvAppsCatchplaySuccess} exact />
          <Route path="/shop/tv/edukids" component={ShopTvAppsEdukids} exact />
          <Route path="/shop/tv/edukids/confirm" component={ShopTvAppsEdukidsConfirm} exact />
          <Route path="/shop/tv/edukids/success" component={ShopTvAppsEdukidsSuccess} exact />
          <Route path="/shop/tv/hybrid-box" component={ShopTvHybridBox} exact />
          <Route path="/shop/tv/hybrid-box/confirm" component={ShopTvHybridBoxConfirm} exact />
          <Route path="/shop/tv/hybrid-box/success" component={ShopTvHybridBoxSuccess} exact />
          <Route path="/shop/tv/tv-storage" component={ShopTvStorage} exact />
          <Route path="/shop/tv/tv-storage/confirm" component={ShopTvStorageConfirm} exact />
          <Route path="/shop/tv/tv-storage/success" component={ShopTvStorageSuccess} exact />
          <Route path="/shop/more/indihome-cloud" component={ShopIndihomeCloud} exact />
          <Route path="/shop/more/indihome-cloud/confirm" component={ShopIndihomeCloudConfirm} exact />
          <Route path="/shop/more/indihome-cloud/success" component={ShopIndihomeCloudSuccess} exact />
          <Route path="/shop/more/cloud-storage" component={ShopCloudStorage} exact />
          <Route path="/shop/more/cloud-storage/confirm:pathSuffix" component={ShopCloudStorageConfirm} exact />
          <Route path="/shop/more/cloud-storage/success" component={ShopCloudStorageSuccess} exact />
          <Route path="/shop/more/indihome-study" component={ShopIndihomeStudy} exact />
          <Route path="/usage-summary" component={UsageSummary} exact />
          <Route path="/" component={LandingPages} exact />
          <Route path="/home" component={Home} exact />
          <Route path="/terms-conditions" component={TermsAndConditions} exact />
          <Route path="/privacy-policy" component={PrivacyPolicy} exact />
          <Route path="/register" component={Register} exact />
          <Route path="/register/account-details" component={AccountDetails} exact />
          <Route path="/register/activation" component={ActivationRegister} exact />
          <Route path="/register/otp" component={OtpRegister} exact />
          <Route path="/register/success" component={SuccessRegister} exact />
          <Route path="/register/request" component={AccessRequest} exact />
          <Route path="/svm2-method" component={Svm2Method} exact />
          <Route path="/svm2-otp" component={Svm2Otp} exact />
          <Route path="/login" component={Login} exact />
          <Route path="/login:pathSuffix" component={Login} exact />
          <Route path="/login/mobile/verification" component={VerificationLogin} exact />
          <Route path="/login/mobile/verification:pathSuffix" component={VerificationLogin} exact />
          <Route path="/login/mobile/otp" component={OtpLogin} exact />
          <Route path="/login/mobile/otp:pathSuffix" component={OtpLogin} exact />
          <Route path="/login/email/password" component={PasswordLogin} exact />
          <Route path="/login/email/password:pathSuffix" component={PasswordLogin} exact />
          <Route path="/login/forgot-password" component={ForgotPassword} exact />
          <Route path="/login/email/new-password" component={NewPassword} exact />
          <Route path="/login/email/recovery-code" component={RecoveryCode} exact />
          <Route path="/login/screen/new-myindihome" component={LoginScreenNewMyIndihome} exact />
          <Route path="/login/method/verification-code" component={LoginMethodGetVerificationCode} exact />
          <Route path="/login/input/verification-code" component={LoginEnterVerificationCode} exact />
          <Route path="/login/new-password" component={LoginNewPassword} exact />
          <Route path="/wallet/details" component={WalletDetails} exact />
          <Route path="/wallet/details-preview" component={WalletDetailsPreview} exact />
        </Switch>
        <Footer condition={condition} images={images} footerCasePage={footerCasePage} />
        <CardNotif active={this.state.isOffline ? 'active' : ''} textTop={'Error'} textCenter={'Please check your internet connection'} />
        {/* Modal Subscribe */}
        <Modal show={modalSubscribe} onHide={() => this.context.modal('close')} centered={true}>
          <div className="modal-body p-box">
            <Link className="close animated fadeInUp" to="#" onClick={() => this.context.modal('close')}>
              <span><i className="far fa-times text-primary"></i></span>
            </Link>
            <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
              <img alt="icon" src={images.common["ic_header_login.png"]} className="w-100px" />
              <h3>Login or Register to Continue</h3>
              <p>Login or create account to continue the purchase process. This will only take a few minutes</p>
            </div>
            <div className="btn-placeholder animated fadeInUp delayp2">
              <div className="row">
                <div className="col-6">
                  <Link to={'/login' + (pathSuffix && `:${pathSuffix}`)} className="btn btn-transparent btn-block" onClick={() => this.context.modal('close')}>Login</Link>
                </div>
                <div className="col-6">
                  <Link to="/register/account-details" className="btn btn-primary btn-block" onClick={() => this.context.modal('close')}>Register</Link>
                </div>
              </div>
            </div>
          </div>
        </Modal>

        {/* Modal Session Time Out */}
        <Modal show={modalSessionTimeOut} onHide={() => {}} centered={true}>
          <div className="modal-body p-box">
            <div className="heading animated fadeInUp delayp1 pt-sm-down-4">
              <img alt="icon" src={images.common["ic_header_login.png"]} className="w-100px" />
              <h3>Session Time Out</h3>
              <p>Please login to continue.</p>
            </div>
            <div className="btn-placeholder animated fadeInUp delayp2">
              <Link to={'/login' + (pathSuffix && `:${pathSuffix}`)} className="btn btn-primary btn-block" onClick={() => this.context.modal('close')}>Login</Link>
              <button className="btn btn-block" onClick={() => {
                clearStorages();
                window.location.reload();
              }}>Continue as guest</button>
            </div>
          </div>
        </Modal>

        {/* MODAL SwitchToApp  */}
        <Modal show={modalSwitchToApp} onHide={() => this.context.modal('close')}>
          <div className="modal-body p-box">
            <Link to="#" className="close animated fadeInUp" onClick={() => this.context.modal('close')}>
              <span><i className="far fa-times text-primary"></i></span>
            </Link>
            <div className="heading text-center animated fadeInUp delayp1 pt-sm-down-4">
              <h3>Get 10% Off by Continuing in App!</h3>
              <p>Download myIndiHome app and scan this QR Code to continue purchase.</p>
              <img alt="img" src={images.common["QR-Code-Standard.jpg"]} className="img-fluid w-200px" />
            </div>
            <div className="btn-placeholder">
              <button className="btn btn-transparent btn-block" onClick={() => this.context.modal('close')}>Open myIndiHome App</button>
              <button className="btn btn-primary btn-block" onClick={() => this.context.modal('close')}>Continue in Website</button>
            </div>
            <div className="btn-placeholder">
              <button className="btn btn-transparent btn-block" onClick={() => this.context.modal('close')}>Open myIndiHome App</button>
              <button className="btn btn-primary btn-block" onClick={() => this.context.modal('close')}>Continue in Website</button>
            </div>
          </div>
        </Modal>
        {/* MODAL Additional / Technician Modal  */}
        <Modal show={modalAdditionalFee} onHide={() => this.modal("additionalFee")}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0 animated fadeInUp">
              <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                <img className="header-img" src={images.common['ic_header_verification.png']} alt="Icon Success" />
              </header>
              <section className="animated fadeInUp delayp2">
                <h1>Confirm request for additional fee</h1>
                <p>You are charged Rp200.000 for additional installation cable. This amount will be billed along with your july 2019 billing.</p>
                <div className="text-box mb-0">
                  <h5 className="title">Tuesday, 23 July 2019</h5>
                  <p className="desc">Estimated arrival time 07:00</p>
                </div>
              </section>
              <div className="btn-placeholder inline bottom animated fadeInUp delayp3">
                <Link to="#" className="btn btn-transparent btn-block" onClick={() => this.modal("additionalFee")}>Ignore Request</Link>
                <Link to="#" className="btn btn-primary btn-block" onClick={() => this.modal("confirmAdditionalFee")}>Confirm Payment</Link>
              </div>
            </div>
          </div>
        </Modal>
        
        {/* MODAL Set New Schedule / Technician Modal  */}
        <Modal show={modalSetNewSchedule} onHide={() => this.modal("setNewSchedule")}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0 animated fadeInUp">
              <header className="header-light white w-l-icon icon-center animated fadeInUp delayp1">
                <img className="header-img" src={images.common['graphic_find2.png']} alt="Icon Success" />
              </header>
              <section className="animated fadeInUp delayp2">
                <h1>Please set a new schedule for installation</h1>
                <p>We are very sorry, your indiHome installation cannot be completed right away. Please set another appointment to complete the installation process</p>
              </section>
              <div className="btn-placeholder inline bottom mt-0 animated fadeInUp delayp3">
                <Link to="#" className="btn btn-transparent btn-block" onClick={() => this.modal("close")}>Set New Schedule</Link>
                <Link to="/home" className="btn btn-primary btn-block">Back To Home</Link>
              </div>
            </div>
          </div>
        </Modal>

        {/* MODAL Rating  */}
        <Modal show={modalRating} onHide={() => this.context.modal("close")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0 pb-4 animated fadeInUp">
              <div className="heading mb-5 animated fadeInUp delayp1 pt-sm-down-4">
                <h4>Based on your experience requesting for support for IndiHome services, how likely are you to recommend it to your friends? </h4>
              </div>
              <section className="py-main-sm pb-0 animated fadeInUp delayp2">
                <form className="needs-validation" id="form" noValidate>
                  <div className="mb-4">
                    <div className="slider-budget">
                      <input type="range" min="1" max="10" defaultValue="10" id="value" onChange={this.context.onChange} />
                      <output style={{ left: config.position, display: config.display }}>
                        <span className="icon">
                          {/* {icon} */}
                          <img alt="img" src={images.common[`${icon}`]} style={{ width: "38%" }} />
                        </span>
                        <span className="value">{value}</span>
                      </output>
                      <div className="desc-slider">
                        <p>Very not likely</p>
                        <p>Very likely</p>
                      </div>
                    </div>
                  </div>
                  {/* <div className="row mb-3">
                    <div className="col-12">
                      <div className="subheading">
                        <p className="text-left">What can we do to improve?(optional)</p>
                      </div>
                    </div>
                    <div className="col-md-6 col-6">
                      <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customControlValidation1" required />
                        <label className="custom-control-label" htmlFor="customControlValidation1">Ease of use</label>
                      </div>
                      <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customControlValidation2" required />
                        <label className="custom-control-label" htmlFor="customControlValidation2">Customer Service</label>
                      </div>
                      <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customControlValidation3" required />
                        <label className="custom-control-label" htmlFor="customControlValidation3">Speed</label>
                      </div>
                    </div>
                    <div className="col-md-6 col-6 mb-3">
                      <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customControlValidation4" required />
                        <label className="custom-control-label" htmlFor="customControlValidation4">Item 4</label>
                      </div>
                      <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customControlValidation5" required />
                        <label className="custom-control-label" htmlFor="customControlValidation5">Item 5</label>
                      </div>
                      <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customControlValidation6" required />
                        <label className="custom-control-label" htmlFor="customControlValidation6">Other</label>
                      </div>
                    </div>
                    <div className="col-12">
                      <Form.Group className="mb-0 w-100">
                        <Form.Control
                          placeholder="Other Feedback"
                        />
                      </Form.Group>
                    </div>
                  </div> */}
                  <div className="btn-placeholder">
                    <Link to="#" className="btn btn-primary btn-block" onClick={() => this.context.modal("ratingSelectReason")}>Next</Link>
                    <Link to="#" className="btn btn-transparent btn-block" onClick={() => this.context.modal("close")}>Skip</Link>
                  </div>
                </form>
              </section>
            </div>
          </div>
        </Modal>
        <Modal show={modalSelectReason} onHide={() => this.context.modal("ratingDone")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0 pb-4 animated fadeInUp">
              <div className="heading mb-5 animated fadeInUp delayp1 pt-sm-down-4">
                <h3>Select your reason below</h3>
              </div>
              <section className="pb-0 animated fadeInUp delayp2">
                <Form>
                  <div className="list-group">
                    <div className="list-group-item w-checkbox" onClick={() => this.context.checkBoxSelectReason(0)}>
                      <div className="form-group checkbox mr-4">
                        <div className="custom-control custom-checkbox">
                          <input type="checkbox" className="custom-control-input check" />
                          <label className="custom-control-label"></label>
                        </div>
                      </div>
                      <div className="list-group-item-content">
                        <h5 className="title hidden">Schedule is as desired</h5>
                        <h5 className="title">Schedule is not as desired</h5>
                      </div>
                    </div>
                    <div className="list-group-item w-checkbox" onClick={() => this.context.checkBoxSelectReason(1)}>
                      <div className="form-group checkbox mr-4">
                        <div className="custom-control custom-checkbox">
                          <input type="checkbox" className="custom-control-input check" />
                          <label className="custom-control-label"></label>
                        </div>
                      </div>
                      <div className="list-group-item-content">
                        <h5 className="title hidden">Technician arrive on time</h5>
                        <h5 className="title">Technician didn't arrive on time</h5>
                      </div>
                    </div>
                    <div className="list-group-item w-checkbox" onClick={() => this.context.checkBoxSelectReason(2)}>
                      <div className="form-group checkbox mr-4">
                        <div className="custom-control custom-checkbox">
                          <input type="checkbox" className="custom-control-input check" />
                          <label className="custom-control-label"></label>
                        </div>
                      </div>
                      <div className="list-group-item-content">
                        <h5 className="title hidden">Installations were completed on the 1st visit</h5>
                        <h5 className="title">Installations weren't complete on the 1st visit</h5>
                      </div>
                    </div>
                    <div className="list-group-item w-checkbox" onClick={() => this.context.checkBoxSelectReason(3)}>
                      <div className="form-group checkbox mr-4">
                        <div className="custom-control custom-checkbox">
                          <input type="checkbox" className="custom-control-input check" />
                          <label className="custom-control-label"></label>
                        </div>
                      </div>
                      <div className="list-group-item-content">
                        <h5 className="title hidden">Installations are neat</h5>
                        <h5 className="title">Installations are not neat</h5>
                      </div>
                    </div>
                  </div>
                  <Form.Group>
                    <Form.Control
                    type="text"
                    placeholder="Other Feedback"
                    />
                  </Form.Group>
                </Form>
                <div className="btn-placholder">
                  <button className="btn btn-primary btn-block" onClick={() => modal('ratingDone')}>Submit</button>
                </div>
              </section>
            </div>
          </div>
        </Modal>
        <Modal show={modalRatingDone} onHide={() => this.context.modal("close")} centered={true}>
          <div className="modal-body p-box-popup">
            <div className="box box-main mb-0 pb-4 animated fadeInUp">
              <header className="header-light white w-l-icon icon-center">
                <img className="header-img" src={images.common["grfx_grfx_rate.png"]} alt="Icon Success" />
              </header>
              <section className="">
                <h1>Thanks for the rating!</h1>
                <p>Enjoying myIndiHome App?Please take a moment to rate our app at the Play Store!</p>
              </section>
              <div className="btn-placeholder">
                <div className="row">
                  <div className="col-md-6 col-6">
                    <Link to="#" className="btn btn-transparent btn-block" onClick={() => this.context.modal("close")}>Later</Link>
                  </div>
                  <div className="col-md-6 col-6">
                    <button className="btn btn-primary btn-block" onClick={() => this.context.modal("close")}>Go</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
        <div className={"card card-notif " + viewNotifAdditionalFee}>
          <p className="notif-text">Additional fee of Rp200.000 will be charged in your next billing.</p>
        </div>
        <div className={"card card-notif " + viewNotifRatingDone}>
          <p className="notif-text">Thank you for your feedback!</p>
        </div>
        <div className={"card card-notif " + viewNotifForgetPassword}>
          <p className="notif-text">Your recovery code has been sent to your email / phone number!</p>
        </div>
        <div className={"card card-notif " + viewNotifRecoveryPassword}>
          <p className="notif-text">Your password has been changed successfully!</p>
        </div>
        <div className={"card card-notif"}>
          <p className="notif-text">Thank you for your feedback!</p>
        </div>
      </div>
    )
  }
}

export default withRouter(Routes)
