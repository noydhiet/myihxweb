import * as firebase from "firebase/app";
import 'firebase/analytics';
import "firebase/messaging";
const initializedFirebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyASM9Ih6DmDU4zFpSN6qQ35IU9Ey9IEaLM",
    authDomain: "myindihome-pccw.firebaseapp.com",
    databaseURL: "https://myindihome-pccw.firebaseio.com",
    projectId: "myindihome-pccw",
    storageBucket: "myindihome-pccw.appspot.com",
    messagingSenderId: "149096829109",
    appId: "1:149096829109:web:36c777967164eaa7662efc",
    measurementId: "G-320P8912NP"
});
let messaging
if (firebase.messaging.isSupported()) {
	messaging = initializedFirebaseApp.messaging();
  messaging.usePublicVapidKey(
    // Project Settings => Cloud Messaging => Web Push certificates
    "BPxpKDKF_9cj6Y8-mPvn2xXH8XAR_40i3av1DiGuFG4LxZp56QgfFyO9Ym8s4X55-DzG_apCJzeUH7VCmWsiSMw"
  );
}
const analytics = firebase.analytics();
export { messaging, analytics };