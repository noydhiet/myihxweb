import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
//Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css'
//React Video
import "video-react/dist/video-react.css";
//css
import './assets/scss/main.scss';
import 'react-day-picker/lib/style.css';
import "react-datepicker/dist/react-datepicker.css";
import "leaflet/dist/leaflet.css";
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import rootReducer from './store/rootReducer'

const store = createStore(rootReducer);
if ("serviceWorker" in navigator) {
    navigator.serviceWorker
      .register("./firebase-messaging-sw.js")
      .then(function(registration) {
        console.log("Registration successful, scope is:", registration.scope);
      })
      .catch(function(err) {
        console.log("Service worker registration failed, error:", err);
      });
      navigator.serviceWorker.addEventListener("message", (message) => {
        if (!("Notification" in window)) {
          return;
        } else if (Notification.permission === "granted") {
          // If it's okay let's create a notification
          var notification = new Notification(message['data']['firebase-messaging-msg-data']['notification']['title'], {
            body: message['data']['firebase-messaging-msg-data']['notification']['body']
          });
          notification.onclick = function(event) {
            event.preventDefault(); // prevent the browser from focusing the Notification's tab
            window.open('/', '_self');
          }
        } else if (Notification.permission !== "denied") {
          Notification.requestPermission().then(function (permission) {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
              var notification = new Notification(message['data']['firebase-messaging-msg-data']['notification']['title'], {
                body: message['data']['firebase-messaging-msg-data']['notification']['body']
              });
              notification.onclick = function(event) {
                event.preventDefault(); // prevent the browser from focusing the Notification's tab
                window.open('/', '_self');
              }
            }
          });
        }
        console.log('ReactDOM', message)
      });
  }
  
ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
