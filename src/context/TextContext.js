import React, { createContext, Component } from 'react';

export const TextContext = createContext();

class TextContextProvider extends Component {
    state = {
        loginText: {
          login: {
            title: "Masuk ke MyIndihome",
            label: "Nomor Ponsel atau Email",
            placeholder: "Masukkan Nomor Ponsel atau Email",
            button: "Masuk",
            alert: "Mohon masukkan nomor ponsel / email yang benar"
          },
          mobileVer: {
            title: "Pilih Metode Verifikasi",
            labelWa: () => <>Melalui <strong>Whatsapp</strong></>,
            labelSms: () => <>Melalui <strong>SMS</strong></>
          },
          mobileOtp: {
            title: "Masuk Menggunakan Nomor Ponsel",
            subtitleWhatsApp: "Masukkan 4 digit kode verifikasi yang telah kami kirim ke nomor Whatsapp ",
            subtitleSMS: "Masukkan 4 digit kode verifikasi yang telah kami kirim ke nomor SMS ",
            btnResendTimer: "Kirim ulang kode dalam",
            btnResendCode: "Kirim ulang kode",
            alertWrong: "Kode yang dimasukkan salah",
            alertExceeded: "Anda telah melampaui batas maksimal memasukkan kode yang salah. Silahkan coba lagi setelah 1 jam"
          },
          loginEmailPass: {
            title: "Masukkan Kata Sandi Anda",
            label: "Kata Sandi",
            placeholder: "Masukkan kata sandi Anda",
            linkForgetPass: "Lupa Kata Sandi",
            btnNext: "Lanjut",
            alert: "Kata sandi salah"
          },
          loginScreenNewMyIn: {
            title: "Halo! Selamat datang di Indihome!",
            subtitle: "Anda telah berhasil melakukan proses migrasi akun myIndiHome. Untuk alasan keamanan, mohon perbarui kata sandi Anda",
            btnStart: "Mulai Sekarang"
          }
        },
        registerText: {
          register: {
            title: "Apakah Anda sudah berlangganan layanan IndiHome?",
            btnYes: "Ya",
            btnNo: "Tidak",
            btnNext: "Lanjut",
            labelAccNo: "Nomor IndiHome",
            placeholderIndiNo: "Masukkan nomor IndiHome Anda di sini",
            alertMaxNo: "Maksimal 12 Karakter",
            alertInvNo: "Silahkan masukkan nomor IndiHome dengan benar.",
            alertNotFound: "Nomor IndiHome tidak ditemukan. pastikan Anda memasukkan nomor dengan benar.",
            modalTitle: "Nomor IndiHome sudah terdaftar",
            modalSubtitle: "Nomor tersebut terkoneksi dengan email xxx@gmail.com / dan nomor ponsel xxx123. Untuk pilihan bantuan, klik opsi berikut ini:",
            // modalSubtitle: () => <>Nomor tersebut terkoneksi dengan email {email} / dan nomor ponsel {mobileNumber}. Untuk pilihan bantuan, klik opsi berikut ini:</>
          },
          accDetails: {
            title: "Pendaftaran Baru",
            labelName: "Nama lengkap",
            labelMobile: "Nomor Ponsel",
            labelEmail: "Email",
            labelPassword: "Kata Sandi",
            phName: "Masukkan nama lengkap",
            phMobile: "E.g 081XXXXX or 62XXXXXX",
            phEmail: "Masukkan alamat email",
            phPassword: "Buat kata sandi baru",
            alertPassword: "Minimal 6 karakter terdiri dari gabungan huruf dan angka",
            alertFullName: "Mohon cantumkan nama lengkap Anda",
            alertMax30: "Maksimal 30 karakter",
            alertValidFullName: "Nama tidak boleh mengandung angka / karakter spesial",
            alertValidEmail: "Mohon cantumkan email yang aktif",
            alertValidPhone: "Mohon cantumkan nomor ponsel yang aktif",
            alertMixChar: "Minimal 6 karakter terdiri dari gabungan huruf dan angka",
            cardLabel: "Punya kode referral? Masukkan kode referral dan dapatkan hadiah spesial untuk Anda dan teman, serta agen kami!",
            cardInputPh: "Masukkan kode referral",
            btnModalTryAgain: "Coba Lagi",
            btnModalLogin: "Masuk",
            btnModalGetHelp: "Minta Bantuan",
            btnRegister: "Daftar Sekarang"
          },
          activation: {
            title: "Pilih Metode Aktivasi",
            subtitle: "Pilih satu dari dua metode aktivasi yang tersedia",
            labelWa: () => <>Melalui <strong>Whatsapp</strong></>,
            labelSms: () => <>Melalui <strong>SMS</strong></>,
          },
          otp: {
            title: "Aktivasi MyIndihome",
            subtitleWhatsApp: "Masukkan 4 digit kode verifikasi yang telah kami kirim ke nomor Whatsapp ",
            subtitleSMS: "Masukkan 4 digit kode verifikasi yang telah kami kirim ke nomor SMS ",
            btnResendTimer: "Kirim ulang kode dalam",
            btnResendCode: "Kirim ulang kode",
            alertWrong: "Kode yang dimasukkan salah",
            alertExceeded: "Anda telah melampaui batas maksimal memasukkan kode yang salah. Silahkan coba lagi setelah 1 jam"
          },
          success: {
            title: "Hore! Pendaftaran Anda berhasil!",
            subtitle: "Kini Anda dapat menikmati berbagai layanan kebutuhan IndiHome, penawaran, serta informasi menarik dari IndhiHome!",
            btnStart: "Yuk, mulai menjelajah!"
          },
          request: {
            title: "Anda berhasil meminta akses masuk myIndiHome!",
            subtitle: "Kami telah meminta akses masuk ke pengguna utama dengan email ***doe@gmail.com. Ketika permintaan telah disetujui. Anda akan kami hubungi lewat email atau telepon",
            btnHome: "Kembali ke beranda"
          }
        },
        landingPagesText: {
            banner: {
                bannerText: "The most complete Digital Services Provider in Indonesia",
                bannerTextDesc: "Mulai berlangganan paket internet, TV dan telepon mulai dari RP150.000 per bulan*",
                bannerTextBtn: "Dapatkan Rekomendasi Paket"
            },
            benefit: {
                benefitCaption: "Keuntungan Berlangganan di Indihome",
                benefitText: "Sinyal kuat, internet cepat, di mana pun Anda berada.",
                benefitTextList: ["Unlimited Quota*", "Layanan customer service 24/7", "Unlimited Quota*", "Layanan customer service 24/7"]
            },
            promo: {
                profoHomeTitle: "Promo Terbaru"
            },
            app: {
                appCaption: "Aplikasi MyIndihome",
                appTitle: "Satu Aplikasi Untuk Segalanya",
                appDesc: "Download aplikasi myIndiHome untuk mendapatkan promosi spesial, bayar tagihan, dan masih banyak lainnya!",
                appTextList: ["Lihat & bayar tagihan dengan mudah", "Harga dan promo special hanya di aplikasi", "Booking servis dan teknisi di aplikasi", "Konten-konten eksklusif hanya di aplikasi"]
            },
            faq: {
                faqCaption: "Bantua dan Pertanyaan",
                faqTitle: "Ada pertanyaan seputar IndiHome?",
                faqDesc: " Tonton berbagai acara dan film internasional dan domestik dengan berlangganan mulai dari Rp100.000/bulan"
            }
        },
        shopText: {
            coverShopLinkText: "Shop",
            coverShopTitle: "Have a need for speed?",
            coverShopDesc: "Upgrade your speed for 10% off.",
            coverShopInternetTextTitle: "Internet",
            coverShopInternetTextDesc: "Mulai berlangganan paket internet, TV dan telepon mulai dari Rp150.000 per bulan *",
            shopInternetOffer: {
                shopInternetOfferTitle: "Paket Internet yang sesuai kebutuhan Anda",
                shopInternetOfferDesc: "Our Internet plans are made to fit your budget and lifestyle. You can work from home, game with friends and browse the web with fast Internet speeds. Stay connected to what matters most."
            },
            shopInternetUnlimitedQuotaTextTitle: "Unlimited Internet Quota",
            shopInternetUnlimitedQuotaTextDesc: "Lorem Ipsum dolor sit amet. consectur adipicsing elti. Lorem Ipsum dolor sit amet. consectur adipicsing elti.",
            shopInternetUpgradeFiberBanner: {
                title: "Upgrade your network to Fiber Optic",
                desc: "Permanently upgrade your internet speed without upgrading your package",
                about: "Khusus pelanggan indiHome, Anda dapat menikmati kemudahan mengakses Wifi.id secara otomatis di seluruh indonesia hanya dengan Rp10.000/bulan per device",
                btn: "More Details"
            },
            shopInternetExtraSpeedBanner: {
                title: "Need a speed boost?",
                desc: "Get extra speed package to boost up your internet speed",
                about: "Khusus pelanggan indiHome, Anda dapat menikmati kemudahan mengakses Wifi.id secara otomatis di seluruh indonesia hanya dengan Rp10.000/bulan per device",
                btn: "More Details"
            },
            shopInternetWifiIdBanner: {
                title: "Wifi.id seamless",
                desc: "Layanan internet dengan kecepatan sampai dengan 100Mpbs di lokasi publik berbasis wireless",
                about: "Khusus pelanggan indiHome, Anda dapat menikmati kemudahan mengakses Wifi.id secara otomatis di seluruh indonesia hanya dengan Rp10.000/bulan per device",
                btn: "More Details"
            },
            shopInternetWifiExtenderBanner: {
                title: "Complete Coverage with wifi extender",
                desc: "Use wifi extender for a stronger internet connection in your house/office",
                about: "Mau internetan dengan jaringan wifi yang lebih luas di rumah anda?IndiHome kini menyediakan perangkat Wifi Extender untuk memperkuat pemancaran sinyal wifi di setiap area sudut rumah Anda tanpa harus menyediakan kabel tambahan. Dengan Wifi Extender dari IndiHome, internetan di setiap sudut rumah jadi semakin leluasa. Hanya Rp35.000/bulan",
                btn: "More Details"
            },
            shopInternetUpgradeSpeedBanner: {
                title: "Upgrade Speed",
                desc: "Permanently upgrade your internet speed without upgrading your package",
                btn: "More Details"
            },
            shopInternetIndiHomeStudyBanner: {
                title: "Upgrade Speed",
                desc: "Permanently upgrade your internet speed without upgrading your package",
                btn: "More Details"
            },
            shopTvMinipackBanner: {
                title: "Get more channels",
                desc: "Get endless entertainment and movies with minipacks!",
                detailPackage: {
                    coverTextTitle: "Dinasty 1",
                    coverTextDesc: "Chinese language channels for the whole family!"
                },
                btn: "More Details"
            },
            shopTvApps: {
                title: "Love Movies?",
                desc: "Subscribe now and stream movies on demand straight to your TV or device!",
                btn: "More Details"
            },
            shopTvEdukidsBanner: {
                title: "Edukids",
                desc: "Bingung mencari tayangan untuk si buah hati yang edukatif dan menghibur? Edukids hadir menjawab kebutuhan Anda!",
                about: "Edukids merupakan pelopor tayangan edukasi serta hiburan ini orang tua dapat dengan mudah mengawasi serta memilihkan jenis tayangan yang sesuai dengan usia anak.",
                btn: "More Details"
            },
            shopTvHybridBoxBanner: {
                title: "Hybrid Box",
                desc: "Watch USeeTV on different television sets at the same time. Feels the experience use Smart TV by using an ordinary TV!",
                about: "Edukids merupakan pelopor tayangan edukasi serta hiburan ini orang tua dapat dengan mudah mengawasi serta memilihkan jenis tayangan yang sesuai dengan usia anak.",
                btn: "More Details"
            },
            shopTvStorageBanner: {
                title: "Tv Storage",
                desc: "Get more storage for your TV recording!",
                about: "Edukids merupakan pelopor tayangan edukasi serta hiburan ini orang tua dapat dengan mudah mengawasi serta memilihkan jenis tayangan yang sesuai dengan usia anak",
                btn: "More Details"
            },
            shopCallCover: {
                title: "Call"
            },
            shopCallBanner: {
                title: "Home phone in your smart phone",
                desc: "For Rp10.000, get 1000 minutes with Movin'",
                about: "Mau terima dan melakukan panggilan telepon rumah dari smartphone? Pakai aplikasi Movin', mudah dan praktis! Dengan biaya berlangganan aplikasi Movin' Rp100.000 per bulan, Anda bisa bebas nelpon lokal dan interlokal hinggan 1000 menit",
                btn: "More Details"
            },
            shopMoreCover: {
                title: "More"
            },
            shopMoreIndihomeCloudBanner: {
                title: "Upload,Access,Share!",
                desc: "Keep your data online in IndiHome Cloud and access it from your devices",
                about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
                btn: "More Details"
            },
            shopMoreCloudStorageBanner: {
                title: "Upload,Access,Share!",
                desc: "Keep your data online in IndiHome Cloud and access it from your devices",
                about: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
                btn: "More Details"
            },
            shopMoreIndihomeStudy: {
                title: "Have fun studying with indiHome Study!",
                desc: "With easy to learn materials and interractive app, this is the best solution for your kids!",
                about: "Dengan IndiHome Study, Anda dapat mengakses buku mata pelajaran kurikulum 2006, buku mata pelajaran kurikulum 2013, buku umum, try out mata pelajaran dan video pendidikan di mana saja dan kapan saja menggunakan laptop maupun smartphone.",
                btn: "More Details"
            }
        },
        helpText: {
            title: "Help",
            desc: "Having problems? Report your issue and our team will be there to help you"
        },
        generalText: {
            upcoming: "Upcoming",
            recommended: "Recommended For You",
            newOffer: "Penawaran Terbaru",
            popularProduct: "Produk paling populer",
            movieSubcribe: {
                movieSubcribeCaption: "Subscibre & Watch",
                movieSubcribeTitle: "Subscribe dan nonton langsung di TV atau perangkat Anda!",
                movieSubcribeDesc: "Tonton berbagai acara dan film internasional dan domestik dengan berlangganan mulai dari Rp100.000/bulan.",
                movieSubcribeBtn: "Subscribe"
            },
            coverage: {
                coverageCaption: "Cek Ketersediaan Indihome Di Daerah Anda",
                coverageText: "Siap melayani Anda, dari Sabang sampai Merauke",
                coverageBtn: "Cek Ketersediaan"
            },
            hooq: {
                title: "Temukan berbagai tayangan petualangan seru, drama, horor, dan komedi non-stop hanya dengan satu klik.",
                desc: "Rp50.000/month"
            },
            iflix: {
                title: "Tonton tayangan favorit Anda mulai dari film lokal, internasional, sampai drama korea!",
                desc: ""
            },
            catchplay: {
                title: "Catchplay dapat membuat Anda merasakan serunya memiliki bisokop pribadi di rumah setiap hari!",
                desc: "Rp45.000/month"
            }
        }
    }
    render() {
        return (
            <TextContext.Provider value={{...this.state}}>
                {this.props.children}
            </TextContext.Provider>
        )
    }
}

export default TextContextProvider