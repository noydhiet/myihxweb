import React, { createContext, Component } from 'react';
import { requireBrand, requireCommon, requireSample } from '../utils/requireContext';
import { getToken } from '../utils/storage';

export const PageContext = createContext();

class PageContextProvider extends Component {
  state = {
    brand: [],
    common: [],
    sample: [],
    condition: null,
    navCasePage: null,
    bgCase: "",
    pageLoad: "",
    footerCasePage: "",
    modalAdditionalFee: false,
    modalSetNewSchedule: false,
    modalRating: false,
    modalSelectReason: false,
    modalRatingDone: false,
    modalSessionTimeOut: false,
    modalSubscribe: false,
    notifAdditionalFee: false,
    notifRating: false,
    notifHelpTicket: false,
    notifForgetPassword: false,
    notifRecoveryPassword: false,
    navView: 1,
    value: "10",
    icon: "icon_ic_nps_high.png",
    position: "92%",
    display: "block",
    imgKtp: '',
    imgSignature: '',
    checkboxes: []
  }

  // START - IMPORT ASSETS IMAGES
  importBrand = (e) => {
    let img = {}
    e.keys().forEach((item, index) => { img[item.replace('./', '')] = e(item); });
    return img;
  }

  importCommon = (e) => {
    let img = {}
    e.keys().forEach((item, index) => { img[item.replace('./', '')] = e(item); });
    return img;
  }

  importSample = (e) => {
    let img = {}
    e.keys().forEach((item, index) => { img[item.replace('./', '')] = e(item); });
    return img;
  }
  // END - IMPORT ASSETS IMAGES

  componentDidMount() {
    // START MOUNT - IMPORT ASSETS IMAGES
    let imgBrand = this.importBrand(requireBrand());
    let imgCommon = this.importCommon(requireCommon());
    let imgSample = this.importSample(requireSample());
    // END MOUNT - IMPORT ASSETS IMAGES
    this.setState({
      // START SET STATE - IMPORT ASSETS IMAGES
      brand: imgBrand,
      common: imgCommon,
      sample: imgSample
      // END SET STATE - IMPORT ASSETS IMAGES
    });
  }

  trigger = (e) => {
    this.setState({
      condition: e
    })
  }

  navCase = (e) => {
    this.setState({
      navCasePage: e
    })
  }

  navView = (e) => {
    this.setState({
      navView: e
    })
  }

  footerCase = (e) => {
    switch (e) {
      case "d-none d-md-block":
        this.setState({
          footerCasePage: "d-none d-md-block"
        });
        break;

      case "w-nav-bottom":
        this.setState({
          footerCasePage: "w-nav-bottom"
        });
        break;

      default:
        return null
    }
  }

  pageCase = (mode) => {
    switch (mode) {
      case "w-bg":
        this.setState({
          bgCase: "w-bg"
        });
        break;

      case "l-bg":
        this.setState({
          bgCase: "l-bg"
        });
        break;

      case "home":
        this.setState({
          pageLoad: "home"
        });
        break;
      case "submitHelpTicket":
        this.setState({
          notifHelpTicket: !this.state.notifHelpTicket,
        })
        break;

      default:
        return null
    }
  }

  checkToken = () => {
    return Boolean(getToken());
  }

  loggedIn = () => {
    if (!getToken()) window.location.replace('/login');
  }

  modal = (mode) => {
    switch (mode) {
      case "additionalFee":
        this.setState({
          modalAdditionalFee: !this.state.modalAdditionalFee
        });
        break;

      case "setNewSchedule":
        this.setState({
          modalSetNewSchedule: !this.state.modalSetNewSchedule
        });
        break;

      case "confirmAdditionalFee":
        this.setState({
          modalAdditionalFee: !this.state.modalAdditionalFee,
          notifAdditionalFee: true
        });
        break;

      case "rating":
        this.setState({
          modalRating: !this.state.modalRating,
          icon: "icon_ic_nps_high.png",
          position: "92%",
          display: "block",
          value: "10"
        });
        break;

      case "ratingSelectReason":
        this.setState({
          modalRating: false,
          modalSelectReason: !this.state.modalSelectReason
        });
        break;

      case "ratingDone":
        this.setState({
          modalRating: false,
          modalSelectReason: false,
          modalRatingDone: !this.state.modalRatingDone,
          notifRating: true
        });
        break;

      case "modalSubscribe":
        this.setState({
          modalSubscribe: true
        });
        break;

      case 'sessionTimeOut':
        this.setState({
          modalSessionTimeOut: true
        });
        break;

      case "close":
        this.setState({
          modalAdditionalFee: false,
          modalSetNewSchedule: false,
          modalRating: false,
          modalRatingDone: false,
          modalSubscribe: false,
          modalSessionTimeOut: false,
        });
        break;

      default:
        return null
    }
  }

  addBg = () => {
    const body = document.getElementsByClassName("body-basic")[0];
    body.classList.add("w-bg");
  }

  removeBg = () => {
    const body = document.getElementsByClassName("body-basic")[0];
    body.classList.remove("w-bg");
  }

  openNotif = (mode) => {
    switch (mode) {
      case 'forgetPassword':
        this.setState({ notifForgetPassword: true })
        break;
      case 'recoveryPassword':
        this.setState({ notifRecoveryPassword: true })
        break;

      default:
        break;
    }
  }

  closeNotif = (mode) => {
    switch (mode) {
      case "additionalFee":
        setTimeout(() => {
          this.setState({
            notifAdditionalFee: false
          })
        }, 2000);
        break;

      case "ratingDone":
        setTimeout(() => {
          this.setState({
            notifRating: false
          })
        }, 2000);
        break;

      case "forgetPassword":
        setTimeout(() => {
          this.setState({
            notifForgetPassword: false
          })
        }, 2500);
        break;

      case "recoveryPassword":
        setTimeout(() => {
          this.setState({
            notifRecoveryPassword: false
          })
        }, 2500);
        break;

      default:
        return null
    }
  }

  setPersonalData = (name, img) => this.setState({ [name]: img });

  onChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
    switch (e.target.value) {

      case "1":
        this.setState({
          icon: "icon_ic_nps_low.png",
          position: "-1%",
          display: "block"
        })
        break;

      case "2":
        this.setState({
          icon: "icon_ic_nps_low.png",
          position: "9.5%",
          display: "block"
        })
        break;

      case "3":
        this.setState({
          icon: "icon_ic_nps_low.png",
          position: "20%",
          display: "block"
        })
        break;

      case "4":
        this.setState({
          icon: "icon_ic_nps_med.png",
          position: "30%",
          display: "block"
        })
        break;

      case "5":
        this.setState({
          icon: "icon_ic_nps_med.png",
          position: "40.5%",
          display: "block"
        })
        break;

      case "6":
        this.setState({
          icon: "icon_ic_nps_med.png",
          position: "50.5%",
          display: "block"
        })
        break;

      case "7":
        this.setState({
          icon: "icon_ic_nps_med.png",
          position: "61%",
          display: "block"
        })
        break;

      case "8":
        this.setState({
          icon: "icon_ic_nps_high.png",
          position: "71.3%",
          display: "block"
        })
        break;

      case "9":
        this.setState({
          icon: "icon_ic_nps_high.png",
          position: "81.8%",
          display: "block"
        })
        break;

      case "10":
        this.setState({
          icon: "icon_ic_nps_high.png",
          position: "92%",
          display: "block"
        })
        break;

      default:
        return null
    }
  }

  checkBoxSelectReason = (e) => {
    const check = document.getElementsByClassName("check");
    for (let i = 0; i < check.length; i++) {
      if (i === e) {
        if (check[i].checked) {
          check[i].checked = false
        } else {
          check[i].checked = true
        }
      }
    }
  }
  render() {
    return (
      <PageContext.Provider value={{ ...this.state, trigger: this.trigger, navCase: this.navCase, footerCase: this.footerCase, pageCase: this.pageCase, checkToken: this.checkToken, loggedIn: this.loggedIn, modal: this.modal, addBg: this.addBg, removeBg: this.removeBg, openNotif: this.openNotif, closeNotif: this.closeNotif, onChange: this.onChange, setPersonalData: this.setPersonalData, checkBoxSelectReason: this.checkBoxSelectReason, navView: this.navView }}>
        {this.props.children}
      </PageContext.Provider>
    );
  }
}

export default PageContextProvider;
