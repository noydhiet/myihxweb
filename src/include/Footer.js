import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";
import { Subheading } from "./../include/SupportComponent";

export class Footer extends Component {
  render() {
    const { condition, images, footerCasePage } = this.props;
    const viewFooter =
        condition === 2 ? (
        <footer id="footer" className={footerCasePage}>
          <div className="footer-top py-main">
            <Container>
              <Row>
                <Col className="col-12 col-md-6 col-lg-4">
                  <div className="footer-item d-none d-md-block">
                    <Subheading text="Powered by" />
                    <img
                      alt=""
                      src={images.brand["logo_telkom-light.png"]}
                      className="img-fluid img-brand"
                    />
                  </div>
                  <div className="footer-item">
                    <Subheading text="Download MyIndiHome App" />
                    <div className="btn-placeholder-download">
                      <Link to="/home">
                        <img
                          alt=""
                          src={images.common["button_appstore.png"]}
                          className="img-fluid"
                        />
                      </Link>
                      <Link to="/home">
                        <img
                          alt=""
                          src={images.common["button_playstore.png"]}
                          className="img-fluid"
                        />
                      </Link>
                    </div>
                  </div>
                </Col>
                <Col className="col-12 col-md-6 col-lg-3 offset-lg-1 footer-item">
                  <Subheading text="Quick Links" />
                  <ul className="footer-list">
                    <li>
                      <Link to="/home">Latest News </Link>
                    </li>
                    <li>
                      <Link to="/home">How to Check My Bills </Link>
                    </li>
                    <li>
                      <Link to="/home">Complaint Ticketing </Link>
                    </li>
                    <li>
                      <Link to="/home">How to Use App </Link>
                    </li>
                    <li>
                      <Link to="/home">Partnership </Link>
                    </li>
                  </ul>
                </Col>
                <Col className="col-md-5 col-lg-4 footer-item mb-0">
                  <Subheading text="Connect With Us" />
                  <div className="list-group mb-1">
                    <a
                      href="tel:147"
                      className="list-group-item footer bg-transparent"
                    >
                      <div className="list-group-icon">
                        <i className="fa fa-phone fa-flip-horizontal" />
                      </div>
                      <div className="list-group-item-content">
                        <h6 className="title">Contact Center</h6>
                        <span className="desc">Sn-M 06:00-20:00</span>
                      </div>
                      <p className="text-white">147</p>
                    </a>
                  </div>
                  <ul className="footer-social">
                    <li>
                      <a
                        href="https://www.facebook.com/indihome/"
                        target="_blank"
                        className="footer-social-fb"
                        rel="noopener noreferrer"
                      >
                        <i className="fab fa-facebook-f footer-icon" />
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://www.instagram.com/indihome/"
                        target="_blank"
                        className="footer-social-instagram"
                        rel="noopener noreferrer"
                      >
                        <i className="fab fa-instagram footer-icon" />
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://twitter.com/indihome/"
                        target="_blank"
                        className="footer-social-twitter"
                        rel="noopener noreferrer"
                      >
                        <i className="fab fa-twitter footer-icon" />
                      </a>
                    </li>
                  </ul>
                </Col>
              </Row>
            </Container>
          </div>
          <div className="footer-bottom">
            <Container>
              <Row>
                <Col className="col-12 col-md-6 order-md-last">
                  <div className="footer-bottom-links">
                    <Link to="/privacy-policy"> Privacy Policy </Link>
                    <Link to="/terms-conditions"> Terms & Conditions </Link>
                  </div>
                </Col>
                <Col className="col-12 col-md-6 order-md-first d-flex align-items-center">
                  <p className="footer-bottom-text mb-0">
                    ©2019 PT Telekomunikasi Indonesia (Persero) Tbk.
                  </p>
                </Col>
              </Row>
            </Container>
          </div>
        </footer>
      ) : (
          <div />
        );
    return <div>{viewFooter}</div>;
  }
}
