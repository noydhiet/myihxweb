import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Slider from 'react-slick'

class ShopInternet extends Component {
    render() {
        const config = {
            carouselTab: {
                className: "tab-list",
                dots: false,
                infinite: false,
                arrows: false,
                speed: 500,
                slidesToShow: 7,
                slidesToScroll: 7,
                responsive: [
                    {
                      breakpoint: 991,
                      settings: {
                        dots: false,
                        slidesToShow: 4.5,
                        slidesToScroll: 4.5
                      }
                    },
                    {
                      breakpoint: 575,
                      settings: {
                        dots: false,
                        slidesToShow: 3.5,
                        slidesToScroll: 3.5
                      }
                    },
                    {
                      breakpoint: 324,
                      settings: {
                        dots: false,
                        slidesToShow: 2.55,
                        slidesToScroll: 2.55
                      }
                    }
                ]
            }
        }
        const { paketActive, extraSpeedActive, wifiIdActive, wifiExtenderActive, upgradeSpeedActive, cloudActive, indihomeStudyActive } = this.props
        return (
        <div>
            <div className="cover cover-sm bg-red-gradient">
              <div className="header-actions d-block d-md-none">
                <Link
                  to="/shop/"
                  className="btn btn-link"
                >
                  <i className="fa fa-arrow-left"></i>
                </Link>
              </div>
              <div className="container">
                <nav aria-label="breadcrumb" className="d-none d-md-block">
                  <ol className="breadcrumb animated fadeInUp">
                    <li className="breadcrumb-item"><Link to="/shop"><i className="fa fa-arrow-left mr-1"></i> Shop</Link></li>
                  </ol>
                </nav>
                <div className="cover-content">
                  <h1 className="cover-title animated fadeInUp delayp1">Internet</h1>
                  <p className="cover-text animated fadeInUp delayp2">Mulai berlangganan paket internet, TV dan telepon mulai dari <strong> Rp150.000 </strong> per bulan <sup>*</sup></p>
                </div>
              </div>
            </div>
            <section className="shop-tab-list animated fadeInUp delayp3">
                <div className="container">
                <Slider {...config.carouselTab}>
                    <div className={"tab-item " + paketActive}> 
                        <Link to="/shop/internet/package">Paket</Link> 
                    </div>
                    <div className={"tab-item " + extraSpeedActive}> 
                        <Link to="/shop/internet/speed-on-demand">Extra Speed</Link>
                    </div>
                    <div className={"tab-item " + wifiIdActive}> 
                        <Link to="/browse-package">wifi.id</Link> 
                    </div>
                    <div className={"tab-item " + wifiExtenderActive}> 
                        <Link to="/browse-package">wifi Extender</Link> 
                    </div>
                    <div className={"tab-item " + upgradeSpeedActive}> 
                        <Link to="/browse-package">Upgrade Speed</Link> 
                    </div>
                    <div className={"tab-item " + cloudActive}> 
                        <Link to="/browse-package">Cloud</Link> 
                    </div>
                    <div className={"tab-item " + indihomeStudyActive}> 
                        <Link to="/browse-package">IndiHome Study</Link> 
                    </div>
                </Slider>
                </div>
            </section>
        </div>
        )
    }
}

export default ShopInternet

    


