import React, { Component } from 'react'
import { Link } from 'react-router-dom'

// export class CardBasic extends Component {
//     render() {
//         const { sumBadge, detailBadge, list, price, time, detail, addClass, btnDetail, label } = this.props
//         const config = {
//             badgeView: sumBadge === undefined ? '' :  <div className="badge-speed"> <h3>{sumBadge}</h3><span>{detailBadge}</span> </div>,
//             btnDetail: btnDetail === undefined ? '' : btnDetail,
//             label: label === undefined ? '' :  <p className="label-card">{label}</p>,
//             classWBtn: btnDetail === undefined ? '' : 'w-btn',
//             time: time === undefined ? '' : <small> / {time}</small>
//         }
//         return (
//             <a href="" className="card card-packages">
//                 <div className={"card-header " + addClass}>
//                     {config.badgeView}
//                     {list}
//                     {config.label}
//                 </div>
//                 <div className={"card-body " + config.classWBtn}>
//                 <h3>Rp.{price}{config.time}</h3>
//                 <small className="help-block">{detail}</small>
//                 {config.btnDetail}
//                 </div>
//             </a>
//         )
//     }
// }

// export class CardDiscount extends Component {
//     render() {
//         const { sumBadge, detailBadge, list, priceDisc, price, time, btnDetail } = this.props
//         return (
//             <a href="" className="card card-packages">
//                 <div className="card-header">
//                     <div className="badge-speed">
//                     <h3>{sumBadge}</h3><span>{detailBadge}</span>
//                     </div>
//                     {list}
//                 </div>
//                 <div className="card-body w-btn">
//                     <h3><span className="text-success">Rp{priceDisc}</span> <small>/ {time}</small></h3>
//                     <del>Rp{price}</del>
//                     {btnDetail}
//                 </div>
//             </a>
//         )
//     }
// }

// export class CardChannel extends Component {
//     render() {
//         const { sumBadge, detailBadge, detailHeader, listChannel, link } = this.props
//         const config = {
//             viewBtn: link == undefined ? null : <div className="btn-placeholder"> {link} </div>
//         }
//         return (
//             <a href="" className="card card-packages w-list-channel">
//                 <div className="card-header">
//                     <div className="badge-channel">
//                         <h3>{sumBadge}</h3><span>{detailBadge}</span>
//                     </div>
//                     <p>{detailHeader}</p>
//                 </div>
//                 <div className="card-body">
//                     <div className="list-channel">
//                         {listChannel}
//                     </div>
//                     {config.viewBtn}
//                 </div>
//             </a>
//         )
//     }
// }

// export class CardBadge extends Component {
//     render() {
//         const { sumBadge, detailBadge, price, detailTime, btnDetail } = this.props
//         return (
//             <div className="card card-packages badge-top-left">
//                 <div className="badge-speed">
//                     <h3>{sumBadge}</h3><span>{detailBadge}</span>
//                 </div>
//                 <div>
//                     <h4>Rp{price}</h4>
//                     <small className="help-block">{detailTime} hours</small>
//                 </div>
//                 <div className="btn-placeholder">
//                     {btnDetail}
//                 </div>
//             </div>
//         )
//     }
// }

export class CardNotif extends Component {
  render() {
    const { active, textTop, textCenter, link } = this.props
    return (
      <div className={"card card-notif " + active}>
        <small> {textTop} </small>
        <b> {textCenter} </b>
        <Link to="/"> {link} </Link>
      </div>
    )
  }
}

export const CardPackages = () => {
  return (
    <Link to="">

    </Link>
  )
}

export const CardBasic = ({ link, className, caption, mainText, label, footerLeftNotes, footerRightNotes, usedPackage, mainUsedTextLeft, mainUsedTextRight, images }) => {
  const mainContent = usedPackage !== true ?
    <h3 className="mb-1">{mainText}</h3> : <div className="used-package mb-1">
      <div className="used-package-item">
        <img alt="" src={images.common["ic_internet.png"]} />
        <h3 className="mb-0">
          {mainUsedTextLeft}<small>Gb</small>
        </h3>
      </div>
      <div className="used-package-item">
        <img alt="" src={images.common["ic_call.png"]} />
        <h3 className="mb-0">
          {mainUsedTextRight}<small>Menit</small>
        </h3>
      </div>
    </div>;
  const labelContent = label === undefined ? null : <label>{label}</label>;
  return (
    <Link to={link} className={"card card-basic " + className}>
      <div className="card-header">
        <p className="help-block mb-1">{caption}</p>
        {mainContent}
        <div className="btn btn-link">
          Lihat Detail <i className="far fa-angle-right"></i>
        </div>
        {labelContent}
      </div>
      <div className="card-footer">
        <span>{footerLeftNotes}</span>
        <span className="font-weight-bold text-dark">
          {footerRightNotes}
        </span>
      </div>
    </Link>
  )
}

export const CardContent = ({ link, className, img, images }) => {
  return (
    <Link to={link} className={"card card-content " + className}>
      <img alt="" src={images.common[`${img}`]} className="w-100" />
    </Link>
  )
}

export const CardContentStacked = ({ link, className, img, mainText, images, caption, title }) => {
  const captionView = caption === undefined ? null : <p className="caption">{caption}</p>;
  const titleView = title ===  undefined ? null : <h4 className="title">{title}</h4>;
  const linkView = link === undefined ? null :  <span
        className="btn btn-link p-0"
        to={link}
        >
        View Details
        </span>
        ;
  return (
    <Link to={link} className={"card card-content-stacked " + className}>
      <div className="img-container">
        <img
          alt=""
          src={images.common[`${img}`]}
          className="img-fluid"
        />
      </div>
      <div className="card-body">
        {captionView}
        {titleView}
        <p>{mainText}</p>
        {linkView}
      </div>
    </Link>
  )
}
