import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export class ListGroupBasic extends Component {
    render() {
        const { link, title, desc, img } = this.props 
        return (
            <Link to={link} className="list-group-item w-arrow">
                {img}
                <div className="list-group-item-content">
                    <h5 className="title">{title}</h5>
                    <span className="desc">{desc}</span>
                </div>
            </Link>
        )
    }
}

export class ListGroupCheck extends Component {
    render() {
        const { trigger, img, title, desc } = this.props
        return (
            <div className="list-group-item w-checkbox list-check" onClick={()=>this.trigger({trigger})}>
                {img}
                <div className="list-group-item-content">
                    <h5 className="title">{title}</h5>
                    <span className="desc">{desc}</span>
                </div>
                <div className="form-group checkbox">
                    <input type="checkbox" className="form-group check"/>
                </div>
            </div>  
        )
    }
}