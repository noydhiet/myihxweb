import React from 'react';
import PropTypes from 'prop-types';

export default class OtpInput extends React.Component {
  constructor(props) {
    super(props);

    const initial = props.names.reduce((acc, n) => ({ ...acc, [n]: '' }), {});
    this.state = {
      ...initial,
    };
  }

  componentDidUpdate(_, prevState) {
    const { names, onSubmit } = this.props;
    const final = names[names.length - 1];

    if (this.state[final] && prevState[final] !== this.state[final]) {
      onSubmit(names.reduce((acc, n) => `${acc}${this.state[n]}`, ''));
    }
  }

  onChange = ({ target }) => {
    const { names } = this.props;
    const { id, value } = target;
    const current = names.findIndex(i => i === id);
    const re = /^[0-9\b]+$/;

    // delete value
    if (!value) this.setState({ [id]: value });
    
    // move focus to previous
    if (!value & current > 0) this.refs[names[current - 1]].focus();
    
    // set value
    if (re.test(value)) this.setState({ [id]: value });

    // move focus to next
    if (re.test(value) && current < names.length - 1) this.refs[names[current + 1]].focus();
  }

  onKeyUp = ({ key, target }) => {
    const { names } = this.props;
    const { id, value } = target;
    const current = names.findIndex(i => i === id);
    const re = /^[0-9\b]+$/;
    const isNewValue = value.length && re.test(key);

    // replace current value
    if (isNewValue) this.setState({ [id]: key });

    // move focus to next
    if (isNewValue && current < names.length - 1) this.refs[names[current + 1]].focus();

    // move focus to previous
    if (!value.length && key === 'Backspace' && current > 0) this.refs[names[current - 1]].focus();
  }

  render() {
    const { disabled, names, type } = this.props;
    const typeView = type === "password" ? "password" : "text";
    return (
      <div className="otp-code">
        {names.map((n, idx) => (
          <input disabled={disabled} key={idx} type={typeView} maxLength="1" className="form-control form-control-otp" ref={n} id={n} required value={this.state[n]} onChange={this.onChange} onKeyUp={this.onKeyUp} />
        ))}
      </div>
    );
  }
}

OtpInput.defaultProps = {
  disabled: false,
  names: [],
  onSubmit: () => {},
};

OtpInput.propTypes = {
  disabled: PropTypes.bool,
  names: PropTypes.arrayOf(PropTypes.string),
  onSubmit: PropTypes.func,
};
