import React, { Component } from 'react'

//Cover 

export const Cover = ({children, className, style}) => {
    const config = {
        className: className === undefined ? "" : className
    }
    return (
        <div className={"cover " + config.className} style={style}>
            {children}
        </div>
    )
}

export const CoverContent = ({children, className}) => {
    const config = {
        className: className === undefined ? "" : className
    }
    return (
        <div className={"cover-content " +  config.className}>
            {children}
        </div>
    )
}


export const CoverContentCenter = ({children}) => {
    return (
        <div className="cover-content text-center">
            {children}
        </div>
    )
}

export const CoverTitle = ({children, className}) => {
    const config = {
        className: className === undefined ? "" : className
    }
    return (
        <div className={"cover-title animated fadeInUp delayp1 " + config.className}>
            {children}
        </div>
    )
}

export const CoverText = ({children, className}) => {
    const config = {
        className: className === undefined ? "" : className
    }
    return (
        <div className={"cover-text animated fadeInUp delayp2 " + config.className}>
            {children}
        </div>
    )
}

//

export const PyMain = ({children, className}) => {
    const config = {
        className: className === undefined ? "" : className
    }
    return (
        <div className={"py-main " + config.className}>
            {children}
        </div> 
    )
}

export const BtnPrimary = ({children, className}) => {
    const config = {
        className: className === undefined ? "" : className
    }
    return (
        <button className={"btn btn-primary " + config.className}>
            {children}
        </button>
    )
}


export class Subheading extends Component {
    render() {
        const { text, className } = this.props
        return (
            <div className={"subheading " + className}>
                <p>{text}</p>
            </div>
        )
    }
}


//Button Customize
export const BtnPlaceholder = ({children, className}) => {
    const config = {
        className: className === undefined ? "" : className
    }
    return (
        <div className={"btn-placeholder " + config.className}>
            {children}
        </div>
    )
}
