import React, { Component } from "react";
import { Link } from "react-router-dom";
import { getToken } from '../utils/storage';
import { PageContext } from '../context/PageContext';

export class NavMobile extends Component {
  static contextType = PageContext;
  componentDidMount() {
    let navbarMain = document.querySelector(".navbar-primary");
    let navbar = document.querySelector(".nav-mobile");
    window.onscroll = function() {
      if (window.pageYOffset > 5) {
        navbar.classList.add("is-scroll");
        navbarMain.classList.add("navbar-scroll");
      } else if (window.pageYOffset < 50) {
        navbar.classList.remove("is-scroll");
        navbarMain.classList.remove("navbar-scroll");
      }
    };
  }

  componentWillUnmount() {
    window.onscroll = null;
  }

  render() {
    const {
      homePage,
      historyPage,
      shopPage,
      helpPage,
      profilePage
    } = this.props;
    const { modal } = this.context;
    const nextScreen = !getToken() ? "#" : "/profile";
    return (
      <div className="nav-mobile d-block-mobile">
        <div className="nav-mobile-content">
          <Link to="/home" className={"nav-mobile-content-item " + homePage}>
            <i className="far fa-home"></i>
            <p>Home</p>
          </Link>
          <Link
            to="/history/bill"
            className={"nav-mobile-content-item " + historyPage}
          >
            <i className="far fa-history"></i>
            <p>History</p>
          </Link>
          <Link to="/shop" className={"nav-mobile-content-item " + shopPage}>
            <i className="far fa-shopping-cart"></i>
            <p>Shop</p>
          </Link>
          <Link to="/help" className={"nav-mobile-content-item " + helpPage}>
            <i className="far fa-question-circle"></i>
            <p>Help</p>
          </Link>
          <Link
            to={ nextScreen }
            className={"nav-mobile-content-item " + profilePage}
            onClick={ () => { if(!getToken()) modal("modalSubscribe")} }
          >
            <i className="far fa-smile"></i>
            <p>Profile</p>
          </Link>
        </div>
      </div>
    );
  }
}

export default NavMobile;
