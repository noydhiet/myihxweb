import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class NavTopLogoDark extends Component {
    render() {
        const { images } = this.props
        return (
        <nav className="navbar navbar-basic light">
            <div className="container container-sm">
                <Link to="/home" className="navbar-brand">
                    <img className="img-fluid" src={images.brand['logo_indihome-dark.png']} alt="IndiHome Logo" />   
                </Link>
            </div>
        </nav>
        )
    }
}

export default NavTopLogoDark