import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Navbar, Container, Form } from "react-bootstrap";
import { PageContext } from '../context/PageContext';
import { getUserData } from '../utils/storage';

export class NavbarMain extends Component {
  static contextType = PageContext;
  state = {
    authCase: 1
  };

  componentDidMount() {
    let navbar = document.querySelector(".navbar-primary");
    window.onscroll = function() {
      if (window.pageYOffset > 5) {
        navbar.classList.add("navbar-scroll");
      } else {
        navbar.classList.remove("navbar-scroll");
      }
    };
  }

  triggerPage = () => {
    const navMenu = document.getElementsByClassName("nav-menu")[0];
    const formSearch = document.getElementsByClassName("form-search")[0];
    navMenu.classList.toggle("open-search");
    formSearch.classList.toggle("open-search");
  }

  navSide = () => {
    const navSide = document.getElementsByClassName("navbar-side-mobile")[0];
    const navToggler = document.getElementsByClassName("navbar-toggler")[0];
    navToggler.classList.toggle("is-open");
    navSide.classList.toggle("is-open");
  }

  openLink = () => {
    console.log("test");
  }

  openFormMobile = () => {
    const navMenuMobile = document.getElementsByClassName("nav-menu-mobile")[0];
    const formSearchMobile = document.getElementsByClassName("form-search-mobile")[0];
    const navBrand = document.getElementsByClassName("navbar-brand-img")[0];
    navBrand.classList.toggle("is-open");
    formSearchMobile.classList.toggle("open-search");
    navMenuMobile.classList.toggle("is-open");
  }

  componentWillUnmount() {
    window.onscroll = null;
  }

  render() {
    const { images, navCasePage } = this.props;
    const { modal } = this.context;
    const userData = getUserData();
    const userName = userData ? userData.userName : '';

    const config = {
      navClassHome: navCasePage === "w-md-up-block" ? "w-md-up-block" : ""
    };
    return (
      <Navbar
        expand="md"
        className={"navbar-primary fixed-top " + config.navClassHome}
      >
        <Container>
          <Link to="/myindihome" className="navbar-brand">
            <img alt="" src={images.brand["logo_indihome-dark.png"]} className="navbar-brand-img"/>
          </Link>
          {/* <Navbar.Toggle aria-controls="basic-navbar-nav" /> */}


         <div className="nav-menu-mobile">
            <Link to="#" className="nav-link-search-mobile">
              {" "}
              <Form.Control
              type="text"
              className="form-search-mobile"
              />
              <i className="fas fa-search content-center"  onClick={()=>this.openFormMobile()}></i>{" "}
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              onClick={this.navSide}
            >
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
         </div>
          <Navbar.Collapse id="basic-navbar-nav">
            <ul className="navbar-nav ml-auto">
              <div className="nav-menu">
                <li className="nav-item">
                  <Link to="/home" className="nav-link">
                    Home
                  </Link>
                </li>
                {/* <li className="nav-item">
                  <Link to="/history" className="nav-link">
                    History
                  </Link>
                </li> */}
                <li className="nav-item">
                  <Link to="/shop" className="nav-link">
                    Shop
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/help" className="nav-link">
                    Help
                  </Link>
                </li>
              </div>
              <li className="nav-item ">
                <Link to="#" className="nav-link d-flex">
                  {" "}
                  <Form.Control
                  type="text"
                  className="form-search"
                  />
                  <i className="fas fa-search content-center" onClick={()=>this.triggerPage()}></i>{" "}
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/inbox/all" className={"nav-link nav-link-message w-notif" + (userData ? '' : ' hidden')}>
                  {" "}
                  <i className="far fa-envelope"></i>{" "}
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/history/bill" className={"nav-link nav-link-message" + (userData ? '' : ' hidden')}>
                  {" "}
                  <i className="far fa-history"></i>
                </Link>
              </li>
              <li className="nav-item content-center-left">
                <Link to="/profile" className={"nav-link nav-link-profile" + (userData ? '' : ' hidden')}>
                  <div className="img-profile w-text ">
                    <span>{userName.charAt(0)}</span>
                  </div>
                </Link>
                <button className={"btn btn-primary" + (userData ? ' hidden' : '')} onClick={() => modal('modalSubscribe')}>
                  Login/Register
                </button>
              </li>
            </ul>
          </Navbar.Collapse>
        </Container>
        <div className="navbar-side-mobile">
          {/* <div className="navbar-brand-mobile">
            <img src={images.brand["logo_indihome-dark.png"]} className="img-fluid w-150px" />
            <Link to="#" onClick={this.navSide}>
              <i className="fas fa-bars"></i>
            </Link>
          </div> */}
          <div className="navbar-nav-mobile">
            <ul>
              <li className="navbar-link-mobile" onClick={this.navSide}><Link to="/home">Home</Link></li>
              <li className="navbar-link-mobile" onClick={this.navSide}><Link to="/shop">Shop</Link></li>
              <li className="navbar-link-mobile" onClick={this.navSide}><Link to="/help">Help</Link></li>
            </ul>
            <div className="btn-placeholder navbar-mobile-bottom bottom">
              <Link to="#" onClick={() => modal('modalSubscribe')} className="btn btn-primary btn-block">Login/Register</Link>
            </div>
          </div>
        </div>
      </Navbar>
    );
  }
}

export default NavbarMain;
