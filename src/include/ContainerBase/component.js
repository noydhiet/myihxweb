import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ContainerBase({ animateSection, children, image, subtitle, title }) {
  return (
    <div className="container container-xs">
      <div className="box animated fadeInUp">
        <header className="header-light white w-icon">
          <div className="header-actions">
            <button className="btn btn-link" onClick={() => window.history.back()}><i className="fa fa-arrow-left"></i></button>
            <Link to="/help" className="btn btn-link"><i className="fa fa-question-circle"></i></Link>
          </div>
          <img className="header-img" src={image.src} alt={image.alt} />
          <div className="heading animated fadeInUp delayp1">
            <h1>{title}</h1>
            {subtitle && <p>{subtitle}</p>}
          </div>
        </header>
        <section className={animateSection ? 'animated fadeInUp delayp2' : ''}>
          {children}
        </section>
      </div>
    </div>
  );
}

ContainerBase.defaultProps = {
  animateSection: false,
  children: null,
  image: {},
  subtitle: '',
  title: '',
};

ContainerBase.propTypes = {
  animateSection: PropTypes.bool,
  children: PropTypes.node,
  image: PropTypes.object,
  subtitle: PropTypes.string,
  title: PropTypes.string,
};
