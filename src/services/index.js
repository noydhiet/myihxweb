import axios, { get, post, put }  from 'axios';
import { getToken, getGuestToken } from '../utils/storage';
import moment from 'moment';

// const isProd = process.env.NODE_ENV === 'production';
//for SIT set it to true
const isProd = true;
console.log('environment', process.env.NODE_ENV)
const BASE_URL_NO_PORT = isProd ? 'https://apigwdev.telkom.co.id:7777/gateway/telkom-myihxmbe-identityserver/1.0' : 'http://ec2-18-140-83-171.ap-southeast-1.compute.amazonaws.com';
const BASE_URL_1337 = isProd  ? 'https://apigwdev.telkom.co.id:7777/gateway/telkom-myihxmbe-account/1.0' : 'http://ec2-18-140-83-171.ap-southeast-1.compute.amazonaws.com:1337';
const BASE_URL_8080 = isProd  ? 'https://apigwdev.telkom.co.id:7777/gateway/telkom-myihxmbe-productinfo/1.0' : 'http://ec2-18-140-83-171.ap-southeast-1.compute.amazonaws.com:8080';
const BASE_URL_8100 = isProd  ? 'https://apigwdev.telkom.co.id:7777/gateway/telkom-myihxmbe-usersupport/1.0' : 'http://ec2-18-140-83-171.ap-southeast-1.compute.amazonaws.com:8100';
const BASE_URL_8110 = isProd  ? 'https://apigwdev.telkom.co.id:7777/gateway/telkom-myihxmbe-metadata/1.0' : 'http://ec2-18-140-83-171.ap-southeast-1.compute.amazonaws.com:8110';

const BASIC_AUTH = 'Basic bXlJbmRpaG9tZVg6Nkw3MUxPdWlubGloOWJuWkhBSUtKMjFIc3Qxcg==';
const BEARER_AUTH = `Bearer ${ !getToken() || getToken() === 'null' ? getGuestToken() : getToken() }`;

const ACCEPT_LANGUAGE = 'en'
moment.locale(ACCEPT_LANGUAGE);

const BASIC_HEADER = { headers: { 'Accept-Language': ACCEPT_LANGUAGE, 'Authorization': BASIC_AUTH, "X-Gateway-APIKey": "617cc860-6701-469c-977a-e97383e2bca8" } }
const BEARER_HEADER = { headers: { 'Accept-Language': ACCEPT_LANGUAGE, 'Authorization': BEARER_AUTH, "X-Gateway-APIKey": "617cc860-6701-469c-977a-e97383e2bca8" } }
const BEARER_HEADER_FOR_DELETE = { 'Accept-Language': ACCEPT_LANGUAGE, 'Authorization': BEARER_AUTH, "X-Gateway-APIKey": "617cc860-6701-469c-977a-e97383e2bca8" } 
const BEARER_HEADER_CONTENT_TYPE = { 'Accept-Language': ACCEPT_LANGUAGE, headers: { 'Authorization': BEARER_AUTH, 'Content-Type': 'application/json', "X-Gateway-APIKey": "617cc860-6701-469c-977a-e97383e2bca8" } }

export const checkAccount = async (number) => (
  await get(`${BASE_URL_1337}/check-indihome-account/${number}`, BASIC_HEADER )
);

export const checkUserExist = async (type, value) => (
  await get(`${BASE_URL_1337}/user/userCheck?type=${type}&value=${value}`, BASIC_HEADER)
);

export const saveDeviceToken = async (payload) => (
  await post(`${BASE_URL_1337}/account/device/`, payload, BEARER_HEADER)
);

export const deleteFcmTokenApi = async (payload) => (
  await await axios({url: `${BASE_URL_1337}/account/device/`, method: 'delete', data: payload, headers: BEARER_HEADER_FOR_DELETE})
);

export const forgetPassword = async (channel, value) => (
  await post(`${BASE_URL_NO_PORT}/user/forget`, {channel, value}, BASIC_HEADER)
);

export const guestUser = async () => (
  await post(`${BASE_URL_NO_PORT}/user/guestLogin`, { deviceType: 'WEB' }, BASIC_HEADER)
);

export const login = async (data) => (
  await post(`${BASE_URL_NO_PORT}/user/login`, data, BASIC_HEADER)
);

export const newPassword = async (data) => (
  await post(`${BASE_URL_NO_PORT}/user/newPassword`, data, BEARER_HEADER)
);

export const register2fo = async (channel, mobile, email, code) => (
  await post(`${BASE_URL_NO_PORT}/otp/verify/${code}/2fo`, { channel, mobile, email }, BASIC_HEADER)
);

export const resetPassword = async (data) => (
  await post(`${BASE_URL_NO_PORT}/user/reset`, data, BASIC_HEADER)
);

export const verifyPasswordRecoveryCode = async (data) => (
  await post(`${BASE_URL_NO_PORT}/user/verifyPasswordRecovery`, data, BASIC_HEADER)
);

export const orderFeasibilityPackage = async (lat, lng) => (
  await get(`${BASE_URL_8080}/packages/availability?latitude=${lat}&longitude=${lng}`, BEARER_HEADER)
);

export const checkCoverage = async (data) => (
  await post(`${BASE_URL_8080}/packages/installation`, data, BEARER_HEADER)
);

export const sendOtp = async (channel, mobile, email) => (
  await post(`${BASE_URL_NO_PORT}/otp/send`, { channel, mobile, email }, BASIC_HEADER)
);

export const signUp = async (data) => (
  await post(`${BASE_URL_1337}/user/signup`, data, BASIC_HEADER)
);

export const userProfile = async () => (
  await get(`${BASE_URL_1337}/user/profile`, BEARER_HEADER)
);

export const userActivation = async (data) => (
  await post(`${BASE_URL_NO_PORT}/user/active`, data, BASIC_HEADER)
);

export const verifyOtp = async (channel, mobile, email, code) => (
  await post(`${BASE_URL_NO_PORT}/otp/verify/${code}/direct`, { channel, mobile, email }, BASIC_HEADER)
);

export const technicianAvailability = async () => (
  await get(`${BASE_URL_8100}/technician/appointment/installation/availability`, BEARER_HEADER)
);

export const technicianAvailabilityFeasibility = async () => (
  await get(`${BASE_URL_8100}/technician/appointment/installation/reschedule/feasibility`, BEARER_HEADER)
);

export const scheduleTechnician = async (data) => (
  await post(`${BASE_URL_8100}/technician/appointment/installation?bookingId=${data}`, null, BEARER_HEADER)
);

export const reScheduleTechnician = async (data) => (
  await post(`${BASE_URL_8100}/technician/appointment/installation/reschedule/${data}`, null, BEARER_HEADER)
);

export const technicianTicket = async (data, queryParam) => (
  await post(`${BASE_URL_8100}/technician/ticket?${queryParam}`, data, BEARER_HEADER)
);

export const reportInstallationIssue = async (data, queryParam) => (
    await post(`${BASE_URL_8100}/support/ticket?${queryParam}`, data, BEARER_HEADER)
);

export const installationStatus = async () => (
    await get(`${BASE_URL_8100}/technician/appointment/installation`, BEARER_HEADER)
);

export const closeInstallation = async (bookingId) => (
  await put(`${BASE_URL_8100}/technician/appointment/installation/close?bookingId=${bookingId}&closed=true`, null, BEARER_HEADER)
);

export const scheduleService = async () => (
  await get(`${BASE_URL_8100}/technician/appointment/service/availability`, BEARER_HEADER)
);

export const listProvinces = async () => (
  await get(`${BASE_URL_8110}/metadata/provinces`, BASIC_HEADER)
);

export const listCities = async (provinceId) => (
  await get(`${BASE_URL_8110}/metadata/cities?provinceId=${provinceId}`, BASIC_HEADER)
)

export const listDistricts = async (legacyCodeCity) => (
  await get(`${BASE_URL_8110}/metadata/districts-core?legacyCodeCity=${legacyCodeCity}`, BASIC_HEADER)
)

export const listStreets = async (legacyCodeCity, legacyCodeDistrict) => (
  await get(`${BASE_URL_8110}/metadata/streets?legacyCodeCity=${legacyCodeCity}&legacyCodeDistrict=${legacyCodeDistrict}`, BASIC_HEADER)
)

export const getInbox = async () => (
  await get(`${BASE_URL_1337}/account/inbox`, BEARER_HEADER)
)

export const setInboxMsgRead = async (messageId, data) => (
  await post(`${BASE_URL_1337}/account/inbox/messages/${messageId}/status`, data, BEARER_HEADER)
)

export const userSupportTicket = async(value) => (
  await get(`${BASE_URL_8100}/support/ticket?open=${value}`, BEARER_HEADER)
)

export const supportTicketCategories = async() => (
  await get(`${BASE_URL_8100}/support/ticket/categories`, BEARER_HEADER)
)

export const fetchIssuesWithoutCategoryId = async() => (
  await get(`${BASE_URL_8100}/technician/ticket/issues`, BEARER_HEADER)
)

export const actionSupportTicketFeasibilty = async(ticketId) => (
  await get(`${BASE_URL_8100}/support/ticket/transactionId/${ticketId}/status`, BEARER_HEADER)
)

export const supportTicketIssues = async(value) => (
  await get(`${BASE_URL_8100}/support/ticket/issues/${value}`, BEARER_HEADER)
)

export const ticketSolution = async(data) => (
  await post(`${BASE_URL_8100}/support/ticket/solutions`, data, BEARER_HEADER)
)

export const actionSupportTicket = async(data, action, body) => (
  await post(`${BASE_URL_8100}/support/ticket/${data}/${action}`, body, BEARER_HEADER_CONTENT_TYPE)
)

export const supportTicketStatus = async(ticketId) => (
  await get(`${BASE_URL_8100}/support/ticket/${ticketId}/status`, BEARER_HEADER)
)

export const googlegeocoder = async(query) => (
  await get(`https://maps.googleapis.com/maps/api/place/findplacefromtext/json?${query}`)
)
