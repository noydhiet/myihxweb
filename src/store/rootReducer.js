import { combineReducers } from 'redux'
import conditionReducer from './condition'

const rootReducer = combineReducers({
    condition: conditionReducer
})

export default rootReducer