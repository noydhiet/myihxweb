const TOKEN_STORAGE = 'myih_token';
const GUEST_TOKEN_STORAGE = 'myih_guest_token';
const EXPIRE_TIME_STORAGE = 'myih_exp';
const EXPIRE_TIME_FOR_GUEST_TOKEN_STORAGE = 'myih_guest_token_exp';
const USER_DATA_STORAGE = 'myih_userdata';
const DEVICE_ID = 'myih_device_id';
const FCM_TOKEN = 'myih_fcm_token';
const BEARER_HOOQ = 'myih_bearer_hooq';
const INSTALLATION_FLAG  = 'myih_installation';
const USER_PROFILE  = 'myih_user_profile';
export const OTP_BLOCKED_TIME = 'myih_otp_blocked_time';
export const NUMBER_BLOCKED = 'myih_number_blocked';
export const OTP_CHANNEL = 'otpChannel';
const IMG_SIGNATURE = 'imgSignature';

export function setUserProfile(data) {
  localStorage.setItem(USER_PROFILE, JSON.stringify(data));
}

export function getUserProfile() {
  return JSON.parse(localStorage.getItem(USER_PROFILE));
}

export function setToken(value) {
  localStorage.setItem(TOKEN_STORAGE, value);
}

export function setGuestToken(value) {
  localStorage.setItem(GUEST_TOKEN_STORAGE, value);
}

export function getToken() {
  return localStorage.getItem(TOKEN_STORAGE);
}

export function getGuestToken() {
  return localStorage.getItem(GUEST_TOKEN_STORAGE);
}

export function setInstallationFlag(value) {
  localStorage.setItem(INSTALLATION_FLAG, value);
}

export function getInstallationFlag() {
  return localStorage.getItem(INSTALLATION_FLAG);
}

export function clearStorages() {
  localStorage.removeItem(USER_PROFILE);
  localStorage.removeItem(TOKEN_STORAGE);
  localStorage.removeItem(EXPIRE_TIME_STORAGE);
  localStorage.removeItem(USER_DATA_STORAGE);
  localStorage.removeItem(INSTALLATION_FLAG);
  localStorage.removeItem(IMG_SIGNATURE);
  localStorage.removeItem(BEARER_HOOQ);
}

export function setExpireTime(value) {
  localStorage.setItem(EXPIRE_TIME_STORAGE, value * 1000);
}

export function checkExpireTime() {
  const time = new Date().getTime();
  const expire = localStorage.getItem(EXPIRE_TIME_STORAGE) * 1 || 0;
  return time > expire;
}

export function setGuestTokenExpireTime(value) {
  localStorage.setItem(EXPIRE_TIME_FOR_GUEST_TOKEN_STORAGE, value * 1000);
}

export function checkGuestTokenExpireTime() {
  const time = new Date().getTime();
  const expire = localStorage.getItem(EXPIRE_TIME_FOR_GUEST_TOKEN_STORAGE) * 1 || 0;
  return time > expire;
}

export function setUserData(value) {
  localStorage.setItem(USER_DATA_STORAGE, JSON.stringify(value));
}

export function getUserData() {
  const retval = localStorage.getItem(USER_DATA_STORAGE);

  return JSON.parse(retval);
}

export function getDeviceId() {
  return localStorage.getItem(DEVICE_ID);
}

export function setDeviceId(value) {
  localStorage.setItem(DEVICE_ID, value);
}

export function getFcmToken() {
  return localStorage.getItem(FCM_TOKEN);
}

export function setFcmToken(value) {
  localStorage.setItem(FCM_TOKEN, value);
}

export function getBearerHooq() {
  return localStorage.getItem(BEARER_HOOQ);
}

export function setBearerHooq(value) {
  localStorage.setItem(BEARER_HOOQ, value);
}

export function setSessionTimer(cb) {
  const time = new Date().getTime();
  const stgExpire = localStorage.getItem(EXPIRE_TIME_STORAGE) || time;
  const expire = parseInt(stgExpire);
  
  if (expire - time > 0) window.setTimeout(() => cb(), expire - time);
};

export function setBlockedNumber(number) {
  localStorage.setItem(NUMBER_BLOCKED, number)
}

export function getBlockedNumber() {
  return localStorage.getItem(NUMBER_BLOCKED)
}

export function setOtpBlockedTime() {
  localStorage.setItem(OTP_BLOCKED_TIME, new Date().getTime() + (60 * 60 * 1000))
}

export function checkOtpIsBlocked() {
  const time = new Date().getTime();
  const expire = localStorage.getItem(OTP_BLOCKED_TIME) * 1 || 0;
  return time < expire;
}
