export const requireBrand = () => require.context('./../assets/img/brand', false, /\.(png|jpe?g|svg|jpeg)$/);
export const requireCommon = () => require.context('./../assets/img/common', false, /\.(png|jpe?g|svg|jpeg)$/);
export const requireSample = () => require.context('./../assets/img/sample', false, /\.(png|jpe?g|svg|jpeg)$/);
