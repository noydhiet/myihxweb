import React, { Component } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import PageContextProvider from './context/PageContext'
import TextContextProvider from './context/TextContext'
import ScrollToTop from 'react-router-scroll-top'
import Routes from './Routes'
import { guestUser } from './services';
import { getToken, checkExpireTime, clearStorages, setGuestToken, getGuestToken, checkGuestTokenExpireTime, setGuestTokenExpireTime } from './utils/storage';

class App extends Component {
  constructor() {
    super();

    if (getToken() && checkExpireTime()) {
      clearStorages();
      window.location.reload();
      return;
    }

    const needLoginPaths = ['/history', '/inbox', '/profile'];
    if (!getToken() && needLoginPaths.some(i => window.location.pathname.match(i))) {
      clearStorages();
      window.location.href = '/login:';
      return;
    }

    const beforeLoginPaths = ['/login', '/register'];
    if (getToken() && beforeLoginPaths.some(i => window.location.pathname.match(i))) {
      window.location.href = '/';
      return;
    }

    if (!getToken() && (!getGuestToken() || checkGuestTokenExpireTime())) {
      guestUser()
        .then(({ data }) => {
          if(data.ok) {
            setGuestToken(data.data.token)
            const guestData = JSON.parse(atob(data.data.token.split('.')[1]));
            setGuestTokenExpireTime(guestData.exp)
          }
        });
    }
  }

  render() {
    return (
      <Router>
        <ScrollToTop>
          <PageContextProvider>
            <TextContextProvider>
              <Routes />
            </TextContextProvider>
          </PageContextProvider> 
        </ScrollToTop>       
      </Router>
    )
  }
}

export default App;