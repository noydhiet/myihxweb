const gulp  = require('gulp');
const sass  = require('gulp-sass');
const watch = require('gulp-watch');

gulp.task('sass', ()=>{
    gulp.src('src/assets/scss/main.scss')
    .pipe(sass())
    .pipe(gulp.dest('src/dist/css'))
});

gulp.task('watch', ()=>{
    gulp.watch('src/assets/scss/main.scss', ['sass']);
})